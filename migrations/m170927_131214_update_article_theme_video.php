<?php

use yii\db\Migration;

class m170927_131214_update_article_theme_video extends Migration
{
    public function safeUp()
    {
        $this->addColumn('article', 'img_radius', $this->string());
        $this->addColumn('video', 'img_radius', $this->string());
        $this->addColumn('theme', 'img_radius', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('article', 'img_radius');
        $this->dropColumn('video', 'img_radius');
        $this->dropColumn('theme', 'img_radius');
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170927_131214_update_article_theme_video cannot be reverted.\n";

        return false;
    }
    */
}
