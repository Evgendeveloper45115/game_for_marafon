<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Article */

$this->title = 'Редактировать главную страницу';
$this->params['breadcrumbs'][] = ['label' => 'main', 'url' => ['update']];
$this->params['breadcrumbs'][] = 'Сохранить';
?>
<div class="article-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
