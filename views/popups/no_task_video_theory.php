<?php
$level = 1;

if(isset(Yii::$app->user->identity->profile->level)){
    $level = Yii::$app->user->identity->profile->level;
}
?>
<div class="js-popup" id="task-no-complete">
    <div class="popup popup--share">
        <div class="popup popup--task">
            <div class="popup-head">
                <h1 class="popup__title">Недоступно</h1>

                <h2 class="popup__subtitle">выполните предыдущее задание, чтобы получить доступ к следующему</h2><span class="popup-close"></span>
            </div>
            <div class="popup-content">
                <p class="popup-content__next-task">Приступить к заданиям уровня</p><a class="btn btn--popup"
                                                                                       href="<?= \yii\helpers\Url::to(['level/index', 'id' => $level]) ?>">Перейти</a>
            </div>
        </div>
    </div>
</div>
