<?php

namespace app\modules\admin\controllers;

use app\models\FinalTestSearch;
use app\models\Test;
use app\models\TestQuestionLevel;
use app\models\TestSearch;
use app\modules\quiz\models\QuizCategory;
use app\modules\quiz\models\QuizQuestion;
use Yii;
use app\models\TestQuestion;
use app\models\TestQuestionSearch;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * TestController implements the CRUD actions for Test model.
 */
class TestController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TestQuestion models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionTestQuestions()
    {
        $searchModel = new TestQuestionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index_question', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAddTestQuestions()
    {
        $model = new TestQuestionLevel();
        if ($model->load(Yii::$app->getRequest()->post())) {
            $model->image = UploadedFile::getInstance($model, 'image');
            if ($model->image) {
                if (!is_dir(Yii::getAlias('@abs_path_to_uploads') . '/test_level_Img')) {
                    mkdir(Yii::getAlias('@abs_path_to_uploads') . '/test_level_Img', 0777, true);
                }
                $file_name = uniqid();
                $model->img = $file_name . '.' . $model->image->extension;
                if (!$model->image->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/test_level_Img/' . $file_name . '.' . $model->image->extension)) {
                    throw new \RuntimeException('Ошибка сохранения файла');
                }
                $model->image = null;
            }
            $model->save();
        }
        return $this->render('add_test_question', ['model' => $model]);
    }

    public function actionEditTestQuestions($id)
    {
        $model = TestQuestionLevel::findOne($id);
        if ($model->load(Yii::$app->getRequest()->post())) {
            $model->image = UploadedFile::getInstance($model, 'image');
            if ($model->image) {
                if (!is_dir(Yii::getAlias('@abs_path_to_uploads') . '/test_level_Img')) {
                    mkdir(Yii::getAlias('@abs_path_to_uploads') . '/test_level_Img', 0777, true);
                }
                $file_name = uniqid();
                $model->img = $file_name . '.' . $model->image->extension;
                if (!$model->image->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/test_level_Img/' . $file_name . '.' . $model->image->extension)) {
                    throw new \RuntimeException('Ошибка сохранения файла');
                }
                $model->image = null;
            }
            $model->save();
        }
        return $this->render('add_test_question', ['model' => $model]);
    }

    /**
     * Displays a single TestQuestion model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TestQuestion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Test();

        if ($model->load(Yii::$app->request->post())) {
            $model->image = UploadedFile::getInstance($model, 'image');
            if ($model->image) {
                if (!is_dir(Yii::getAlias('@abs_path_to_uploads') . '/')) {
                    mkdir(Yii::getAlias('@abs_path_to_uploads') . '/', 0777, true);
                }
                $file_name = uniqid();
                $model->img_radius = $file_name . '.' . $model->image->extension;
                if (!$model->image->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/' . $file_name . '.' . $model->image->extension)) {
                    throw new \RuntimeException('Ошибка сохранения файла');
                }
                $model->image = null;
            }
            $model->weight = 5;
            $model->save();

            return $this->redirect('/admin/test/index');
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TestQuestion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        /**
         * @var $model  Test
         */
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->image = UploadedFile::getInstance($model, 'image');
            if ($model->image) {
                if (!is_dir(Yii::getAlias('@abs_path_to_uploads') . '/')) {
                    mkdir(Yii::getAlias('@abs_path_to_uploads') . '/', 0777, true);
                }
                $file_name = uniqid();
                $model->img_radius = $file_name . '.' . $model->image->extension;
                if (!$model->image->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/' . $file_name . '.' . $model->image->extension)) {
                    throw new \RuntimeException('Ошибка сохранения файла');
                }
                $model->image = null;
            }
            $model->weight = 5;
            $model->save();

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TestQuestion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDeleteTestQuestions($id)
    {
        TestQuestionLevel::findOne($id)->delete();

        return $this->redirect(['test-questions']);
    }

    /**
     * Finds the TestQuestion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Test the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Test::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
