<?php
/**
 * @var $spec_time
 * @var $discount
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$final_price = $full_price - $points_discount;

if ($discount == 0) {
    $final_price = $full_price;
}

if (!$good_answers) {
    $good_answers = 0;
}
?>

    <div class="results">

        <?php if ($param == 'program'): ?>
            <div class="final-offer js_final_offer">
                <div class="final-offer__top clearfix">
                    <div class="final-offer__top-left col-4">
                        <img src="../images/bg_girls.png">
                    </div>
                    <div class="final-offer__top-left-mobile col-4">
                        <img src="../images/bg_girls-mobile.png">
                    </div>
                    <div class="final-offer__top-right col-8">
                        <div class="clearfix final-offer__title"><span
                                    class="final-offer__title-week col-2">9&nbsp;недель</span>
                            <h2 class="final-offer__title-text col-9"
                                style="text-align: left;"><?= $main->program_name ?></h2>
                        </div>
                        <p class="final-offer__top-text" style="text-align: left;"><?= $main->program_description ?></p>
                        <div class="final_pay_block"><a class="btn btn--final-offer-pay js-pay">Оплата</a></div>
                    </div>
                </div>
                <div class="final-offer__program">
                    <p class="program__title">Программа включает в себя:</p>
                    <div class="program__items">
                        <div class="program__item">
                            <div class="program__item-img">
                                <img src="../images/restaurant.png">
                            </div>
                            <p class="program__text">Питание</p>
                        </div>
                        <div class="program__item">
                            <div class="program__item-img">
                                <img src="../images/dumb.png">
                            </div>
                            <p class="program__text">Тренировки</p>
                        </div>
                        <div class="program__item">
                            <div class="program__item-img">
                                <img src="../images/target.png">
                            </div>
                            <p class="program__text">Мотивация</p>
                        </div>
                        <div class="program__item">
                            <div class="program__item-img">
                                <img src="../images/people.png">
                            </div>
                            <p class="program__text">Единомышленники</p>
                        </div>
                        <div class="program__item">
                            <div class="program__item-img">
                                <img src="../images/curator.png">
                            </div>
                            <p class="program__text">Кураторы</p>
                        </div>
                    </div>
                    <a class="btn btn--program" href="https://beauty-matrix.ru/">Перейти на сайт программы</a>
                </div>

                <p class="comment__title">Отзывы:</p>
                <div class="final-offer__comment">
                    <button class="NextBtn slider-comments__arrow slider-comments__arrow--next slick-arrow"></button>
                    <button class="PrevBtn slider-comments__arrow slider-comments__arrow--prev slick-arrow"></button>
                    <div class="owl-carousel owl-theme" style="margin-top:15px;">
                        <?php foreach ($reviews as $review): ?>
                            <div class="item">
                                <img src="../uploads/<?= $review->photo_before ?>">
                                <span class="comment__item-kg">-<?= $review->weight ?> кг</span>
                                <p class="comment__text"><?= $review->name ?></p><a onclick="yaCounter46569435.reachGoal('feedback')" class="comment__link"
                                                                                    href="http://feedback.beauty-matrix.ru">Читать
                                    отзыв</a>
                            </div>

                        <?php endforeach; ?>
                    </div>

                    <a onclick="yaCounter46569435.reachGoal('feedback')" class="btn btn--comments all-reviews" href="http://feedback.beauty-matrix.ru/">Смотреть все
                        отзывы</a>
                </div>
            </div>
        <?php endif; ?>

        <?php if ($param != 'program'): ?>
            <div class="intro">
                <div class="results-top <?= $isPassedTest ? 'fireworks fireworks--lg' : ''; ?> ">
                    <h1 class="results-top__title title">
                        <?= $isPassedTest ? 'Вы прошли тест' : 'Вы не прошли тест'; ?>
                    </h1>

                    <p class="results-top__answers">Правильных ответов: <span
                                class="results-top__points results-top__points--orange"><?= $good_answers . '/' . $amount_questions ?></span>
                    </p>

                    <div class="new-points"><span
                                class="new-points__count"><?= $isPassedTest ? '+100' : '0'; ?></span><span
                                class="new-points__text"> Баллов</span>
                    </div>
                </div>
                <?php if ($isPassedTest): ?>
                    <div class="clearfix results-content">
                        <div class="col-12 results-content__right">
                            <p class="text">Поздравляем! Вы успешно прошли итоговый тест и получаете возможность
                                участвовать
                                в нашей групповой программе похудения Beauty Matrix по специальной цене, действительной
                                в течение 48 часов! Кроме того вы можете потратить заработанные баллы в форме ниже
                                для получения максимальной скидки! 1 балл = 1 рублю скидки. Получить консультацию
                                по участию в программе можно, нажав на кнопку WhatsApp снизу, она мгновенно перенаправит
                                вас в WhatsApp к консультанту.</p>
                        </div>

                        <div class="clearfix whatsup whatsup_row">
                            <div class="col-12 whatsup_col">
                                <div class="whatsup_col__title">
                                    Получить консультацию по программе:
                                </div>
                                <div class="whatsup_btn">
                                    <a onclick="yaCounter46569435.reachGoal('consult')" class="btn btn--comments" target="_blank"
                                       href="http://whatsapp.beauty-matrix.ru/">WhatsApp</a>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix whatsup review_row">
                            <div class="col-12 whatsup_col">
                                <div class="whatsup_col__title">
                                    Посмотреть отзывы о программе:
                                </div>
                                <div class="whatsup_btn">
                                    <a onclick="yaCounter46569435.reachGoal('feedback')" class="btn btn--comments" target="_blank"
                                       href="http://feedback.beauty-matrix.ru/">Отзывы</a>
                                </div>
                            </div>
                        </div>


                    </div>
                <?php endif; ?>
                <?php
                if (!$isPassedTest) {
                    ?>

                    <div class="clearfix results-content">
                        <div class="col-12 results-content__right">
                            <p class="text">Сожалеем, но вы не набрали достаточного количества баллов для успешного
                                прохождения итогового теста. Однако, вы можете использовать накопленные баллы,
                                чтобы приобрести дистанционную программу похудению Beauty Matrix! На программе
                                вы получите индивидуальный рацион, посчитанный в граммах, программу тренировок
                                на основе ваших целей и исходных данных, мотивационные задания, а также сопровождение
                                кураторов и психологическую поддержку на протяжении всех 9 недель. Для получения
                                выгодного специального предложения попробуйте пройти тест еще раз. Получить
                                консультацию по участию в программе можно, нажав на кнопку Whatsapp снизу,
                                она мгновенно перенаправит вас в WhatsApp к консультанту. </p>
                        </div>
                    </div>

                    <div class="clearfix whatsup whatsup_row">
                        <div class="col-12 whatsup_col">
                            <div class="whatsup_col__title">
                                Получить консультацию по программе:
                            </div>
                            <div class="whatsup_btn">
                                <a class="btn btn--comments" target="_blank" href="http://whatsapp.beauty-matrix.ru/">WhatsApp</a>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix whatsup review_row">
                        <div class="col-12 whatsup_col">
                            <div class="whatsup_col__title">
                                Посмотреть отзывы о программе:
                            </div>
                            <div class="whatsup_btn">
                                <a class="btn btn--comments" target="_blank" href="http://feedback.beauty-matrix.ru/">Отзывы</a>
                            </div>
                        </div>
                    </div>

                    <div class="again">
                        <p class="again__text">Вы можете пройти тест еще раз</p><a class="btn btn--again"
                                                                                   href="/test/pravilnoe-pitanie">Пройти
                            тест</a>
                    </div>
                    <?php
                }
                ?>
                <div class="clearfix results-more">
                    <div class="col-3 results-more__left">
                        <img src="../images/top_logo.png">
                    </div>
                    <div class="col-6 results-more__right">
                        <p class="popup-content__text">Программа создания стройной фигуры дистационные групповые
                            занятия</p><span class="results-more__week">9 недель</span>
                    </div>
                    <div class="col-3 results-more__button"><a class="btn btn--more js-more"
                                                               href="<?= Url::to(['test/result', 'param' => 'program']) ?>">Подробнее</a>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php if ($isPassedTest && $spec_time > 0): ?>
            <div class="buy">
                <h3 class="buy__title">Купить программу со скидкой <span
                            class="buy__old-price strike"> 11900 руб.</span><span
                            class="buy__price"> <?= 11900 - $discount ?> руб.</span></h3>
                <b class="buy__special">Специальные условия доступны в течение 48 часов</b>

                <div class="timer">
                    <div id="clockdiv">
                        <div>
                            <div class="smalltext timer__day">Часов</div>
                            <span class="hours"></span>
                        </div>
                        <div>
                            <div class="smalltext timer__day">Минут</div>
                            <span class="minutes"></span>
                        </div>
                        <div>
                            <div class="smalltext timer__day">Секунд</div>
                            <span class="seconds"></span>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php if (!$isPassedTest || $spec_time <= 0): ?>
            <div class="buy">
                <h3 class="buy__title buy__title--single">Купить программу
                     11900 руб.
                </h3>
                <?php if (!$isPassedTest) { ?>
                    <p class="buy__text">Пройдите тест еще раз, чтобы получить специальное предложение <a
                                class="buy__link" href="/test/pravilnoe-pitanie"> пройти тест</a>
                    </p>
                <?php } ?>

            </div>
        <?php endif; ?>

    </div>
    <div class="payment">
        <p class="payment__points">Вы заработали <span
                    class="payment__points-count"><?= $points?></span> баллов</p>

        <p class="payment__text">Вы можете использовать баллы для оплаты части программы</p>


        <div class="points_form payment-form__points">
            <?php $form = ActiveForm::begin(); ?>

            <div class="payment__amount payment-form__points">
                <span>ввести количество баллов для оплаты</span>

                <?= $form->field($point_form, 'amount')->textInput(['class' => 'points-textbox'])->label(false); ?>
            </div>

            <div class="payment__submit">
                <?= Html::submitButton('Подтвердить', ['class' => 'btn btn--accept']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>

        <?php
            if ($isPassedTest && $spec_time > 0){
                $program_price = '<span class="strike">11900</span> 8900 ';
            }
            else{
                $program_price = '11900 ';
            }
        ?>


        <p class="payment__price">Стоимость программы <?= $program_price ?> руб.</p>



        <?php
            if ($isPassedTest)
            {
                $final_price = $full_price - $points_discount;
                if ($spec_time > 0)
                {
                    $final_price -= $discount;
                }
            }
        ?>

        <p class="payment__total">Итоговая стоимость <span
                    class="payment__total-sum"> <span id="final_price"><?= $final_price ?></span> руб.</span>
        </p>

        <div class="payment-form__money">
            <?= Html::beginForm('https://beauty-matrix.ru/game', 'post', ['data-pjax' => '', 'class' => 'form-inline', 'id' => 'payform', 'csrf' => false]); ?>
            <?= Html::input('text', 'frst_name', '', ['class' => 'textbox', 'placeholder' => "Имя", 'required' => 'required']) ?>
            <?= Html::input('text', 'last_name', '', ['class' => 'textbox', 'placeholder' => "Фамилия", 'required' => 'required']) ?>
            <?= Html::input('tel', 'cps_phone', '', ['class' => 'textbox', 'placeholder' => "Телефон", 'required' => 'required']) ?>
            <?= Html::input('email', 'cps_email', '', ['class' => 'textbox', 'placeholder' => "E-mail", 'required' => 'required']) ?>

            <?= Html::hiddenInput('orderDetails', 'Дистанционная групповая программа похудения Beauty Matrix'); ?>
            <?= Html::hiddenInput('shopId', '159464'); ?>
            <?= Html::hiddenInput('program', '1p9w_b'); ?>
            <?= Html::hiddenInput('scid', '558664'); ?>
            <?= Html::hiddenInput('customerNumber', time()); ?>
            <?= Html::hiddenInput('sum', $final_price . '.00', ['id' => 'input_price']); ?>

            <div id="checkbox_oferta" data-checked="enabled">
                <label class="checkbox">
                    <input id="checkbox_input" type="checkbox" checked required="required"><i></i><span
                            class="checkbox__text">Я принимаю условия <a class="js-venobox vbox-item dogovor_link"
                                                                         href="#dogovor" data-vbtype="inline">договора оферты</a></span>
                </label>
            </div>

            <div class="agreement" style="display:none;">
                <?= Html::checkbox('agreement', 'true', ['id' => 'agreement']) ?>
                <?= Html::label('Я принимаю условия <a class="btn js-venobox vbox-item" href="#dogovor" data-vbtype="inline">договора оферты</a>', 'agreement', ['class' => 'label label_agreement']) ?>
            </div>

            <?= Html::dropDownList('paymentType', 'AC', [
                'AC' => 'Оплата банковской картой',
                'SB' => 'Оплата через Сбербанк',
                'QW' => 'Оплата через Qiwi Wallet',
                'PC' => 'Оплата со счета в Яндекс.Деньгах',
                'WM' => 'Оплата cо счета WebMoney',
                'AB' => 'Оплата через Альфа-Клик',
                'MA' => 'Оплата через MasterPass',
                'GP' => 'Оплата по коду через терминал'],
                ['class' => 'combobox']); ?>

            <?= Html::submitButton('Оплатить', ['class' => 'btn btn--pay', 'id' => 'sabmit_btn']) ?>
            <?= Html::endForm() ?>
        </div>

    </div>

    <a style="display:none;" class="btn js-venobox vbox-item" href="#share" id="sharer" data-vbtype="inline">share</a>
    <div class="js-popup" id="share">
        <div class="popup popup--share">
            <div class="popup-head">
                <h1 class="popup__title">Ошибка</h1><span class="popup-close"></span>
            </div>
            <div class="popup-content popup-content--share">
                У вас на счету меньше баллов, чем вы пытаетесь ввести
            </div>
        </div>
    </div>

    <div class="js-popup" id="dogovor">
        <div class="popup popup--comments"><span class="popup-close"></span>
            <h2 class="popup__title">ДОГОВОР ПУБЛИЧНОЙ ОФЕРТЫ</h2>
            <div class="popup-content__text">
                <?= $this->render('_dogovor'); ?>
            </div>

        </div>
    </div>
<?php

if ($error) {
    echo $this->render('/popups/fail_result');
}
$js = <<<JS
$('#w0').on('beforeSubmit', function(){        
       var data = $(this).serialize();
        $.ajax({
            url: '/test/points',
            type: 'POST',
            data: data,
            success: function(res){
                //console.log(res);                
                if (res == 'error'){
                    $('#sharer').trigger('click');                       
                }   
                else{                    
                    var input_price = $('#input_price').val();
                    input_price = input_price - res;
                    $('#input_price').val(input_price + '.00');
                    $('#final_price').html(input_price)
                }                                                              
            },
            error: function(){
                //alert('Error!');
            }
        });
        return false;
});

function getTimeRemaining(date) {
    var t = Date.parse(date) - Date.parse(new Date());
    var seconds = Math.floor((t / 1000) % 60);
    var minutes = Math.floor((t / 1000 / 60) % 60);
    var hours = Math.floor((t / (1000 * 60 * 60)) % 48);  
    
    return {
        'total': t,    
        'hours': hours,
        'minutes': minutes,
        'seconds': seconds
   };
}

function initializeClock(id, end_date) {
    var clock = document.getElementById(id);
    var hoursSpan = clock.querySelector('.hours');
    var minutesSpan = clock.querySelector('.minutes');
    var secondsSpan = clock.querySelector('.seconds');
    
    function updateClock() {
        var t = getTimeRemaining(end_date);
        hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
        minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
        secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);
        
        if (t.total <= 0) {
            clearInterval(timeinterval);
        }
    }
    
    updateClock();
    var timeinterval = setInterval(updateClock, 1000);
}

if(parseFloat('$spec_time') > 0 && Boolean('$isPassedTest')){
    initializeClock('clockdiv',  new Date(Date.parse(new Date()) + +('$spec_time') * 1000));    
}


$(document).on('click', '.js-pay', function () {    
    var scrollTop = $('.payment').offset().top;
    $(document).scrollTop(scrollTop);           
}); 

$(document).on('click', '#checkbox_input', function () {    
    //$('.checkbox i').toggleClass('red_i');  
    $('.checkbox i').removeClass('red_i');    
    //console.log($('#checkbox_oferta').attr('data-checked'));
    $('#checkbox_oferta').attr('data-checked', $('#checkbox_oferta').attr('data-checked') == 'enabled' ? 'disabled' : 'enabled');
}); 


$('#sabmit_btn').on('click', function(){       
    yaCounter46569435.reachGoal('oplata')
      if($('#checkbox_oferta').attr('data-checked') == 'disabled'){
          $('.checkbox i').addClass('red_i');  
      }
});


if('$param' == 'program') {
    $('.js-more').trigger('click');
}

var owl = $('.owl-carousel');

owl.owlCarousel({
loop:true,
margin:10,
navigation : true,
navigationText : ["prev","next"],
responsive:{
    0:{
        items:1
    },
    751:{
        items:3
    }
}});

$('.NextBtn').click(function() {
    owl.trigger('next.owl.carousel');
});

$('.PrevBtn').click(function() {
    owl.trigger('prev.owl.carousel');
});
JS;
$this->registerJs($js);
?>