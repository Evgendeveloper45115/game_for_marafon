<?php

Yii::setAlias('@rel_path_to_uploads', '/uploads');
Yii::setAlias('@abs_path_to_uploads', dirname(__DIR__) . '/web/uploads');

return [
    'adminEmail' => 'admin@example.com',
];
