<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "measure".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $height
 * @property integer $weight
 * @property integer $breast
 * @property integer $hip
 * @property integer $butt
 * @property integer $date
 * @property integer $waist
 *
 * @property User $user
 */
class Measure extends \yii\db\ActiveRecord
{

    const HYPERSTHENIC = 10;
    const NORMOSTHENIC = 20;
    const ASTHENIC = 30;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'measure';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'height', 'weight', 'breast', 'hip', 'butt', 'date', 'waist'], 'required'],
            [['user_id', 'height', 'weight', 'breast', 'hip', 'butt', 'date', 'waist'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'height' => 'Рост (см)',
            'weight' => 'Вес (кг)',
            'breast' => 'Объем груди (см)',
            'hip' => 'Объем бедер (см)',
            'butt' => 'Объем ягодиц (см)',
            'date' => 'Дата замера',
            'waist' => 'Объем талии (см)',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

}
