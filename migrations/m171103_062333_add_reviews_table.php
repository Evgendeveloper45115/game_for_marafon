<?php

use yii\db\Migration;

class m171103_062333_add_reviews_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }


        $this->createTable('{{%review}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'photo_before' => $this->string(),
            'photo_after' => $this->string(),
            'weight' => $this->integer(),
            'review' => $this->text(),
        ], $tableOptions);
    }

    public function safeDown()
    {
        echo "m171103_062333_add_reviews_table cannot be reverted.\n";

        return false;
    }
}
