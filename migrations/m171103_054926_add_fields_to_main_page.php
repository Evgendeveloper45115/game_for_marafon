<?php

use yii\db\Migration;

class m171103_054926_add_fields_to_main_page extends Migration
{
    public function safeUp()
    {
        $this->addColumn('main_page', 'program_name', $this->string());
        $this->addColumn('main_page', 'program_description', $this->text());
    }

    public function safeDown()
    {
        echo "m171103_054926_add_fields_to_main_page cannot be reverted.\n";

        return false;
    }

}
