<?php
/**
 * @var $passedTest \app\models\PassedTest
 * @var $model \app\models\Test
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

?>

<div class="results">
    <div class="intro">
        <div class="results-top <?= $passedTest ? 'fireworks fireworks--lg' : ''; ?> ">
            <h1 class="results-top__title title">
                <?= $passedTest ? 'Вы прошли тест' : 'Вы не прошли тест'; ?>
            </h1>

            <p class="results-top__answers">Правильных ответов: <span
                        class="results-top__points results-top__points--orange"><?= ($passedTest->percent ?: '0') . '/' . count($model->testQuestionLevels) ?></span>
            </p>

            <div class="new-points"><span
                        class="new-points__count"><?= $passedTest->percent ? '+' . $model->point : '0'; ?></span><span
                        class="new-points__text"> Баллов</span>
            </div>
        </div>
        <div class="clearfix results-content">
            <div class="col-3 results-content__left">
                <img src="../images/antisugar.png">
            </div>
            <div class="col-9 results-content__right">
                <p class="text">Под словом «фитнес» вошедшим в активный словарь современного русского языка не так
                    давно, мы подразумеваем целую вселенную видов активности: от кручения педалей на велотренажере
                    до пауэр-лифтинга. Обобщает их одна отправная точка: фитнесом принято
                    называть упорядоченный физкультурный комплекс, направленный на улучшение фигуры и общее
                    оздоровление.</p>
            </div>
        </div>
        <?php
        if (!$passedTest) {
            ?>
            <div class="again">
                <p class="again__text">Вы можете пройти тест еще раз</p><a class="btn btn--again"
                                                                           href="/test/pravilnoe-pitanie?id="<?= $model->id ?>>Пройти
                    тест</a>
            </div>
            <?php
        }
        ?>
    </div>
</div>
