<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LevelUp */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="level-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'level')->textInput() ?>

    <?= $form->field($model, 'title')->textInput() ?>

    <?= $form->field($model, 'description', [
        'template' => "<div>{input}\n<div class=\"error\">{error}</div></div>",
    ])->widget(\zxbodya\yii2\tinymce\TinyMce::className(), [
        'options' => ['rows' => 15, 'style' => 'margin-top:5vw'],
        'language' => 'ru',
//        'spellcheckerUrl'=>'http://speller.yandex.net/services/tinyspell',
        'fileManager' => [
            'class' => \zxbodya\yii2\elfinder\TinyMceElFinder::className(),
            'connectorRoute' => 'el-finder/connector',
        ],
    ]); ?>

    <?php if ($model->image && $model->image != 'blank.jpg') {
        ?>
        <?= Html::img(Yii::getAlias('@rel_path_to_uploads') . '/levelUp/' . $model->image, ['class' => 'profile_foto']); ?>
        <div class="del_link">
            <?php
            if ($model->image != 'blank.jpg') {
                echo Html::a('Удалить', ['level/delete-img'], [
                    'data-method' => 'POST',
                    'data-params' => [
                        'id' => $model->id,
                    ],
                ]);
            }
            ?>
        </div>
    <?php } ?>

    <?php
    if (!$model->image || $model->image == 'blank.jpg') {
        echo Html::img('/uploads/blank.jpg', ['class' => 'profile_foto']);
    }
    ?>


    <?= $form->field($model, 'file')->fileInput(['onchange' => 'readURL(this);']); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
