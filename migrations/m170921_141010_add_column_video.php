<?php

use yii\db\Migration;

class m170921_141010_add_column_video extends Migration
{
    public function safeUp()
    {
        $this->addColumn('video', 'type', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn('video', 'type');
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170921_141010_add_column_video cannot be reverted.\n";

        return false;
    }
    */
}
