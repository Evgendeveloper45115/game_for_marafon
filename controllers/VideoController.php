<?php

namespace app\controllers;

use app\models\PassedVideo;
use Yii;
use yii\web\Controller;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\helpers\VarDumper;
use app\models\Video;


class VideoController extends Controller
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }


    public function actionIndex($id)
    {
        $video = Video::find()
            ->where(['id' => $id])
            ->one();

        if (!$video) {
            throw new NotFoundHttpException('Видео не найдено');
        }
        $passedVideo = PassedVideo::find()
            ->where(['user_id' => Yii::$app->getUser()->getId()])
            ->andWhere(['video_id' => $id])
            ->one();
        return $this->render('index', [
            'video' => $video,
            'passedVideo' => $passedVideo,
        ]);
    }

}
