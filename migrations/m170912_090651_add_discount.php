<?php

use yii\db\Migration;

class m170912_090651_add_discount extends Migration
{
    public function safeUp()
    {
        $this->addColumn('settings', 'discount', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn('settings', 'discount');
    }

}
