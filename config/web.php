<?php

$soc_params = require(dirname(dirname(__DIR__)) . '/soc_params.php');

$params = require(__DIR__ . '/params.php');
$db = is_file(__DIR__ . '/db_local.php') ? require(__DIR__ . '/db_local.php') : require(__DIR__ . '/db.php');
$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'ru',
    'on beforeAction' => function ($event) {

        if (!Yii::$app->user->isGuest) {
            \app\components\Time::update();
        } elseif (Yii::$app->user->isGuest && Yii::$app->controller->action->id != 'login' &&
            Yii::$app->controller->action->id != 'signup' && Yii::$app->controller->id != 'mail'
            && Yii::$app->controller->id != 'test' && Yii::$app->controller->id != 'notification'
            && Yii::$app->controller->id != 'landing'
            && Yii::$app->controller->action->id != 'request-password-reset'&&
            (Yii::$app->controller->id != 'site' && Yii::$app->controller->action->id != 'index')
        ) {
            Yii::$app->controller->redirect(\yii\helpers\Url::to('/site/signup'));
        }


        if (!Yii::$app->user->isGuest && isset(Yii::$app->user->identity->soc_profile['id'])) {
            $user = \app\models\User::findBySocialMediaInfo(Yii::$app->user->identity->soc_profile['service'], Yii::$app->user->identity->soc_profile['id']);
            Yii::$app->user->identity = $user;
        }

        if(Yii::$app->controller->module->id == 'admin'  && (!isset(Yii::$app->user->identity->role) || Yii::$app->user->identity->role != \app\models\User::ROLE_ADMIN))
        {
            throw new DomainException('Доступно только администратору');
        }

    },
    'modules' => [
        'quiz' => [
            'class' => 'app\modules\quiz\Quiz',
        ],
        'admin' => [
            'class' => 'app\modules\admin\Module',
        ],
    ],
    'components' => [

        'eauth' => [
            'class' => 'nodge\eauth\EAuth',
            'popup' => true, // Use the popup window instead of redirecting.
            'cache' => false, // Cache component name or false to disable cache. Defaults to 'cache' on production environments.
            'cacheExpire' => 0, // Cache lifetime. Defaults to 0 - means unlimited.
            'httpClient' => [
                // uncomment this to use streams in safe_mode
                //'useStreamsFallback' => true,
            ],
            'services' => [ // You can change the providers and their classes.
                'google' => [
                    // register your app here: https://code.google.com/apis/console/
                    'class' => 'app\services\GoogleOAuth2Service',
                    'clientId' => '11329683909-buojep8n01ol3l5r33fd268t8kscmnck.apps.googleusercontent.com',
                    'clientSecret' => 'pgm4lvYbkP9PfhogQH6pbPZa',
                    'title' => 'Google',
                ],
                'yandex' => [
                    // register your app here: https://oauth.yandex.ru/client/my
                    'class' => 'nodge\eauth\services\YandexOAuth2Service',
                    'clientId' => '...',
                    'clientSecret' => '...',
                    'title' => 'Yandex',
                ],
                'facebook' => [
                    // register your app here: https://developers.facebook.com/apps/
                    'class' => 'app\services\FacebookOAuth2Service',
                    'clientId' => '290043384832924',
                    'clientSecret' => 'fbbaadc85cab701eb9d437d8c64471b3',
                ],
                'vkontakte' => [
                    // register your app here: https://vk.com/editapp?act=create&site=1
                    'class' => 'app\services\VKontakteOAuth2Service',
                    'clientId' => $soc_params['vk_id'],
                    'clientSecret' => $soc_params['vk_secret'],
                ],
                'mailru' => [
                    // register your app here: http://api.mail.ru/sites/my/add
                    'class' => 'nodge\eauth\services\MailruOAuth2Service',
                    'clientId' => '756796',
                    'clientSecret' => '40791a1811360eb72e6920d14fedcffb',
                ],
            ],
        ],
        'assetManager' => [
            'bundles' => [
                'yii\bootstrap\BootstrapAsset' => [
                    'basePath'   => '@web',
                    'sourcePath' => null,
                    'css'        => [ 'css/bootstrap_instead.css' ]
                ],
                'yii\web\JqueryAsset' => [
                  //  'sourcePath' => null,   // отключение дефолтного jQuery
                    'js' => [
                        'https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js',
                    ]
                ],
            ],
        ],        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'GKxgWfjHIMr8boo1siaFtxDo4XkKe0vY',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'on afterLogin' => function($event)
            {
                if(isset(Yii::$app->user->identity->profile)){
                    if(Yii::$app->user->identity->profile->passed_test){
                        Yii::$app->response->redirect(\yii\helpers\Url::to('/test/result'))->send();
                        Yii::$app->end();
                    }
                }
            }
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@app/mail',
            'htmlLayout' => 'layouts/trigger_mail_html',
            'textLayout' => 'layouts/trigger_mail_text',
            'messageConfig' => [
                'charset' => 'UTF-8',
                'from' => ['admin@beauty-matrix.ru' => 'game.beauty-matrix.ru'],
            ],
            'useFileTransport' => false,
            /*'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.yandex.com',
                'username' => 'admin@beauty-matrix.ru',
                'password' => 'Qwe123zxc]',
                'port' => '465',
                'encryption' => 'ssl',
            ],*/


        ],
        'mail' => [
            'class' => 'zyx\phpmailer\Mailer',
            'viewPath' => '@app/mail',
            'htmlView' => '@app/mail',
            'useFileTransport' => false,
            'messageConfig' => [
                'from' => ['admin@beauty-matrix.ru' => 'game.beauty-matrix.ru'],
            ],
            'config' => [
                'mailer' => 'smtp',
                'host' => 'smtp.yandex.ru',
                'port' => '465',
                'smtpsecure' => 'ssl',
                'smtpauth' => true,
                'username' => 'admin@beauty-matrix.ru',
                'password' => 'Qwe123zxc]',
            ],
        ],
        'session' => [
            'class' => 'yii\web\Session',
            'cookieParams' => ['httponly' => true, 'lifetime' => 3600*24],
            'timeout' => 3600*24,
            'useCookies' => true,
        ],
        'i18n' => [
            'translations' => [
                'eauth' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@eauth/messages',
                ],
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['yii\swiftmailer\Logger::add'],
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => false,
            'showScriptName' => false,
            'rules' => [
                'admin/el-finder/connector' => 'el-finder/connector',
                'quiz' => 'quiz/site',
                '1' => 'landing/first',
                '2' => 'landing/second',
                'admin' => 'admin/level',
                'article/<id:\d+>' => 'article/index',
                'test/<id:\d+>' => 'test/index',
                'video/<id:\d+>' => 'video/index',
                'theme/<id:\d+>' => 'theme/index',
                'level/<number:\d+>' => 'level/index',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                'login/<service:google|facebook|vk|mailru>' => 'site/login',
            ],
        ],
    ],
    'params' => $params,
];


if (false) {

    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['127.0.0.1', '::1'],
    ];

}


if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    /* $config = yii\helpers\ArrayHelper::merge(
         require('params-local.php')
     );*/
}


return $config;
