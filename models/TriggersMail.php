<?php

namespace app\models;

use app\models\User;
use Yii;

/**
 * This is the model class for table "triggers_mail".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $not_visit1
 * @property integer $not_visit3
 * @property integer $not_visit6
 * @property integer $finished_5_level
 * @property integer $finished_4_level
 * @property integer $finished_6_level
 * @property integer $passed_test
 * @property integer $not_passed_test
 * @property integer $not_passed_not_buy
 * @property integer $not_passed_not_retest
 *
 * @property User $user
 */
class TriggersMail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'triggers_mail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'not_visit1', 'not_visit3', 'not_visit6', 'finished_5_level','finished_4_level','finished_6_level', 'passed_test', 'not_passed_test', 'not_passed_not_buy', 'not_passed_not_retest'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'not_visit1' => 'Not Visit1',
            'not_visit3' => 'Not Visit3',
            'not_visit6' => 'Not Visit6',
            'finished_5_level' => 'Finished 5 Level',
            'passed_test' => 'Passed Test',
            'not_passed_test' => 'Not Passed Test',
            'not_passed_not_buy' => 'Not Passed Not Buy',
            'not_passed_not_retest' => 'Not Passed Not Retest',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

}
