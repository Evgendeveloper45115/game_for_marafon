<?php

namespace app\helpers;

use app\models\PassedTest;
use app\models\PassedVideo;
use app\models\TestSuccess;
use yii\helpers\ArrayHelper;
use app\models\PassedArticle;
use app\models\PassedTheme;
use yii\helpers\VarDumper;

class CommonHelper
{

    public static function isPassedArticleByUser($article_id, $user_id)
    {

        return PassedArticle::find()
            ->where(['article_id' => $article_id])
            ->andWhere(['user_id' => $user_id])
            ->one();
    }

    public static function isPassedThemeByUser($theme_id, $user_id)
    {

        return PassedTheme::find()
            ->where(['theme_id' => $theme_id])
            ->andWhere(['user_id' => $user_id])
            ->one();
    }

    public static function isPassedVideoByUser($video_id, $user_id)
    {

        return PassedVideo::find()
            ->where(['video_id' => $video_id])
            ->andWhere(['user_id' => $user_id])
            ->one();
    }

    /**
     * @param $test_id
     * @param $user_id
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function isPassedTest($test_id, $user_id)
    {
        return PassedTest::find()
            ->where(['test_id' => $test_id])
            ->andWhere(['user_id' => $user_id])
            ->one();
    }

    public static function isPassedByUser($item, $user_id)
    {
        switch (get_class($item)) {
            case 'app\models\Test':
                return self::isPassedTest($item->id, $user_id);
                break;
            case 'app\models\Article':
                return self::isPassedArticleByUser($item->id, $user_id);
                break;
            case 'app\models\Theme':
                return self::isPassedThemeByUser($item->id, $user_id);
                break;
            case 'app\models\Video':
                return self::isPassedVideoByUser($item->id, $user_id);
                break;
        }
    }

    public static function getCountPassedItemsByUser($theme_id, $user_id)
    {

        $theme_articles = \app\models\Article::find()
            ->select('id')
            ->where(['theme_id' => $theme_id])
            ->asArray()
            ->all();

        $theme_articles_id = ArrayHelper::getColumn($theme_articles, 'id');

        $passed_article = PassedArticle::find()
            ->where(['in', 'article_id', $theme_articles_id])
            ->andWhere(['user_id' => $user_id])
            ->count();

        if ($passed_article == 3) {
            self::passTheme($theme_id, $user_id);
        }

        return $passed_article;

    }

    public static function passTheme($theme_id, $user_id)
    {
        $theme = PassedTheme::find()
            ->where(['theme_id' => $theme_id])
            ->andWhere(['user_id' => $user_id])
            ->one();

        if (!$theme) {
            $theme = new PassedTheme([
                'user_id' => $user_id,
                'theme_id' => $theme_id
            ]);

            $theme->save();
        }
    }

    public static function getAlias($task, $visibility = '')
    {
        switch (get_class($task)) {
            case 'app\models\Article':
                if (stristr($visibility, 'closed') || stristr($visibility, 'locked')) {
                    return '#task-no-complete';
                    break;
                }
                return 'article/';
                break;
            case 'app\models\Theme':
                if (stristr($visibility, 'closed') || stristr($visibility, 'locked')) {
                    return '#task-no-complete';
                    break;
                }
                return 'theme/';
                break;
            case 'app\models\Video':
                if (stristr($visibility, 'closed') || stristr($visibility, 'locked')) {
                    return '#task-no-complete';
                    break;
                }
                return 'video/';
            case 'app\models\Test':
                if (stristr($visibility, 'closed') || stristr($visibility, 'locked')) {
                    return '#task-no-complete';
                    break;
                }
                return '/test/pravilnoe-pitanie?level=';
                break;
        }
    }

    public static function getTaskVisibility($counter, $tasks, $prev_theory = null)
    {

        $user_id = \Yii::$app->user->identity->getId();

        if ($counter == 1) {
            return 'block__round--opened';
        }

        if($counter == 2) {
            if(self::isPassedByUser($tasks[0], $user_id)){
                return 'block__round--opened';
            } else{
                return 'block__round--closed js-venobox vbox-item';
            }
        }

        if ($counter == 3 && isset($prev_theory)) {
            if (self::isPassedByUser($prev_theory, $user_id)) {
                return 'block__round--opened';
            } else {
                return 'block__round--closed js-venobox vbox-item';
            }
        }

        if($counter == 4) {
            if(self::isPassedByUser($tasks[0], $user_id)){
                return 'block__round--opened';
            } else{
                return 'block__round--closed js-venobox vbox-item';
            }
        }
    }

    public static function getLineTaskVisibility($counter, $tasks)
    {

        if ($counter == 1) {
            return 'level--active';
        }

        if (self::isPassedByUser($tasks[$counter - 2], \Yii::$app->user->identity->getId())) {
            return 'level--active';
        } else {
            return 'level--locked js-venobox vbox-item';
        }
    }

    public static function getLineTaskActivity($counter, $tasks)
    {

        if ($counter == 1) {
            return 'block__round--opened';
        }

        if (self::isPassedByUser($tasks[$counter - 2], \Yii::$app->user->identity->getId())) {
            return 'block__round--opened';
        }
    }


}