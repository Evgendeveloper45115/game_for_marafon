<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "test".
 *
 * @property integer $id
 * @property integer $level_id
 * @property string $name
 * @property string $img_radius
 * @property string $percent
 * @property string $success_text
 * @property string $fail_text
 * @property string $point
 *
 * @property Level $level
 * @property TestQuestionLevel[] $testQuestionLevels
 */
class Test extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $image;

    public static function tableName()
    {
        return 'test';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['level_id', 'point'], 'integer'],
            [['success_text', 'fail_text'], 'safe'],
            [['name', 'img_radius', 'percent'], 'string', 'max' => 255],
            [['level_id'], 'exist', 'skipOnError' => true, 'targetClass' => Level::className(), 'targetAttribute' => ['level_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'level_id' => 'Уровень',
            'name' => 'Название под кружком',
            'img_radius' => 'картинка кружка',
            'image' => 'картинка кружка',
            'percent' => 'Процент',
            'fail_text' => 'Текст если ответов меньше чем нужно',
            'success_text' => 'Текст если ответов Окей',
            'point' => 'к-во балов',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLevel()
    {
        return $this->hasOne(Level::className(), ['id' => 'level_id']);
    }

    public function getTestQuestionLevels()
    {
        return $this->hasMany(TestQuestionLevel::className(), ['test_id' => 'id'])->all();
    }
}
