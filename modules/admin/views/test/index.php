<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Тестирование';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="test-question-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить тест', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Добавить вопросы к тесту', ['test-questions'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //  ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute' => 'level_id',
                'content' => function ($data) {
                    return $data->level ? $data->level->name : null;
                },
                //'filter' => Faq::getFaqType(),
            ],
            [
                'attribute' => 'img_radius',
                'content' => function ($data) {
                    return Html::img(Yii::getAlias('@rel_path_to_uploads') . '/' . $data->img_radius, ['class' => 'profile_foto']);
                },
                //'filter' => Faq::getFaqType(),
            ],
            'name',
            'success_text',
            'fail_text',
            'percent',
            'name',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}&nbsp;{delete}'
            ],
        ],
    ]); ?>
</div>
