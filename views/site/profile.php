<?php

/* @var $this yii\web\View */
/* @var $profile \app\models\Profile */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'Профиль';
$active_menu = 'profile';
?>
<div class="page_trainings page_with_menu page_red clearfix">
    <div class="page_content">
        <h1>Профиль</h1>

        <div class="name">Загрузите вашу фотографию</div>
        <div class="profile-form">
            <?php
            $form = ActiveForm::begin(['fieldConfig' => ['options' => ['class' => 'form_group clearfix field-user-avatar required'],
                'template' => "{label}\n<div>{input}\n<div class=\"error\">{error}</div></div>",],]);
            ?>
            <?= $form->field($profile, 'foto', ['inputOptions' => ['class' => 'upload-input-avatar', 'style' => 'display:none']])->fileInput(['onchange'=>'readURL(this);'])
                ->label('<img class="img_profile data-img="' . $profile->getAvatarSrc() . '" src="' . $profile->getAvatarSrc(). '">') ?>
            <div class="">
                <label>&nbsp;</label>

                <div class="form_group clearfix avatar-submit">
                    <button type="button" id="cancel-upload">Отмена</button>
                    <?= Html::submitButton('Загрузить фото', ['style' => 'width: 245px;']) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>