<?php

use yii\db\Migration;

class m170913_075824_change_desc_type extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('article', 'description');
        $this->dropColumn('video', 'description');
        $this->addColumn('article', 'description', $this->text());
        $this->addColumn('video', 'description', $this->text());
    }

    public function safeDown()
    {
        $this->dropColumn('article', 'description');
        $this->dropColumn('video', 'description');
        $this->addColumn('article', 'description', $this->integer());
        $this->addColumn('video', 'description', $this->integer());
    }

}
