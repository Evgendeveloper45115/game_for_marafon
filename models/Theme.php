<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "theme".
 *
 * @property integer $id
 * @property integer $level_id
 * @property string $name
 * @property string $description
 * @property string $type
 * @property string $img_radius
 * @property Level $level
 * @property Article $articles
 */
class Theme extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $types = [
        'Теория',
        'Задание'
    ];
    const THEORY = 0;
    const TASK = 1;
    public $image_radius;

    public function FillBlankImg()
    {
        if (!$this->img_radius) {
            $this->img_radius = "blank.jpg";
        }
    }

    public static function tableName()
    {
        return 'theme';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['level_id', 'name', 'description', 'type', 'img_radius'], 'safe'],
            [['level_id', 'weight'], 'integer'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['level_id'], 'exist', 'skipOnError' => true, 'targetClass' => Level::className(), 'targetAttribute' => ['level_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'level_id' => 'Уровень',
            'name' => 'Название',
            'description' => 'Описание',
            'weight' => 'Вес(для сортировки)',
            'type' => 'Тип(задание или статья)',
            'img_radius' => 'Картинка в кружке',
            'image_radius' => 'Картинка в кружке',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLevel()
    {
        return $this->hasOne(Level::className(), ['id' => 'level_id']);
    }
    public function getArticles()
    {
        return $this->hasMany(Level::className(), ['id' => 'theme_id']);
    }

    public function dropOldImg()
    {
        if ($this->img_radius) {
            $path_to_file = Yii::getAlias('@abs_path_to_uploads') . '/' . $this->img_radius;

            if (file_exists($path_to_file) && $this->img_radius != 'blank.jpg') {
                unlink($path_to_file);
                $this->img_radius = 'blank.jpg';
                $this->save(false);
            }
        }
    }

    public static function getValuesArray()
    {
        // Выбираем только те категории, у которых есть дочерние категории
        $themes = self::find()
            ->select('name')
            ->all();

        return ArrayHelper::map($themes, 'name', 'name');
    }

}
