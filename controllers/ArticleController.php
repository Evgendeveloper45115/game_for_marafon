<?php

namespace app\controllers;

use app\models\PassedArticle;
use app\models\TaskAnswers;
use Yii;
use yii\web\Controller;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\helpers\VarDumper;
use app\models\Article;
use yii\web\UploadedFile;
use yii\filters\AccessControl;


class ArticleController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }


    public function actionIndex($id)
    {
        /**
         * @var $article Article
         */
        $article = Article::find()
            ->where(['id' => $id])
            ->one();
        $passedArt = PassedArticle::find()
            ->where(['user_id' => Yii::$app->getUser()->getId()])
            ->andWhere(['article_id' => $id])
            ->one();
        if (!$article) {
            throw new NotFoundHttpException('Статья не найдена');
        }
        $task_answer = TaskAnswers::find()->where(['user_id' => Yii::$app->getUser()->id, 'article_id' => $article->id])->one();
        if(!$task_answer){
            $task_answer = new TaskAnswers();
        }
        $task_answer->article_id = $article->id;
        $task_answer->user_id = Yii::$app->getUser()->identity->getId();
        $is_sent_answer = false;

        if ($task_answer->load(Yii::$app->request->post())) {
            $task_answer->save(false);

            $is_sent_answer = true;
        }
        return $this->render('index', [
            'article' => $article,
            'passedArt' => $passedArt,
            'task_answer' => $task_answer,
            'is_sent_answer' => $is_sent_answer,
        ]);
    }


}
