<?php
use app\helpers\CommonHelper;
use yii\helpers\Url;

/**
 * @var $article \app\models\Article
 */
count($articles) == CommonHelper::getCountPassedItemsByUser($theme->id, $user->id) ? $fireworks = 'fireworks' : $fireworks = false;
?>

<a class="back-link" href="<?= Url::to(['level/index', 'id' => $theme->level_id]) ?>">Назад к заданиям</a>
<div class="top-content clearfix">
    <div class="col-4 top-content__right <?= $fireworks ?>">
        <div class="completed">
            <?php
            if ($fireworks) {
                ?>
                <b class="completed__text c-red">Выполнено </b><span
                    class="completed__step"><?= CommonHelper::getCountPassedItemsByUser($theme->id, $user->id); ?>
                    /<?= count($articles) ?></span><a class="btn btn--next-block"
                                                      href="/level/index/<?= $theme->level->id ?>">Перейти к новому
                    блоку</a>
                <?php
            } else {
                ?>
                <b class="d-blue completed__text">Выполнено</b>
                <span
                    class="completed__step"><?= CommonHelper::getCountPassedItemsByUser($theme->id, $user->id); ?>
                    /<?= count($articles) ?></span>
                <?php
            }
            ?>
        </div>
    </div>
    <div class="col-8 top-left">
        <h2 class="top-content__title"><?= $theme->name ?></h2>

        <div class="text"><?= $theme->description ?></div>
    </div>
</div>
<div class="articles clearfix">
    <?php foreach ($articles as $article): ?>
        <div class="col-4">
            <a href="<?= Url::to(['article/index', 'id' => $article->id]) ?>">
                <div class="article">
                    <div
                        class="article__img <?php if (CommonHelper::isPassedArticleByUser($article->id, $user->id)) echo 'article__img--readed' ?>">
                        <img src="<?= Yii::getAlias('@rel_path_to_uploads') . '/' . $article->preview_image ?>">
                        <a class="get-points get-points--article" href="#">
                            <div class="get-points__inner">
                                <span><?= CommonHelper::isPassedArticleByUser($article->id, $user->id) ? 'Получено' : 'Получить:' ?> </span><span
                                    class="c-red"><?= $article->points ?> баллов</span>
                            </div>
                        </a>
                    </div>
                    <p class="article__name"><b class="c-red"><?= $article->name ?> </b>

                    <div class="article__desc text"><?= $article->preview_text ?></div>
                </div>
            </a>
        </div>
    <?php endforeach; ?>
</div>
<a style="display:none;" class="btn js-venobox vbox-item" id="time_popup" href="#task-no-complete-time"
   data-vbtype="inline">task complete 2</a>

<?php
$js = <<<JS
        $(function(){
        if("$fireworks" == 'fireworks'){
                $('#time_popup').trigger('click');
                
            window.setTimeout(function(){
            var venoOptions = {
                // is called after plugin initialization.
                cb_init: function(plugin){
                    $('.vbox-close').css('left', '-1000px');
                },
            }
            $('.venobox').venobox(venoOptions);
        }, 200);
                
        }
    })
JS;
$this->registerJs($js);
?>
<div class="js-popup" id="task-no-complete-time">
    <div class="popup popup--share">
        <div class="popup popup--task">
            <div class="popup-head">
                <h1 class="popup__title">Вы выполнили все задания!</h1>
            </div>
            <div class="popup-content">
                <p class="popup-content__next-task">Приступить к следующему заданию.</p><a class="btn btn--popup"
                                                                                           href="<?= '/level/index/' . $theme->level_id ?>">Перейти</a>
            </div>
        </div>
    </div>
</div>


