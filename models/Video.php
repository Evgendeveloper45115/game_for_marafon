<?php

namespace app\models;

use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "video".
 *
 * @property integer $id
 * @property integer $weight
 * @property integer $level_id
 * @property string $name
 * @property string $description
 * @property string $img
 * @property string $type
 * @property integer $timer
 * @property integer $points
 * @property string $img_radius
 * @property PassedVideo[] $passedVideos
 * @property Level $level
 */
class Video extends \yii\db\ActiveRecord
{
    public $types = [
        'Теория',
        'Задание'
    ];
    const THEORY = 0;
    const TASK = 1;

    public $file;
    public $image_radius;

    public function FillBlankImg()
    {
        if (!$this->img) {
            $this->img = "blank.jpg";
        }
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'video';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['level_id', 'name', 'description', 'img', 'timer', 'points'], 'required'],
            [['type', 'img_radius'], 'safe'],
            [['level_id', 'timer', 'points', 'weight'], 'integer', 'max' => 999999999],
            [['file'], 'file', 'extensions' => 'gif, jpg, jpeg, png'],
            [['name', 'img', 'code'], 'string', 'max' => 255],
            [['level_id'], 'exist', 'skipOnError' => true, 'targetClass' => Level::className(), 'targetAttribute' => ['level_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'level_id' => 'Уровень',
            'name' => 'Название видео',
            'description' => 'Контент',
            'img' => 'Изображение',
            'timer' => 'Таймер',
            'points' => 'Количество баллов',
            'file' => 'Файл изображения',
            'code' => 'Ссылка на видео',
            'weight' => 'Вес(для сортировки)',
            'type' => 'Тип Видео',
            'img_radius' => 'Картинка в кружке',
            'image_radius' => 'Картинка в кружке',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPassedVideos()
    {
        return $this->hasMany(PassedVideo::className(), ['video_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLevel()
    {
        return $this->hasOne(Level::className(), ['id' => 'level_id']);
    }

    public function dropOldImg()
    {
        if ($this->img) {
            $path_to_file = Yii::getAlias('@abs_path_to_uploads') . '/' . $this->img;

            if (file_exists($path_to_file) && $this->img != 'blank.jpg') {
                unlink($path_to_file);
                $this->img = 'blank.jpg';
                $this->save(false);
            }
        }
    }

    public function dropOldImg3()
    {
        if ($this->img_radius) {
            $path_to_file = Yii::getAlias('@abs_path_to_uploads') . '/' . $this->img_radius;

            if (file_exists($path_to_file) && $this->img_radius != 'blank.jpg') {
                unlink($path_to_file);
                $this->img_radius = 'blank.jpg';
                $this->save(false);
            }
        }
    }

}
