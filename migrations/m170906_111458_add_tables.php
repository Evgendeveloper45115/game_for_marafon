<?php

use yii\db\Migration;

class m170906_111458_add_tables extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('{{%shape}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'foto' => $this->string()->notNull(),

        ], $tableOptions);

        $this->createTable('{{%constitution}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'foto' => $this->string()->notNull(),

        ], $tableOptions);

        $this->createTable('{{%profile}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'first_name' => $this->string(),
            'last_name' => $this->string(),
            'foto' => $this->string(),
            'shape_type' => $this->integer(),
            'constitution_type' => $this->integer(),
            'level' => $this->integer()->defaultValue(1),
            'account' => $this->integer()->defaultValue(0),
            'time_on_site' => $this->integer(),
            'last_activity' => $this->integer(),
            'passed_test' => $this->boolean()->defaultValue(false),
            'bought_programm' => $this->boolean()->defaultValue(false),

        ], $tableOptions);

        $this->createTable('{{%measure}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'height' => $this->integer()->notNull(),
            'weight' => $this->integer()->notNull(),
            'breast' => $this->integer()->notNull(),
            'hip' => $this->integer()->notNull(),
            'butt' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk-profile-user_id-user-id', 'profile', 'user_id', 'user', 'id',
            'RESTRICT', 'CASCADE'
        );

        $this->addForeignKey(
            'fk-profile-shape_type-shape-id', 'profile', 'shape_type', 'shape', 'id',
            'RESTRICT', 'CASCADE'
        );

        $this->addForeignKey(
            'fk-profile-constitution_type-constitution-id', 'profile', 'constitution_type', 'constitution', 'id',
            'RESTRICT', 'CASCADE'
        );

        $this->addForeignKey(
            'fk-measure-user_id-user-id', 'measure', 'user_id', 'user', 'id',
            'RESTRICT', 'CASCADE'
        );

    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-profile-user_id-user-id', 'profile');
        $this->dropForeignKey('fk-profile-shape_type-shape-id', 'profile');
        $this->dropForeignKey('fk-profile-constitution_type-constitution-id', 'profile');
        $this->dropForeignKey('fk-measure-user_id-user-id', 'measure');

        $this->dropTable('user');
        $this->dropTable('shape');
        $this->dropTable('constitution');
        $this->dropTable('profile');
        $this->dropTable('measure');
    }


}
