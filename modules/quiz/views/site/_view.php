<?php
/**
 * Yii quiz
 *
 * @author Marc Oliveras Galvez <oligalma@gmail.com>
 * @link http://www.oligalma.com
 * @copyright 2016 Oligalma
 * @license GPL License
 */

use yii\helpers\Html;
?>
<div class="quiz-answer">
    <div class="My-question">
    <?php
        echo 'Вопрос: '. $model->title;
    ?>
    </div>
    <br/>
    <?php
        echo Html::radioList($model->id, $model->user_answer, $model->answers, array('class' => 'question'));
    ?>
</div>