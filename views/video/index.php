<?php
/**
 * @var $video \app\models\Video
 * @var $passedVideo \app\models\PassedVideo
 */
$class = 'btn--radio-disactive';
$noClass = 'btn--radio-active';
if ($passedVideo) {
    $class = 'btn--radio-active';
    $noClass = 'btn--radio-disactive';
}
?>
<div class="video">
    <h1 class="video__title title"><?= $video->name ?></h1>
    <div class="video__text text">
        <?= $video->description ?>
    </div>
    <div class="video__frame">
        <a class="video__frame-link js-venobox js-video vbox-item" href="<?= $video->code ?>" data-autoplay="true"
           data-vbtype="video">
            <img src="<?= Yii::getAlias('@rel_path_to_uploads') . '/' . $video->img ?>" alt="">
        </a>
    </div>
    <div class="video__buttons"><span class="video__buttons-text">Я посмотрел(а) фильм:</span>

        <div class="video__buttons-btn"><a data-id="<?= $video->id ?>" data-ps="<?= $video->points ?>"
                                           class="btn btn_yes <?= !$passedVideo ? 'ok_btn' : '' ?> btn--radio <?= $class ?>"
                                           href="#">Да</a><a
                class="btn btn_no btn--radio btn--radio-right <?= $noClass ?>" href="#">Нет</a>
        </div>
    </div>
</div>
<div id="pop_block"></div>
<a style="display:none;" class="btn js-venobox vbox-item" id="time_popup" href="#task-no-complete-time"
   data-vbtype="inline">task complete 2</a>
<a style="display:none;" class="btn js-venobox vbox-item" id="video_popup" href="#task-complete-plus"
   data-vbtype="inline">task complete 2</a>

<?php
$js = <<<JS
        var s = 0;
 var button = false;
            function func() {
                s++;
                if(s < '$video->timer'){
                button = false;
                }else{
                button = true;
                }
               }
                      setInterval(func, 1000);
    $(document).on('click','.btn_yes.ok_btn',(function(e){
       e.preventDefault();
       if(button == true){
              var video_id = $(this).data('id');
              $(this).removeClass('btn--radio-disactive').addClass('btn--radio-active');
              $('.btn_no').removeClass('btn--radio-active').addClass('btn--radio-disactive');

        $.ajax({
            url: "/passed/video",
            data: 'video_id='+video_id+'&point='+$(this).data('ps'),
            success: function(res){
                $('#pop_block').append(res);
                $('#video_popup').trigger('click');
                $('.btn_yes').removeClass('ok_btn');
            },
            error: function(error){
            }
        });
        return false;
       }else{
                $('#time_popup').trigger('click');
       }
    }));
    $('.btn_no').click(function(e){
       e.preventDefault();
    });
    $(document).on('click','.btn-close',function(e){
       $('.popup-close').click();
        return false;
    });
JS;
$this->registerJs($js);
?>
<div class="js-popup" id="task-no-complete-time">
    <div class="popup popup--share">
        <div class="popup popup--task">
            <div class="popup-head">
                <h1 class="popup__title">Вы не просмотрели видео</h1>
                <span class="popup-close"></span>
            </div>
            <div class="popup-content">
                <p class="popup-content__next-task">Вернитесь и просмотрите видео полностью</p><a
                    class="btn btn--popup btn-close"
                                                                                                  href="#">Вернуться</a>
            </div>
        </div>
    </div>
</div>
