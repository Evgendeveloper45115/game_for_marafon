<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "test_question_level".
 *
 * @property integer $id
 * @property string $test_one
 * @property string $test_two
 * @property string $test_three
 * @property string $test_four
 * @property string $test_five
 * @property string $img
 * @property integer $test_id
 * @property string $title
 *
 * @property Test $test
 */
class TestQuestionLevel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $image;

    public static function tableName()
    {
        return 'test_question_level';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['test_id'], 'integer'],
            [['test_one', 'test_two', 'test_three', 'test_four', 'test_five', 'img', 'title'], 'string', 'max' => 255],
            [['test_id'], 'exist', 'skipOnError' => true, 'targetClass' => Test::className(), 'targetAttribute' => ['test_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'test_one' => 'Правильный ответ',
            'test_two' => 'Ответ-2',
            'test_three' => 'Ответ-3',
            'test_four' => 'Ответ-4',
            'test_five' => 'Ответ-5',
            'img' => 'Картинка теста',
            'test_id' => 'Тест',
            'percent' => 'Процент',
            'title' => 'Вопрос',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTest()
    {
        return $this->hasOne(Test::className(), ['id' => 'test_id']);
    }
}
