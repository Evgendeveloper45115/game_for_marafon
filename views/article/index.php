<?php

use app\helpers\MenuHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;

/**
 * @var $this \yii\web\View
 * @var $this \yii\web\View
 */
?>
<?php
$text = 'Получено:';
if (!$passedArt) {
    $text = 'Получить:';
}
?>
<a class="back-link" href="<?= MenuHelper::backToTask($article); ?>">Назад к заданиям</a>
<div class="content-head"
     style="background-image: url(<?= Yii::getAlias('@rel_path_to_uploads') . '/' . $article->img ?>);">
    <h1 class="content-head__title"><?= $article->name ?></h1>

    <p class="content-head__points"><?= $text ?>
        <br><?= $article->points ?> баллов</p>
</div>

<div class="text"><?= $article->description ?></div>
<?php if ($article->color_content): ?>
    <div class="text text--quote text--greenborder"
         style="border-color: <?= $article->block_color ?>"><?= $article->color_content ?>
    </div>
<?php endif; ?>

<?php if ($article->description_short): ?>
    <div class="clearfix">
        <div class="col-7">
            <div class="bottom-left">
                <img src="/uploads/<?= $article->imgFooter ?>">
            </div>
        </div>
        <div class="col-5">
            <div class="bottom-right">
                <div class="text"><?= $article->description_short ?></div>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php


$recipe_col = 'col-9';
$text = 'Получено:';
$is_hide = 'hide_btn;';
if (!$passedArt) {
    $text = 'Получить:';
    $is_hide = '';
}
?>

<?php if ($article->task_title): ?>
    <div class="task_block">
        <div class="contest-task">
            <div class="contest__subtitle marginbottom1 task_margin">Задание</div>
            <div class="contest-task__short margintop1"></div>
            <div class="contest-task__text text margintop2 marginbottom2"><?= $article->task_text ?></div>
        </div>
        <div class="contest-recipe clearfix">
            <?php $form = ActiveForm::begin([
                'id' => 'form_user_data',
                'options' => ['enctype' => 'multipart/form-data']
            ]); ?>
            <?php if ($article->is_task_photo): ?>
                <div class="recipe-photo col-3">


                    <p class="contest__subtitle marginbottom1 photo_margin">Добавьте ваше фото</p>
                    <div class="file_input">
                        <?php echo $form->field($task_answer, 'photo_file')->widget(FileInput::classname(), [
                            'options' => ['accept' => 'image/*', 'multiple' => false],
                            'pluginOptions' => [
                                'uploadUrl' => Url::to(['/site/file-upload']),
                                'maxFileCount' => 1,
                                'maxFileSize' => 28000,
                                'uploadExtraData' => new \yii\web\JsExpression('function() {
                        var obj = {};
                        obj[\'text\'] = $(\'#taskanswers-text\').val();
                        obj[\'article_id\'] = $(\'#article_id\').val();
                        return obj;
                    }'),
                                'showUpload' => false
                            ]
                        ])->label('');

                        // $form->field($task_answer, 'foto_file', ['inputOptions' => [ 'class' => '', 'style' => 'display:none;']])->fileInput(['onchange'=>'readURLClassName(this, "add-photo");'])
                        //     ->label('<div class="add-photo"></div>')
                        ?>
                    </div>

                </div>
                <?php $recipe_col = 'col-9'; endif; ?>
            <?php

            ?>
            <div class="recipe-form <?= $recipe_col ?>">
                <div class="contest__subtitle marginbottom1"><?= $article->task_title ?></div>
                <?= $form->field($task_answer, 'text',
                    ['template' => '{input}', 'options' => ['class' => 'user_input_wrapper']])
                    ->textarea(['class' => 'textarea'])->label(false); ?>
            </div>
            <div class="col-12">

                <?php if (!$passedArt) echo Html::submitButton('Отправить', ['class' => 'btn btn--recipe btn--read' . $is_hide, 'data-article' => $article->id]) ?>

                <div class="get-points get-points--btn">
                    <div class="get-points__inner"><span><?= $text ?> </span><span class="c-red"><span
                                    class="points"><?= $article->points ?></span>
                <span>баллов</span></span>
                    </div>
                </div>
            </div>
            <?= Html::hiddenInput('article_id', $article->id, ['id' => 'article_id']); ?>
            <?= Html::hiddenInput('user_id', Yii::$app->user->identity->getId()); ?>
            <?php ActiveForm::end(); ?>

        </div>
    </div>
<?php endif; ?>

<?php
$isDisplayed = '';
if ($article->task_title) {
    $isDisplayed = 'disnone';
}
?>
<div class="bottom-left__points">
    <?php if (!$passedArt): ?>
        <a class="btn btn--read js_read <?= $isDisplayed ?>" data-article="<?= $article->id ?>">Я прочитал(а) статью</a>
    <?php endif; ?>
    <div class="get-points get-points--btn <?= $isDisplayed ?>">
        <div class="get-points__inner"><span><?= $text ?> </span><span class="c-red"><span
                        class="points"><?= $article->points ?></span>
                <span>баллов</span></span>
        </div>
    </div>
</div>


<a style="display:none;" class="btn js-venobox vbox-item" id="task_popup" href="#task-complete-plus"
   data-vbtype="inline">task complete 2</a>

<a style="display:none;" class="btn js-venobox vbox-item" id="time_popup" href="#task-no-complete-time"
   data-vbtype="inline">task complete 2</a>

<a style="display:none;" class="btn js-venobox vbox-item" id="text_task_popup" href="#you_must_add_text"
   data-vbtype="inline">task complete 2</a>

<a style="display:none;" class="btn js-venobox vbox-item" id="photo_task_popup" href="#you_must_add_photo"
   data-vbtype="inline">task complete 2</a>

<div id="pop_block"></div>

<?php
$js = <<<JS

var file_sent = false;

$(document).ready(function(){
    if($('.file-drop-zone-title').length){
        $('.file-drop-zone-title').html('Добавить фото');
    }
});

 $('#form_user_data').on('beforeSubmit', function(){

    if(file_sent){
        return true;
    }
    else{
             return $('#form_user_data').on('fileuploaded', function(event, data, previewId, index) {
              var form = data.form, 
              files = data.files, extra = data.extra,          
              response = data.response, reader = data.reader;         
              file_sent = true;
              $('#form_user_data').trigger('submit'); 
              return false;  
    });
    }
 
 });

$('#form_user_data').on('beforeValidate', function(){    
    
    if($('.file-drop-zone').length != 0 && $('.kv-file-content').length == 0 && $('#taskanswers-text').val() == ""){
        $('#photo_task_popup').trigger('click');
        return false;
    }
    
    
   $($('.kv-file-upload')[0]).trigger('click'); 
    return true;  
});






        var s = 0;
 var button = false;
            function func() {
                s++;
                if(s < '$article->timer'){
                button = false;
                }else{
                button = true;
                }
               }
                      setInterval(func, 1000);
                      $(document).on('click','.popup.popup--readed .btn--popup',function(e){
                      $('.popup-close').click();
                      });
    $('.js_read').click(function(e){
       //e.preventDefault();                     
       if(button == true || '$is_sent_answer'){
              var article_id = $(this).attr('data-article');
        $.ajax({
            url: "/passed/article",
            data: 'article_id='+article_id+'&point='+$('.points').text(),
            success: function(res){
                $('#pop_block').append(res);
                $('#task_popup').trigger('click');
                $('.btn--read').remove();
            },
            error: function(error){
            }
        });
        return false;
       }else{
                $('#time_popup').trigger('click');
       }
    });
     
    
if('$is_sent_answer'){
   $('.js_read').trigger('click');    
};

$('.file-drop-zone').click(function() {
   $('#taskanswers-photo_file').trigger('click');
});

JS;
$this->registerJs($js);
?>
<div class="js-popup" id="task-no-complete-time">
    <div class="popup popup--readed"><span class="popup-close"></span>

        <p class="popup-content__text popup-content__text--readed">Вы слишком быстро прочитали статью.</p>

        <p class="popup-content__text popup-content__text--readed">Прочтите статью более внимательно, чтобы
            <br>получить баллы</p><a class="btn btn--popup btn--arrow-up" href="#">Перейти в начало статьи</a>
    </div>
    <!--<div class="popup popup--share">
        <div class="popup popup--task">
            <div class="popup-head">
                <h1 class="popup__title">Вы не прочитали статью</h1>

                <h2 class="popup__subtitle">Вернитесь и прочитайте статью полностью</h2><span
                    class="popup-close"></span>
            </div>
        </div>
    </div>-->
</div>


<div class="js-popup" id="you_must_add_text">
    <div class="popup popup--level">
        <div class="popup-head">
            <h1 class="popup__title">Задание не выполнено</h1>
            <span class="popup-close"></span>
        </div>
        <div class="popup-content clearfix">
            <div class="popup-content__text">
                Впишите ответ на предложенное задание и нажмите отправить.
            </div>
        </div>
    </div>
</div>

<div class="js-popup" id="you_must_add_photo">
    <div class="popup popup--level">
        <div class="popup-head">
            <h1 class="popup__title">Задание не выполнено</h1>
            <span class="popup-close"></span>
        </div>
        <div class="popup-content clearfix">
            <div class="popup-content__text">
                Приложите к заданию фото или впишите ответ на предложенное задание и нажмите отправить.
            </div>
        </div>
    </div>
</div>
