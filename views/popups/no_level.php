<?php
$level = 1;

if(isset(Yii::$app->user->identity->profile->level)){
    $level = Yii::$app->user->identity->profile->level;
}


?>
<div class="js-popup" id="task-complete-plus">
    <div class="popup popup--share">
        <div class="popup popup--task">
            <div class="popup-head">
                <h1 class="popup__title">Вы не прошли уровень</h1>

                <h2 class="popup__subtitle">Пройдите уровень №<?= $level ?></h2><span class="popup-close"></span>
            </div>
            <div class="popup-content">
                <p class="popup-content__next-task">Приступить к заданиям уровня</p><a class="btn btn--popup"
                                                                                       href="<?= \yii\helpers\Url::to(['level/index', 'id' => $level]) ?>">Перейти</a>
            </div>
        </div>
    </div>
</div>
