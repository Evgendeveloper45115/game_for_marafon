<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "main_page".
 *
 * @property integer $id
 * @property string $image
 * @property string $link_video
 * @property string $header
 * @property string $content_block_color
 * @property string $block_color
 * @property string $description
 * @property string $program_name
 * @property string $program_description
 */
class MainPage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'main_page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image', 'link_video', 'header', 'content_block_color', 'block_color', 'program_name'], 'string', 'max' => 255],
            [['program_description'], 'string'],
            [['description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Картинка',
            'link_video' => 'Ссылка на видео',
            'header' => 'Заголовок',
            'content_block_color' => 'Текст цветного блока',
            'block_color' => 'Цвет блока',
            'description' => 'Описание',
            'program_name' => 'Название программы в результатах теста',
            'program_description' => 'Описание программы в результатах теста',
        ];
    }

    public function dropOldImg()
    {
        if ($this->image) {
            $path_to_file = Yii::getAlias('@abs_path_to_uploads') . '/' . $this->image;

            if (file_exists($path_to_file) && $this->image != 'blank.jpg') {
                unlink($path_to_file);
                $this->image = 'blank.jpg';
                $this->save(false);
            }
        }
    }

}
