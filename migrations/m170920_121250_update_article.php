<?php

use yii\db\Migration;

class m170920_121250_update_article extends Migration
{
    public function safeUp()
    {
        $this->addColumn('article', 'theme_id', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn('article', 'theme_id');
        return false;
    }
}
