<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

    <div class="private">
        <div class="private-top clearfix">
            <div class="private-avatar col-6">
                <div class="private-top-desc">
                    <?php if ($avatar_img) { ?>
                        <a class="private-top__link js-venobox" href="#add-photo" data-vbtype="inline">Изменить
                            фотографию</a>
                    <?php } else { ?>
                        <p class="private-top__text">Добавьте вашу фотографию</p>
                    <?php } ?>
                </div>
                <?php if ($avatar_img) { ?>
                    <img class="private-avatar__img"
                         src="<?= Yii::getAlias('@rel_path_to_uploads') . '/profile/' . $avatar_img ?>">
                <?php } else { ?>

                    <a class="private-avatar__add js-venobox" href="#add-photo" data-vbtype="inline"></a>
                <?php } ?>
            </div>
            <div class="private-awards col-6">

            </div>
        </div>
        <p class="private__text text">Чтобы мы могли вам оптимальную информацию подходящую под ваши, текущие физические
            данные нам необходимо узнать.</p>

        <div class="private-types clearfix">
            <div class="private-type private-type--figure col-6"
                 style="background-image:url(<?= '..' . Yii::getAlias('@rel_path_to_uploads') . '/' . $shape_type_img ?>)">
                <h1 class="private-type__title title">Ваш тип фигуры</h1>

                <p class="private-type__text text">Помните любой тип фигуры прекрасен, если приложить достаточно
                    усилий</p><a class="link js-venobox" href="#add-shape" data-vbtype="inline">Выбрать тип
                    фигуры</a><span class="private-type__desc private-type__desc--figure">Яблоко</span>
            </div>
            <div class="private-type private-type--body col-6"
                 style="background-image:url(<?= '..' . Yii::getAlias('@rel_path_to_uploads') . '/' . $constitution_type_img ?>)">
                <h1 class="private-type__title title">Ваш тип телосложения</h1>

                <p class="private-type__text text">С любым типом телосложения вы можете выглядеть замечательно</p><a
                    class="link js-venobox" href="#add-constitution" data-vbtype="inline">Выбрать тип фигуры</a><span
                    class="private-type__desc private-type__desc--body">Эктоморф</span>
            </div>
        </div>
        <div class="clearfix">
            <div class="col-6">
                <p class="sizes__text text">Напишите ваше объемы, чтобы следить за изменениями в вашем теле, потому что
                    вес не всегда показывает истинную картину</p>
            </div>
            <div class="col-6"><a class="btn btn--size-old js-show_old_sizes">Показать прошлые замеры</a>
            </div>
        </div>
        <div class="clearfix">
            <div class="sizes col-6">

                <?php $form = ActiveForm::begin(['action' => ['//profile/save-measure/']]); ?>

                <div
                    class="size"><?= $form->field($measure, 'height')->textInput(['class' => 'textbox textbox--size'])->label('Рост (см):', ['class' => 'size__text']) ?></div>
                <div
                    class="size"><?= $form->field($measure, 'weight')->textInput(['class' => 'textbox textbox--size'])->label('Вес (кг):', ['class' => 'size__text']) ?></div>
                <div
                    class="size"><?= $form->field($measure, 'breast')->textInput(['class' => 'textbox textbox--size'])->label('Объем груди (см):', ['class' => 'size__text']) ?></div>
                <div
                    class="size"><?= $form->field($measure, 'hip')->textInput(['class' => 'textbox textbox--size'])->label('Объем талии (см):', ['class' => 'size__text']) ?></div>
                <div
                    class="size"><?= $form->field($measure, 'butt')->textInput(['class' => 'textbox textbox--size'])->label('Объем ягодиц (см):', ['class' => 'size__text']) ?></div>
                <div
                    class="size"><?= $form->field($measure, 'waist')->textInput(['class' => 'textbox textbox--size'])->label('Объем бедер (см):', ['class' => 'size__text']) ?></div>

                <div class="form-group">
                    <?= Html::submitButton('Отправить', ['class' => 'btn btn--size .js-send-measure']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
            <div class="sizes-right col-6">
                <div class="size-article buy">
                    <p class="text">Помните! Ваши замеры это просто цифры, которые показывают в какой точке пути вы
                        находитесь в данный момент и ничего больше!</p>
                </div>
            </div>
        </div>
        <div class="old-sizes">
            <?php foreach ($old_measures as $measure): ?>
                <div class="size-remark">
                    <p class="size-remark__text text">Замеры от <?= Yii::$app->formatter->asDate($measure->date); ?></p>

                    <p class="size-remark__text text">Рост (см): <?= $measure->height; ?></p>

                    <p class="size-remark__text text">Вес (кг): <?= $measure->weight; ?></p>

                    <p class="size-remark__text text">Объем груди (см): <?= $measure->breast; ?></p>

                    <p class="size-remark__text text">Объем груди (см): <?= $measure->waist; ?></p>

                    <p class="size-remark__text text">Объем ягодиц (см): <?= $measure->butt; ?></p>

                    <p class="size-remark__text text">Объем бедер (см): <?= $measure->hip; ?></p>
                </div>
            <?php endforeach;  ?>
        </div>
    </div>


    <?php  // $this->render('/popups/avatar'); ?>


    <!-- попап для смены фото -->
    <div class="js-popup" id="add-photo">
        <div class="popup popup--level fireworks fireworks--popup">
            <div class="popup-head">
                <h1 class="popup__title">Фотография профиля</h1><span class="popup-close"></span>
            </div>
            <div class="popup-content clearfix">
                <div class="popup-content__img col-12">

                    <?php $form = ActiveForm::begin(
                        [
                            'action' => ['/profile/save-avatar/']
                        ]
                    ); ?>

                    <?= $form->field($photo_form, 'file', ['inputOptions' => [ 'class' => '', 'style' => 'display:none;']])->fileInput(['onchange'=>'readURLClassName(this, "add-photo");'])
                        ->label('<div class="add-photo"></div>') ?>



                    <div class="form-group">
                        <?= Html::submitButton('Загрузить', ['class' => 'btn btn--size']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>

    <!-- попап для выбора типа фигуры-->
    <div class="js-popup" id="add-shape">
        <div class="popup popup--level fireworks fireworks--popup big_popup">
            <div class="popup-head">
                <h1 class="popup__title">Выбор типа фигуры</h1>
            </div>
            <div class="popup-content clearfix">
                <div class="popup-content__img col-12">

                    <?php $form = ActiveForm::begin(['action' => ['//profile/save-shape/']]);; ?>

                    <div class="form-group">
                        <?=
                        $form->field($shape_form, 'shape')
                            ->radioList($shapes, [
                                'item' => function ($index, $label, $name, $checked, $value) use ($shapes_images) {

                                    $return = '<label class="modal-radio">';
                                    $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" tabindex="3">';
                                    $return .= '<i>' . Html::img(Yii::getAlias('@rel_path_to_uploads') . '/' . array_shift($shapes_images)) . '</i>';
                                    $return .= '<span class="form_label">' . ucwords($label) . '</span>';
                                    $return .= '</label>';
                                    $return .= '<div class="clearfix"></div>';

                                    return $return;
                                }
                            ])->label(false);
                        ?>
                    </div>
                    <div class="form-group">
                        <?= Html::submitButton('Отправить', ['class' => 'btn btn--size']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>

    <!-- попап для выбора типа телосложения-->
    <div class="js-popup" id="add-constitution">
        <div class="popup popup--level fireworks fireworks--popup">
            <div class="popup-head">
                <h1 class="popup__title">Выбор телосложения</h1>
            </div>
            <div class="popup-content clearfix">
                <div class="popup-content__img col-12">
                    <?php $form = ActiveForm::begin(['action' => ['//profile/save-constitution/']]);; ?>

                    <div class="form-group">
                        <?=
                        $form->field($constitution_form, 'constitution')
                            ->radioList($constitutions, [
                                'item' => function ($index, $label, $name, $checked, $value) use ($constitutions_images) {

                                    $return = '<label class="modal-radio2">';
                                    $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" tabindex="3">';
                                    $return .= '<i>' . Html::img(Yii::getAlias('@rel_path_to_uploads') . '/' . array_shift($constitutions_images)) . '</i>';
                                    $return .= '<span class="form_label">' . ucwords($label) . '</span>';
                                    $return .= '</label>';
                                    $return .= '<div class="clearfix"></div>';

                                    return $return;
                                }
                            ])->label(false);
                        ?>
                    </div>

                    <div class="form-group">
                        <?= Html::submitButton('Отправить', ['class' => 'btn btn--size']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>


<?php /*
$js = <<<JS
    $('#w0').on('beforeSubmit', function(){        
       var data = $(this).serialize();
        $.ajax({
            url: 'profile/save-measure/',
            type: 'POST',
            data: data,
            success: function(res){
                alert('Замеры получены');
                console.log(res);
                $('#alert').append(res)  
                //$('#comment-message').val('');                              
            },
            error: function(){
                alert('Ошибка отправки замеров');
            }
        });
        return false;
    });
JS;
$this->registerJs($js);
 */
?>