<?php

use yii\db\Migration;

class m171031_143726_question_test_level extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%test_question_level}}', [
            'id' => $this->primaryKey(),
            'test_one' => $this->string(),
            'test_two' => $this->string(),
            'test_three' => $this->string(),
            'test_four' => $this->string(),
            'test_five' => $this->string(),
            'img' => $this->string(),
            'test_id' => $this->integer(),
            'percent' => $this->integer(),
            'title' => $this->string()
        ], $tableOptions);

        $this->addForeignKey(
            'fk-test-test_id-test-id', 'test_question_level', 'test_id', 'test', 'id',
            'RESTRICT', 'CASCADE'
        );

    }


    public function safeDown()
    {
        echo "m171031_143726_question_test_level cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171031_143726_question_test_level cannot be reverted.\n";

        return false;
    }
    */
}
