<?php

namespace app\controllers;

use app\models\Theme;
use app\models\User;
use Yii;
use yii\web\Controller;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\helpers\VarDumper;
use app\models\Level;
use app\models\Article;
use app\models\Video;


class ThemeController extends Controller
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }


    public function actionIndex($id)
    {
        $theme = Theme::findOne(['id' => $id]);
        $user = Yii::$app->user->getIdentity();

        $articles = Article::find()
                ->where(['theme_id' => $id])
                ->limit(5)
                ->orderBy('weight')
                ->all();

        return $this->render('index', [
            'articles' => $articles,
            'theme' => $theme,
            'user' => $user,
        ]);
    }

}
