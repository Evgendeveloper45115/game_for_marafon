<?php
/**
 * @var $points string
 * @var $link string
 * @var $point_img bool
 */
$questions = count($tests->testQuestionLevels);
?>
<div class="js-popup" id="task-complete-plus">
    <div class="popup popup--share">
        <div class="popup popup--task">
            <div class="popup-head">
                <h1 class="popup__title">Вы выполнили задание!</h1>

                <h2 class="popup__subtitle">Правильных ответов <span id="success"></span>/<?= $questions ?></h2>
                <h2 class="popup__subtitle">+<?= $points ?> баллов</h2><span class="popup-close"></span>
            </div>
            <div class="popup-content">
                <p class="popup-content__next-task">Приступить к следующему заданию.</p><a class="btn btn--popup"
                                                                                           href="<?= $link ?>">Перейти</a>
            </div>
        </div>
    </div>
</div>
