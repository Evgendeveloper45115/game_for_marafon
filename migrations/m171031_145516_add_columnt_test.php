<?php

use yii\db\Migration;

class m171031_145516_add_columnt_test extends Migration
{
    public function safeUp()
    {
        $this->addColumn('test', 'name', $this->string());
    }

    public function safeDown()
    {
        echo "m171031_145516_add_columnt_test cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171031_145516_add_columnt_test cannot be reverted.\n";

        return false;
    }
    */
}
