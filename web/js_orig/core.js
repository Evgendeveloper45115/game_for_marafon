$('.js-slider').slick({
    slidesToShow: 5,
    slidesToScroll: 5,
    dots: false,
    prevArrow: '<button class="slider__arrow slider__arrow--prev"></button>',
    nextArrow: '<button class="slider__arrow slider__arrow--next"></button>',
    infinite: false,
    responsive: [{
        breakpoint: 750,
        settings: "unslick"
    }]
});

var vbox = $('.js-venobox').venobox({
    bgcolor: "rgba(255,255,255,0)"
});
$(document).on('click', '.popup-close', function (e) {
    vbox.VBclose();
});
$('.js-slider-comments').slick({
    slidesToShow: 3,
    slidesToScroll: 3,
    dots: false,
    prevArrow: '<button class="slider-comments__arrow slider-comments__arrow--prev"></button>',
    nextArrow: '<button class="slider-comments__arrow slider-comments__arrow--next"></button>',
    infinite: true,
    responsive: [{
        breakpoint: 750,
        settings: {
            arrows: true,
            centerMode: true,
            centerPadding: '10px',
            slidesToShow: 1
        }
    }]
});

$('#show-menu').click(function () {
    if ($(this).hasClass('show-menu--open')) {
        $(this).removeClass('show-menu--open');
        $('#menu').removeClass('menu--open');
        $('body').removeClass('menu-opened');
    } else {
        $(this).addClass('show-menu--open');
        $('#menu').addClass('menu--open');
        $('body').addClass('menu-opened');
    }
});

if ($('.js-progress').length) {
    var progress = $('.js-progress').data('progress'),
        $svgPath = $('.js-progress .top-user__svg--2 path'),
        length = $svgPath[0].getTotalLength();


    $svgPath.attr('stroke-dashoffset', Math.round(length * progress / 100));
}
$(function () {
    $(document).on('click', '.profile', function () {
        window.location.replace("/site/profile");
    });
    $(document).on('click', '.block__round--closed', function (e) {
        return false;
    });
    $(document).on('click', '.popup__link--vk', function (e) {
        setTimeout(function () {
            window.location.replace('/level/index/4');
        }, 10000);
    });
    $(document).on('click', '.popup__link--fb', function (e) {
        setTimeout(function () {
            window.location.replace('/level/index/4');
        }, 10000);
    });
    $(document).on('click', '.article__img--readed', function (e) {
        window.location.replace($(this).find('a').attr('href'));
    });
});