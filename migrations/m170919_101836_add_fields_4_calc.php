<?php

use yii\db\Migration;

class m170919_101836_add_fields_4_calc extends Migration
{
    public function safeUp()
    {
/*        $this->addColumn('constitution', 'breast_from', $this->integer());
        $this->addColumn('constitution', 'breast_to', $this->integer());
        $this->addColumn('constitution', 'waist_from', $this->integer());
        $this->addColumn('constitution', 'waist_to', $this->integer());
        $this->addColumn('constitution', 'hip_from', $this->integer());
        $this->addColumn('constitution', 'hip_to', $this->integer());*/

        $this->addColumn('shape', 'breast_from', $this->integer());
        $this->addColumn('shape', 'breast_to', $this->integer());
        $this->addColumn('shape', 'waist_from', $this->integer());
        $this->addColumn('shape', 'waist_to', $this->integer());
        $this->addColumn('shape', 'hip_from', $this->integer());
        $this->addColumn('shape', 'hip_to', $this->integer());

    }

    public function safeDown()
    {
/*        $this->dropColumn('constitution', 'breast_from');
        $this->dropColumn('constitution', 'breast_to');
        $this->dropColumn('constitution', 'waist_from');
        $this->dropColumn('constitution', 'waist_to');
        $this->dropColumn('constitution', 'hip_from');
        $this->dropColumn('constitution', 'hip_to');*/

        $this->dropColumn('shape', 'breast_from');
        $this->dropColumn('shape', 'breast_to');
        $this->dropColumn('shape', 'waist_from');
        $this->dropColumn('shape', 'waist_to');
        $this->dropColumn('shape', 'hip_from');
        $this->dropColumn('shape', 'hip_to');
    }

}
