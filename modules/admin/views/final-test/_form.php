<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TestQuestion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="test-question-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput() ?>
    <?= $form->field($model, 'answer')->textInput() ?>
    <?= $form->field($model, 'answer2')->textInput() ?>
    <?= $form->field($model, 'answer3')->textInput() ?>
    <?= $form->field($model, 'answer4')->textInput() ?>
    <?php if ($model->img) { ?>
        <?= Html::img(Yii::getAlias('@rel_path_to_uploads') . '/testImg/' . $model->img, ['class' => 'profile_foto']); ?>
        <div class="del_link">
            <?php
            if ($model->img != 'blank.jpg') {
                echo Html::a('Удалить', ['article/delete-img'], [
                    'data-method' => 'POST',
                    'data-params' => [
                        'id' => $model->id,
                    ],
                ]);
            }
            ?>
        </div>
    <?php } ?>

    <?php
    if (!$model->img) {
        echo Html::img('/uploads/blank.jpg', ['class' => 'profile_foto']);
    }
    ?>


    <?= $form->field($model, 'image')->fileInput(['onchange' => 'readURL(this);']); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
