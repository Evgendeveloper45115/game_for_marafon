<?php

namespace app\assets;

use yii\web\AssetBundle;
/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class LandingAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'landing/css/reset.css',
        'landing/css/core.css',
        'landing/css/core_.css',

    ];
    public $js = [
    ];
    public $depends = [
    ];
}