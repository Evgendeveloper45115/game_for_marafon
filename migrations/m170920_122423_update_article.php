<?php

use yii\db\Migration;

class m170920_122423_update_article extends Migration
{
    public function safeUp()
    {
        $this->addColumn('article', 'block_color', $this->string());
        $this->addColumn('article', 'color_content', $this->text());
        $this->addColumn('article', 'description_short', $this->text());
        $this->addColumn('article', 'imgFooter', $this->text());
    }

    public function safeDown()
    {
        $this->dropColumn('article', 'block_color');
        $this->dropColumn('article', 'color_content');
        $this->dropColumn('article', 'description_short');
        $this->dropColumn('article', 'imgFooter');
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170920_122423_update_article cannot be reverted.\n";

        return false;
    }
    */
}
