<?php
namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\VarDumper;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $role
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    const ROLE_USER = 0;
    const ROLE_ADMIN = 1;
    const SCENARIO_CHANGE_PASSWORD_USER = 'change_password_user';
    public $orig_password;
    public $new_password;
    public $password1;
    public $soc_profile;
    static $users;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role'], 'integer'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            ['orig_password', 'myCompare', 'on' => self::SCENARIO_CHANGE_PASSWORD_USER],
        ];
    }


    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CHANGE_PASSWORD_USER] = ['orig_password', 'new_password', 'password1'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
/*    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }*/

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Get all statuses
     * @return array
     */
    public static function getStatuses()
    {
        return [
            self::STATUS_ACTIVE => 'Активен',
            self::STATUS_DELETED => 'Удален',
        ];
    }

    public static function getRole()
    {
        return [
            self::ROLE_USER => 'Пользователь',
            self::ROLE_ADMIN => 'Администратор',
        ];
    }

    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'id']);
    }

    public function myCompare($attribute, $params)
    {
        if (!password_verify($this->$attribute, $this->password_hash)) {
            $this->addError($attribute, 'Старый пароль не совпадает');
        } elseif ($this->new_password != $this->password1) {
            $this->addError('new_password', 'Пароли не совпадают');
            $this->addError('password1', 'Пароли не совпадают');
        }
    }

    public function getTimeFromLastActivity(){
        return time() - $this->profile->last_activity;
    }

    public function isPassedTest(){
        if($this->profile->passed_test){
            return true;
        }
            return false;
    }

    public function getTimeAfterTest(){
        if($this->profile->test_start_time){
            return time() - $this->profile->test_start_time;
        }
            return 0;

    }

    public function dropOldAvatar()
    {
        if($this->profile->foto){
            $path_to_file = Yii::getAlias('@abs_path_to_uploads') . '/' . $this->profile->foto;

            if (file_exists($path_to_file))
            {
                unlink($path_to_file);
                $this->save(false);
            }
        }
    }

    public function updatePassword($new_password)
    {
        $this->password_hash = password_hash($new_password, PASSWORD_BCRYPT);
        $this->save(false);
    }


    public static function findIdentity($id)
    {
        if (\Yii::$app->getSession()->has('user-' . $id)) {
            return new self(\Yii::$app->getSession()->get('user-' . $id));
        } else {
            return isset(self::$users[$id]) ? new self(self::$users[$id]) : User::findOne(['id' => $id]);
        }
    }

    /**
     * @param \nodge\eauth\ServiceBase $service
     * @return User
     * @throws ErrorException
     */
    public static function findByEAuth($service) {
        if (!$service->getIsAuthenticated()) {
            throw new ErrorException('EAuth user should be authenticated before creating identity.');
        }

        $id = $service->getServiceName().'-'.$service->getId();
        $attributes = [
            'id' => $id,
            //'first_name' => $service->getAttribute('name'),
            //'authKey' => md5($id),
            'soc_profile' => $service->getAttributes(),
            'role' => User::ROLE_USER
        ];
        $attributes['soc_profile']['service'] = $service->getServiceName();
        Yii::$app->getSession()->set('user-'.$id, $attributes);
        return new self($attributes);
    }

    /**
     * Finds out user by social media info - type and id
     *
     * @param int $type Type of social media, vkontakte, facebook
     * @param int $id
     * @return static|null
     */
    public static function findBySocialMediaInfo($type, $id)
    {
        return static::findOne([
            'social_media_type' => $type,
            'social_media_id' => $id
        ]);
    }


}
