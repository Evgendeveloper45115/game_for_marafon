<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shape".
 *
 * @property integer $id
 * @property string $name
 * @property string $foto
 *
 * @property Profile[] $profiles
 */
class Shape extends \yii\db\ActiveRecord
{
    public $file;

    public function FillBlankImg(){
        if (!$this->foto){
            $this->foto = "blank.jpg";
        }
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shape';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'foto'], 'required'],
            [['name', 'foto'], 'string', 'max' => 255],
            [['breast_from', 'breast_to', 'waist_from', 'waist_to', 'hip_from', 'hip_to'], 'integer', 'max' => 999],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'foto' => 'Foto',
            'breast_from' => 'Грудь от',
            'breast_to' => 'Грудь до',
            'waist_from' => 'Талия от',
            'waist_to' => 'Талия до',
            'hip_from' => 'Бедра от',
            'hip_to' => 'Бедра до',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfiles()
    {
        return $this->hasMany(Profile::className(), ['shape_type' => 'id']);
    }

    public function dropOldImg()
    {
        if($this->foto){
            $path_to_file = Yii::getAlias('@abs_path_to_uploads') . '/' . $this->foto;

            if (file_exists($path_to_file) && $this->foto != 'blank.jpg')
            {
                unlink($path_to_file);
                $this->foto = 'blank.jpg';
                $this->save(false);
            }
        }
    }

}
