<?php
/**
 * @var $main \app\models\MainPage
 * @var $this \yii\web\View
 */

use yii\helpers\Url;

$this->title = 'Игра: «Построй стройную фигуру» | Beauty-Matrix';

$id_counter = 0;
$finalTest = false;
?>

    <div class="clearfix">
        <h1 class="title title__hidden title--ico title--game"><?= $main->header ?></h1>

        <div class="col-5">
            <a onclick="<?= 'yaCounter46569435.reachGoal(\'video\')'?>"  class="venobox_custom left-image youtube-link js-venobox vbox-item" data-autoplay="true"
               href="<?= $main->link_video ?>"
               data-vbtype="video">
                <img src="<?= '/uploads/' . $main->image ?>" alt="">
            </a>
        </div>
        <div class="col-7">
            <div class="content__right">
                <h1 class="title title--ico title--game"><?= $main->header ?></h1>

                <p class="text text--quote text"
                   style="background-color: <?= $main->block_color ?>"><?= $main->content_block_color ?></p>

                <div class="text"><?= $main->description ?></div>
            </div>

            <?php if (Yii::$app->user->isGuest): ?>
                <div class="start">
                    <a onclick="yaCounter46569435.reachGoal('start')" href="/site/signup">
                        <button class="btn btn--recipe ">Начать</button>
                    </a>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="js-slider levels">
        <?php
        foreach ($levels as $key => $level): ?>
            <?php
            if ($level->number_int > $user_level) {
                $class = 'level--locked js-venobox 
vbox-item';
                $link = '#task-complete-plus';
                $id = 'task-complete-plus';
            } else {
                $class = 'level--active';
                $link = Url::to(['level/index', 'id' => $level->id]);
                $id = '';
            }
            ?>

            <?php
            if (Yii::$app->user->isGuest) {
                $link = Url::to(['site/signup']);
                $class = 'level--locked';
            }
            ?>

            <a id="round_<?= ++$id_counter ?>" class="level <?= $class ?>" href="<?= $link ?>" data-vbtype="inline">
                <div class="level__round" style="background-image: url('/uploads/<?= $level->icon ?>')">
                    <?php
                    $style = null;
                    if (($id_counter == 1) && (Yii::$app->user->isGuest) || ($level->number_int == $user_level) && (($level->number_int != count($levels) && $user_level != count($levels)))) {
                        echo '<span class="btn btn--level ' . ($key == 0 && Yii::$app->user->isGuest ? 'start_class' : null) . '">Начать</span>';
                    } elseif ($level->number_int <= $user_level) {
                        if ($level->number_int == 6 && count($passedArt) == 5 && $key == 5) {
                            $style = 'background-color: transparent!important; box-shadow: none!important;';
                            echo '<span style="' . $style . '" class="btn btn--level">Пройдено</span>';
                        } elseif ($level->number_int == 6 && count($passedArt) != 5 && $key == 5) {
                            echo '<span class="btn btn--level">Начать</span>';
                        } else {
                            $style = 'background-color: transparent!important; box-shadow: none!important;';
                            echo '<span style="' . $style . '" class="btn btn--level">Пройдено</span>';
                        }
                    } ?>
                </div>
                <p class="level__name"><?= $level->number ?></p>
            </a>
            <?php
            if (count($passedArt) == 5):
                if ($finalTest == false && $key == 5) {
                    $finalTest = true;
                    ?>
                    <a class="level level--exam" href="/test/pravilnoe-pitanie">
                        <div class="level__round"></div>
                        <p class="level__name level__name--map">Экзамен</p>
                    </a>
                    <?php
                }
                $not_show_test_link = false; ?>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
<?= $this->render('/popups/no_level') ?>
<?php
$js = <<<JS
$('.level__round').click(function() {      
   window.setTimeout(function(){  $('.vbox-close').css({'left':'-1000px'});}, 100);              
});

$(document).ready(function(){  
});      
JS;
$this->registerJs($js);


