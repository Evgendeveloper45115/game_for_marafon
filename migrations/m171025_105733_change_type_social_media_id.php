<?php

use yii\db\Migration;

class m171025_105733_change_type_social_media_id extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('user', 'social_media_id');
        $this->addColumn('user', 'social_media_id', $this->string());
    }

    public function safeDown()
    {

    }

}
