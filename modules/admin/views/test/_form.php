<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Test */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="test-question-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput() ?>
    <?= $form->field($model, 'level_id')->dropDownList(['' => ''] + \yii\helpers\ArrayHelper::map(\app\models\Level::find()->asArray()->orderBy('id')->all(), 'id', 'name')) ?>
    <?php if ($model->img_radius) { ?>
        <?= Html::img(Yii::getAlias('@rel_path_to_uploads') . '/test_level_Img/' . $model->img_radius, ['class' => 'profile_foto']); ?>
    <?php } ?>

    <?php
    if (!$model->img_radius) {
        echo Html::img('/uploads/blank.jpg', ['class' => 'profile_foto']);
    }
    ?>
    Текст при нужном наборе баллов
    <?= $form->field($model, 'success_text', [
        'template' => "<div>{input}\n<div class=\"error\">{error}</div></div>",
    ])->widget(\zxbodya\yii2\tinymce\TinyMce::className(), [
        'options' => ['rows' => 15, 'style' => 'margin-top:5vw'],
        'language' => 'ru',
//        'spellcheckerUrl'=>'http://speller.yandex.net/services/tinyspell',
        'fileManager' => [
            'class' => \zxbodya\yii2\elfinder\TinyMceElFinder::className(),
            'connectorRoute' => 'el-finder/connector',
        ],
    ]); ?>
    Текст если клиент не набрал нужное к-тво баллов
    <?= $form->field($model, 'fail_text', [
        'template' => "<div>{input}\n<div class=\"error\">{error}</div></div>",
    ])->widget(\zxbodya\yii2\tinymce\TinyMce::className(), [
        'options' => ['rows' => 15, 'style' => 'margin-top:5vw'],
        'language' => 'ru',
//        'spellcheckerUrl'=>'http://speller.yandex.net/services/tinyspell',
        'fileManager' => [
            'class' => \zxbodya\yii2\elfinder\TinyMceElFinder::className(),
            'connectorRoute' => 'el-finder/connector',
        ],
    ]); ?>


    <?= $form->field($model, 'image')->fileInput(['onchange' => 'readURL(this);']); ?>
    <?= $form->field($model, 'percent')->textInput(); ?>
    <?= $form->field($model, 'point')->textInput(); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
