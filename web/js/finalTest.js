$(function () {
    $('.quest_tab').eq(0).show().addClass('active');
    var answers = [];
    var total = $('.quest_tab').length;
    $('.pr').text('1/' + total).width(Math.round($('.progress').width() / total));

    $('input[name=answer]').prop('checked', false);
    $('input[name=answer]').change(function () {
        $('.next').addClass('active');

    });

    $('.next').click(function (e) {
        e.preventDefault();
        $('.next').removeClass('active');
        index_tab = $('.quest_tab.active').index();
        if ($('.quest_tab').eq(index_tab).find('input:checked').val()) {
            flag = true
        } else {
            flag = false
        }
        var val = $('.quest_tab').eq(index_tab).find('input:checked').val();
        var id = $('.quest_tab').eq(index_tab).find('input:checked').closest('form').data('id');
        answers[id] = val;
        // answers.push(item);

        if (flag) {
            $('.quest_tab.active').removeClass('active').hide();
            index_tab++;
            if (index_tab <= (total - 1)) {
                $('.quest_tab').eq(index_tab).show().addClass('active');
                $('.pr').text(++index_tab + '/' + total).css('margin-left', --index_tab * $('.pr').width());
            } else {

                $('html, body').stop().animate({
                    scrollTop: $('header').offset().top
                }, 1000);

                $('.quest_tab').hide();
                $('.content-footer').hide();
                $('#submit').click();

                // ga('send', 'event', 'test', 'finish');


                var progressbar = $("#progressbar"),
                    progressLabel = $(".progress-label");

                progressbar.progressbar({
                    value: false,
                    change: function () {
                        progressLabel.text(progressbar.progressbar("value") + "%");
                    },
                    complete: function () {
                        $('.form').show();
                        $('.progressbars--block').hide();
                    }
                });

                function progress() {
                    var val = progressbar.progressbar("value") || 0;

                    progressbar.progressbar("value", val + 10);

                    if (val < 99) {
                        setTimeout(progress, 1000);
                    }
                }

                progress();


                var progressbar2 = $("#progressbar2"),
                    progressLabel2 = $(".progress-label2");

                progressbar2.progressbar({
                    value: false,
                    change: function () {
                        progressLabel2.text(progressbar2.progressbar("value") + "%");
                    },
                    complete: function () {
                        $('.form').show();
                        $('.progressbars--block').hide();
                    }
                });

                function progress2() {
                    var val = progressbar2.progressbar("value") || 0;

                    progressbar2.progressbar("value", val + 10);

                    if (val < 99) {
                        setTimeout(progress2, 1000);
                    }
                }

                progress2();


            }
        }

    });

    $('#submit').click(function (e) {
        str = '';
        $('.quest_tab').each(function () {
            str += '<input type="hidden" name="answer[' + $(this).find('form').attr('data-id') + ']" value="' + $(this).find('input:checked').val() + '">';
        });
        $('#result-form').append(str);
        var level = getParameterByName('level', location.href);
        var url = "/test/pravilnoe-pitanie";
        if (level !== '') {
            url = "/test/pravilnoe-pitanie?level=" + level
        }
        $.ajax({
            type: "POST",
            url: url,
            data: {answers: answers},
            success: function (res) {
                var q = JSON.parse(res);
                if (q.success !== 'true' && q.success !== 'false') {
                    window.location.replace(q.success);
                } else {
                    if (q.success === 'true') {
                        $('#success').text(q.successAnswers);
                        $("#task_popup").venobox().trigger('click');
                    } else {
                        $('#no_comp span#success').text(q.successAnswers);
                        $("#no_complete").venobox().trigger('click');
                    }
                }
            },
            error: function (error) {
            }
        });
        return false;
    });

    function getParameterByName(name, href) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(href);
        if (results == null)
            return "";
        else
            return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
});