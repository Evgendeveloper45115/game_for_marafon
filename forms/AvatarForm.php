<?php
namespace app\forms;

use yii\base\Model;
use yii\db\Exception;

class AvatarForm extends Model
{
    public $file;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file'], 'file'],
        ];
    }

}
