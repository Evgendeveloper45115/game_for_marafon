<?php

namespace app\modules\admin\controllers;

use app\models\LevelUp;
use app\models\PopupSearch;
use Yii;
use app\models\Level;
use app\models\LevelSearch;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * PopupController implements the CRUD actions for Popup model.
 */
class PopupController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * Lists all Level models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PopupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Level model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Level model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LevelUp();
        $model->FillBlankImg();

        if ($model->load(Yii::$app->request->post())) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->file) {
                if (!is_dir(Yii::getAlias('@abs_path_to_uploads') . '/levelUp')) {
                    mkdir(Yii::getAlias('@abs_path_to_uploads') . '/levelUp', 0777, true);
                }
                $model->dropOldImg();
                $file_name = uniqid();
                $model->image = $file_name . '.' . $model->file->extension;
                if (!$model->file->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/levelUp/' . $file_name . '.' . $model->file->extension)) {
                    throw new \RuntimeException('Ошибка сохранения файла');
                }
                $model->file = null;
            }
            $model->save();
            return $this->redirect('index');
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Level model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->file) {
                $model->dropOldImg();
                $file_name = uniqid();
                $model->image = $file_name . '.' . $model->file->extension;
                if (!$model->file->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/levelUp/' . $file_name . '.' . $model->file->extension)) {
                    throw new \RuntimeException('Ошибка сохранения файла');
                }
                $model->file = null;
            }
            $model->save();
            return $this->redirect('index');
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Level model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $this->findModel($id)->dropOldImg();
            $this->findModel($id)->delete();
        } catch (yii\db\IntegrityException $e) {
            Yii::$app->session->setFlash('error', 'Уровень содержит контент');
            return $this->redirect(['index']);
        }

        return $this->redirect(['index']);
    }

    public function actionDeleteImg()
    {
        if (Yii::$app->request->post()) {
            $id = Yii::$app->request->post('id');
            $this->findModel($id)->dropOldImg();
        }

        return $this->redirect(Yii::$app->request->referrer);
    }


    /**
     * Finds the Level model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Level the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LevelUp::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
