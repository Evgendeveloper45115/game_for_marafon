<?php

use yii\db\Migration;

class m170929_032205_add_col_quiz extends Migration
{
    public function safeUp()
    {
        $this->addColumn('quiz_question', 'img', $this->string());
    }

    public function safeDown()
    {
        echo "m170929_032205_add_col_quiz cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170929_032205_add_col_quiz cannot be reverted.\n";

        return false;
    }
    */
}
