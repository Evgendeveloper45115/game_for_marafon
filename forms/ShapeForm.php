<?php
namespace app\forms;

use yii\base\Model;

class ShapeForm extends Model
{
    public $shape;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['shape'], 'required'],
            [['shape'], 'integer'],
        ];
    }

}
