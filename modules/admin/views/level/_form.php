<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Level */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="level-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'number_int')->textInput() ?>

    <?= $form->field($model, 'number')->textInput() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= '<label class="control-label">Выбрать цвет</label>' ?>;
    <?= \kartik\color\ColorInput::widget([
        'model' => $model,
        'attribute' => 'color',
    ]) ?>
    <?= $form->field($model, 'color_content')->textInput() ?>

    <?= '<label class="control-label">Тип Уровня</label>' ?>
    <?= $form->field($model, 'type',
        ['template' => '{label}<div class="">{input}{hint} {error}</div>']
    )
        ->radioList(
            $model->types,
            [
                'item' => function ($index, $label, $name, $checked, $value) {
                    return '<label class="btn btn-white btn-info btn-round' . ($checked ? " active" : null) . ' ">' . Html::radio($name, $checked, ['value' => $value, 'autocomplete' => 'off']) . $label . '</label>';
                },
                'tag' => 'div', // this is by default
                'class' => 'btn-group',
                'data-toggle' => 'buttons',
            ]
        )->label(false) ?>
    <?= $form->field($model, 'description', [
        'template' => "<div>{input}\n<div class=\"error\">{error}</div></div>",
    ])->widget(\zxbodya\yii2\tinymce\TinyMce::className(), [
        'options' => ['rows' => 15, 'style' => 'margin-top:5vw'],
        'language' => 'ru',
//        'spellcheckerUrl'=>'http://speller.yandex.net/services/tinyspell',
        'fileManager' => [
            'class' => \zxbodya\yii2\elfinder\TinyMceElFinder::className(),
            'connectorRoute' => 'el-finder/connector',
        ],
    ]); ?>

    <?php if ($model->img) { ?>
        <?= Html::img(Yii::getAlias('@rel_path_to_uploads') . '/' . $model->img, ['class' => 'profile_foto']); ?>
        <div class="del_link">
            <?php
            if ($model->img != 'blank.jpg') {
                echo Html::a('Удалить', ['level/delete-img'], [
                    'data-method' => 'POST',
                    'data-params' => [
                        'id' => $model->id,
                    ],
                ]);
            }
            ?>
        </div>
    <?php } ?>

    <?php
    if (!$model->img) {
        echo Html::img('/uploads/blank.jpg', ['class' => 'profile_foto']);
    }
    ?>

    <?= $form->field($model, 'file')->fileInput(['onchange' => 'readURL(this);']); ?>




    <?php if ($model->icon) { ?>
        <?= Html::img(Yii::getAlias('@rel_path_to_uploads') . '/' . $model->icon, ['class' => 'icon']); ?>
        <div class="del_link">
            <?php
            if ($model->icon != 'icon.png') {
                echo Html::a('Удалить', ['level/delete-icon'], [
                    'data-method' => 'POST',
                    'data-params' => [
                        'id' => $model->id,
                    ],
                ]);
            }
            ?>
        </div>
    <?php } ?>

    <?php
    if (!$model->icon) {
        echo Html::img('/uploads/icon.png', ['class' => 'icon']);
    }
    ?>

    <?= $form->field($model, 'icon_file')->fileInput(['onchange' => 'readURL(this, "icon");']); ?>

    <?= $form->field($model, 'is_active')->checkbox(); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
