<?php

use yii\db\Migration;

class m171103_082150_change_weight extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('review', 'weight');
        $this->addColumn('review', 'weight', $this->string());
    }

    public function safeDown()
    {
        echo "m171103_082150_change_weight cannot be reverted.\n";

        return false;
    }

}
