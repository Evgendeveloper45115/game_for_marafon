<?php

namespace app\controllers;

use app\forms\AvatarForm;
use app\models\Profile;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\BadRequestHttpException;
use yii\web\UploadedFile;
use app\helpers\SimpleImage;
use yii\imagine\Image;

class AvatarController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


    public function beforeAction($action) {
        // var_dump($action->id);die();
        $this->enableCsrfValidation = ($action->id !== "upload-avatar");
        return parent::beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new AvatarForm();

        return $this->render('change-avatar', [
            'model' => $model
        ]);
    }


    public function actionUploadAvatar()
    {
        $user = User::findOne(['id' => Yii::$app->user->getId()]);
        $model = new AvatarForm();

        $chk = Yii::$app->request->post();
        $model->load(Yii::$app->request->post());
        if(!$model->validate()){
            throw new \RuntimeException('Не файл изображений');
        }
        $model->file = UploadedFile::getInstance($model,'file');
        if ($model->file) {
            $file_name = uniqid();
            $user->profile->foto = $file_name . '.' . $model->file->extension;
            if (!$model->file->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/profile/' . $file_name . '.' . $model->file->extension)) {
                throw new \RuntimeException('Ошибка сохранения файла');
            }
        }
        $user->profile->save();

        echo '{}';

        return $this->redirect(['/avatar/index']);

        if($model->file){
            // return $this->redirect(['/avatar/crop']);
        }
    }

    public function actionSaveAvatar()
    {
        $user = User::findOne(['id' => Yii::$app->user->getId()]);
        $model = new AvatarForm();
        $redirect = false;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if($model->file){
                $user->dropOldAvatar();
                $file_name = $user->getId();
                $user->profile->foto = $file_name . '.' . $model->file->extension;
                if (!$model->file->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/profile/' . $file_name . '.' . $model->file->extension)){
                    throw new \RuntimeException('Ошибка сохранения файла');
                }
                $model->file = null;
                $redirect = true;
                $user->profile->save();
            }
        }

        if($redirect){
            return $this->redirect(['/avatar/crop']);
        }

        return $this->redirect(Yii::$app->request->referrer);

    }

    public function actionCrop()
    {
        $user = User::findOne(['id' => Yii::$app->user->getId()]);

        if(!$user->profile->foto){
            Yii::$app->getSession()->setFlash('danger', "Загрузите аватар");
            return $this->redirect('/avatar');
        }

        $img = new SimpleImage();

        $img->load(Yii::getAlias('@abs_path_to_uploads') . '/' . $user->profile->foto);
        $img->resizeToWidth($img->getWidth());

        $img->save(Yii::getAlias('@abs_path_to_uploads') . '/' . $user->profile->foto);

        return $this->render('index', [
            'cropper' => $user,
        ]);

    }


    public function actionCropImage()
    {
        if(Yii::$app->request->post()){
            $x = Yii::$app->request->post('x');
            $y = Yii::$app->request->post('y');
            $width = Yii::$app->request->post('width');
            $height = Yii::$app->request->post('height');

            $profile = Profile::find()
                ->where(['user_id' => Yii::$app->getUser()->id])
                ->one();

            $avatar_file = Yii::getAlias('@avatar') . '/' . $profile->avatar;
            $new_avatar_name = '0' . $profile->foto;

            Image::crop($avatar_file, $width, $height, [$x, $y])->save(Yii::getAlias('@abs_path_to_uploads') . '/' . $new_avatar_name);

            $profile->deleteOldAvatar();
            $profile->avatar = $new_avatar_name;
            $profile->save(false);

            return $this->redirect(['/avatar']);

        }

        else{
            throw new BadRequestHttpException('Доступ ограничен');
        }

    }



}
