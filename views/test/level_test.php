<?php
/**
 * @var $tests \app\models\Test
 * @var $question \app\models\TestQuestionLevel
 */
?>
    <div class="content">
        <?php
        if ($tests) {
            if (!empty($tests->testQuestionLevels)) {
                foreach ($tests->testQuestionLevels as $key => $question) {
                    $answers = [];
                    $answers[] = '<p><input id="an2-' . $key . 1 . '" autocomplete="off" type="radio" name="answer"
                              value="' . $question->test_one . '"><label for="an2-' . $key . 1 . '">
                            <span class="radio-button"></span>' . $question->test_one . '</label></p>';

                    $answers[] = '<p><input id="an2-' . $key . 2 . '" autocomplete="off" type="radio" name="answer"
                              value="' . $question->test_two . '"><label for="an2-' . $key . 2 . '">
                            <span class="radio-button"></span>' . $question->test_two . '</label></p>';

                    if ($question->test_three) {
                        $answers[] = '<p><input id="an2-' . $key . 3 . '" autocomplete="off" type="radio" name="answer"
              value="' . $question->test_three . '"><label for="an2-' . $key . 3 . '">
            <span class="radio-button"></span>' . $question->test_three . '</label></p>';
                    }

                    if ($question->test_four) {
                        $answers[] = '<p><input id="an2-' . $key . 4 . '" autocomplete="off" type="radio" name="answer"
                              value="' . $question->test_four . '"><label for="an2-' . $key . 4 . '">
                            <span class="radio-button"></span>' . $question->test_four . '</label></p>';
                    }
                    if ($question->test_five) {
                        $answers[] = '<p><input id="an2-' . $key . 5 . '" autocomplete="off" type="radio" name="answer"
                              value="' . $question->test_five . '"><label for="an2-' . $key . 5 . '">
                            <span class="radio-button"></span>' . $question->test_five . '</label></p>';
                    }
                    shuffle($answers);
                    ?>
                    <div class="quest_tab">
                        <section class="content-header">
                            <img style="width: 100%" src="/uploads/test_level_Img/<?= $question->img ?>"
                                 alt="picture-top">
                        </section>
                        <section class="test">
                            <h2 style="padding-bottom: 2.5vw"><?= $question->title ?></h2>

                            <form id="test-answer-<?= $key ?>" action="#" method="post" data-id="<?= $question->id ?>">
                                <?php
                                foreach ($answers as $answer) {
                                    echo $answer;
                                }
                                ?>
                            </form>
                        </section>
                    </div>
                    <?php
                }
            }
        }
        ?>

        <section class="content-footer">
            <a class="next" href="#">Следующий вопрос<span class="arrow"></span></a>

            <br>
            <div style="clear: both"></div>
            <div class="progress">
                <p class="pr">2/10</p>
            </div>
            <br>
        </section>

        <div class="progressbars" style="display:none;">
            <div class="progressbars--content">
                <h2><b>Благодарим Вас за участие в тестировании.</b></h2>

                <h3><b>Обработка результатов может занять некоторое время. Предлагаем Вам прочитать статью про переход
                        на
                        правильное питание.</b></h3>

                <p>&nbsp;</p>

                <p style="padding-left: 30px;">Многие при переходе на правильное питание начинают воспринимать его, как
                    систему запретов. &laquo;Белую муку нельзя, сахар и сладкое нельзя, картофель нельзя, жарить на
                    масле
                    нельзя, фрукты вечером нельзя, кока-колу нельзя&raquo;, и так далее. Все зависит от того, каков ваш
                    "стакан" - наполовину пуст или наполовину полон. В действительности, конечно, все это можно, вопрос
                    в
                    каких количествах, и, главное, для чего.&nbsp;<br/>Ради чего эти запреты, ограничения? Если вы
                    можете
                    себе четко ответить на каждое &laquo;нельзя&raquo;, то значит, у вас есть понимание.</p>

                <p style="padding-left: 30px;">А если попробовать сфокусироваться на бонусах, которые дает Правильное
                    питание?<br/>- Вы ощущаете чистый вкус хороших продуктов, не замусоренный сахаром и ароматизаторами.&nbsp;<br/>-
                    Вы начинаете любить монопродукты. Когда я готовила еду раньше, если бы мне кто-то сказал, что я буду
                    любить гречку с солью, я бы покрутила пальцем у виска. Теперь для меня чем проще продукты - тем
                    лучше.<br/>- Отсутствие скачков инсулина не влияет на настроение.<br/>- Кожа становится чище без
                    сладкого и пищевого мусора.<br/>- Качество тела заметно улучшается, уходит целлюлит.&nbsp;<br/>- От
                    регулярного употребления сложных углеводов вы становитесь энергичнее.&nbsp;<br/>- Вы снижаете в разы
                    риски возникновения сердечно-сосудистых заболеваний, диабета 2-го типа.<br/>.... &nbsp;<b>Продолжение
                        статьи читайте на почте.</b></p>

                <p>&nbsp;<b><i>Анна, нутрициолог и фитнес тренер. Основатель программы&nbsp;<a
                                    href="http://beauty-matrix.ru/">Beauty-matrix</a>.</i></b></p>
            </div>
            <div class="progressbars--block">
                <div class="progressbars--name">Пожалуйста, подождите...</div>
                <div id="progressbar">
                    <div class="progress-label">Loading...</div>
                </div>
                <div class="progressbars--info">Идет обработка ваших результатов теста и подготовка рекомендации</div>
            </div>


        </div>

        <section class="form" style="padding-top: 0px;margin-top:0px;">
            <form class="results" id="result-form" action="chek_form" method="post">
                <input id="submit" type="submit" name="submit" value="ПОЛУЧИТЬ РЕЗУЛЬТАТЫ">
            </form>
        </section>
    </div><!--content-->
    <a style="display:none;" class="btn js-venobox vbox-item" id="task_popup" href="#task-complete-plus"
       data-vbtype="inline">task complete 2</a>

    <a style="display:none;" class="btn js-venobox vbox-item" id="no_complete" href="#no_comp"
       data-vbtype="inline">task complete 2</a>
<?= Yii::$app->controller->renderPartial('/passed/_test_level', [
    'points' => $tests->point,
    'link' => '/level/index/2',
    'tests' => $tests,
    'successAnswers' => $successAnswers,
]); ?>
<?= Yii::$app->controller->renderPartial('/passed/_test_level_no_comp', [
    'points' => $tests->point,
    'tests' => $tests,
    'link' => '/level/index/2',
    'successAnswers' => $successAnswers,
]);

