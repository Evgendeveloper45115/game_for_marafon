<?php

namespace app\components;

use app\models\TriggersMail;
use yii\helpers\VarDumper;

const ONE_DAY = 86400;
const THREE_DAYS = 172800;
const SIX_DAYS = 259200;
const TWELVE_HOURS = 129600;
const TWELVE_HOURS_2 = 172800;

/*const ONE_DAY = 86400;
const THREE_DAYS = 259200;
const SIX_DAYS = 518400;
const TWELVE_HOURS = 43200;*/


class TriggerMailer
{

    public static function notVisit1DayChecker($user)
    {

        $triggers_mail = TriggersMail::findOne(['user_id' => $user->id]);
        if ($triggers_mail) {
            if (!$triggers_mail->not_visit1 && ($user->getTimeFromLastActivity() > ONE_DAY)) {
                self::send($user, 'not_visit1', 'Продолжи игру "Построй стройную фигуру!"');
                $triggers_mail->not_visit1 = true;
                $triggers_mail->save();
            }

        }

    }

    public static function notVisit3DayChecker($user)
    {

        $triggers_mail = TriggersMail::findOne(['user_id' => $user->id]);
        if ($triggers_mail) {
            if (!$triggers_mail->not_visit3 && ($user->getTimeFromLastActivity() > THREE_DAYS)) {
                self::send($user, 'not_visit3', 'Этапы построения тела');
                $triggers_mail->not_visit3 = true;
                $triggers_mail->save();
            }
        }

    }

    public static function notVisit6DayChecker($user)
    {

        $triggers_mail = TriggersMail::findOne(['user_id' => $user->id]);
        if ($triggers_mail) {
            if (!$triggers_mail->not_visit6 && ($user->getTimeFromLastActivity() > SIX_DAYS)) {
                self::send($user, 'not_visit6', 'О движении бодипозитив и любви к себе');
                $triggers_mail->not_visit6 = true;
                $triggers_mail->save();
            }

        }
    }

    public static function finished5Level($user)
    {

        $triggers_mail = TriggersMail::find()->where(['user_id' => $user->id])->one();
        if ($triggers_mail) {
            if (!$triggers_mail->finished_5_level) {
                self::send($user, 'finished_5_level', 'Как мотивировать себя построить стройную фигуру?');
                $triggers_mail->finished_5_level = true;
                $triggers_mail->save();
            }

        }
    }
    public static function finished4Level($user)
    {

        $triggers_mail = TriggersMail::find()->where(['user_id' => $user->id])->one();
        if ($triggers_mail) {
            if (!$triggers_mail->finished_4_level) {
                self::send($user, 'finished_4_level', 'С чего начинается похудение?');
                $triggers_mail->finished_4_level = true;
                $triggers_mail->save();
            }

        }
    }
    public static function finished6Level($user)
    {

        $triggers_mail = TriggersMail::find()->where(['user_id' => $user->id])->one();
        if ($triggers_mail) {
            if (!$triggers_mail->finished_6_level) {
                self::send($user, 'finished_6_level', 'Что такое дистанционная групповая программа похудения Beauty Matrix?');
                $triggers_mail->finished_6_level = true;
                $triggers_mail->save();
            }

        }
    }

    public static function passedTest($user)
    {

        $triggers_mail = TriggersMail::findOne(['user_id' => $user->id]);
        if ($triggers_mail) {
            if (!$triggers_mail->passed_test) {
                self::send($user, 'passed_test', 'Поздравляем с успешным прохождением теста!');
                $triggers_mail->passed_test = true;
                $triggers_mail->save();
            }

        }
    }

    public static function notPassedTest($user)
    {

        $triggers_mail = TriggersMail::findOne(['user_id' => $user->id]);
        if ($triggers_mail) {
            if (!$triggers_mail->not_passed_test) {
                self::send($user, 'not_passed_test', 'Попробуйте пройти тест еще раз!');
                $triggers_mail->not_passed_test = true;
                $triggers_mail->save();
            }
        }
    }


    public static function notPassedTestNotBuyChecker($user)
    {

        $triggers_mail = TriggersMail::findOne(['user_id' => $user->id]);
        if ($triggers_mail) {
            if (!$triggers_mail->not_passed_not_buy && $user->isPassedTest() && !$user->profile->bought_programm && ($user->getTimeAfterTest() > TWELVE_HOURS)) {
                self::send($user,   'not_passed_not_buy', 'До конца спецпредложения осталось всего 12 часов!');
                $triggers_mail->not_passed_not_buy = true;
                $triggers_mail->save();
            }
        }
    }

    public static function notPassedTestNotRetestChecker($user)
    {

        $triggers_mail = TriggersMail::findOne(['user_id' => $user->id]);
        if ($triggers_mail) {
            if (!$triggers_mail->not_passed_not_retest && !$user->isPassedTest() && ($user->getTimeAfterTest() > TWELVE_HOURS_2)) {
                self::send($user, 'not_passed_not_retest', 'Участвуйте в дистанционной программе похудения Beauty Matrix уже сейчас!');
                $triggers_mail->not_passed_not_retest = true;
                $triggers_mail->save();
            }
        }
    }

    public static function send($user, $view, $subject)
    {
        $result = \Yii::$app->mail->compose('views/' . $view . '_html',
            ['user' => 1, 'htmlLayout' => 'layouts/trigger_mail_html'])
            ->setTo($user->email)
            ->setSubject($subject)
            ->send();
        return $result;
    }

}