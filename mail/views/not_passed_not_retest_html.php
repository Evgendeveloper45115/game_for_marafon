<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title>Email</title>
    <style type="text/css">


        *{font-family:  Arial, Helvetica, sans-serif; font-size: 15px;/*outline: 1px solid red;*/}

        div, p, span, strong, b, em, i, a, li, td {
            -webkit-text-size-adjust: none;
        }
        img{max-width:100%;text-align: center;}


        @media (min-width: 749px)  {

        }
        @media only screen and (max-device-width: 748px), only screen and (max-width: 748px)  {
            img[class="block"]{display: none !important;}
            body{margin: 0 auto;max-width: 320px;}
            table{width:320px !important;}
            span.size28{font-size: 28px !important;}
            .block{display: block;width:257px !important;margin: 0 auto !important;}
            .margin0{margin: 0 !important;}
            .margin_bl{display: block;margin: 0 auto 28px;}
            .button{width: 295px !important;margin-top: 30px !important;}
            .padding{padding: 6px 0 0 32px !important;}
            .block2{display: table;width: 145px !important;vertical-align: bottom;}
            .block3{width: 140px !important;display: table;}
            .block4{width: 140px !important;display: table;}
            .height0{height: 0 !important;}
            .center{font-size: 14px !important;}
            .width155{width: 147px !important;}
            .widht150{width: 150px !important;}
            .height{height: 0 !important;}
            .float{float: none !important;margin: 0 auto !important;}
            .none{display: none !important;}
            .width100{width: 100% !important;}
            .paddi{padding: 6px 0 0 0 !important;}
        }
    </style>
</head>
<body style="margin: 0 auto;padding: 0;">




<table cellpadding="0" cellspacing="0" width="100%" border="0" style="min-width:300px;max-width: 740px; border-collapse:collapse;" align="center">
    <tr>
        <td align="center">

            <table cellpadding="0" cellspacing="0" width="100%" border="0" style="min-width:300px;max-width: 740px; border-collapse:collapse;background: url(https://preview.ibb.co/eZZcFF/bg_1.jpg) no-repeat top center;background-size: cover;" align="center" bgcolor="#55bd9d">
                <tr>
                    <td align="center">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0" style="border-collapse:collapse;max-width: 500px;" align="center">
                            <tr>
                                <td height="398" align="center" valign="top">
                                    <table align="center" style="max-width: 500px; margin-top: 30px; border-collapse:collapse;" cellspacing="0" cellpadding="0" border="0" width="100%">
                                        <tr>
                                            <td align="center" width="96">
                                                <a href="#" target="_blank">
                                                    <img src="https://image.ibb.co/hk8tpa/logo.png" alt="">
                                                </a>
                                            </td>
                                        </tr>

                                        <tr><td width="1" height="70"></td></tr>

                                        <tr>
                                            <td>
                                                <span style="display: block; font-family: 'Roboto Slab', serif; font-size: 34px;color: #ffffff;text-align: center;" class="size28"><br/>Более 4500 довольных клиентов! </span>
                                            </td>
                                        </tr>

                                        <tr><td width="1" height="40"></td></tr>
                                    </table>


                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </table>

            <table cellpadding="0" cellspacing="0" width="100%" border="0" style="border-collapse:collapse;max-width: 700px;min-width: 300px;" align="center">
                <tr>
                    <td align="center">

                        <table width="100%" align="left" border="0" style="border-collapse:collapse; max-width:700px;min-width: 300px;">
                            <tr>
                                <td>
                                    <table align="left" width="100%" border="0" style="border-collapse:collapse;max-width:700px;">
                                        <tr><td width="1" height="0"></td></tr>
                                        <tr>
                                            <td align="left" width="100%" style="color: #676767;line-height: 25px;font-weight: 300;font-size: 17px;" class="widht300 margin0 center"><br/>Чтобы вас немного мотивировать принять участие в групповой программе Beauty Matrix хотим рассказать вам о двух девушках, которые пройдя программу не просто похудели, а поменяли образ жизни, чему безумно рады! Больше историй преображения читайте по ссылке: </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr><td width="1" height="10"></td></tr>
                        </table>
                    </td>
                </tr>
            </table>






    <tr><td width="1" height="20"></td></tr>

    <tr>
        <td>
            <table width="100%" align="center" border="0" style="border-collapse:collapse;max-width: 500px;min-width: 300px;">

                <tr>
                    <td align="center" valign="top">
                        <a href="http://feedback.beauty-matrix.ru?utm_source=email&utm_medium=game10&utm_campaign=final_return" target="" style="background: #1fe2a6;border-radius: 50px;display: block;width: 330px;height: 37px;margin:0 auto;padding:20px 0 0;text-align: center;font-weight: bold;font-family: 'Roboto Slab', serif;text-transform: uppercase;font-size: 15px;color: #ffffff;text-decoration: none;" class="button">посмотреть истории</a>
                    </td>
                </tr>
                <tr><td width="1" height="20"></td></tr>

            </table>
        </td>
    </tr>




    <table cellpadding="0" cellspacing="0" width="100%" border="0" style="border-collapse:collapse;max-width: 700px;min-width: 300px;" align="center">
        <tr>
            <td align="center">

                <table width="100%" align="left" border="0" style="border-collapse:collapse; max-width:700px;min-width: 300px;">
                    <tr>
                        <td>
                            <table align="left" width="100%" border="0" style="border-collapse:collapse;max-width:700px;">
                                <tr><td width="1" height="0"></td></tr>
                                <tr>
                                    <td align="left" width="100%" style="color: #676767;line-height: 25px;font-weight: 300;font-size: 17px;" class="widht300 margin0 center"><br/>На групповой дистанционной программе Beauty Matrix вы получите индивидуальный рацион, посчитанный в граммах, программу тренировок на основе ваших целей и исходных данных, мотивационные задания, а также сопровождение кураторов и психологическую поддержку на протяжении всех 9 недель.</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr><td width="1" height="10"></td></tr>
                </table>
            </td>
        </tr>
    </table>




    <tr><td width="1" height="20"></td></tr>

    <tr>
        <td>
            <table width="100%" align="center" border="0" style="border-collapse:collapse;max-width: 500px;min-width: 300px;">

                <tr>
                    <td align="center" valign="top">
                        <a href="https://game.beauty-matrix.ru/test/result?param=program&utm_source=email&utm_medium=game10&utm_campaign=final_return" target="" style="background: #1fe2a6;border-radius: 50px;display: block;width: 330px;height: 37px;margin:0 auto;padding:20px 0 0;text-align: center;font-weight: bold;font-family: 'Roboto Slab', serif;text-transform: uppercase;font-size: 15px;color: #ffffff;text-decoration: none;" class="button">Записаться на программу</a>
                    </td>
                </tr>
                <tr><td width="1" height="20"></td></tr>

            </table>
        </td>
    </tr>


    <table width="100%" align="center" border="0" style="border-collapse:collapse;max-width:600px;" cellspacing="0" cellpadding="0">
        <tr><td width="1" height="10"></td></tr>
        <tr>
            <td>
                <span style="font-family: 'Roboto Slab', serif;font-weight: bold;display: block;text-align: center;color: #b460d2;font-size: 22px;">Средняя оценка нашей программы  <br> всеми участниками</span>
            </td>
        </tr>

        <tr><td width="1" height="30"></td></tr>

        <tr>
            <td align="center">
                <img src="https://image.ibb.co/m3zgcv/img_9.png" alt="">
            </td>
        </tr>

        <tr><td width="1" height="30"></td></tr>

        <tr>
            <td>
                <table width="100%" align="center" border="0" style="border-collapse:collapse;max-width:580px;min-width: 300px;">
                    <tr>
                        <td align="center" valign="top">
                            <span style="font-size: 17px;color: #676767;">Если у вас остались вопросы по программе Beauty Matrix вы можете получить консультацию, нажав на кнопку WhatsApp ниже, она мгновенно перенаправит вас в WhatsApp к консультанту.</span><br/><br/>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            <a href="http://whatsapp.beauty-matrix.ru" target="" style="background: #1fe2a6;border-radius: 50px;display: block;width: 330px;height: 37px;margin:0 auto;padding:20px 0 0;text-align: center;font-weight: bold;font-family: 'Roboto Slab', serif;text-transform: uppercase;font-size: 15px;color: #ffffff;text-decoration: none;" class="button">Консультация по Whatsapp</a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>








    <tr><td width="1" height="30"></td></tr>

</table>

<table><tr><td width="1" height="30"></td></tr></table>


<table width="100%" align="center" border="0" style="max-width: 600px; border-collapse:collapse;" cellpadding="0" cellspacing="0">

    <tr>
        <td>
            <table align="center" border="0" style="border-collapse:collapse;max-width: 420px;" width="100%">
                <tr>
                    <td align="left">
                        <span style="font-weight:bold;color:#ebb464;">-</span>
                    </td>

                    <td align="center">
                        <span style="font-weight:bold;font-family: 'Roboto Slab', serif;font-size: 22px;text-transform: uppercase;">результаты наших клиентов</span>
                    </td>

                    <td>
                        <span style="color:#ebb464;font-weight: bold;">-</span>
                    </td>
                </tr>
                <tr><td width="1" height="20"></td></tr>
            </table>

            <table align="center" width="100%" border="0" style="border-collapse:collapse;">
                <tr>
                    <td align="center" class="margin_bl">
                        <img src="https://beauty-matrix.ru/img2/82.jpg" alt="">
                    </td>
                </tr>

                <tr>
                    <td align="center">
                        <p style="font-weight: bold;text-align: center;color: #676767;text-transform: uppercase;font-size: 17px;">Екатерина <span style="color:#b460d2;text-transform: uppercase;font-size: 17px;font-weight: bold;"></span></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr><td width="1" height="40"></td></tr>
</table>


<table align="center" border="0" style="border-collapse:collapse;max-width: 705px;min-width: 300px;" width="100%" bgcolor="#f9f9f9">
    <tr>
        <td>
            <table align="center" border="0" style="border-collapse:collapse;max-width: 660px;" width="100%">
                <tr><td width="1" height="30"></td></tr>
                <tr>
                    <td width="35" valign="top">
                        <img src="http://i66.tinypic.com/346qibp.jpg" alt="">
                    </td>

                    <td>
                        <span style="margin:0 0 22px 0;display:block;font-weight:300;font-style:italic;font-size: 17px;line-height: 25px;color: #676767;" class="center">Я всегда до рождения детей была стройной, после первых двух родов практически не поправилась. А вот с младшей дочкой в беременность я сильно поправилась - как никогда. И после родов вес не ушёл, и после года кормления. Мне не хотелось ничего покупать себе, никуда выходить из дома. Ни встречаться со знакомыми. Особенно с теми, кто помнил меня только стройной. Фотографироваться не хотела - на всех фото я себе очень не нравилась. Я успокаивала себя тем, что я мать троих детей, что уже не девочка. Ещё часто читала о том, что на ГВ невозможно похудеть. </span>


                        <span style="margin:0 0 22px 0;display:block;font-weight:300;font-style:italic;font-size: 17px;line-height: 25px;color: #676767;text-indent: 30px;" class="center"> Потом случайно нашла <a href="https://www.instagram.com/beatrix_fit/">Анину страничку</a>. Сначала просто читала, не веря, что это меня коснётся :) потом потихоньку уговорила мужа купить у Ани программу.
                Я стала бегать по утрам и заниматься с гантелями пока дочка спит днём.
                Вкус очистился, я стала чувствовать вкус продуктов, пропала тяга к сладкому и мучному. Понравилось питаться простой пищей. Никогда раньше я и подумать не могла, что мне будет казаться очень вкусной простая несладкая перловка, ячка или овсянка на воде! </span>


                        <span style="margin:0 0 22px 0;display:block;font-weight:300;font-style:italic;font-size: 17px;line-height: 25px;color: #676767;text-indent: 30px;" class="center"> Я не могу даже передать словами, как изменялось моё внутреннее состояние- это как небо и земля! Я стала уверенной в себе, мне хочется наряжаться , мне все идёт! Хочется встречаться с друзьями, фотографироваться. Я чувствую себя помолодевшей , бодрой, энергичной. И даже муж, который был скептически настроен сначала, был удивлён моим упорством и силой воли. В итоге он стал питаться вместе со мной правильно и тренироваться. Это время мне удалось найти и с тремя маленькими детьми, которые всегда дома со мной! </span>


                        <span style="margin:0 0 22px 0;display:block;font-weight:300;font-style:italic;font-size: 17px;line-height: 25px;color: #676767;text-indent: 30px;" class="center"> После похудения останавливаться в самосовершенствовании мне не захотелось, планка требований к себе растёт) Сейчас я на наборе, + 2.5 кг. + 2 см ягодицы. Продолжаю работать</span>
                    </td>

                    <td width="27" valign="bottom">
                        <img src="http://i66.tinypic.com/15i6qme.jpg" alt="">
                    </td>
                </tr>

            </table>

            <table align="center" cellspacing="0" cellpadding="0" border="0" width="100%" style="max-width: 300px">
                <tr>
                    <td align="center" width="300" style="max-width: 300px;">
                        <p style="color: #b460d2;font-size: 17px;font-weight: 600;line-height: 7px;">Вес: <span style="color: #676767;font-size: 17px;">- 15 кг</span></p>
                        <p style="color: #b460d2;font-size: 17px;font-weight: 600;line-height: 7px;">Грудь: <span style="color: #676767;font-size: 17px;">- 13 см</span></p>
                        <p style="color: #b460d2;font-size: 17px;font-weight: 600;line-height: 7px;">Талия: <span style="color: #676767;font-size: 17px;">- 13 см</span></p>
                        <p style="color: #b460d2;font-size: 17px;font-weight: 600;line-height: 7px;">Бедра: <span style="color: #676767;font-size: 17px;"> - 11 см</span></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr><td width="1" height="30"></td></tr>
</table>

<table><tr><td width="1" height="50"></td></tr></table>




<table width="100%" align="center" border="0" style="border-collapse:collapse;max-width: 600px;" cellpadding="0" cellspacing="0">
    <tr><td width="1" height="50"></td></tr>
    <tr>
        <td>
            <table align="center" width="100%" border="0" style="border-collapse:collapse;">
                <tr>
                    <td align="center" class="margin_bl">
                        <img src="https://beauty-matrix.ru/img2/51.jpg" alt="">
                    </td>
                </tr>

                <tr>
                    <td align="center">
                        <p style="font-weight: bold;text-align: center;color: #676767;text-transform: uppercase;font-size: 17px;">Екатерина<span style="color:#b460d2;text-transform: uppercase;font-size: 17px;font-weight: bold;"></span></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr><td width="1" height="20"></td></tr>
</table>


<table align="center" border="0" style="border-collapse:collapse;max-width: 750px;" width="100%" bgcolor="#f9f9f9">
    <tr>
        <td>
            <table align="center" border="0" style="border-collapse:collapse;max-width: 660px;" width="100%">
                <tr><td width="1" height="30"></td></tr>
                <tr>
                    <td width="35" valign="top">
                        <img src="http://i66.tinypic.com/346qibp.jpg" alt="">
                    </td>

                    <td>
                        <span style="margin:0 0 22px 0;display:block;font-weight:300;font-style:italic;font-size: 17px;line-height: 25px;color: #676767;" class="center">Изначально я, как и многие участницы, искала себя, пыталась бороться с лишним весом. Но, как выяснилось в ходе программы, чистить нужно не только свой рацион, но и свои привычки, мысли, чувства. Придя к Анне на групповую программу, я , честно говоря, не очень рассчитывала на результат. Но время шло, а я все больше и больше узнавала себя и замечала изменения.</span>


                        <span style="margin:0 0 22px 0;display:block;font-weight:300;font-style:italic;font-size: 17px;line-height: 25px;color: #676767;text-indent: 30px;" class="center"> Изменения не только в зеркале, но и в голове‼ Я узнала о себе много нового, оказывается я более волевой и целеустремленный человек, чем раньше думала. Все время программы я боролась, но по большей части не с лишним весом , а со своими привычками, со своими тараканами в голове, со своей ленью. Похудеть не сложно, сложно измениться внутренне, сложно заставить себя бегать в плохую погоду, не заедать стресс и плохое настроение, сложно себя постоянно пинать. Но оно того стоит! Программа завершилась с результатом - 8 кг! </span>


                        <span style="margin:0 0 22px 0;display:block;font-weight:300;font-style:italic;font-size: 17px;line-height: 25px;color: #676767;text-indent: 30px;" class="center"> На данный момент спустя 5 месяцев работы над собой я имею следующие результаты:
вес -12 кг, ОГ -6 см, ОТ -11 см, ОБ -11 см! Ощущения непередаваемые!! А по окончанию программы следовало ещё более важное событие в жизни - предложение стать частью команды Beauty Matrix!! Это не просто работа, это стиль жизни, мировоззрение, мой путь. Помогать людям менять себя - что может быть прекраснее?! И конечно не маловажный момент - это ежедневное самосовершенствование, так как в нашей профессии быть "сапожником без сапог" недопустимо!</span>

                    </td>

                    <td width="27" valign="bottom">
                        <img src="http://i66.tinypic.com/15i6qme.jpg" alt="">
                    </td>
                </tr>

            </table>
        </td>
    </tr>
    <tr><td width="1" height="30"></td></tr>
</table>





<table width="100%" align="center" border="0" style="max-width:640px;" cellspacing="0" cellpadding="0">
    <tr>
        <td width="1" height="50"></td>
    </tr>
    <tr>
        <td align="center">
            <span style="color: #3e3e3e;font-size: 17px;">С наилучшими пожеланиями,</span> <br><b style="color:#3e3e3e;font-size:17px;">команда Beauty Matrix</b>
        </td>
    </tr>
    <tr>
        <td width="1" height="50"></td>
    </tr>
</table>
<table width="100%" align="center" border="0" style="background:#232323;max-width:740px;" cellspacing="0" cellpadding="0">
    <tr>
        <td>

            <table width="100%" align="center" style="border-collapse:collapse;max-width:740px;" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="1" height="40"></td>
                </tr>
                <tr>
                    <td align="center">
                        <a href="#" target="_blank" style="text-decoration:none;">
                            <img src="https://image.ibb.co/goY1cv/logo_footer.png" alt=""></a>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <span style="color: #676767;">Все права защищены</span>
                    </td>
                </tr>
                <tr>
                    <td width="1" height="10"></td>
                </tr>
            </table>

        </td>
    </tr>
</table></body>
</html>