<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "task_answers".
 *
 * @property integer $id
 * @property integer $article_id
 * @property integer $user_id
 * @property string $photo_file
 * @property string $text
 */
class TaskAnswers extends \yii\db\ActiveRecord
{
    public $photo_file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task_answers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['article_id', 'user_id'], 'required'],
            [['article_id', 'user_id'], 'integer'],
            [['text', 'foto', 'text'], 'string'],
            [['photo_file'], 'file'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'article_id' => 'Article ID',
            'user_id' => 'User ID',
            'photo_file' => 'Foto',
            'text' => 'Text',
            'photo_file' => 'Файл с изображением',
        ];
    }
}
