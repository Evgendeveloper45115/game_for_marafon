<?php
/**
 * @var $levelUp \app\models\LevelUp
 */
?>

    <a style="display:none;" class="btn js-venobox vbox-item" id="time_popup" href="#level-up"
       data-vbtype="inline">task complete 2</a>
    <div class="js-popup" id="level-up">
        <div class="form__sales-bg form__sales-bg2">
            <div class="form__sales-name">Вы уже продвинулись довольно далеко <br> в нашей игре и у нас есть для вас награда!</div>
            <div class="form__sales-text">Вы можете получить бесплатную консультацию нутрициолога на тему правильного питания <br> На консультации вы разберете ошибки вашего питания, а также получите рекомендации по его улучшению </div>

            <div class="hide_form_theme_form hide_form_theme_form2 fr_1 " style="opacity: 1; display: block;">
                <input class="first ccs_trans" type="text" name="name" placeholder="Имя">
                <input class="ccs_trans" type="text" name="ml" placeholder="whatsapp" id="email11">
                <button class="ccs_trans" id="watsapp" data-index="b_form_3">отправить</button>

            </div>
            <p class="agree-text">Нажимая кнопку "отправить", вы даете своё согласие на обработку персональных данных</p>
        </div>
    </div>
<?php
$js = <<<JS
        $(function(){
                $('#time_popup').trigger('click');
                
            window.setTimeout(function(){
            var venoOptions = {
                // is called after plugin initialization.
                cb_init: function(plugin){
                    $('.vbox-close').css('left', '-1000px');
                },
            }
            $('.venobox').venobox(venoOptions);
        }, 200);
                
    })
JS;
$this->registerJs($js);
?>