<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title = 'Игра: «Построй стройную фигуру» | Beauty-Matrix';
?>


<header class="top">
    <a class="top__logo" href="/site/signup">
        <img src="../landing/images/logo.png" alt="">
    </a>
</header>
<div class="game">
    <div class="game-top">
        <h1 class="title game__title">Игра: построй стройную фигуру!</h1>
        <p class="game__text">Получай баллы и выигрывай крутые призы! а самое главное фигуру своей мечты.</p>
    </div>
    <div class="game-items clearfix">
        <div class="game-item game-item--active">
            <div class="game-item__round game-item__round--active">
                <img src="../landing/images/game1.png"><a class="btn btn--activegame" href="/site/signup">Начать</a>
            </div>
            <p class="game-item__name">1 уровень <span>Борец с сахаром</span>
            </p>
        </div>
        <div class="game-item">
            <div class="game-item__round game-item__round--locked">
                <img src="../landing/images/game2.png"><a class="btn btn--game" href="/site/signup">Начать</a>
            </div>
            <p class="game-item__name">2 уровень <span>Разрушитель мифов</span>
            </p>
        </div>
        <div class="game-item">
            <div class="game-item__round game-item__round--locked">
                <img src="../landing/images/game3.png"><a class="btn btn--game" href="/site/signup">Начать</a>
            </div>
            <p class="game-item__name">3 уровень <span>Подсознание и питание</span>
            </p>
        </div>
    </div>
</div>
<div class="join">
    <h3 class="join__text">Присоединятесь к бесплатной игре</h3>
    <h2 class="join__game">«Построй стройную фигуру»</h2>
    <?php $form = ActiveForm::begin(['action' => '/site/signup',
        'options' => [
            'class' => 'join-form'
        ]
    ]); ?>

    <?= $form->field($model, 'name', ['template' => '{input}'])->textInput(['class' => 'textbox textbox--join', 'placeholder' => 'Имя'])->label(false); ?>
    <?= $form->field($model, 'email', ['template' => '{input}'])->textInput(['class' => 'textbox textbox--join', 'placeholder' => 'E-mail'])->label(false); ?>
    <?= $form->field($model, 'password', ['template' => '{input}'])->passwordInput(['class' => 'textbox textbox--join', 'placeholder' => 'Пароль'])->label(false); ?>

    <?= Html::submitButton('Присоединиться', ['class' => 'btn btn--join', 'name' => 'signup-button']) ?>

    <?php ActiveForm::end(); ?>
</div>