<?php
namespace app\forms;

use yii\base\Model;
use app\models\User;
use app\models\Profile;
use yii\db\Exception;

class PhotoForm extends Model
{
    public $file;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file'], 'file', 'extensions' => 'gif, jpg, jpeg, png'],
        ];
    }

}
