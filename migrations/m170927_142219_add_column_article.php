<?php

use yii\db\Migration;

class m170927_142219_add_column_article extends Migration
{
    public function safeUp()
    {
        $this->addColumn('article', 'preview_image', $this->string());
        $this->addColumn('article', 'preview_text', $this->text());
    }

    public function safeDown()
    {
        echo "m170927_142219_add_column_article cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170927_142219_add_column_article cannot be reverted.\n";

        return false;
    }
    */
}
