<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'first_name',
                'label' => 'Имя',
                'filter' => Html::textInput('UserSearch[first_name]'),
                'format' => ['raw'],
                'value' => function ($data) {
                    return Html::a($data->profile->first_name, \yii\helpers\Url::to(['/admin/user/profile', 'id' => $data->id]));
                }
            ],
            'email:email',
            [
                'attribute' => 'created_at',
                'label' => 'Дата Регистрация',
                'value' => function ($data) {
                    return Yii::$app->formatter->asDate($data->created_at);
                },
            ],
            [
                'attribute' => 'last_activity',
                'format' => ['html'],
                'label' => 'Последний вход',
                'value' => function ($data) {
                    return Yii::$app->formatter->asDatetime($data->profile->last_activity);
                },
            ],

            [
                'attribute' => 'bought_programm',
                'label' => 'Купил программу',
                'format' => ['html'],
                'value' => function ($data) {
                    return Yii::$app->formatter->asBoolean($data->profile->bought_programm);
                }
            ],
            [
                'attribute' => 'account',
                'label' => 'Баллов',
                'value' => 'profile.account',
            ],
            [
                'attribute' => 'time_on_site',
                'format' => ['date', 'php:h:m:s'],
                'label' => 'Время проведённое на сайте',
                'value' => 'profile.time_on_site',
            ],
            [
                'attribute' => 'level',
                'label' => 'Уровень',
                'value' => 'profile.level',
                'filter' => Html::dropDownList('UserSearch[level]', $searchModel->level, ['' => ''] + [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7])
            ],
            [
                'attribute' => 'passed_test',
                'label' => 'Финальное тестирование',
                'value' => function ($data) {
                    return Yii::$app->formatter->asBoolean($data->profile->passed_test);
                },
            ],
            [
                'attribute' => 'social_media_id',
                'label' => 'Количество переходов',
                'value' => function ($data) {
                    return $data->social_media_id;
                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}&nbsp;{delete}'
            ],
        ],
    ]); ?>
</div>
