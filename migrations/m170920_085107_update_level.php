<?php

use yii\db\Migration;

class m170920_085107_update_level extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('level', 'number');
        $this->addColumn('level', 'number', $this->text());
        $this->addColumn('level', 'color', $this->string());
        $this->addColumn('level', 'color_content', $this->text());
        $this->addColumn('level', 'type', $this->integer());
    }

    public function safeDown()
    {
        $this->addColumn('level', 'number', $this->text());
        $this->dropColumn('level', 'number');
        $this->dropColumn('level', 'color');
        $this->dropColumn('level', 'color_content');
        $this->dropColumn('level', 'type');
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170920_085107_update_level cannot be reverted.\n";

        return false;
    }
    */
}
