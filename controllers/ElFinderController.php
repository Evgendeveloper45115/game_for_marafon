<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\VarDumper;
use zxbodya\yii2\elfinder\ConnectorAction;
use app\models\Article;


class ElFinderController extends Controller
{

    public function actions()
    {
        return [
            'connector' => array(
                'class' => ConnectorAction::className(),
                'settings' => array(
                    'root' => Yii::getAlias('@webroot') . '/uploads/',
                    'URL' => Yii::getAlias('@web') . '/uploads/',
                    'rootAlias' => 'Home',
                    'mimeDetect' => 'none'
                )
            ),
        ];
    }

    public function actionTest(){

        $model = new Article();

        return $this->render('index', [
            'model' => $model,
        ]);
    }

}
