<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "level".
 *
 * @property integer $id
 * @property integer $number
 * @property integer $type
 * @property integer $hide
 * @property string $name
 * @property string $img
 * @property string $icon
 * @property string $color
 * @property string $color_content
 * @property Article[] $articles
 * @property Video[] $videos
 */
class Level extends \yii\db\ActiveRecord
{

    public $file;
    public $icon_file;
    public $types = [
        'Блочный',
        'Линейный'
    ];
    const BLOCK = 0;
    const LINE = 1;

    public function FillBlankImg(){
        if (!$this->img){
            $this->img = "blank.jpg";
        }
    }

    public function FillBlankIcon(){
        if (!$this->icon){
            $this->icon = "icon.png";
        }
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'level';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number', 'number_int', 'name', 'img'], 'required'],
            [['name', 'img', 'icon'], 'string', 'max' => 255],
            [['number_int'], 'integer', 'max' => 99999],
            [['is_active'], 'boolean'],
            [['description','type','color','color_content'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number' => 'Название',
            'number_int' => 'Порядковый номер уровня',
            'name' => 'Заголовок',
            'img' => 'Изображение',
            'icon_file' => 'Иконка уровня',
            'file' => 'Файл изображения',
            'description' => 'Контент',
            'type' => 'Тип Уровня',
            'color' => 'Цвет рамки',
            'color_content' => 'Контент в цветной рамке',
            'is_active' => 'Активный уровень',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasMany(Article::className(), ['level_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVideos()
    {
        return $this->hasMany(Video::className(), ['level_id' => 'id']);
    }

    public function dropOldImg()
    {
        if($this->img){
            $path_to_file = Yii::getAlias('@abs_path_to_uploads') . '/' . $this->img;

            if (file_exists($path_to_file) && $this->img != 'blank.jpg')
            {
                unlink($path_to_file);
                $this->img = 'blank.jpg';
                $this->save(false);
            }
        }
    }

    public function dropOldIcon()
    {
        if($this->icon){
            $path_to_file = Yii::getAlias('@abs_path_to_uploads') . '/' . $this->icon;

            if (file_exists($path_to_file) && $this->icon != 'icon.png')
            {
                unlink($path_to_file);
                $this->icon = 'icon.png';
                $this->save(false);
            }
        }
    }

    public static function getValuesArray()
    {
        // Выбираем только те категории, у которых есть дочерние категории
        $levels = self::find()
            ->select('name')
            ->all();

        return ArrayHelper::map($levels, 'name', 'name');
    }

}
