
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Email</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,700&amp;subset=cyrillic" rel="stylesheet">

    <style type="text/css">
        *{
            font-family:Arial,Helvetica,sans-serif;
            font-size:15px;
        }
        div,p,span,strong,b,em,i,a,li,td{
            -webkit-text-size-adjust:none;
        }
        img{
            max-width:100%;
            text-align:center;
        }
        @media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            body{
                margin:0 auto;
                max-width:320px;
            }

        }	@media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            table{
                width:320px !important;
            }

        }	@media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            span.size28{
                font-size:28px !important;
            }

        }	@media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            .block{
                display:block;
                width:257px !important;
                margin:0 auto !important;
            }

        }	@media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            .margin0{
                margin:0 !important;
            }

        }	@media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            .margin_bl{
                display:block;
                margin:0 auto 28px;
            }

        }	@media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            .button{
                width:295px !important;
                margin-top:30px !important;
            }

        }	@media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            .padding{
                padding:6px 0 0 32px !important;
            }

        }	@media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            .block2{
                display:table;
                width:145px !important;
                vertical-align:bottom;
            }

        }	@media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            .block3{
                width:140px !important;
                display:table;
            }

        }	@media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            .block4{
                width:140px !important;
                display:table;
            }

        }	@media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            .height0{
                height:0 !important;
            }

        }	@media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            .center{
                font-size:14px !important;
            }

        }	@media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            .width155{
                width:147px !important;
            }

        }	@media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            .widht150{
                width:150px !important;
            }

        }	@media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            .height{
                height:0 !important;
            }

        }	@media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            .float{
                float:none !important;
                margin:0 auto !important;
            }

        }	@media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            .none{
                display:none !important;
            }

        }	@media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            .width100{
                width:100% !important;
            }

        }	@media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            .paddi{
                padding:6px 0 0 !important;
            }

        }</style></head>
<body style="margin: 0 auto;padding: 0;">



<table cellpadding="0" cellspacing="0" width="100%" border="0" style="min-width:300px;max-width:740px;border-collapse:collapse;" align="center">
    <tr>
        <td align="center">

            <table cellpadding="0" cellspacing="0" width="100%" border="0" style="min-width:300px;max-width:740px;border-collapse:collapse;background:url('https://beauty-matrix.ru/img2/bg2.jpg') no-repeat top;background-size:cover;" align="center" bgcolor="#55bd9d">
                <tr>
                    <td align="center">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0" style="border-collapse:collapse;max-width:700px;" align="center">
                            <tr>
                                <td height="250" align="center" valign="top">
                                    <table align="center" style="max-width:700px;margin-top:30px;border-collapse:collapse;" cellspacing="0" cellpadding="0" border="0" width="100%">
                                        <tr>
                                            <td align="center" width="96">
                                                <a href="#" target="_blank">
                                                    <img src="https://image.ibb.co/cUZUvF/logo.png" alt=""></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span style="margin-top: 20px; display: block; font-family: 'Roboto Slab', serif; font-size: 34px;color:#ffffff;text-align: center;line-height: 35px;" class="size28">Продолжи игру: <br/>"Построй стройную фигуру!"</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="1" height="80"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" width="100%" border="0" style="border-collapse:collapse;max-width:600px;" align="center">
                <tr>
                    <td width="1" height="50"></td>
                </tr>
                <tr>
                    <td align="center">
                        <span style="display:block;color: #2a2a2a;font-size: 22px;font-family: 'Roboto Slab', serif;font-weight: bold;">Этапы построения тела</span><br/>
                    </td>
                </tr>
                <tr>
                    <td>
                  <span style="font-size: 17px;color: #676767;">Всю жизнь нам окружающие говорили, что выше головы не прыгнешь, от осинки не родятся апельсинки, что с какими данными мы родились, с такими и жить. Но теперь вы узнаете, что это не так! Более того, можно построить себе то тело, которое хочется, инструменты - питание и тренировки! О том, какие бывают этапы построения тела, как грамотно по ним пройти - смотрите в видео "Этапы построения тела"

               </span>
                    </td>
                </tr>
                <tr>
                    <td style="padding:20px 0;">
                        <a href="http://www.youtube.com/watch?v=x-VPO7zh-Lc" style="position:relative;min-height:40px;display:block;">
                            <span style="position:absolute;top:10px;left:65px;">Смотреть видео на youtube</span>
                            <img src="https://beauty-matrix.ru/img/youtube_image9.jpg" style="position:relative;display:block;width:100%;height:auto;">
                        </a>
                    </td>
                </tr>
                <tr>
                    <td width="1" height="40"></td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" align="center" border="0" style="border-collapse:collapse;max-width:500px;min-width:300px;background:url('http://i66.tinypic.com/ff2yjr.jpg') no-repeat;">
                            <tr>
                                <td align="center" valign="top">
                                    <a href="https://game.beauty-matrix.ru/?utm_source=email&utm_medium=game2&utm_campaign=vozvrat48" target="" style="background:#1fe2a6;border-radius:50px;display:block;max-width:330px;height:37px;margin:0 auto;padding:20px 0 0;text-align:center;font-weight:bold;font-family:'Roboto Slab', serif;text-transform:uppercase;font-size:15px;color:#ffffff;text-decoration:none;" class="button">Продолжить играть</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="1" height="40"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table width="100%" align="center" border="0" style="border-collapse:collapse;max-width:750px;min-width:300px;" bgcolor="#f9f9f9">
    <tr>
        <td width="1" height="20"></td>
    </tr>
    <tr>
        <td>
            <table cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse;max-width:640px;" width="100%" align="center">
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse;max-width:390px;display:inline-block;" width="100%" align="center">
                            <tr>
                                <td align="center">
                                    <p style="padding:0 0 0 5px;font-size:17px;color:#676767;line-height:25px;">
                                        Узнать больше о правильном питании, тренировках, мотивации и психологии, чтобы похудеть и построить стройную фигуру:
                                    </p>
                                    <p>
                                        <a href="https://game.beauty-matrix.ru/?utm_source=email&utm_medium=game2&utm_campaign=vozvrat48" target="" style="background:#1fe2a6;border-radius:50px;display:block;max-width:330px;height:37px;padding:20px 0 0;text-align:center;font-weight:bold;font-family:'Roboto Slab', serif;text-transform:uppercase;font-size:15px;color:#ffffff;text-decoration:none;" class="button">Вернуться в игру</a>
                                    </p>
                                </td>
                            </tr>
                        </table>
                        <table cellspacing="0" cellpadding="0" border="0" style="margin:15px 0 0 80px;border-collapse:collapse;max-width:160px;display:inline-block;" width="100%">
                            <tr>
                                <td>
                                    <img src="https://beauty-matrix.ru/img2/2.png" alt="">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td width="1" height="20"></td>
    </tr>
</table>
<table width="100%" align="center" border="0" style="max-width:640px;" cellspacing="0" cellpadding="0">
    <tr>
        <td width="1" height="50"></td>
    </tr>
    <tr>
        <td align="center">
            <span style="color: #3e3e3e;font-size: 17px;">С наилучшими пожеланиями,</span> <br><b style="color:#3e3e3e;font-size:17px;">команда Beauty Matrix</b>
        </td>
    </tr>
    <tr>
        <td width="1" height="50"></td>
    </tr>
</table>
<table width="100%" align="center" border="0" style="background:#232323;max-width:740px;" cellspacing="0" cellpadding="0">
    <tr>
        <td>

            <table width="100%" align="center" style="border-collapse:collapse;max-width:740px;" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="1" height="40"></td>
                </tr>
                <tr>
                    <td align="center">
                        <a href="#" target="_blank" style="text-decoration:none;">
                            <img src="https://image.ibb.co/goY1cv/logo_footer.png" alt=""></a>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <span style="color: #676767;">Все права защищены</span>
                    </td>
                </tr>
                <tr>
                    <td width="1" height="10"></td>
                </tr>
            </table>

        </td>
    </tr>
</table>
</body>
</html>