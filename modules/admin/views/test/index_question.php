<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Вопросы к тестированию';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="test-question-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить тест', ['add-test-questions'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //  ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute' => 'test_id',
                'content' => function ($data) {
                    return $data->test ? $data->test->name : null;
                },
            ],
            'title',
            'test_one',
            'test_two',
            'test_three',
            'test_four',
            'test_five',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}', // the default buttons + your custom button
                'buttons' => [
                    'update' => function ($url, $model, $key) {     // render your custom button
                        return Html::a('', \yii\helpers\Url::to(['edit-test-questions', 'id' => $model->id]), ['class' => 'glyphicon glyphicon-pencil']);
                    },
                    'delete' => function ($url, $model, $key) {     // render your custom button
                        return Html::a('', \yii\helpers\Url::to(['delete-test-questions', 'id' => $model->id]), ['class' => 'glyphicon glyphicon-trash']);
                    }
                ]
            ]
        ],
    ]); ?>
</div>
