<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'Смена пароля';
$active_menu = 'change_password';
?>
<div class="page_content">
    <div class="page_trainings page_with_menu page_red clearfix">
        <div class="page_content">
            <h1>Смена пароля</h1>
            <?php if (!empty($success)) { ?>
                <h3>Пароль сменился успешно</h3>
            <?php } else { ?>

                <div class="password_change_wrapper sign_in_wrap">
                    <?php
                    $form = ActiveForm::begin([
                        'enableClientValidation' => true,
                        'validateOnChange' => true,
                        'validateOnType' => true,
                        'fieldConfig' => [
                            'options' => ['class' => 'form_group clearfix'],
                            'template' => "{label}\n<div class=\"col_right\">{input}\n<div class=\"error\">{error}</div></div>",
                        ],
                    ]) ?>
                    <?= $form->field($user, 'orig_password')->textInput(['class' => 'sing_input', 'placeholder' => 'Старый пароль'])->label(false) ?>
                    <?= $form->field($user, 'new_password')->textInput(['class' => 'sing_input', 'placeholder' => 'Пароль'])->label(false); ?>
                    <?= $form->field($user, 'password1')->textInput(['class' => 'sing_input', 'placeholder' => 'Повтор пароля'])->label(false); ?>
                    <div class="form_group clearfix">
                        <label>&nbsp;</label>

                        <div class="col_right">
                            <?= Html::submitButton('Сменить пароль') ?>
                        </div>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
