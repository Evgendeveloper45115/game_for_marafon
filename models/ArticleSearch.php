<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Article;

/**
 * ArticleSearch represents the model behind the search form about `app\models\Article`.
 */
class ArticleSearch extends Article
{
    public $level_name;
    public $theme_name;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'level_id', 'timer', 'points'], 'integer'],
            [['name', 'description', 'img', 'level_name', 'theme_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Article::find()
            ->joinWith('level')
            ->joinWith('theme');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['level_name'] = [
            'asc' => ['level.name' => SORT_ASC],
            'desc' => ['level.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['theme_name'] = [
            'asc' => ['theme.name' => SORT_ASC],
            'desc' => ['theme.name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'level_id' => $this->level_id,
            'timer' => $this->timer,
            'points' => $this->points,
            'level_name' => $this->level->name,
            'theme_name' => $this->theme->name,
        ]);

        $query->andFilterWhere(['like', 'article.name', $this->name])
            ->andFilterWhere(['like', 'article.description', $this->description])
            ->andFilterWhere(['like', 'theme.name', $this->theme_name])
            ->andFilterWhere(['like', 'level.name', $this->level_name])
            ->andFilterWhere(['like', 'article.img', $this->img]);

        return $dataProvider;
    }
}
