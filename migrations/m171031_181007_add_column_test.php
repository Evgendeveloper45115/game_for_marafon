<?php

use yii\db\Migration;

class m171031_181007_add_column_test extends Migration
{
    public function safeUp()
    {
        $this->addColumn('test', 'success_text', $this->text());
        $this->addColumn('test', 'fail_text', $this->text());
    }

    public function safeDown()
    {
        echo "m171031_181007_add_column_test cannot be reverted.\n";

        return false;
    }
}
