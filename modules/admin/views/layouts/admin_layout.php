<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AdminAsset;
use app\components\Alert;

AdminAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<?php
$main = \app\models\MainPage::findOne(1);
?>
<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Админ-панель',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Главная', 'url' => [\yii\helpers\Url::to([$main ? ('/admin/main/update?id=1') : '/admin/main/create'])]],
            ['label' => 'Уровни', 'url' => ['/admin/level']],
            ['label' => 'Темы', 'url' => ['/admin/theme']],
            ['label' => 'Статьи', 'url' => ['/admin/article']],
            ['label' => 'Видео', 'url' => ['/admin/video']],
            ['label' => 'Тесты', 'url' => ['/admin/test']],
            ['label' => 'Финальное тестирование', 'url' => ['/admin/final-test']],
            ['label' => 'Попапы', 'url' => ['/admin/popup']],
            ['label' => 'Настройки', 'items' => [
                ['label' => 'Телосложения', 'url' => '/admin/constitution'],
                ['label' => 'Типы фигуры', 'url' => '/admin/shape'],
            ],],
            ['label' => 'Клиенты', 'items' => [
                ['label' => 'Пользователи', 'url' => ['/admin/user']],
                ['label' => 'Задания', 'url' => ['/admin/task-answer']],
                ['label' => 'Отзывы', 'url' => ['/admin/review']],
            ],],
            ['label' => 'Выход', 'url' => ['/site/logout']],


        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <div class="row">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </div>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
