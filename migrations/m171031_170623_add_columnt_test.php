<?php

use yii\db\Migration;

class m171031_170623_add_columnt_test extends Migration
{
    public function safeUp()
    {
        $this->addColumn('test', 'percent', $this->string());
        $this->dropColumn('test_question_level', 'percent');
    }

    public function safeDown()
    {
        echo "m171031_170623_add_columnt_test cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171031_170623_add_columnt_test cannot be reverted.\n";

        return false;
    }
    */
}
