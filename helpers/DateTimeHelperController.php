<?php
/**
 * Created by PhpStorm.
 * User: egor
 * Date: 18.05.2016
 * Time: 11:45
 */

namespace app\helpers;


class DateTimeHelperController
{
    public static function getUTCNow()
    {
        $tz = 'UTC';
        $dt = new \DateTime("now", new \DateTimeZone($tz));
        return $dt->format('Y-m-d H:i:s');
    }
}