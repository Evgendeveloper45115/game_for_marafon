<?php

use yii\db\Migration;

class m171031_063035_add_hide_level extends Migration
{
    public function safeUp()
    {
        $this->addColumn('level', 'is_active', $this->boolean());
    }

    public function safeDown()
    {
        $this->dropColumn('level', 'is_active');
    }

}
