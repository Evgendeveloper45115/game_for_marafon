<?php

use yii\db\Migration;

class m170929_031013_add_col_us extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user', 'testAnswer', $this->integer());
    }

    public function safeDown()
    {
        echo "m170929_031013_add_col_us cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170929_031013_add_col_us cannot be reverted.\n";

        return false;
    }
    */
}
