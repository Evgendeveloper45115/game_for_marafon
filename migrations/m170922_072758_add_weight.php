<?php

use yii\db\Migration;

class m170922_072758_add_weight extends Migration
{
    public function safeUp()
    {
        $this->addColumn('article', 'weight', $this->integer()->defaultValue(5));
        $this->addColumn('video', 'weight', $this->integer()->defaultValue(5));
    }

    public function safeDown()
    {
        $this->dropColumn('article', 'weight');
        $this->dropColumn('video', 'weight');
    }

}
