<?php

namespace app\models;

use app\modules\quiz\models\QuizQuestion;
use app\modules\quiz\Quiz;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TestQuestion;
use yii\helpers\VarDumper;

/**
 * FinalTestSearch represents the model behind the search form about `app\models\TestQuestion`.
 */
class FinalTestSearch extends QuizQuestion
{

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = QuizQuestion::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'title' => $this->title,
            'answer' => $this->answer,
            'answer2' => $this->answer2,
            'answer3' => $this->answer3,
            'answer4' => $this->answer4,
            'answer5' => $this->answer5,
            'answer6' => $this->answer6,
        ]);


        return $dataProvider;
    }
}
