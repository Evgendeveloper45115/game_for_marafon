<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Регистрация';
$this->params['breadcrumbs'][] = $this->title;
?>


<?php
if (Yii::$app->getSession()->hasFlash('error')) {
    echo '<div class="alert alert-danger">' . Yii::$app->getSession()->getFlash('error') . '</div>';
}
?>

<?php // echo \nodge\eauth\Widget::widget(['action' => 'site/login']); ?>

    <div class="social">
        <h1 class="title">Войти, используя ваш аккаунт в социальных сетях</h1>

        <ul class="social-items">
            <li class="social-item social-item--vk">
                <a href="/site/login?service=vkontakte">
                    <img src="/images/vk-logo.png">
                </a>
            </li>
            <li class="social-item social-item--fb">
                <a href="/site/login?service=facebook">
                    <img src="/images/facebook.png">
                </a>
            </li>
            <li class="social-item  social-item--mru">
                <a href="/site/login?service=mailru">
                    <img src="/images/mailru.png">
                </a>
            </li>
            <li class="social-item  social-item--gl">
                <a href="/site/login?service=google">
                    <img src="/images/google.png">
                </a>
            </li>
        </ul>


        <table class="soc_items_mob">
            <tr>
                <td class="vk_td">
                    <a href="/site/login?service=vkontakte">
                        <img src="/images/vk-logo.png">
                    </a>
                </td>
                <td class="fb_td">
                    <a href="/site/login?service=facebook">
                        <img src="/images/facebook.png">
                    </a>
                </td>
                <td class="mailru_td">
                    <a href="/site/login?service=mailru">
                        <img src="/images/mailru.png">
                    </a>
                </td>
                <td class="google_td">
                    <a href="/site/login?service=google">
                        <img src="/images/google.png">
                    </a>
                </td>
            </tr>
        </table>


    </div>


    <div class="registration">
        <h3 class="registration__title"><span id="sign_up">Быстрая регистрация</span> / <span id="sign_in">Войти</span>
        </h3>
        <div id="sign_in_block">
            <?php $form_sign_in = ActiveForm::begin([
                'id' => 'form-signup_one',
                'class' => 'registration-form',
                'validateOnChange' => false,
                'validateOnBlur' => false,
                'action' => '/site/login',
                'enableClientValidation' => false,
                'enableAjaxValidation' => true,
            ]); ?>

            <div class="registration-item">
                <?= $form_sign_in->field($model_login, 'email')->textInput(['class' => 'textbox textbox--registration', 'placeholder' => 'E-mail'])->label(false); ?>
            </div>

            <div class="registration-item">
                <?= $form_sign_in->field($model_login, 'password')->passwordInput(['class' => 'textbox textbox--registration', 'placeholder' => 'Пароль'])->label(false); ?>
            </div>

            <?= Html::submitButton('Играть', ['class' => 'btn btn--registration', 'name' => 'signup-button']) ?>
            <p class="registration__rules">нажимая на кнопку "играть" вы соглашаетесь c политикой обработки персональных
                данных</p>

            <?php ActiveForm::end(); ?>
        </div>

        <div id="sign_up_block">
            <?php $form = ActiveForm::begin(
                [
                    'id' => 'form-signup',
                    'class' => 'registration-form',
                    'validateOnBlur' => false,
                    'validateOnChange' => false,
                    'enableClientValidation' => false,
                    'enableAjaxValidation' => true,
                ]
            ); ?>

            <div class="registration-item">
                <?php echo $form->field($model, 'name')->textInput(['class' => 'textbox textbox--registration', 'placeholder' => 'Имя'])->label(false); ?>
            </div>

            <div class="registration-item">
                <?= $form->field($model, 'email')->textInput(['class' => 'textbox textbox--registration', 'placeholder' => 'E-mail'])->label(false); ?>
                <span class="label" for="email">ваш email будет вашим логином на сайте</span>
            </div>

            <div class="registration-item">
                <?= $form->field($model, 'password')->passwordInput(['class' => 'textbox textbox--registration', 'placeholder' => 'Пароль'])->label(false); ?>
                <span class="label" for="password">с этим паролем вы будите входить в игру</span>
            </div>

            <?= Html::submitButton('Играть', ['class' => 'btn btn--registration', 'name' => 'signup-button']) ?>
            <p class="registration__rules">нажимая на кнопку "играть" вы соглашаетесь c политикой обработки персональных
                данных</p>

            <?php ActiveForm::end(); ?>
        </div>

    </div>


<?php


$js = <<<JS

$(document).ready(function() {
  $('#sign_up').click(function(){      
      $('#sign_in_block').hide();
      $('#sign_up_block').show();
      $('#sign_up').css('cursor', 'auto');
      $('#sign_up').css('text-decoration', 'none');
      $('#sign_in').css('cursor', 'pointer');
      $('#sign_in').css('text-decoration', 'underline');
  });
    $('#sign_in').click(function(){
      $('#sign_up_block').hide();
      $('#sign_in_block').show();
      $('#sign_in').css('cursor', 'auto');
      $('#sign_in').css('text-decoration', 'none');
      $('#sign_up').css('cursor', 'pointer');
      $('#sign_up').css('text-decoration', 'underline');
  });
    $('.btn--registration').click(function(){
         fbq('track', 'CompleteRegistration');
    });
    
});

JS;
$this->registerJs($js);