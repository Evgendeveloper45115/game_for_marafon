<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Level;
use yii\helpers\ArrayHelper;
//use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model app\models\Article */
/* @var $form yii\widgets\ActiveForm */

if($model->theme_id){
    $type_article = 1;
}
else {
    $type_article = 2;
}

?>

<div class="article-form">

    <h3>Тип статьи</h3>
    <?= Html::radioList('article_type', $type_article, [
            1 => 'Статья темы',
            2 => 'Статья уровня'
    ]); ?>


    <?php $form = ActiveForm::begin(); ?>


    <!-- Если статья темы -->
    <div class="theme_article_block">
    <?= $form->field($model, 'theme_id')->dropDownList(['' => ''] + ArrayHelper::map(\app\models\Theme::find()->asArray()->orderBy('id')->all(), 'id', 'name')) ?>

    <?php if ($model->preview_image) { ?>
        <?= Html::img(Yii::getAlias('@rel_path_to_uploads') . '/' . $model->preview_image, ['class' => 'profile_preview']); ?>
        <div class="del_link">
            <?php
            if ($model->preview_image != 'blank.jpg') {
                echo Html::a('Удалить', ['article/delete-img4'], [
                    'data-method' => 'POST',
                    'data-params' => [
                        'id' => $model->id,
                    ],
                ]);
            }
            ?>
        </div>
    <?php } ?>

    <?php
    if (!$model->preview_image) {
        echo Html::img('/uploads/blank.jpg', ['class' => 'profile_preview']);
    }
    ?>


    <?= $form->field($model, 'preview_img')->fileInput(['onchange' => 'readURL(this, "profile_preview");']); ?>
    <?= $form->field($model, 'preview_text')->textInput() ?>

    </div>


    <!-- Если статья уровня -->
    <div class="level_article_block">
        <?= $form->field($model, 'level_id')->dropDownList(['' => ''] + ArrayHelper::map(Level::find()->asArray()->orderBy('id')->all(), 'id', 'name')) ?>

        <?= '<label class="control-label">Тип Уровня</label>' ?>
        <?= $form->field($model, 'type',
            ['template' => '{label}<div class="">{input}{hint} {error}</div>']
        )
            ->radioList(
                $model->types,
                [
                    'item' => function ($index, $label, $name, $checked, $value) {
                        return '<label class="btn btn-white btn-info btn-round' . ($checked ? " active" : null) . ' ">' . Html::radio($name, $checked, ['value' => $value, 'autocomplete' => 'off']) . $label . '</label>';
                    },
                    'tag' => 'div', // this is by default
                    'class' => 'btn-group',
                    'data-toggle' => 'buttons',
                ]
            )->label(false) ?>
    </div>

    <br />

    <?php if ($model->img) { ?>
        <?= Html::img(Yii::getAlias('@rel_path_to_uploads') . '/' . $model->img, ['class' => 'profile_foto']); ?>
        <div class="del_link">
            <?php
            if ($model->img != 'blank.jpg') {
                echo Html::a('Удалить', ['article/delete-img'], [
                    'data-method' => 'POST',
                    'data-params' => [
                        'id' => $model->id,
                    ],
                ]);
            }
            ?>
        </div>
    <?php } ?>

    <?php
    if (!$model->img) {
        echo Html::img('/uploads/blank.jpg', ['class' => 'profile_foto']);
    }
    ?>


    <?= $form->field($model, 'file')->fileInput(['onchange' => 'readURL(this);']); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= '<label class="control-label">Описание</label>' ?>;
    <?= $form->field($model, 'description', [
        'template' => "<div>{input}\n<div class=\"error\">{error}</div></div>",
    ])->widget(\zxbodya\yii2\tinymce\TinyMce::className(), [
        'options' => ['rows' => 15, 'style' => 'margin-top:5vw'],
        'language' => 'ru',
//        'spellcheckerUrl'=>'http://speller.yandex.net/services/tinyspell',
        'fileManager' => [
            'class' => \zxbodya\yii2\elfinder\TinyMceElFinder::className(),
            'connectorRoute' => 'el-finder/connector',
        ],
    ]); ?>
    <?= '<label class="control-label">Выбрать цвет</label>' ?>;
    <?= \kartik\color\ColorInput::widget([
        'model' => $model,
        'attribute' => 'block_color',
    ]) ?>
    <?= '<label class="control-label">Описание в рамочке</label>' ?>;
    <?= $form->field($model, 'color_content', [
        'template' => "<div>{input}\n<div class=\"error\">{error}</div></div>",
    ])->widget(\zxbodya\yii2\tinymce\TinyMce::className(), [
        'options' => ['rows' => 15, 'style' => 'margin-top:5vw'],
        'language' => 'ru',
//        'spellcheckerUrl'=>'http://speller.yandex.net/services/tinyspell',
        'fileManager' => [
            'class' => \zxbodya\yii2\elfinder\TinyMceElFinder::className(),
            'connectorRoute' => 'el-finder/connector',
        ],
    ]); ?>
    <?= '<label class="control-label">Короткое описание</label>' ?>;
    <?= $form->field($model, 'description_short', [
        'template' => "<div>{input}\n<div class=\"error\">{error}</div></div>",
    ])->widget(\zxbodya\yii2\tinymce\TinyMce::className(), [
        'options' => ['rows' => 15, 'style' => 'margin-top:5vw'],
        'language' => 'ru',
//        'spellcheckerUrl'=>'http://speller.yandex.net/services/tinyspell',
        'fileManager' => [
            'class' => \zxbodya\yii2\elfinder\TinyMceElFinder::className(),
            'connectorRoute' => 'el-finder/connector',
        ],
    ]); ?>

    <?php if ($model->imgFooter) { ?>
        <?= Html::img(Yii::getAlias('@rel_path_to_uploads') . '/' . $model->imgFooter, ['class' => 'imgFooter']); ?>
        <div class="del_link">
            <?php
            if ($model->imgFooter != 'blank.jpg') {
                echo Html::a('Удалить', ['article/delete-img2'], [
                    'data-method' => 'POST',
                    'data-params' => [
                        'id' => $model->id,
                    ],
                ]);
            }
            ?>
        </div>
    <?php } ?>
    <?= $form->field($model, 'image')->fileInput(['onchange' => 'readURL(this, "imgFooter");']); ?>


    <?php
    if (!$model->imgFooter) {
        echo Html::img('/uploads/blank.jpg', ['class' => 'imgFooter']);
    }
    ?>

    <?php if ($model->img_radius) { ?>
        <?= Html::img(Yii::getAlias('@rel_path_to_uploads') . '/' . $model->img_radius, ['class' => 'profile_radius']); ?>
        <div class="del_link">
            <?php
            if ($model->img_radius != 'blank.jpg') {
                echo Html::a('Удалить', ['article/delete-img3'], [
                    'data-method' => 'POST',
                    'data-params' => [
                        'id' => $model->id,
                    ],
                ]);
            }
            ?>
        </div>
    <?php } ?>

    <?php
    if (!$model->img_radius) {
        echo Html::img('/uploads/blank.jpg', ['class' => 'profile_radius']);
    }
    ?>


    <?= $form->field($model, 'image_radius')->fileInput(['onchange' => 'readURL(this, "profile_radius");']); ?>

    <div class="if-task">
        <h2>Если статья с заданием</h2>
        <?= $form->field($model, 'task_title')->textInput(); ?>

        <?= $form->field($model, 'task_text', [
            'template' => "<div>{input}\n<div class=\"error\">{error}</div></div>",
        ])->widget(\zxbodya\yii2\tinymce\TinyMce::className(), [
            'options' => ['rows' => 15, 'style' => 'margin-top:5vw'],
            'language' => 'ru',
//        'spellcheckerUrl'=>'http://speller.yandex.net/services/tinyspell',
            'fileManager' => [
                'class' => \zxbodya\yii2\elfinder\TinyMceElFinder::className(),
                'connectorRoute' => 'el-finder/connector',
            ],
        ]); ?>


        <?= $form->field($model, 'is_task_photo')->checkbox() ?>

    </div>

    <?= $form->field($model, 'timer')->textInput() ?>

    <?= $form->field($model, 'points')->textInput() ?>

    <?= $form->field($model, 'weight')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$js = <<<JS

function show_block(){
    var type = $('input[name=article_type]:checked', '.article-form').val();
            
      if(type == 1){
        $('.theme_article_block').show();
        $('.level_article_block').hide();        
    }
    
    if(type == 2 ){
        $('.level_article_block').show();
        $('.theme_article_block').hide();        
    }
}

$('.article-form input').on('change', function() {            
    show_block();
});

$(document).ready(function(){
   show_block();             
});
JS;
$this->registerJs($js);
?>