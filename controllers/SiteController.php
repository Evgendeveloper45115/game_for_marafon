<?php

namespace app\controllers;

use app\forms\PhotoForm;
use app\helpers\ImageHelperController;
use app\models\MainPage;
use app\models\PassedArticle;
use app\models\Profile;
use app\models\TaskAnswers;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\forms\LoginForm;
use app\forms\ContactForm;
use app\forms\PasswordResetRequestForm;
use app\forms\ResetPasswordForm;
use app\forms\SignupForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use app\models\Level;
use yii\web\UploadedFile;
use app\models\TriggersMail;
use yii\widgets\ActiveForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'eauth' => [
                // required to disable csrf validation on OpenID requests
                'class' => \nodge\eauth\openid\ControllerBehavior::className(),
                'only' => ['login'],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    // 'logout' => ['post'],
                    'file-upload' => ['POST']
                ],
            ],
        ];
    }


    public function beforeAction($action)
    {
        // var_dump($action->id);die();
        $this->enableCsrfValidation = ($action->id !== "file-upload");
        return parent::beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $levels = Level::find()
            ->where(['is_active' => true])
            ->all();
        $main = MainPage::findOne(1);

        $user_level = 1;
        if (isset(Yii::$app->user->identity->profile->level)) {
            $user_level = Yii::$app->user->identity->profile->level;
        }
        $passedArt = PassedArticle::find()->where(['article_id' => [60, 61, 62, 63, 64], 'user_id' => Yii::$app->user->id])->all();        return $this->render('index', [
            'levels' => $levels,
            'main' => $main,
            'user_level' => $user_level,
            'passedArt' => $passedArt,
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */

    public function actionLogin()
    {
        //VarDumper::dump(Yii::$app->user->identity->soc_profile);die;
        $this->layout = 'login';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax) {
                $model->login();
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            if (Yii::$app->user->identity->profile->passed_test) {
                return $this->redirect('/test/result');
            }
            if (Yii::$app->user->identity->profile->level > 1) {
                return $this->redirect('/level/index/' . Yii::$app->user->identity->profile->level);
            }
            return $this->goBack();
        } else {
            $serviceName = Yii::$app->getRequest()->getQueryParam('service');
            if (isset($serviceName)) {

                /** @var $eauth \nodge\eauth\ServiceBase */
                $eauth = Yii::$app->get('eauth')->getIdentity($serviceName);
                $eauth->setRedirectUrl(Yii::$app->getUser()->getReturnUrl());
                $eauth->setCancelUrl(Yii::$app->getUrlManager()->createAbsoluteUrl('site/signup'));


                // var_dump($eauth->authenticate()); die;
                try {
                    if ($eauth->authenticate()) {
                        //var_dump($eauth->getIsAuthenticated(), $eauth->getAttributes()); exit;

                        $attributes = $eauth->getAttributes();
                        $service_name = $eauth->getServiceName();
                        $user_model = User::findBySocialMediaInfo($service_name, $attributes['id']);

                        if (!$user_model) {
                            $user_model = new User();
                            $user_model->social_media_type = $service_name;
                            $user_model->social_media_id = $attributes['id'];
                            $user_model->password = md5(uniqid(rand(), 1));
                            $user_model->auth_key = md5($attributes['id']);
                            $user_model->role = User::ROLE_USER;
                            $user_model->email = 'soc_'.$attributes['id'] . '@' . $service_name . '.com';
                            if ($attributes['email'] && !User::findOne(['email' => $attributes['email']])) {
                                $user_model->email = $attributes['email'];
                            }

                            if ($user_model->save()) {
                                $profile = new Profile([
                                    'user_id' => $user_model->id,
                                    'first_name' => $attributes['name'],
                                    'last_activity' => time()
                                ]);
                                $profile->save();

                                $triggers_mail = new TriggersMail();
                                $triggers_mail->user_id = $user_model->id;
                                $triggers_mail->save();

                                $user_model->trigger(\yii\web\User::EVENT_AFTER_LOGIN);
                            }
                        }


                        $identity = User::findByEAuth($eauth);
                        Yii::$app->getUser()->login($identity);


                        // special redirect with closing popup window
                        $eauth->redirect();
                    } else {
                        // close popup window and redirect to cancelUrl
                        $eauth->cancel();
                    }
                } catch (\nodge\eauth\ErrorException $e) {
                    // save error to show it later
                    Yii::$app->getSession()->setFlash('error', 'EAuthException: ' . $e->getMessage());

                    // close popup window and redirect to cancelUrl
//              $eauth->cancel();
                    $eauth->redirect($eauth->getCancelUrl());
                }
            }
            $this->redirect(Yii::$app->getUrlManager()->createAbsoluteUrl('site/signup'));
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionChangeAvatar()
    {
        $model = new PhotoForm();

        return $this->render('change-avatar', [
            'model' => $model
        ]);
    }


    /**
     * @return array|string|Response
     */
    public function actionSignup()
    {
        $this->layout = 'sign_up';

        $model = new SignupForm();
        $model_login = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            } else {
                if (Yii::$app->getUser()->login($model->signup())) {
                    return $this->goHome();
                }
            }
        }


        return $this->render('signup', [
            'model' => $model,
            'model_login' => $model_login,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * @return string
     */
    public function actionChangePassword()
    {
        /* @var \app\models\User $user */
        $user = Yii::$app->user->identity;
        $user->scenario = User::SCENARIO_CHANGE_PASSWORD_USER;
        if ($user->load(Yii::$app->request->post()) && $user->validate()) {
            $user->setPassword($user->new_password);
            $user->save(false);
            return $this->render('change-password', ['user' => $user, 'success' => true]);
        }
        return $this->render('change-password', ['user' => $user]);
    }

    public function actionProfile()
    {
        /** @var Profile $profile */
        $profile = Profile::find()->where(['user_id' => Yii::$app->getUser()->getId()])->one();
        if ($profile) {
            $avatar = $profile->foto;
        } else {
            $profile = new Profile();
        }
        if ($profile->load(Yii::$app->request->post())) {
            $dir = Yii::getAlias('@webroot/uploads/profile');
            if (!is_dir($dir)) {
                mkdir($dir, 0777, true);
            }
            $file = UploadedFile::getInstance($profile, 'foto');
            if (!empty($file)) {
                $fname = uniqid("", true) . '.' . $file->extension;
                $profile->foto = $fname;
                $file->saveAs($dir . DIRECTORY_SEPARATOR . $fname);
                if (!ImageHelperController::is_image($dir . DIRECTORY_SEPARATOR . $fname)) {
                    return $this->render('profile', ['profile' => $profile]);
                } else {
                    $ret = ImageHelperController::imageCreateFromAny($dir . DIRECTORY_SEPARATOR . $fname);
                    $image = $ret[0];
                    $type = $ret[1];

                    $size_x = imagesx($image);
                    $size_y = imagesy($image);
                    $size = min($size_x, $size_y);
                    $half_delta = (max($size_x, $size_y) - $size) / 2;
                    $new_image = imagecreatetruecolor(390, 390);
                    if ($size_x > $size_y) {
                        imagecopyresampled($new_image, $image, 0, 0, $half_delta, 0, 390, 390, $size, $size);
                    } else {
                        imagecopyresampled($new_image, $image, 0, 0, 0, $half_delta, 390, 390, $size, $size);
                    }
                    $image = $new_image;
                    if ($type == 2) {
                        $exif = @exif_read_data($dir . DIRECTORY_SEPARATOR . $fname);
                        if (!empty($exif['Orientation'])) {
                            switch ($exif['Orientation']) {
                                // Поворот на 180 градусов
                                case 3: {
                                    $image = imagerotate($image, 180, 0);
                                    break;
                                }
                                // Поворот вправо на 90 градусов
                                case 6: {
                                    $image = imagerotate($image, -90, 0);
                                    break;
                                }
                                // Поворот влево на 90 градусов
                                case 8: {
                                    $image = imagerotate($image, 90, 0);
                                    break;
                                }
                            }
                        }
                    }
                    imagejpeg($image, $dir . DIRECTORY_SEPARATOR . $fname, 90);
                }
            } else {
                $profile->foto = $avatar;
            }
            if ($profile->validate()) {
                $profile->save();
                return $this->redirect('profile');
            }
        }
        return $this->render('profile', ['profile' => $profile]);
    }

    public function actionRecoveryPassword()
    {
        Yii::$app->request->isAjax;
        $post = Yii::$app->request->post();
        if (isset($post['mail'])) {
            $user = User::findOne(['email' => $post['mail']]);
            if ($user) {
                $password = Yii::$app->getSecurity()->generateRandomString(9);
                $user->updatePassword($password);
                Yii::$app->mailer->compose()
                    ->setFrom(['admin@beauty-matrix.ru' => 'Beauty matrix'])
                    ->setTo($user->email)
                    ->setSubject('Востановления пароля')
                    ->setTextBody('Ваш новый пароль: ' . $password)
                    ->send();
                echo Json::encode(['code' => 'ok']);
                exit;
            } else {
                echo Json::encode(['value' => 'Такого пользователя нет']);
                exit;
            }
        }
    }

    public function actionPolitic()
    {

        return $this->render('politic', [
        ]);
    }

    public function actionAgreement()
    {

        return $this->render('agreement', [
        ]);
    }

    public function actionTesting()
    {

        return $this->render('testing', [

        ]);
    }

    public function actionFileUpload()
    {
        $task_answer = new TaskAnswers();
        $chk = Yii::$app->request->post();
        if (isset($chk['file_id'])) {
            $task_answer->load(Yii::$app->request->post());
            $task_answer->article_id = $chk['article_id'];
            $task_answer->user_id = Yii::$app->user->getId();
            $task_answer->text = $chk['text'];
            $task_answer->photo_file = UploadedFile::getInstance($task_answer, 'photo_file');
            if ($task_answer->photo_file) {
                $file_name = uniqid();
                $task_answer->foto = $file_name . '.' . $task_answer->photo_file->extension;
                if (!$task_answer->photo_file->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/user_uploads/' . $file_name . '.' . $task_answer->photo_file->extension)) {
                    throw new \RuntimeException('Ошибка сохранения файла');
                }
            }
            $task_answer->save(false);
        }

        echo '{}';
    }


    public function actionUploadAvatar()
    {
        $user = User::findOne(['id' => Yii::$app->user->getId()]);
        $model = new PhotoForm();

        $chk = Yii::$app->request->post();
        $model->load(Yii::$app->request->post());
        $model->file = UploadedFile::getInstanceByName('file');
        if ($model->file) {
            $file_name = uniqid();
            $user->profile->foto = $file_name . '.' . $model->file->extension;
            if (!$model->file->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/profile/' . $file_name . '.' . $model->file->extension)) {
                throw new \RuntimeException('Ошибка сохранения файла');
            }
        }
        $user->profile->save();

        echo '{}';
    }


    //при клике по шаринг кнопке добавим баллов
    public function actionPoint()
    {
        if (\Yii::$app->request->isAjax) {

            $shared = false;
            $session = Yii::$app->session;

            if ($session->has('shared')) {
                $shared = $session->get('shared');
            }

            if (!$shared) {
                $profile = Profile::findOne(['user_id' => Yii::$app->user->identity->getId()]);
                $profile->account += 50;
                $profile->save();

                $session->set('shared', true);

                return '+50';
            }

            return '+50';
        }

    }

    public function actionUpload()
    {
        $file_path = '/var/www/work.dev/game/web/vardump.txt';
        $file = fopen($file_path, "w+");
        ob_start();
        //var_dump(Yii::$app->request->post());
        var_dump($_FILES);
        var_dump($_POST);
        $output = ob_get_clean();
        fwrite($file, $output);
        fclose($file);
    }

    public function actionFB()
    {
        $this->layout = 'face_book';
    }
}
