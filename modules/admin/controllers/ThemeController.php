<?php

namespace app\modules\admin\controllers;

use app\models\Theme;
use app\models\ThemeSearch;
use Yii;
use app\models\Level;
use app\models\LevelSearch;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ThemeController implements the CRUD actions for Level model.
 */
class ThemeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * Lists all Level models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ThemeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Level model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Level model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Theme();
        $model->weight = 5;
        $model->FillBlankImg();

        if ($model->load(Yii::$app->request->post())) {
            $model->image_radius = UploadedFile::getInstance($model, 'image_radius');
            if ($model->image_radius) {
                $model->dropOldImg();
                $file_name = uniqid();
                $model->img_radius = $file_name . '.' . $model->image_radius->extension;
                if (!$model->image_radius->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/' . $file_name . '.' . $model->image_radius->extension)) {
                    throw new \RuntimeException('Ошибка сохранения файла');
                }
                $model->image_radius = null;
            }
            $model->save();
            return $this->redirect('index');
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Theme model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->FillBlankImg();

        if ($model->load(Yii::$app->request->post())) {
            $model->image_radius = UploadedFile::getInstance($model, 'image_radius');
            if ($model->image_radius) {
                $model->dropOldImg();
                $file_name = uniqid();
                $model->img_radius = $file_name . '.' . $model->image_radius->extension;
                if (!$model->image_radius->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/' . $file_name . '.' . $model->image_radius->extension)) {
                    throw new \RuntimeException('Ошибка сохранения файла');
                }
                $model->image_radius = null;
            }
            $model->save();
            return $this->redirect('index');
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }


    /**
     * Finds the Level model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Theme the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Theme::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
