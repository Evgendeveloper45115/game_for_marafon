<?php

use yii\db\Migration;

class m171024_090626_add_role_to_user extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user', 'role', $this->smallInteger());
    }

    public function safeDown()
    {
        $this->dropColumn('user', 'role');
    }

}
