<?php

use yii\db\Migration;

class m170912_085528_change_passed_test extends Migration
{
    public function safeUp()
    {
        $this->addColumn('profile', 'test_start_time', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn('profile', 'test_start_time');
    }

}
