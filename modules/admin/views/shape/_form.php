<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Shape */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shape-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'breast_from')->textInput() ?>
    <?= $form->field($model, 'breast_to')->textInput() ?>
    <?= $form->field($model, 'waist_from')->textInput() ?>
    <?= $form->field($model, 'waist_to')->textInput() ?>
    <?= $form->field($model, 'hip_from')->textInput() ?>
    <?= $form->field($model, 'hip_to')->textInput() ?>

    <?php if ($model->foto) { ?>
        <?= Html::img(Yii::getAlias('@rel_path_to_uploads') . '/' . $model->foto, ['class' => 'profile_foto']); ?>
        <div class="del_link">
            <?php
            if ($model->foto != 'blank.jpg'){
                echo Html::a('Удалить', ['shape/delete-img'], [
                    'data-method' => 'POST',
                    'data-params' => [
                        'id' => $model->id,
                    ],
                ]);
            }
            ?>
        </div>
    <?php } ?>

    <?php
    if (!$model->foto){
        echo Html::img('/uploads/blank.jpg', ['class' => 'profile_foto']);
    }
    ?>


    <?= $form->field($model, 'file')->fileInput(['onchange'=>'readURL(this);']); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
