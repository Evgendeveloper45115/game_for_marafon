<?php
namespace app\forms;

use yii\base\Model;

class PointForm extends Model
{
    public $amount;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amount'], 'integer'],
        ];
    }

}
