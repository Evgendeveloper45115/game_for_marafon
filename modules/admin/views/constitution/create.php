<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Constitution */

$this->title = 'Добавить телосложение';
$this->params['breadcrumbs'][] = ['label' => 'Constitutions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="constitution-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
