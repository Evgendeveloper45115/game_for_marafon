<?php

use yii\helpers\Url;
use app\helpers\CommonHelper;

$counter = 1;
$prev_theory = null;
$begin_label = true; // для учета метки Начать
$not_one_passed = true; // ни одно задание еще не выполнено

/**
 * @var $level \app\models\Level
 * @var $themesTheory \app\models\Theme[]
 * @var $video \app\models\Video[]
 * @var $themesTask \app\models\Theme[]
 * @var $articlesTheory \app\models\Article[]
 * @var $articlesTask \app\models\Article[]
 * @var $levelUP bool
 * @var $social bool
 */
?>
    <div class="clearfix">
        <div class="col-5">
            <h1 class="title title__hidden"><?= $level->number ?></h1>
            <h1 class="title title__hidden"><?= $level->name ?></h1>

            <div class="left-image">
                <img src="<?= Yii::getAlias('@rel_path_to_uploads') . '/' . $level->img ?>" alt="">
            </div>
        </div>
        <div class="col-7">
            <div class="content__right">
                <div class="title_block">
                    <h1 class="title"><?= $level->number ?></h1>
                    <h1 class="title"><?= $level->name ?></h1>
                </div>

                <p class="text text--quote text--border"
                   style="border-color: <?= $level->color ?>"><?= $level->color_content ?></p>

                <div class="text"><?= $level->description ?></div>
            </div>
        </div>
    </div>
<?php
if ($level->type == \app\models\Level::BLOCK) {
    ?>
    <div class="blocks">
        <div class="block block--blue">
            <h2 class="block__title">Теория</h2>
            <?php

            if (!empty($theory)) {
                for ($i = 0; $i <= 1; $i++): ?>
                    <?php if (isset($theory[$i])): ?>
                        <div class="block__content">
                            <a onclick="<?= $start == 1 ? 'yaCounter46569435.reachGoal(\'start\')' : '' ?>" data-vbtype="inline"
                               style="background: url('/uploads/<?= $theory[$i]->img_radius ?>') 100% 100% no-repeat; background-size: cover;"
                               class="block__round level__round <?= $taskVis = CommonHelper::getTaskVisibility($counter, $theory); ?>"
                               href="<?= stristr(CommonHelper::getAlias($theory[$i], $taskVis), '#') ? CommonHelper::getAlias($theory[$i], $taskVis) : Url::to([CommonHelper::getAlias($theory[$i]) . $theory[$i]->id]) ?>">
                                <?php
                                $style = null;
                                if (CommonHelper::isPassedByUser($theory[$i], Yii::$app->user->identity->getId())) {
                                    $style = 'background-color: transparent!important; box-shadow: none!important;';
                                    echo '<span style="' . $style . '" class="btn btn--level">Пройдено</span>';
                                    $not_one_passed = false;
                                } elseif ($begin_label) {
                                    echo '<span id="target" class="btn btn--level">Начать</span>';
                                    $begin_label = false;
                                } ?>
                            </a><span
                                    class="block__text"><?= $counter++ . '. ' . $theory[$i]->name; ?></span>
                        </div>
                        <?php $prev_theory = $theory[$i]; ?>
                    <?php endif; ?>
                <?php endfor;
            }
            ?>
        </div>

        <div class="block block--red">
            <h2 class="block__title">Задание</h2>
            <?php
            if (!empty($tasks)) {
                for ($i = 0; $i <= 1; $i++): ?>
                    <?php if (isset($tasks[$i])): ?>
                        <div class="block__content">
                            <a data-vbtype="inline"
                               style="background: url('/uploads/<?= $tasks[$i]->img_radius ?>') 100% 100% no-repeat; background-size: cover;"
                               class="block__round level__round <?= $theoryVis = CommonHelper::getTaskVisibility($counter, $tasks, $prev_theory); ?>"
                               href="<?= stristr(CommonHelper::getAlias($tasks[$i], $theoryVis), '#') ? CommonHelper::getAlias($tasks[$i], $theoryVis) : Url::to([CommonHelper::getAlias($tasks[$i]) . $tasks[$i]->id]) ?>">
                                <?php
                                $style = null;
                                if (CommonHelper::isPassedByUser($tasks[$i], Yii::$app->user->identity->getId())) {
                                    $style = 'background-color: transparent!important; box-shadow: none!important;';
                                    echo '<span style="' . $style . '" class="btn btn--level">Пройдено</span>';
                                    $not_one_passed = false;
                                } elseif ($begin_label) {
                                    echo '<span id="target" class="btn btn--level">Начать</span>';
                                    $begin_label = false;
                                } ?>
                            </a><span
                                    class="block__text"><?= $counter++ . '. ' . $tasks[$i]->name; ?></span>
                        </div>
                    <?php endif; ?>
                <?php endfor;
            }
            ?>
        </div>
    </div>
    <?php
} else {

    //линейный вид
    ?>
    <div class="js-slider levels">
        <?php

        if (!empty($line)) {
            for ($i = 0; $i <= 4; $i++): ?>
                <?php if (isset($line[$i])): ?>
                    <a onclick="<?= $start == 1 ? 'yaCounter46569435.reachGoal(\'start\')' : '' ?>" data-vbtype="inline"
                       class="level <?= $taskVis = CommonHelper::getLineTaskVisibility($counter, $line); ?>"
                       href="<?= stristr(CommonHelper::getAlias($line[$i], $taskVis), '#') ? CommonHelper::getAlias($line[$i], $taskVis) : Url::to([CommonHelper::getAlias($line[$i]) . $line[$i]->id]) ?>">
                        <div class="level__round  <?= CommonHelper::getLineTaskActivity($counter, $line); ?>"
                             style="background-image: url('/uploads/<?= $line[$i]->img_radius ?>')">
                            <?php
                            $style = null;
                            if (CommonHelper::isPassedByUser($line[$i], Yii::$app->user->identity->getId())) {
                                $style = 'background-color: transparent!important; box-shadow: none!important;';
                                echo '<span style="' . $style . '" class="btn btn--level">Пройдено</span>';
                                $not_one_passed = false;
                            } elseif ($begin_label) {
                                echo '<span id="target" class="btn btn--level">Начать</span>';
                                $begin_label = false;
                            } ?>
                        </div>
                        <p class="level__name"><?= $counter++ . '. ' . $line[$i]->name; ?></p>
                    </a>
                <?php endif; ?>
            <?php endfor;
}

        ?>

    </div>
    <?php
}
if ($levelUP) {
    echo $this->render('/popups/level_up', ['text' => $text]);
}
/*if ($emailPopup) {
                                echo $this->render('/popups/emailPopup');
                            }
                            if ($social) {
                                echo $this->render('/popups/friend');
                            }*/
?>
<?= $this->render('/popups/no_task_video_theory') ?>

<?php
$js = <<<JS
$(document).ready(function(){

            window.setTimeout(function(){
            var venoOptions = {
                // is called after plugin initialization.
                cb_init: function(plugin){
                    $('.vbox-close').css('left', '-1000px');
                    
                },
            }
            $('.venobox').venobox(venoOptions);
        }, 200);
    
if($('*').is('#target') && $count !== 0 && '!$not_one_passed') {
var scrollTop = $('#target').offset().top;
$(document).scrollTop(scrollTop);
}
});
JS;
$this->registerJs($js);