<?php

namespace app\controllers;

use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use app\components\TriggerMailer;
use app\models\User;


class MailController extends Controller
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }


    //чекеры
    public function action1DayChecker()
    {
        $users = User::find()->all();
        foreach ($users as $user) {
            TriggerMailer::notVisit1DayChecker($user);
        }
    }

    public function action3DayChecker()
    {
        $users = User::find()->all();
        foreach ($users as $user) {
            TriggerMailer::notVisit3DayChecker($user);
        }
    }

    public function action6DayChecker()
    {
        $users = User::find()->all();
        foreach ($users as $user) {
            TriggerMailer::notVisit6DayChecker($user);
        }
    }

    public function actionNotPassedNotBuy()
    {
        $users = User::find()->all();

        foreach ($users as $user) {
            TriggerMailer::notPassedTestNotBuyChecker($user);
        }
    }

    public function actionNotPassedNotRetest()
    {
        $users = User::find()->all();
        foreach ($users as $user) {
            TriggerMailer::notPassedTestNotRetestChecker($user);
        }
    }

    //по событиям
    public function actionPassedTest()
    {
        $user = \Yii::$app->user->getIdentity();
        TriggerMailer::passedTest($user);
    }

    public function actionNotPassedTest()
    {
        $user = \Yii::$app->user->getIdentity();
        TriggerMailer::notPassedTest($user);
    }

    public function actionFinished5Level()
    {
        $user = \Yii::$app->user->getIdentity();
        TriggerMailer::finished5Level($user);
    }

    public function actionMailTest()
    {
        \Yii::$app->mail->compose('views/not_visit1_html',
            ['user' => 1, 'htmlLayout' => 'layouts/trigger_mail_html'])
            ->setTo('d.smirnov@specialview.ru')
            ->setSubject('Продолжи игру "Построй стройную фигуру!"')
            ->send();
        \Yii::$app->mail->compose('views/not_visit3_html',
            ['user' => 1, 'htmlLayout' => 'layouts/trigger_mail_html'])
            ->setTo('d.smirnov@specialview.ru')
            ->setSubject('Этапы построения тела')
            ->send();
        \Yii::$app->mail->compose('views/not_visit6_html',
            ['user' => 1, 'htmlLayout' => 'layouts/trigger_mail_html'])
            ->setTo('d.smirnov@specialview.ru')
            ->setSubject('О движении бодипозитив и любви к себе')
            ->send();

        \Yii::$app->mail->compose('views/not_passed_test_html',
            ['user' => 1, 'htmlLayout' => 'layouts/trigger_mail_html'])
            ->setTo('d.smirnov@specialview.ru')
            ->setSubject('Попробуйте пройти тест еще раз!')
            ->send();
        \Yii::$app->mail->compose('views/not_passed_not_retest_html',
            ['user' => 1, 'htmlLayout' => 'layouts/trigger_mail_html'])
            ->setTo('d.smirnov@specialview.ru')
            ->setSubject('Участвуйте в дистанционной программе похудения Beauty Matrix уже сейчас!')
            ->send();
        \Yii::$app->mail->compose('views/not_passed_not_buy_html',
            ['user' => 1, 'htmlLayout' => 'layouts/trigger_mail_html'])
            ->setTo('d.smirnov@specialview.ru')
            ->setSubject('До конца спецпредложения осталось всего 12 часов!')
            ->send();
        \Yii::$app->mail->compose('views/finished_6_level_html',
            ['user' => 1, 'htmlLayout' => 'layouts/trigger_mail_html'])
            ->setTo('d.smirnov@specialview.ru')
            ->setSubject('Что такое дистанционная групповая программа похудения Beauty Matrix?')
            ->send();
        \Yii::$app->mail->compose('views/finished_5_level_html',
            ['user' => 1, 'htmlLayout' => 'layouts/trigger_mail_html'])
            ->setTo('d.smirnov@specialview.ru')
            ->setSubject('Как мотивировать себя построить стройную фигуру?')
            ->send();
        \Yii::$app->mail->compose('views/finished_4_level_html',
            ['user' => 1, 'htmlLayout' => 'layouts/trigger_mail_html'])
            ->setTo('d.smirnov@specialview.ru')
            ->setSubject('С чего начинается похудение?')
            ->send();

        \Yii::$app->mail->compose('views/passed_test_html',
            ['user' => 1, 'htmlLayout' => 'layouts/trigger_mail_html'])
            ->setTo('d.smirnov@specialview.ru')
            ->setSubject('Поздравляем с успешным прохождением теста!')
            ->send();



        /*        \Yii::$app->mailer->compose('views/not_visit3_html')
                    ->setTo('roma@specialview.ru')
                    ->setSubject('Мифы, которые мешают нам быть стройными')
                    ->send();
                \Yii::$app->mailer->compose('views/not_visit6_html')
                    ->setTo('roma@specialview.ru')
                    ->setSubject('Как сделать живот плоским не качая пресс?')
                    ->send();
                \Yii::$app->mailer->compose('views/finished_5_level_html')
                    ->setTo('roma@specialview.ru')
                    ->setSubject('Порция мотивации, чтобы вы дошли до конца')
                    ->send();
                \Yii::$app->mailer->compose('views/passed_test_html')
                    ->setTo('roma@specialview.ru')
                    ->setSubject('Поздравляем с успешным прохожденим теста')
                    ->send();
                \Yii::$app->mailer->compose('views/not_passed_test_html')
                    ->setTo('roma@specialview.ru')
                    ->setSubject('Хотите пройти тест еще раз?')
                    ->send();
                \Yii::$app->mailer->compose('views/not_passed_not_buy_html')
                    ->setTo('roma@specialview.ru')
                    ->setSubject('До конца спецпредложения осталось всего 12 часов!')
                    ->send();
                \Yii::$app->mailer->compose('views/not_passed_not_retest_html')
                    ->setTo('roma@specialview.ru')
                    ->setSubject('Более 4500 довольных клиентов групповой программы Beauty Matrix!')
                    ->send();*/
    }

}
