<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAssetQuiz extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css',
        'css/finalTest.css',
        'js/slick/slick.css',
        'js/slick/slick-theme.css',
        'js/venobox/venobox.css',

    ];
    public $js = [
        "https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js",
        "https://code.jquery.com/ui/1.12.1/jquery-ui.js",
        'http://downloads.mailchimp.com/js/goal.min.js',
        'js/jquery.tap.js',
        'js/slick/slick.js',
        'js/venobox/venobox.js',
        'js/core.js',
        'js/miscellaneous.js',
        'js/finalTest.js',

    ];
    public $jsOptions = [
        'position' => View::POS_HEAD
    ];
    /* public $depends = [
         'yii\web\YiiAsset',
         'yii\bootstrap\BootstrapAsset',
     ];*/
}
