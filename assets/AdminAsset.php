<?php

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AdminAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap.css',
        'css/admin.css',
        'css/MyCssDM.css',
        'js/slick/slick.css',
        'js/slick/slick-theme.css',
        'js/venobox/venobox.css',
    ];
    public $js = [
        'js/admin.js',
        'js/jquery-ui.min.js',
        'js/jquery.tap.js',
        'js/venobox/venobox.js',
        'js/slick/slick.js',
        'js/core.js',
        'js/miscellaneous.js',
        //'js/jquery.film_roll.min.js',
        // 'js/jquery.touchSwipe.min.js',

    ];
    public $jsOptions = [
        'position' => View::POS_END
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}