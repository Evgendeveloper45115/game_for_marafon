function readURL(input, classname) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        if(classname){
            reader.onload = function (e) {
                $('.' + classname)
                    .attr('src', e.target.result)
                    .width(150);
                $('.del_link').hide();
            };
        }
        else{
            reader.onload = function (e) {
                $('.profile_foto')
                    .attr('src', e.target.result)
                    .width(150);
                $('.del_link').hide();
            };
        }


        reader.readAsDataURL(input.files[0]);
    }
}