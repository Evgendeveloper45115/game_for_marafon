
    <div class="congrat">
        <div class="congrat-top fireworks fireworks--lg fireworks--lg-congrat">
            <h1 class="congrat-top__title">Поздравляем вас!</h1>
        </div>
        <div class="congrat-content">
            <p class="congrat-top__desc">Вы успешно завершили тест</p>
            <p class="congrat__text text congrat__text--test">Мы правда гордимся вами, вы выполнили много довольно сложных заданий, мы
                уверены такой целеустремленный человек обязательно добьется своего</p>
            <p class="congrat-top__desc congrat-top__desc--test">Теперь вы можете использовать ваши баллы и
                ознакомиться с программой похудения Beauty Matrix! </p>
            <a class="btn btn--congrat" href="<?= \yii\helpers\Url::to(['/test/result']);?>">Смотреть</a>
        </div>
    </div>
