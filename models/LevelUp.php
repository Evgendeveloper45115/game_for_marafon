<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "level_up".
 *
 * @property integer $id
 * @property integer $level
 * @property string $title
 * @property string $image
 * @property string $description
 */
class LevelUp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file;

    public static function tableName()
    {
        return 'level_up';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['level'], 'integer'],
            [['description'], 'string'],
            [['title', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'level' => 'Level',
            'title' => 'Title',
            'image' => 'Image',
            'description' => 'Description',
        ];
    }

    public function FillBlankImg()
    {
        if (!$this->image) {
            $this->image = "blank.jpg";
        }
    }

    public function dropOldImg()
    {
        if ($this->image) {
            $path_to_file = Yii::getAlias('@abs_path_to_uploads') . '/levelUp' . $this->image;

            if (file_exists($path_to_file) && $this->image != 'blank.jpg') {
                unlink($path_to_file);
                $this->image = 'blank.jpg';
                $this->save(false);
            }
        }
    }

}
