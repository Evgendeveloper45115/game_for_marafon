<?php

use yii\db\Migration;

class m171006_155357_drop_passed_fk extends Migration
{
    public function safeUp()
    {
        $this->dropForeignKey('fk-passed_theme-user_id-user-id', 'passed_theme');
        $this->dropForeignKey('fk-passed_theme-theme_id-theme-id', 'passed_theme');
        $this->dropForeignKey('fk-passed_video-video_id-video-id', 'passed_video');
        $this->dropForeignKey('fk-passed_article-article_id-article-id', 'passed_article');
        $this->dropForeignKey('fk-passed_video-user_id-user-id', 'passed_video');
        $this->dropForeignKey('fk-passed_article-user_id-user-id', 'passed_article');
    }

    public function safeDown()
    {
        $this->addForeignKey(
            'fk-passed_theme-user_id-user-id', 'passed_theme', 'user_id', 'user', 'id',
            'RESTRICT', 'CASCADE'
        );

        $this->addForeignKey(
            'fk-passed_theme-theme_id-theme-id', 'passed_theme', 'theme_id', 'theme', 'id',
            'RESTRICT', 'CASCADE'
        );

        $this->addForeignKey(
        'fk-passed_article-user_id-user-id', 'passed_article', 'user_id', 'user', 'id',
        'RESTRICT', 'CASCADE'
    );

        $this->addForeignKey(
            'fk-passed_video-user_id-user-id', 'passed_video', 'user_id', 'user', 'id',
            'RESTRICT', 'CASCADE'
        );

        $this->addForeignKey(
            'fk-passed_article-article_id-article-id', 'passed_article', 'article_id', 'article', 'id',
            'RESTRICT', 'CASCADE'
        );

        $this->addForeignKey(
            'fk-passed_video-video_id-video-id', 'passed_video', 'video_id', 'video', 'id',
            'RESTRICT', 'CASCADE'
        );
    }


}
