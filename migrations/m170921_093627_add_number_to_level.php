<?php

use yii\db\Migration;

class m170921_093627_add_number_to_level extends Migration
{
    public function safeUp()
    {
        $this->addColumn('level', 'number_int', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn('level', 'number_int');
    }

}
