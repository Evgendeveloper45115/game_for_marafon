<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'css/core_original.css',
        'js/slick/slick.css',
        //'js/slick/original_slick.css',
        'js/slick/slick-theme.css',
        'js/venobox/venobox.css',
        'css/cross.css',
        'css/core.css',
        'css/miscellaneous.css',
        'css/MyCssDM.css',
        //'css/fi.css',
        'css/owl.carousel.css',
    ];
    public $js = [
      //  '//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js',
        'js/jquery.tap.js',
        'js/slick/slick.js',
        'js/venobox/venobox.js',
        'js/core.js',
        'js/miscellaneous.js',
        'js/owl.carousel.js',
    ];
    public $jsOptions = [
        'position' => View::POS_END
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
