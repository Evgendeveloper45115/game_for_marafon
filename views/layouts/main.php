<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\web\JqueryAsset;
use app\helpers\UserHelper;
use app\components\Alert;
use app\helpers\MenuHelper;

$class = 'content';
$style = false;
AppAsset::register($this);
if (Yii::$app->controller->action->id == 'result') {
    \app\assets\TimerAsset::register($this);
}

$img = '/images/default_profile.png';
$profile = \app\models\Profile::find()->where(['user_id' => Yii::$app->getUser()->getId()])->one();
if ($profile && $profile->foto) {
    $img = '/uploads/profile/' . $profile->foto;
}

if (Yii::$app->user->isGuest) {
    \app\assets\GuestAsset::register($this);
}

/*if (Yii::$app->controller->id != 'site') {
    \app\assets\CrossAsset::register($this);
}*/

$first_name = '';
$account = 0;

if (isset(Yii::$app->user->identity->profile)) {
    $first_name = Yii::$app->user->identity->profile->first_name;
    $account = Yii::$app->user->identity->profile->account;
}

$total_points = 0; //  сколько всего баллов пользователь уже списал
$session = Yii::$app->session;

if ($session->has('total_points')) {
    $total_points = $session->get('total_points');
}

$account -= $total_points;

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:title" content="Игра: «Построй стройную фигуру»"/>
    <meta property="og:image" content="https://game.beauty-matrix.ru/uploads/sss.jpg"/>
    <meta property="og:description"
          content="Выполняй задания и худей, узнавай новую и эффективную информацию о питании и построй фигуру своей мечты"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1542289642483304');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1542289642483304&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->
    <?php
    $this->head() ?>
</head>
<body style="<?= $style ? 'padding-bottom: 0;' : null ?>">
<?php $this->beginBody() ?>
<!-- Yandex.Metrika counter -->
<script type="text/javascript"> (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter46569435 = new Ya.Metrika({
                    id: 46569435,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true,
                    webvisor: true
                });
            } catch (e) {
            }
        });
        var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () {
            n.parentNode.insertBefore(s, n);
        };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";
        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks"); </script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/46569435" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript> <!-- /Yandex.Metrika counter -->

<header class="top <?= (Yii::$app->user->isGuest) ? 'display_none' : ''; ?>">
    <a class="top__logo top__item" href="/  ">
        <img src="/images/top_logo.png" alt="">
    </a>

    <div class="top-user top__item top-user--withmenu" id="top-user">
        <div class="top-user__avatar">
            <div class="top-user__progress js-progress" data-progress="70" style="display: none;">
                <svg viewBox="-10 -10 220 220">
                    <g fill="none" stroke-width="8" transform="translate(100,100)">
                        <path d="M 0,-100 A 100,100 0 0,1 86.6,-50" stroke="url(#cl1)"></path>
                        <path d="M 86.6,-50 A 100,100 0 0,1 86.6,50" stroke="url(#cl1)"></path>
                        <path d="M 86.6,50 A 100,100 0 0,1 0,100" stroke="url(#cl1)"></path>
                        <path d="M 0,100 A 100,100 0 0,1 -86.6,50" stroke="url(#cl1)"></path>
                        <path d="M -86.6,50 A 100,100 0 0,1 -86.6,-50" stroke="url(#cl1)"></path>
                        <path d="M -86.6,-50 A 100,100 0 0,1 0,-100" stroke="url(#cl1)"></path>
                    </g>
                </svg>
                <svg class="top-user__svg--2 profile" viewBox="-10 -10 220 220">
                    <path
                            d="M200,100 C200,44.771525 155.228475,0 100,0 C44.771525,0 0,44.771525 0,100 C0,155.228475 44.771525,200 100,200 C155.228475,200 200,155.228475 200,100 Z"
                            stroke-dashoffset="440"></path>
                </svg>
                <svg width="0" height="0">
                    <defs>
                        <linearGradient id="cl1" gradientUnits="objectBoundingBox" x1="0" y1="0" x2="1" y2="1">
                            <stop stop-color="#bf58eb"></stop>
                            <stop offset="100%" stop-color="#bf58eb"></stop>
                        </linearGradient>
                        <linearGradient id="cl2" gradientUnits="objectBoundingBox" x1="0" y1="0" x2="0" y2="1">
                            <stop stop-color="#bf58eb"></stop>
                            <stop offset="100%" stop-color="#8476e6"></stop>
                        </linearGradient>
                        <linearGradient id="cl3" gradientUnits="objectBoundingBox" x1="1" y1="0" x2="0" y2="1">
                            <stop stop-color="#8476e6"></stop>
                            <stop offset="100%" stop-color="#8476e6"></stop>
                        </linearGradient>
                        <linearGradient id="cl4" gradientUnits="objectBoundingBox" x1="1" y1="1" x2="0" y2="0">
                            <stop stop-color="#8476e6"></stop>
                            <stop offset="100%" stop-color="#8476e6"></stop>
                        </linearGradient>
                        <linearGradient id="cl5" gradientUnits="objectBoundingBox" x1="0" y1="1" x2="0" y2="0">
                            <stop stop-color="#8476e6"></stop>
                            <stop offset="100%" stop-color="#d464ee"></stop>
                        </linearGradient>
                        <linearGradient id="cl6" gradientUnits="objectBoundingBox" x1="0" y1="1" x2="1" y2="0">
                            <stop stop-color="#d464ee"></stop>
                            <stop offset="100%" stop-color="#bf58eb"></stop>
                        </linearGradient>
                    </defs>
                </svg>
            </div>
            <img src="<?= $img ?>" alt="">
        </div>
        <div class="top-user__desc"><span class="top-user__name top__text"><?= $first_name ? $first_name : ''; ?></span><span
                    class="top-user__point top__text">кол-во баллов:<b> <?= $account ?></b></span>
        </div>
        <div class="top-user__menu" id="user-menu"><span class="top-user__close"></span>
            <ul>
                <li class="top-user__item"><a class="top-user__link" href="/avatar">Сменить аватар</a></li>
                <li class="top-user__item"><a class="top-user__link"
                                              href="<?= \yii\helpers\Url::to('/site/change-password') ?>">Поменять
                        пароль</a>
                </li>
                <li class="top-user__item"><a class="top-user__link" href="<?= \yii\helpers\Url::to('/site/logout') ?>">Выйти
                        из личного кабинета</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="top__item top__item--level"><span class="top__text top__text--level">Уровень</span><span
                class="top__level"><?= UserHelper::getUserLevel(); ?></span>
    </div>
    <div class="points_mob top__item">
        <?= $account ?>
        <div class="tooltop_wrapper">
            <div id="tooltip">Позже вы сможете потратить баллы на что-то ценное</div>
        </div>
    </div>
    <a class="show-menu" href="#" id="show-menu"><span></span></a>
</header>
<nav class="menu" id="menu">
    <a class="menu__close"></a>
    <ul class="menu__list">
        <li class="menu__item menu__item--logo">
            <a href="#">
                <img src="/images/menu_logo.png" alt="">
            </a>
        </li>
        <?php if (MenuHelper::isSpecialOfferAvailable()) { ?>
            <li class="menu__item menu__item--spec"><a class="menu__link"
                                                       href="<?= \yii\helpers\Url::to('/test/result'); ?>">Специальное
                    предложение</a>
            </li>
        <?php } ?>

        <li class="menu__item"><a class="menu__link" href="<?= \yii\helpers\Url::to('/level/map') ?>">Карта уровней</a>
        </li>
        <?php if (MenuHelper::isTestAvailable()) { ?>
            <li class="menu__item"><a class="menu__link" href="/test/pravilnoe-pitanie">Финальный тест</a>
            </li>
        <?php } ?>
        <li class="menu__item"><a class="menu__link" href="<?= \yii\helpers\Url::to('/site/change-password') ?>">Изменить
                пароль</a>
        </li>
        <li class="menu__item menu__item--last"><a class="menu__link"
                                                   href="<?= \yii\helpers\Url::to('/site/logout') ?>">Выйти</a>
        </li>
    </ul>
</nav>

<div class="alert <?= $class ?>"></div>

<div class="<?= $class ?>">
    <?= Alert::widget() ?>
    <?= $content ?>
</div>


<?php if (Yii::$app->controller->id == 'article') { ?>
    <div class="abs-image abs-image--blueball"></div>
    <div class="abs-image abs-image--ruler"></div>
    <div class="abs-image abs-image--yellowball"></div>
<?php } ?>
<?php if (Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'index') { ?>
    <div class="abs-image abs-image--ball"></div>
    <div class="abs-image abs-image--apples"></div>
    <div class="abs-image abs-image--rope"></div>
    <div class="abs-image abs-image--dumbbells"></div>
<?php } ?>

<?php if (Yii::$app->controller->id == 'video') { ?>
    <div class="abs-image abs-image--dumbbells2 abs-image--mobile-none"></div>
    <div class="abs-image abs-image--bottle abs-image--mobile-none"></div>
<?php } ?>

<?php if (Yii::$app->controller->id == 'personification') { ?>
    <div class="abs-image abs-image--dumbbells2 abs-image--mobile-none"></div>
    <div class="abs-image abs-image--bottle"></div>
<?php } ?>

<?php if (Yii::$app->controller->id == 'theme') { ?>
    <div class="abs-image abs-image--ball"></div>
    <div class="abs-image abs-image--apples abs-image--mobile-none"></div>
    <div class="abs-image abs-image--dumbbells"></div>
<?php } ?>

<?php if (Yii::$app->controller->id == 'test' && Yii::$app->controller->action->id == 'result') { ?>
    <div class="abs-image abs-image--yellowball"></div>
<?php } ?>

<?php $this->endBody() ?>
<script type="text/javascript">

    window.onload = function () {
        yaCounter46569435.reachGoal('ERROR404');
    };

</script>
</body>

</html>
<?php $this->endPage() ?>
