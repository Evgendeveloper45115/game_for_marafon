<?php

use yii\db\Migration;

class m170907_130241_create_task_tables extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%settings}}', [
            'score' => $this->integer(),
        ], $tableOptions);

        $this->createTable('{{%level}}', [
            'id' => $this->primaryKey(),
            'number' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'img' => $this->string()->notNull(),
        ], $tableOptions);

        $this->createTable('{{%article}}', [
            'id' => $this->primaryKey(),
            'level_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'description' => $this->string()->notNull(),
            'img' => $this->string()->notNull(),
            'timer' => $this->integer()->notNull(),
            'points' => $this->integer()->notNull(),

        ], $tableOptions);

        $this->createTable('{{%video}}', [
            'id' => $this->primaryKey(),
            'level_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'description' => $this->string()->notNull(),
            'img' => $this->string()->notNull(),
            'timer' => $this->integer()->notNull(),
            'points' => $this->integer()->notNull(),

        ], $tableOptions);

        $this->createTable('{{%passed_article}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'article_id' => $this->integer()->notNull(),

        ], $tableOptions);

        $this->createTable('{{%passed_video}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'video_id' => $this->integer()->notNull(),

        ], $tableOptions);

        $this->addForeignKey(
            'fk-article-level_id-level-id', 'article', 'level_id', 'level', 'id',
            'RESTRICT', 'CASCADE'
        );

        $this->addForeignKey(
            'fk-video-level_id-level-id', 'video', 'level_id', 'level', 'id',
            'RESTRICT', 'CASCADE'
        );

        $this->addForeignKey(
            'fk-passed_article-user_id-user-id', 'passed_article', 'user_id', 'user', 'id',
            'RESTRICT', 'CASCADE'
        );

        $this->addForeignKey(
            'fk-passed_video-user_id-user-id', 'passed_video', 'user_id', 'user', 'id',
            'RESTRICT', 'CASCADE'
        );

        $this->addForeignKey(
            'fk-passed_article-article_id-article-id', 'passed_article', 'article_id', 'article', 'id',
            'RESTRICT', 'CASCADE'
        );

        $this->addForeignKey(
            'fk-passed_video-video_id-video-id', 'passed_video', 'video_id', 'video', 'id',
            'RESTRICT', 'CASCADE'
        );

    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-passed_video-video_id-video-id');
        $this->dropForeignKey('fk-passed_article-article_id-article-id');
        $this->dropForeignKey('fk-passed_video-user_id-user-id');
        $this->dropForeignKey('fk-passed_article-user_id-user-id');
        $this->dropForeignKey('fk-video-level_id-level-id');
        $this->dropForeignKey('fk-article-level_id-level-id');

        $this->dropTable('settings');
        $this->dropTable('level');
        $this->dropTable('article');
        $this->dropTable('video');
        $this->dropTable('passed_article');
        $this->dropTable('passed_video');
    }

}
