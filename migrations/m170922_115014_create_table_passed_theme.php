<?php

use yii\db\Migration;

class m170922_115014_create_table_passed_theme extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%passed_theme}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'theme_id' => $this->integer()->notNull(),

        ], $tableOptions);

        $this->addForeignKey(
            'fk-passed_theme-user_id-user-id', 'passed_theme', 'user_id', 'user', 'id',
            'RESTRICT', 'CASCADE'
        );

        $this->addForeignKey(
            'fk-passed_theme-theme_id-theme-id', 'passed_theme', 'theme_id', 'theme', 'id',
            'RESTRICT', 'CASCADE'
        );

    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-passed_theme-user_id-user-id', 'passed_theme');
        $this->dropForeignKey('fk-passed_theme-theme_id-theme-id', 'passed_theme');

        $this->dropTable('passed_theme');
    }

}
