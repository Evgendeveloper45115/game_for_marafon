<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**@var $profile \app\models\Profile */

$this->title = 'Профиль';
$this->params['breadcrumbs'][] = $this->title;
$articlesPass = \app\models\PassedArticle::find()->where(['user_id' => $profile->user_id])->all();

?>
<div class="user-index">
    <?= \yii\bootstrap\Tabs::widget([
        'items' => [
            [
                'label' => 'Уровень 1',
                'content' => Yii::$app->controller->renderPartial('level', ['level' => 1, 'profile' => $profile, 'articlesPass' => $articlesPass]),
                'active' => true,
            ],
            [
                'label' => 'Уровень 2',
                'content' => Yii::$app->controller->renderPartial('level', ['level' => 2, 'profile' => $profile, 'articlesPass' => $articlesPass]),
            ],
            [
                'label' => 'Уровень 3',
                'content' => Yii::$app->controller->renderPartial('level', ['level' => 3, 'profile' => $profile, 'articlesPass' => $articlesPass]),
            ],
            [
                'label' => 'Уровень 4',
                'content' => Yii::$app->controller->renderPartial('level', ['level' => 4, 'profile' => $profile, 'articlesPass' => $articlesPass]),
            ],
            [
                'label' => 'Уровень 5',
                'content' => Yii::$app->controller->renderPartial('level', ['level' => 5, 'profile' => $profile, 'articlesPass' => $articlesPass]),
            ],
            [
                'label' => 'Уровень 6',
                'content' => Yii::$app->controller->renderPartial('level', ['level' => 6, 'profile' => $profile, 'articlesPass' => $articlesPass]),
            ],
            [
                'label' => 'Уровень 7',
                'content' => Yii::$app->controller->renderPartial('level', ['level' => 7, 'profile' => $profile, 'articlesPass' => $articlesPass]),
            ],
        ],
    ]); ?>
</div>