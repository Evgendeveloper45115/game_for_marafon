<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "profile".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $first_name
 * @property string $last_name
 * @property string $foto
 * @property integer $shape_type
 * @property integer $constitution_type
 * @property integer $level
 * @property integer $account
 * @property integer $time_on_site
 * @property integer $last_activity
 * @property integer $passed_test
 * @property integer $bought_programm
 *
 * @property Constitution $constitutionType
 * @property Shape $shapeType
 * @property User $user
 */
class Profile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'shape_type', 'constitution_type', 'level', 'account', 'time_on_site', 'last_activity', 'passed_test', 'bought_programm'], 'integer'],
            [['first_name', 'last_name', 'foto'], 'string', 'max' => 255],
            [['constitution_type'], 'exist', 'skipOnError' => true, 'targetClass' => Constitution::className(), 'targetAttribute' => ['constitution_type' => 'id']],
            [['shape_type'], 'exist', 'skipOnError' => true, 'targetClass' => Shape::className(), 'targetAttribute' => ['shape_type' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'foto' => 'Foto',
            'shape_type' => 'Shape Type',
            'constitution_type' => 'Constitution Type',
            'level' => 'Level',
            'account' => 'Account',
            'time_on_site' => 'Time On Site',
            'last_activity' => 'Last Activity',
            'passed_test' => 'Passed Test',
            'bought_programm' => 'Bought Programm',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConstitutionType()
    {
        return $this->hasOne(Constitution::className(), ['id' => 'constitution_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShapeType()
    {
        return $this->hasOne(Shape::className(), ['id' => 'shape_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    public function getAvatarSrc()
    {
        return ($this->foto) ? '/uploads/profile/' . $this->foto : '/images/no-avatar2.png';
    }

}
