<?php

use yii\db\Migration;

class m171011_133208_add_task_field_to_article extends Migration
{
    public function safeUp()
    {
        $this->addColumn('article', 'task_title', $this->text());
        $this->addColumn('article', 'task_text', $this->text());
        $this->addColumn('article', 'is_task_photo', $this->smallInteger());
    }

    public function safeDown()
    {
        $this->dropColumn('article', 'task_title');
        $this->dropColumn('article', 'task_text');
        $this->dropColumn('article', 'is_task_photo');
    }

}
