<?php

use yii\db\Migration;

class m170919_124522_create_main extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%main_page}}', [
            'id' => $this->primaryKey(),
            'image' => $this->string(),
            'link_video' => $this->string(),
            'header' => $this->string(),
            'content_block_color' => $this->string(),
            'block_color' => $this->string(),
            'description' => $this->string(),
        ], $tableOptions);

    }

    public function safeDown()
    {
        $this->dropTable('main_page');
    }


    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170919_124522_create_main cannot be reverted.\n";

        return false;
    }
    */
}
