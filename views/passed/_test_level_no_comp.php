<?php
/**
 * @var $points string
 * @var $link string
 * @var $point_img bool
 */
$questions = count($tests->testQuestionLevels);
?>
<div class="js-popup" id="no_comp">
    <div class="popup popup--share">
        <div class="popup popup--task">
            <div class="popup-head">
                <h1 class="popup__title">Вы не прошли тест!</h1>
                <h2 class="popup__subtitle">Правильных ответов <span id="success"></span>/<?= $questions ?></h2>
                <span class="popup-close"></span>
            </div>
            <div class="popup-content">
                <p class="popup-content__next-task">Изучите внимательно материалы по данной теме и попробуйте еще
                    раз!</p><a class="btn btn--popup"
                               href="<?= $link ?>">Перейти</a>
            </div>
        </div>
    </div>
</div>
