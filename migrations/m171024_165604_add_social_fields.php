<?php

use yii\db\Migration;

class m171024_165604_add_social_fields extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user', 'social_media_type', $this->string());
        $this->addColumn('user', 'social_media_id', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn('user', 'social_media_type');
        $this->dropColumn('user', 'social_media_id');
    }

}
