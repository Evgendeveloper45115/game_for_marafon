<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "article".
 *
 * @property integer $id
 * @property integer $weight
 * @property integer $level_id
 * @property string $name
 * @property string $description
 * @property string $img
 * @property integer $timer
 * @property integer $points
 * @property integer $theme_id
 * @property integer $block_color
 * @property integer $color_content
 * @property integer $imgFooter
 * @property integer $description_short
 * @property integer $type
 * @property string $img_radius
 * @property string $preview_image
 * @property string $preview_text
 * @property string $task_title
 * @property string $task_text
 * @property integer $is_task_photo
 *
 * @property Level $level
 * @property Theme $theme
 * @property PassedArticle[] $passedArticles
 * @property TaskAnswers $taskAnswer
 */
class Article extends \yii\db\ActiveRecord
{
    public $types = [
        'Теория',
        'Задание'
    ];
    const THEORY = 0;
    const TASK = 1;

    public $file;
    public $image;
    public $image_radius;
    public $preview_img;

    public function FillBlankImg()
    {
        if (!$this->img) {
            $this->img = "blank.jpg";
        }
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'timer', 'points'], 'required'],
            [['theme_id', 'description', 'img', 'level_id', 'block_color', 'color_content', 'description_short', 'imgFooter', 'type', 'img_radius', 'preview_image', 'preview_text'], 'safe'],
            [['level_id', 'timer', 'points', 'weight', 'is_task_photo'], 'integer', 'max' => 999999999],
            [['file'], 'file', 'extensions' => 'gif, jpg, jpeg, png'],
            [['name', 'img'], 'string', 'max' => 255],
            [['task_title', 'task_text'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'level_id' => 'Уровень',
            'theme_id' => 'Тема',
            'name' => 'Название статьи',
            'description' => 'Контент',
            'img' => 'Изображение',
            'timer' => 'Таймер',
            'points' => 'Количество баллов',
            'file' => 'Файл изображения в шапке',
            'block_color' => 'Цвет рамочки',
            'color_content' => 'Контент в рамочке',
            'description_short' => 'короткое описание',
            'image' => 'Картинка',
            'weight' => 'Вес(для сортировки)',
            'type' => 'Тип(задание или статья)',
            'img_radius' => 'Картинка в кружке',
            'image_radius' => 'Картинка в кружке',
            'preview_img' => 'Картинка для превью в теме',
            'preview_text' => 'Текст для превью в теме',
            'task_title' => 'Заголовок названия задания',
            'task_text' => 'Текст задания',
            'is_task_photo' => 'В задании есть загрузка фото?',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLevel()
    {
        return $this->hasOne(Level::className(), ['id' => 'level_id']);
    }

    public function getTaskAnswer()
    {
        return $this->hasOne(TaskAnswers::className(), ['id' => 'level_id']);
    }

    public function getTheme()
    {
        return $this->hasOne(Theme::className(), ['id' => 'theme_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPassedArticles()
    {
        return $this->hasMany(PassedArticle::className(), ['article_id' => 'id']);
    }

    public function dropOldImg()
    {
        if ($this->img) {
            $path_to_file = Yii::getAlias('@abs_path_to_uploads') . '/' . $this->img;

            if (file_exists($path_to_file) && $this->img != 'blank.jpg') {
                unlink($path_to_file);
                $this->img = 'blank.jpg';
                $this->save(false);
            }
        }
    }

    public function dropOldImg2()
    {
        if ($this->imgFooter) {
            $path_to_file = Yii::getAlias('@abs_path_to_uploads') . '/' . $this->imgFooter;

            if (file_exists($path_to_file) && $this->imgFooter != 'blank.jpg') {
                unlink($path_to_file);
                $this->imgFooter = 'blank.jpg';
                $this->save(false);
            }
        }
    }

    public function dropOldImg3()
    {
        if ($this->img_radius) {
            $path_to_file = Yii::getAlias('@abs_path_to_uploads') . '/' . $this->img_radius;

            if (file_exists($path_to_file) && $this->img_radius != 'blank.jpg') {
                unlink($path_to_file);
                $this->img_radius = 'blank.jpg';
                $this->save(false);
            }
        }
    }

    public function dropOldImg4()
    {
        if ($this->img_radius) {
            $path_to_file = Yii::getAlias('@abs_path_to_uploads') . '/' . $this->preview_image;

            if (file_exists($path_to_file) && $this->preview_image != 'blank.jpg') {
                unlink($path_to_file);
                $this->preview_image = 'blank.jpg';
                $this->save(false);
            }
        }
    }

    public function dropOldPreview()
    {
        if ($this->preview_image) {
            $path_to_file = Yii::getAlias('@abs_path_to_uploads') . '/' . $this->preview_image;

            if (file_exists($path_to_file) && $this->preview_image != 'blank.jpg') {
                unlink($path_to_file);
                $this->preview_image = 'blank.jpg';
                $this->save(false);
            }
        }
    }

    /*    public function my_required($attribute_name, $params)
        {
            if (empty($this->description) && empty($this->description))
            {
                $this->addError($attribute_name, 'Загрузите картинку или добавьте описание');
                return false;
            }
            return true;
        }*/

}
