<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;
use yii\helpers\VarDumper;

/**
 * UserSearch represents the model behind the search form about `app\models\User`.
 */
class UserSearch extends User
{
    public $bought_programm;
    public $account;
    public $role;
    public $time_on_site;
    public $level;
    public $passed_test;
    public $first_name;
    public $last_activity;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['auth_key', 'password_hash', 'password_reset_token', 'email', 'bought_programm', 'account', 'role', 'level', 'first_name', 'last_activity'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find()->joinWith('profile');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['bought_programm'] = [
            'asc' => ['profile.bought_programm' => SORT_ASC],
            'desc' => ['profile.bought_programm' => SORT_DESC],
        ];

        $dataProvider->sort->defaultOrder = [
            'id' => SORT_DESC,
        ];

        $dataProvider->sort->attributes['account'] = [
            'asc' => ['profile.account' => SORT_ASC],
            'desc' => ['profile.account' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['time_on_site'] = [
            'asc' => ['profile.account' => SORT_ASC],
            'desc' => ['profile.account' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['last_activity'] = [
            'asc' => ['profile.last_activity' => SORT_ASC],
            'desc' => ['profile.last_activity' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['level'] = [
            'asc' => ['profile.account' => SORT_ASC],
            'desc' => ['profile.account' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['passed_test'] = [
            'asc' => ['profile.passed_test' => SORT_ASC],
            'desc' => ['profile.passed_test' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['first_name'] = [
            'asc' => ['profile.first_name' => SORT_ASC],
            'desc' => ['profile.first_name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'bought_programm' => $this->bought_programm,
            'account' => $this->account,
            'time_on_site' => $this->time_on_site,
            'passed_test' => $this->passed_test,
        ]);
        $query->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'profile.bought_programm', $this->bought_programm])
            ->andFilterWhere(['like', 'profile.account', $this->account])
            ->andFilterWhere(['like', 'profile.time_on_site', $this->time_on_site])
            ->andFilterWhere(['like', 'profile.level', $this->level])
            ->andFilterWhere(['like', 'profile.passed_test', $this->passed_test])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'profile.first_name', $this->first_name]);

        return $dataProvider;
    }
}
