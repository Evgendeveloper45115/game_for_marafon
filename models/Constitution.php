<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "constitution".
 *
 * @property integer $id
 * @property string $name
 * @property string $foto
 *
 * @property Profile[] $profiles
 */
class Constitution extends \yii\db\ActiveRecord
{
    public $file;

    public function FillBlankImg(){
        if (!$this->foto){
            $this->foto = "blank.jpg";
        }
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'constitution';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'foto'], 'required'],
            [['name', 'foto'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'foto' => 'Foto',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfiles()
    {
        return $this->hasMany(Profile::className(), ['constitution_type' => 'id']);
    }

    public function dropOldImg()
    {
        if($this->foto){
            $path_to_file = Yii::getAlias('@abs_path_to_uploads') . '/' . $this->foto;

            if (file_exists($path_to_file) && $this->foto != 'blank.jpg')
            {
                unlink($path_to_file);
                $this->foto = 'blank.jpg';
                $this->save(false);
            }
        }
    }

}
