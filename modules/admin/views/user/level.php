<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**@var $profile \app\models\Profile */
/**@var $passedArt \app\models\PassedArticle */
/**@var $passedVid \app\models\PassedVideo */
/**@var $videos \app\models\Video [] */
/**@var $articles \app\models\Article [] */
/**@var $level int */

$this->title = 'Профиль';
$this->params['breadcrumbs'][] = $this->title;
$user_id = $profile->user_id;
$count = 0;
if ($level !== 7) {
    $articles = \app\models\Article::find()->where(['level_id' => $level])->all();
    $videos = \app\models\Video::find()->where(['level_id' => $level])->all();
    $tests = \app\models\Test::find()->where(['level_id' => $level])->all()
    ?>
    <div class="user-index">
        <div style="font-size: 18px">Статьи:</div>
        <div class="row">
            <?php
            foreach ($articles as $key => $article) {
                ?>
                <div class="col-md-12" style="display: inline-flex">
                    <?php
                    $passedArt = \app\models\PassedArticle::find()->where(['article_id' => $article->id, 'user_id' => $user_id])->one();
                    // if ($passedArt->article->level_id == $level) {
                    ?>
                    <div style="padding: 1vw"><span
                                style="font-weight: 800">Статья:</span> <?= Html::a($article->name, \yii\helpers\Url::to(['/article', 'id' => $article->id])) ?>
                    </div>
                    <div style="padding: 1vw"><span style="font-weight: 800">Баллы: </span><?= $article->points ?></div>
                    <div style="padding: 1vw"><span
                                style="font-weight: 800">Статус: </span><?= $article->id == $passedArt->article_id ? 'Прошел +' . $article->points : 'Не прошел' ?>
                    </div>
                    <?php
                    $tA = \app\models\TaskAnswers::find()->where(['article_id' => $passedArt->article_id, 'user_id' => $user_id])->one();
                    if ($tA) {
                        echo \kartik\popover\PopoverX::widget([
                            'header' => 'Комментарий',
                            'options' => ['id' => $key.$level],
                            'type' => \kartik\popover\PopoverX::TYPE_INFO,
                            'placement' => \kartik\popover\PopoverX::ALIGN_RIGHT,
                            'content' => '<div>' . Html::img('/uploads/user_uploads/' . $tA->foto, ['style' => 'width:100%']) . '</div></br><div>' . $tA->text . '</div>',
                            'toggleButton' => ['label' => 'Комментарий!', 'class' => 'btn btn-info'],
                        ]);
                    }
                    ?>
                </div>
                <?php
                //  }
            }
            ?>
        </div>

        <div style="font-size: 18px">Видео:</div>
        <div class="row">
            <?php
            if (!empty($videos)) {
                foreach ($videos as $key => $video) {
                    ?>
                    <div class="col-md-12" style="display: inline-flex">
                        <?php
                        $passedVid = \app\models\PassedVideo::find()->where(['video_id' => $video->id, 'user_id' => $user_id])->one();
                        // if ($passedArt->article->level_id == $level) {
                        ?>
                        <div style="padding: 1vw"><span
                                    style="font-weight: 800">Видео:</span> <?= Html::a($video->name, \yii\helpers\Url::to(['/video', 'id' => $video->id])) ?>
                        </div>
                        <div style="padding: 1vw"><span style="font-weight: 800">Баллы: </span><?= $video->points ?>
                        </div>
                        <div style="padding: 1vw"><span
                                    style="font-weight: 800">Статус: </span><?= $video->id == $passedVid->video_id ? 'Прошел +' . $video->points : 'Не прошел' ?>
                        </div>
                        <?php
                        $q = "<div class=\"video__frame\" style='width: 100%;'>
                        <a class=\"video__frame-link js-venobox js-video vbox-item\" href=\"$video->code \" data-autoplay=\"true\"
                           data-vbtype=\"video\">
                            <img style='width: 100%' src=\"" . Yii::getAlias('@rel_path_to_uploads') . '/' . $video->img . "\" alt=\"\">
                        </a>
                    </div>";
                        ?>

                        <?php
                        echo \kartik\popover\PopoverX::widget([
                            'header' => 'Видео-урок',
                            'options' => ['id' => $level.$key . 'vid'],
                            'type' => \kartik\popover\PopoverX::TYPE_INFO,
                            'placement' => \kartik\popover\PopoverX::ALIGN_RIGHT,
                            'content' => $q,
                            'toggleButton' => ['label' => 'Посмотри видео!', 'class' => 'btn btn-danger'],
                        ]);
                        ?>
                    </div>
                    <?php
                    //  }
                }
            } else {
                echo 'Видео на данном уровне нету=(';
            }
            ?>
        </div>
        <div style="font-size: 18px">Тесты:</div>
        <div class="row">
            <?php
            if (!empty($tests)) {
                foreach ($tests as $key => $test) {
                    ?>
                    <div class="col-md-12" style="display: inline-flex">
                        <?php
                        $passedTest = \app\models\PassedTest::find()->where(['test_id' => $test->id, 'user_id' => $user_id])->one();
                        // if ($passedArt->article->level_id == $level) {
                        ?>
                        <div style="padding: 1vw"><span
                                    style="font-weight: 800">Тест:</span> <?= Html::a($test->name, \yii\helpers\Url::to(['/test/pravilnoe-pitanie', 'level' => $test->id])) ?>
                        </div>
                        <div style="padding: 1vw"><span style="font-weight: 800">Баллы: </span><?= $test->point ?></div>
                        <div style="padding: 1vw"><span
                                    style="font-weight: 800">Статус: </span><?= $test->id == $passedTest->test_id ? 'Прошел +' . $test->point : 'Не прошел' ?>
                        </div>
                    </div>
                    <?php
                    //  }
                }
            } else {
                echo "Тестов на этом уровне нету=(";
            }
            ?>
        </div>
    </div>
    <?php
} else {
    ?>
    <div class="user-index">
        <h2>Финальное тестирование:</h2>
        <div>
            Статус: <?= $profile->passed_test == 0 ? ($profile->user->testAnswer ? 'Не прошел но попытка была' : 'Не прошел попыток не было') : ('Пройденно, правильных ответов: ' . $profile->user->testAnswer) ?></div>
    </div>
    <?php
}