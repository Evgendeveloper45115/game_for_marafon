<?php

namespace app\components;

use Yii;
use app\models\Profile;
use app\models\User;
use yii\helpers\VarDumper;

class Time
{

    public static function update()
    {

        $user_id = Yii::$app->user->getId();
        $duration = 0;

        if ($user_id) {
            $profile = Yii::$app->db->createCommand('SELECT `last_activity`, `time_on_site` FROM profile WHERE user_id=:user_id')
                ->bindValue(':user_id', $user_id)
                ->queryOne();

            $duration = time() - $profile['last_activity'];

            if($duration > 300) $duration = 300;

            $time_on_site = $profile['time_on_site'] + $duration;

            Yii::$app->db->createCommand()->update('profile', ['last_activity' => time(), 'time_on_site' => $time_on_site], 'user_id=:user_id')
                ->bindValue(':user_id', $user_id)
                ->execute();
        }
    }

}