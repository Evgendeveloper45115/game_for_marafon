<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\forms\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div id=top_wrap class=align>
    <h1 style="border-bottom: none"> <a href="/" class="logo"></a></h1>
    <div id=sign_in_wrap>
        <p id=sign_in_title>Вход</p>
        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'options' => ['class' => 'form-horizontal', 'name' => 'sign_in'],
            'fieldConfig' => [
                'options' => ['class' => 'form_group clearfix'],
                'template' => "{label}\n<div class=\"\">{input}\n<div class=\"error\">{error}</div></div>",
            ],
        ]); ?>

        <?= $form->field($model, 'email')->textInput(['class' => 'sing_input', 'placeholder' => 'Логин(E-mail)'])->label(false) ?>

        <?= $form->field($model, 'password')->passwordInput(['class' => 'sing_input', 'placeholder' => 'Пароль'])->label(false) ?>


        <div id=form_action>
            <a href="/site/request-password-reset">Забыли пароль</a>
            <button>Отправить</button>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

