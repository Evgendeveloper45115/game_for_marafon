<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Review;
use app\models\ReviewSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ReviewController implements the CRUD actions for Review model.
 */
class ReviewController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Review models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ReviewSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Review model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Review model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Review();

        if ($model->load(Yii::$app->request->post())) {

            $model->file_before = UploadedFile::getInstance($model, 'file_before');
            if($model->file_before){
                $model->dropPhotoBefore();
                $file_name = uniqid();
                $model->photo_before = $file_name . '.' . $model->file_before->extension;
                if (!$model->file_before->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/' . $file_name . '.' . $model->file_before->extension)){
                    throw new \RuntimeException('Ошибка сохранения файла');
                }
                $model->file_before = null;
            }

            $model->file_after = UploadedFile::getInstance($model, 'file_after');
            if($model->file_after){
                $model->dropPhotoAfter();
                $file_name = uniqid();
                $model->photo_after = $file_name . '.' . $model->file_after->extension;
                if (!$model->file_after->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/' . $file_name . '.' . $model->file_after->extension)){
                    throw new \RuntimeException('Ошибка сохранения файла');
                }
                $model->file_after = null;
            }

            $model->save();

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Review model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $model->file_before = UploadedFile::getInstance($model, 'file_before');
            if($model->file_before){
                $model->dropPhotoBefore();
                $file_name = uniqid();
                $model->photo_before = $file_name . '.' . $model->file_before->extension;
                if (!$model->file_before->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/' . $file_name . '.' . $model->file_before->extension)){
                    throw new \RuntimeException('Ошибка сохранения файла');
                }
                $model->file_before = null;
            }

            $model->file_after = UploadedFile::getInstance($model, 'file_after');
            if($model->file_after){
                $model->dropPhotoBefore();
                $file_name = uniqid();
                $model->photo_after = $file_name . '.' . $model->file_after->extension;
                if (!$model->file_after->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/' . $file_name . '.' . $model->file_after->extension)){
                    throw new \RuntimeException('Ошибка сохранения файла');
                }
                $model->file_after = null;
            }

            $model->save(false);

            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Review model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Review model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Review the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Review::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
