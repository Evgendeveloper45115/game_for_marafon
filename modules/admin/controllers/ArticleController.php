<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Article;
use app\models\ArticleSearch;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Article model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Article model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Article();
        $model->weight = 5;
        $model->FillBlankImg();

        if ($model->load(Yii::$app->request->post())) {

            $model->file = UploadedFile::getInstance($model, 'file');
            if($model->file){
                $model->dropOldImg();
                $file_name = uniqid();
                $model->img = $file_name . '.' . $model->file->extension;
                if (!$model->file->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/' . $file_name . '.' . $model->file->extension)){
                    throw new \RuntimeException('Ошибка сохранения файла');
                }
                $model->file = null;
            }
            $model->image = UploadedFile::getInstance($model, 'image');
            if($model->image){
                $model->dropOldImg2();
                $file_name = uniqid();
                $model->imgFooter = $file_name . '.' . $model->image->extension;
                if (!$model->image->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/' . $file_name . '.' . $model->image->extension)){
                    throw new \RuntimeException('Ошибка сохранения файла');
                }
                $model->image = null;
            }
            $model->image_radius = UploadedFile::getInstance($model, 'image_radius');
            if ($model->image_radius) {
                $model->dropOldImg();
                $file_name = uniqid();
                $model->img_radius = $file_name . '.' . $model->image_radius->extension;
                if (!$model->image_radius->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/' . $file_name . '.' . $model->image_radius->extension)) {
                    throw new \RuntimeException('Ошибка сохранения файла');
                }
                $model->image_radius = null;
            }
            $model->preview_img = UploadedFile::getInstance($model, 'preview_img');
            if ($model->preview_img) {
                $model->dropOldImg4();
                $file_name = uniqid();
                $model->preview_image = $file_name . '.' . $model->preview_img->extension;
                if (!$model->preview_img->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/' . $file_name . '.' . $model->preview_img->extension)) {
                    throw new \RuntimeException('Ошибка сохранения файла');
                }
                $model->preview_img = null;
            }


            $model->save();
            return $this->redirect('index');
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Article model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if($model->file){
                $model->dropOldImg();
                $file_name = uniqid();
                $model->img = $file_name . '.' . $model->file->extension;

                if (!$model->file->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/' . $file_name . '.' . $model->file->extension)){
                    throw new \RuntimeException('Ошибка сохранения файла');
                }
                $model->file = null;
            }
            $model->image = UploadedFile::getInstance($model, 'image');
            if($model->image){
                $model->dropOldImg2();
                $file_name = uniqid();
                $model->imgFooter = $file_name . '.' . $model->image->extension;
                if (!$model->image->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/' . $file_name . '.' . $model->image->extension)){
                    throw new \RuntimeException('Ошибка сохранения файла');
                }
                $model->image = null;
            }
            $model->image_radius = UploadedFile::getInstance($model, 'image_radius');
            if ($model->image_radius) {
                $model->dropOldImg3();
                $file_name = uniqid();
                $model->img_radius = $file_name . '.' . $model->image_radius->extension;
                if (!$model->image_radius->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/' . $file_name . '.' . $model->image_radius->extension)) {
                    throw new \RuntimeException('Ошибка сохранения файла');
                }
                $model->image_radius = null;
            }
            $model->preview_img = UploadedFile::getInstance($model, 'preview_img');
            if ($model->preview_img) {
                $model->dropOldImg4();
                $file_name = uniqid();
                $model->preview_image = $file_name . '.' . $model->preview_img->extension;
                if (!$model->preview_img->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/' . $file_name . '.' . $model->preview_img->extension)) {
                    throw new \RuntimeException('Ошибка сохранения файла');
                }
                $model->preview_img = null;
            }

            if($model->save()){
                return $this->redirect('index');
            }else{
                VarDumper::dump($model->getErrors(),11,1);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Article model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->dropOldImg();
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDeleteImg()
    {
        if(Yii::$app->request->post()){
            $id = Yii::$app->request->post('id');
            $this->findModel($id)->dropOldImg();
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionDeleteImg2()
    {
        if(Yii::$app->request->post()){
            $id = Yii::$app->request->post('id');
            $this->findModel($id)->dropOldImg2();
        }

        return $this->redirect(Yii::$app->request->referrer);
    }
    public function actionDeleteImg3()
    {
        if(Yii::$app->request->post()){
            $id = Yii::$app->request->post('id');
            $this->findModel($id)->dropOldImg3();
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionDeleteImg4()
    {
        if(Yii::$app->request->post()){
            $id = Yii::$app->request->post('id');
            $this->findModel($id)->dropOldImg4();
        }

        return $this->redirect(Yii::$app->request->referrer);
    }


    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
