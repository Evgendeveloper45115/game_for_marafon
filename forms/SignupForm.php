<?php
namespace app\forms;

use yii\base\Model;
use app\models\User;
use app\models\Profile;
use app\models\TriggersMail;
use yii\db\Exception;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $name;
    public $email;
    public $password;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            [['email', 'password'], 'required'],
            ['email', 'email'],
            [['email', 'name'], 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'Данный email уже есть в базе.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * @return User
     * @throws Exception
     */
    public function signup()
    {
        if (!$this->validate()) {
            throw new Exception('Ошибка при валидации формы');
        }
        
        $user = new User();
        $user->email = $this->email;
        $user->role = User::ROLE_USER;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        
        if (!$user->save()){
            throw new Exception('Ошибка при создании пользователя');
        }

        $profile = new Profile();
        $profile->user_id = $user->id;
        $profile->first_name = $this->name;
        $profile->last_activity = time();
        $profile->save();

        $triggers_mail = new TriggersMail();
        $triggers_mail->user_id = $user->id;
        $triggers_mail->save();

        return $user;
    }
}
