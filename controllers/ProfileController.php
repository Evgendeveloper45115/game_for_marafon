<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\helpers\VarDumper;
use yii\helpers\Url;
use app\forms\PhotoForm;
use app\forms\ShapeForm;
use app\forms\ConstitutionForm;
use yii\web\UploadedFile;
use app\models\User;
use app\models\Measure;
use app\models\Shape;
use app\models\Constitution;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;


class ProfileController extends Controller
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $user = Yii::$app->user->identity;
        $photo_form = new PhotoForm();

        $measure = new Measure();

        $shape_form = new ShapeForm(['shape' => 1]);
        $constitution_form = new ConstitutionForm();
        $shapes = ArrayHelper::map(Shape::find()->asArray()->orderBy('id')->all(), 'id', 'name');
        $constitutions = ArrayHelper::map(Constitution::find()->asArray()->orderBy('id')->all(), 'id', 'name');
        $shapes_images = ArrayHelper::map(Shape::find()->asArray()->orderBy('id')->all(), 'id', 'foto');
        $constitutions_images = ArrayHelper::map(Constitution::find()->asArray()->orderBy('id')->all(), 'id', 'foto');
        $shape_type_img = Shape::findOne(['id' => $user->profile->shape_type])->foto;
        $constitution_type_img = Constitution::findOne(['id' => $user->profile->constitution_type])->foto;
        $old_measures = Measure::findAll(['user_id' => $user->id]);
        $avatar_img = $user->profile->foto;

        if (!$user){
            Yii::$app->controller->redirect(Url::to('/site/login'));
        }

        if ($measure->load(Yii::$app->request->post()) && $measure->validate()) {
            $measure->save();
            Yii::$app->session->setFlash('success', 'Ваши замеры отправлены');
        }

        return $this->render('index',[
            'user' => $user,
            'photo_form' => $photo_form,
            'measure' => $measure,
            'shape_form' => $shape_form,
            'shapes' => $shapes,
            'constitutions' => $constitutions,
            'constitution_form' => $constitution_form,
            'shape_type_img' => $shape_type_img,
            'constitution_type_img' => $constitution_type_img,
            'old_measures' => $old_measures,
            'avatar_img' => $avatar_img,
            'shapes_images' => $shapes_images,
            'constitutions_images' => $constitutions_images,
        ]);
    }

    public function actionSaveMeasure(){
        $model = new Measure();
        $model->user_id = Yii::$app->user->getId();
        $model->date = time();

        if ($model->load(Yii::$app->request->post())) {

            if(!$model->validate()){
                return ActiveForm::validate($model);
            }
            $model->save();

            return $this->redirect(Yii::$app->request->referrer);
        }

       /* if(\Yii::$app->request->isAjax){
            if ($model->load(Yii::$app->request->post())) {
                if(!$model->validate()){
                    return ActiveForm::validate($model);
                }
                $model->save();
                //Yii::$app->session->setFlash('success', 'Ваши замеры отправлены');
                return $this->renderPartial('_measure');
            }
        }*/

    }

    public function actionSaveAvatar()
    {
        $user = User::findOne(['id' => Yii::$app->user->getId()]);
        $model = new PhotoForm();

        if ($model->load(Yii::$app->request->post())) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if($model->file){
                $user->dropOldAvatar();
                $file_name = uniqid();
                $user->profile->foto = $file_name . '.' . $model->file->extension;
                if (!$model->file->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/profile/' . $file_name . '.' . $model->file->extension)){
                    throw new \RuntimeException('Ошибка сохранения файла');
                }
                $model->file = null;
                $user->profile->save();
            }
        }

        return $this->redirect(Yii::$app->request->referrer);

    }


    public function actionSaveShape()
    {
        $model = new ShapeForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user = User::findOne(['id' => Yii::$app->user->getId()]);
            $user->profile->shape_type = $model->shape;
            $user->profile->save();
        }

        return $this->redirect(Yii::$app->request->referrer);

    }

    public function actionSaveConstitution()
    {
        $model = new ConstitutionForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user = User::findOne(['id' => Yii::$app->user->getId()]);
            $user->profile->constitution_type = $model->constitution;
            $user->profile->save();
        }

        return $this->redirect(Yii::$app->request->referrer);

    }

}
