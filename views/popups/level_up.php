<?php
/**
 * @var $levelUp \app\models\LevelUp
 */
$levelUp = \app\models\LevelUp::find()
    ->where(['level' => Yii::$app->user->identity->profile->level])
    ->one();
?>

    <a style="display:none;" class="btn js-venobox vbox-item" id="time_popup" href="#level-up"
       data-vbtype="inline">task complete 2</a>
    <div class="js-popup" id="level-up">
        <div class="popup popup--level fireworks fireworks--popup">
            <div class="popup-head">
                <h1 class="popup__title">Поздравлям вас!</h1>

                <h2 class="popup__subtitle">Вы получили
                    уровень <?= Yii::$app->user->identity->profile->level ?></h2><span
                    class="popup-close"></span>
                <?php
                if(isset($text) && $text != ''){
                 ?>
                    <h3 style="margin-top: 1vw;" class="popup__subtitle"><?=$text?></h3>
                    <?php
                }
                ?>

            </div>
            <div class="popup-content clearfix">
                <div class="popup-content__img col-4">
                    <img src="/uploads/levelUp/<?= $levelUp->image ?>">
                </div>
                <div class="popup-content__right col-8">
                    <h3 class="popup-content__title"><?= $levelUp->title ?></h3>

                    <div class="popup-content__text"><?= $levelUp->description ?></div>
                    <a class="btn btn--popup"
                       href="<?= '/level/index/' . Yii::$app->user->identity->profile->level ?>">Перейти
                        на следующий уровень</a>
                </div>
            </div>
        </div>
    </div>
<?php
$js = <<<JS
        $(function(){
                $('#time_popup').trigger('click');
                
            window.setTimeout(function(){
            var venoOptions = {
                // is called after plugin initialization.
                cb_init: function(plugin){
                    $('.vbox-close').css('left', '-1000px');
                },
            }
            $('.venobox').venobox(venoOptions);
        }, 200);
                
    })
JS;
$this->registerJs($js);
?>