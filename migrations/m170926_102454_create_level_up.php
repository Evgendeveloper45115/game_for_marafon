<?php

use yii\db\Migration;

class m170926_102454_create_level_up extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%level_up}}', [
            'id' => $this->primaryKey(),
            'level' => $this->integer(),
            'title' => $this->string(),
            'image' => $this->string(),
            'description' => $this->text(),

        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('level_up');
    }

}
