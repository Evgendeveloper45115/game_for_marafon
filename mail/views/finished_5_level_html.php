<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title>Email</title>
    <style type="text/css">


        * {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 15px; /*outline: 1px solid red;*/
        }

        div, p, span, strong, b, em, i, a, li, td {
            -webkit-text-size-adjust: none;
        }

        img {
            max-width: 100%;
            text-align: center;
        }

        @media (min-width: 749px) {

        }

        @media only screen and (max-device-width: 748px), only screen and (max-width: 748px) {
            img[class="block"] {
                display: none !important;
            }

            body {
                margin: 0 auto;
                max-width: 320px;
            }

            table {
                width: 320px !important;
            }

            span.size28 {
                font-size: 28px !important;
            }

            .block {
                display: block;
                width: 257px !important;
                margin: 0 auto !important;
            }

            .margin0 {
                margin: 0 !important;
            }

            .margin_bl {
                display: block;
                margin: 0 auto 28px;
            }

            .button {
                width: 295px !important;
                margin-top: 30px !important;
            }

            .padding {
                padding: 6px 0 0 32px !important;
            }

            .block2 {
                display: table;
                width: 145px !important;
                vertical-align: bottom;
            }

            .block3 {
                width: 140px !important;
                display: table;
            }

            .block4 {
                width: 140px !important;
                display: table;
            }

            .height0 {
                height: 0 !important;
            }

            .center {
                font-size: 14px !important;
            }

            .width155 {
                width: 147px !important;
            }

            .widht150 {
                width: 150px !important;
            }

            .height {
                height: 0 !important;
            }

            .float {
                float: none !important;
                margin: 0 auto !important;
            }

            .none {
                display: none !important;
            }

            .width100 {
                width: 100% !important;
            }

            .paddi {
                padding: 6px 0 0 0 !important;
            }
        }
    </style>
</head>
<body style="margin: 0 auto;padding: 0;">


<table cellpadding="0" cellspacing="0" width="100%" border="0"
       style="min-width:300px;max-width: 740px; border-collapse:collapse;" align="center">
    <tr>
        <td align="center">

            <table cellpadding="0" cellspacing="0" width="100%" border="0"
                   style="min-width:300px;max-width: 740px; border-collapse:collapse;background: url(https://preview.ibb.co/eZZcFF/bg_1.jpg) no-repeat top center;background-size: cover;"
                   align="center" bgcolor="#55bd9d">
                <tr>
                    <td align="center">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0"
                               style="border-collapse:collapse;max-width: 500px;" align="center">
                            <tr>
                                <td height="398" align="center" valign="top">
                                    <table align="center"
                                           style="max-width: 500px; margin-top: 30px;border-collapse:collapse;"
                                           cellspacing="0" cellpadding="0" border="0" width="100%">
                                        <tr>
                                            <td align="center" width="96">
                                                <a href="#" target="_blank">
                                                    <img src="https://image.ibb.co/hk8tpa/logo.png" alt="">
                                                </a>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td width="1" height="10"></td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <span style="display: block; font-family: 'Roboto Slab', serif; font-size: 34px;color: #ffffff;text-align: center;"
                                                      class="size28"><br/>Настоящая мотивация </span>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td width="1" height="80"></td>
                                        </tr>
                                    </table>

                                    <table width="100%" align="center" border="0"
                                           style="border-collapse:collapse;max-width: 500px;min-width: 300px;">
                                        <tr>
                                            <td align="center" valign="top">
                                                <a href="http://feedback.beauty-matrix.ru?utm_source=email&utm_medium=game5&utm_campaign=motivation"
                                                   target=""
                                                   style="background: #1fe2a6;border-radius: 50px;display: block;min-width: 200px; max-width: 330px;height: 37px;margin:0 auto;padding:20px 0 0;text-align: center;font-weight: bold;font-family: 'Roboto Slab', serif;text-transform: uppercase;font-size: 15px;color: #ffffff;text-decoration: none;"
                                                   class="button">посмотреть 100 отзывов</a>
                                            </td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </table>


            <table cellpadding="0" cellspacing="0" width="100%" border="0"
                   style="border-collapse:collapse;max-width: 300px;" align="center">
                <tr>
                    <td width="1" height="50"></td>
                </tr>
                <tr>
                    <td width="220" align="center">
                        <img src="http://i63.tinypic.com/339lxs5.jpg" alt="">
                    </td>
                </tr>

                <tr>
                    <td width="1" height="25"></td>
                </tr>

                <tr>
                    <td align="center">
                        <span style="text-align: center;font-size: 17px;text-transform: uppercase;color: #676767;">Добрый день!</span><br>
                        <p style="line-height: 0px; font-weight: 300;font-size: 17px;color:#676767;text-align: center;">
                            На связи Анна из Beauty-Matrix</p>

                    </td>
                </tr>

                <tr>
                    <td width="1" height="10"></td>
                </tr>
            </table>


            <table cellpadding="0" cellspacing="0" width="100%" border="0"
                   style="border-collapse:collapse;max-width: 670px;min-width: 300px;" align="center">
                <tr>
                    <td align="center">

                        <table width="100%" align="left" border="0"
                               style="border-collapse:collapse; max-width:670px;min-width: 300px;">
                            <tr>
                                <td>
                                    <table align="left" width="100%" border="0"
                                           style="border-collapse:collapse;max-width:650px;">
                                        <tr>
                                            <td width="1" height="0"></td>
                                        </tr>
                                        <tr>
                                            <td align="left" width="100%"
                                                style="color: #676767;line-height: 25px;font-weight: 300;font-size: 17px;"
                                                class="widht300 margin0 center"><br/>Я считаю, что лучше всего
                                                мотивируют на изменения живые <span
                                                        style="color: #b460d2; font-weight: 600;" class="center">истории обыкновенных людей</span>,
                                                которые смогли похудеть, а не моделей из глянцевых журналов. Глядя на
                                                них, у вас возникает ощущение, что если она смогла, то <span
                                                        style="color: #b460d2; font-weight: 600;" class="center">получится и у меня</span>.
                                                И в этом нет ничего плохого, ведь, то что эти истории происходят не с
                                                кем-то с другой планеты, а вполне с реальными людьми, наполняет вас
                                                уверенностью в том, что у вас тоже все получится.
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td width="1" height="10"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>


            <table cellpadding="0" cellspacing="0" width="100%" border="0"
                   style="border-collapse:collapse;max-width: 750px;min-width: 300px;" align="center">
                <tr>
                    <td width="1" height="20"></td>
                </tr>
                <tr>
                    <td align="center">
                        <img src="https://preview.ibb.co/dnXRaF/bg_2.png" alt="" width="740">
                    </td>
                </tr>
            </table>

        </td>
    </tr>
</table>


<table width="100%" align="center" border="0" style="border-collapse:collapse;max-width:850px;min-width: 300px;">

    <tr>
        <td width="1" height="20"></td>
    </tr>

    <tr>
        <td>
            <table cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse;max-width:850px;"
                   width="100%" align="center">
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" border="0"
                               style="border-collapse:collapse;max-width:850px;" width="100%">
                            <tr>
                                <td align="center" width="200"
                                    style="max-width:200px;min-width: 150px;display: inline-block;margin: 0 0 0 60px;">
                                    <img src="https://image.ibb.co/c7ahhv/icon_2.png" alt="">
                                    <p style="color: #7a7a7a;font-size: 17px;">более 4500 участниц <br> прошли программу
                                    </p>
                                </td>


                                <td align="center" width="170"
                                    style="max-width:170px;min-width: 150px;display: inline-block;margin: 0 0 0 70px;">
                                    <img src="https://image.ibb.co/do1KvF/icon_1.png" alt="">
                                    <p style="color: #7a7a7a;font-size: 17px;">Индивидуальный <br> подход</p>
                                </td>


                                <td align="center" width="220"
                                    style="max-width:220px;min-width: 150px;display: inline-block;margin: 0 0 0 50px;">
                                    <img src="https://image.ibb.co/ffJtNv/icon_3.png" alt="">
                                    <p style="color: #7a7a7a;font-size: 17px;">На 12 743 кг похудели <br> наши участницы
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td width="1" height="20"></td>
                </tr>

                <table width="100%" align="center" border="0"
                       style="border-collapse:collapse;max-width: 690px;min-width: 300px;">
                    <tr>
                        <td>
                            <span style="color: #676767;line-height: 25px;font-weight: 300;font-size: 17px;">Программу похудени Beauty Matrix, автором которой я являюсь прошло уже более <span
                                        style="color: #b460d2;font-weight: 600;">4500 участниц</span>. Специально для вас я собрала более <span
                                        style="color: #b460d2;font-weight: 600;">100 отзывов</span> обычных девушек с обычными проблемами и лишним весом. Я верю, что вы сможете среди этих отзывов найти ту же проблему, которая очень похожа на вашу и это смотивирует и даст вам силы <span
                                        style="color: #b460d2;font-weight: 600;">меняться</span> в лучшую сторону, в сторону фигуры <span
                                        style="color: #b460d2;font-weight: 600;">вашей мечты</span>.  </span>
                        </td>
                    </tr>
                </table>

                <tr>
                    <td width="1" height="50"></td>
                </tr>

                <tr>
                    <td>
                        <table width="100%" align="center" border="0"
                               style="border-collapse:collapse;max-width: 500px;min-width: 300px;">
                            <tr>
                                <td align="center" valign="top">
                                    <a href="http://feedback.beauty-matrix.ru?utm_source=email&utm_medium=game5&utm_campaign=motivation"
                                       target=""
                                       style="background: #1fe2a6;border-radius: 50px;display: block;width: 330px;height: 37px;margin:0 auto;padding:20px 0 0;text-align: center;font-weight: bold;font-family: 'Roboto Slab', serif;text-transform: uppercase;font-size: 15px;color: #ffffff;text-decoration: none;"
                                       class="button">посмотреть истории</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td width="1" height="30"></td>
    </tr>

</table>

<table>
    <tr>
        <td width="1" height="60"></td>
    </tr>
</table>


<table width="100%" align="center" border="0" style="max-width: 600px; border-collapse:collapse;" cellpadding="0"
       cellspacing="0">

    <tr>
        <td>
            <table align="center" border="0" style="border-collapse:collapse;max-width: 420px;" width="100%">
                <tr>
                    <td align="left">
                        <span style="font-weight:bold;color:#ebb464;">-</span>
                    </td>

                    <td align="center">
                        <span style="font-weight:bold;font-family: 'Roboto Slab', serif;font-size: 22px;text-transform: uppercase;">результаты наших клиентов</span>
                    </td>

                    <td>
                        <span style="color:#ebb464;font-weight: bold;">-</span>
                    </td>
                </tr>
                <tr>
                    <td width="1" height="20"></td>
                </tr>
            </table>

            <table align="center" width="100%" border="0" style="border-collapse:collapse;">
                <tr>
                    <td align="center" class="margin_bl">
                        <img src="http://i67.tinypic.com/10hlrtc.jpg" alt="">
                    </td>
                </tr>

                <tr>
                    <td align="center">
                        <p style="font-weight: bold;text-align: center;color: #676767;text-transform: uppercase;font-size: 17px;">
                            ирина, <span
                                    style="color:#b460d2;text-transform: uppercase;font-size: 17px;font-weight: bold;">40 лет</span>
                        </p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td width="1" height="40"></td>
    </tr>
</table>


<table align="center" border="0" style="border-collapse:collapse;max-width: 705px;min-width: 300px;" width="100%"
       bgcolor="#f9f9f9">
    <tr>
        <td>
            <table align="center" border="0" style="border-collapse:collapse;max-width: 660px;" width="100%">
                <tr>
                    <td width="1" height="30"></td>
                </tr>
                <tr>
                    <td width="35" valign="top">
                        <img src="http://i66.tinypic.com/346qibp.jpg" alt="">
                    </td>

                    <td>
                        <span style="margin:0 0 22px 0;display:block;font-weight:300;font-style:italic;font-size: 17px;line-height: 25px;color: #676767;"
                              class="center">Мне 40 лет и 20 лет из них я  <span
                                    style="color: #b460d2;font-weight:600;"> борюсь со своим неуемным аппетитом</span> и желанием быть стройной и в хорошей спортивной форме. Борьба была нелегкой. В моем шкафу всегда было 3 размера одежды: зимний размер, весенне-осенний и летний. Весной я брала себя в руки и как мне казалось начинала правильно питаться (конечно это получалась низкоуглеводная диета).</span>


                        <span style="margin:0 0 22px 0;display:block;font-weight:300;font-style:italic;font-size: 17px;line-height: 25px;color: #676767;text-indent: 30px;"
                              class="center">  Я худела, но в конце концов еле ноги волочила и тренироваться уже нормально не могла. Да и надолго меня при таком питании не хватало. И к зиме я благополучно отъедалась. И так каждый год - как гармошка! Все 20 лет! <span
                                    style="color: #b460d2;font-weight: 600;">И тут случилось чудо!</span> Я случайно наткнулась на Анин инст. и меня так зацепило...Первый месяц я выстраивала питание по постам самостоятельно и скинула 4 кг. Потом я пришла на вашу программу И теперь, благодаря вам, программе и замечательным девочкам-кураторам, я счастлива! </span>


                        <span style="margin:0 0 22px 0;display:block;font-weight:300;font-style:italic;font-size: 17px;line-height: 25px;color: #676767;text-indent: 30px;"
                              class="center"> У меня произошла полная перепрошивка,  <span
                                    style="color: #b460d2;font-weight:600;">революция в моей голове</span>! Я могу не морить себя голодом, с последующим зажором, а много, вкусно и хорошо кушать и при этом худеть!!! СПАСИБО!!! Это были 9 недель кайфа! Я получала удовольствие от себя, от еды, от тренировок, от общения в группе, от своих результатов в изменении качества тела! </span>


                        <span style="margin:0 0 22px 0;display:block;font-weight:300;font-style:italic;font-size: 17px;line-height: 25px;color: #676767;text-indent: 30px;"
                              class="center"> Меня больше не будет штормить по размерам!  <span
                                    style="color: #b460d2;font-weight:600;">Я легла на правильный курс!</span>! Только вперед! А мое лицо! О, как оно благодарно этому питанию! У меня всю жизнь вскакивали прыщи, один за другим. И не важно ела я сладкое или сидела на диете. После первой недели на программе- больше ни одного прыща не вскочило до сих пор! 9 недель подряд без прыщей?! Такого никогда не было! </span>


                        <span style="margin:0 0 22px 0;display:block;font-weight:300;font-style:italic;font-size: 17px;line-height: 25px;color: #676767;text-indent: 30px;"
                              class="center">Низкий вам поклон за вашу работу, за то, что открыли мне глаза, сделали гораздо качественнее мою жизнь и гармоничнее. <span
                                    style="color: #b460d2;font-weight:600;">Теперь я просто красотка</span>, а не 40 летняя тетка! Моя 15 летняя дочь восхищается мной, не говоря уже про мужа! А еще благодаря вам, "я его похудела" на 16 кг за 4 месяца. По постам я начала питаться 9 марта, программа началась 19 апреля.</span>
                    </td>

                    <td width="27" valign="bottom">
                        <img src="http://i66.tinypic.com/15i6qme.jpg" alt="">
                    </td>
                </tr>

            </table>

            <table align="center" cellspacing="0" cellpadding="0" border="0" width="100%" style="max-width: 300px">
                <tr>
                    <td align="center" width="300" style="max-width: 300px;">
                        <p style="color: #b460d2;font-size: 17px;font-weight: 600;line-height: 7px;">Вес: <span
                                    style="color: #676767;font-size: 17px;">- 9 кг</span></p>
                        <p style="color: #b460d2;font-size: 17px;font-weight: 600;line-height: 7px;">Талия: <span
                                    style="color: #676767;font-size: 17px;">- 10 см</span></p>
                        <p style="color: #b460d2;font-size: 17px;font-weight: 600;line-height: 7px;">Объем ягодиц: <span
                                    style="color: #676767;font-size: 17px;">- 8 см</span></p>
                        <p style="color: #b460d2;font-size: 17px;font-weight: 600;line-height: 7px;">Бедро: <span
                                    style="color: #676767;font-size: 17px;"> - 5 см</span></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td width="1" height="30"></td>
    </tr>
</table>

<table>
    <tr>
        <td width="1" height="50"></td>
    </tr>
</table>

<table width="100%" align="center" border="0" style="border-collapse:collapse;max-width: 500px;min-width: 300px;">
    <tr>
        <td align="center" valign="top">
            <a href="http://feedback.beauty-matrix.ru?utm_source=email&utm_medium=game5&utm_campaign=motivation"
               target=""
               style="background: #1fe2a6;border-radius: 50px;display: block;width: 330px;height: 37px;margin:0 auto;padding:20px 0 0;text-align: center;font-weight: bold;font-family: 'Roboto Slab', serif;text-transform: uppercase;font-size: 15px;color: #ffffff;text-decoration: none;"
               class="button">посмотреть истории</a>
        </td>
    </tr>
</table>

<table width="100%" align="center" border="0" style="border-collapse:collapse;max-width: 600px;" cellpadding="0"
       cellspacing="0">
    <tr>
        <td width="1" height="50"></td>
    </tr>
    <tr>
        <td>
            <table align="center" width="100%" border="0" style="border-collapse:collapse;">
                <tr>
                    <td align="center" class="margin_bl">
                        <img src="http://i63.tinypic.com/2efrkft.jpg" alt="">
                    </td>
                </tr>

                <tr>
                    <td align="center">
                        <p style="font-weight: bold;text-align: center;color: #676767;text-transform: uppercase;font-size: 17px;">
                            ЮЛИЯ, <span
                                    style="color:#b460d2;text-transform: uppercase;font-size: 17px;font-weight: bold;">39 лет</span>
                        </p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td width="1" height="20"></td>
    </tr>
</table>


<table align="center" border="0" style="border-collapse:collapse;max-width: 750px;" width="100%" bgcolor="#f9f9f9">
    <tr>
        <td>
            <table align="center" border="0" style="border-collapse:collapse;max-width: 660px;" width="100%">
                <tr>
                    <td width="1" height="30"></td>
                </tr>
                <tr>
                    <td width="35" valign="top">
                        <img src="http://i66.tinypic.com/346qibp.jpg" alt="">
                    </td>

                    <td>
                        <span style="margin:0 0 22px 0;display:block;font-weight:300;font-style:italic;font-size: 17px;line-height: 25px;color: #676767;"
                              class="center">Моя программа Бьюти Матрикс началась с девиза: «Всё в топку!» Так я себе говорила, когда овсянка, гречка, кургрудь и пр. ПП-шная еда не лезла мне в горло, ни по вкусноте, ни по кол-ву. Вообще, в программе не сомневалась с самого начала, т.к. пришла по рекомендации. Сначала сильно раздражало, что нужно все время готовить и считать граммы.  <span
                                    style="color: #b460d2;font-weight:600;">Привыкание шло недели 2</span>, потом втянулась.</span>


                        <span style="margin:0 0 22px 0;display:block;font-weight:300;font-style:italic;font-size: 17px;line-height: 25px;color: #676767;text-indent: 30px;"
                              class="center"> Как кормящая мать, практически на полном ГВ, могу однозначно рекомендовать программу для мамочек. Молока меньше не стало, любые проявления аллергии у ребёнка прекратились. Прикорм у нас также по рациону ПП - это очень удобно, т.к. не нужно готовить кому-то из нас отдельно.</span>


                        <span style="margin:0 0 22px 0;display:block;font-weight:300;font-style:italic;font-size: 17px;line-height: 25px;color: #676767;text-indent: 30px;"
                              class="center">  Ещё, стоит отметить, что я принадлежу к категории женщин, ну, типа «в возрасте». Мне 39. <span
                                    style="color: #b460d2;font-weight:600;">Вес уходит ни чем не хуже чем у молодых девчонок </span>. Над качеством тела, конечно же, нужно пахать с двойной силой, т.к. дряблость и обвислость уже имеет место быть, но со спортом пока сильно не дружу, все никак не состыкую детский график со своим спортивным. Работаю над этим. </span>


                        <span style="margin:0 0 22px 0;display:block;font-weight:300;font-style:italic;font-size: 17px;line-height: 25px;color: #676767;text-indent: 30px;"
                              class="center">По здоровью: где-то на 80% уменьшилось вздутие живота, прекратилось брожение (а ведь собиралась лечиться после завершения ГВ). Сэкономила! - Наладился стул. У кого с этим проблемы, поймут, что это может быть настоящим мучением. - Ушла отечность и с лица и с тела. - Ещё, у меня в последние года два-три были цыпки на предплечьях. Так вот, они исчезли! - На 100% увеличилась энергия!</span>


                        <span style="margin:0 0 22px 0;display:block;font-weight:300;font-style:italic;font-size: 17px;line-height: 25px;color: #676767;text-indent: 30px;"
                              class="center"> Встаю в 6-7 утра, и до 22:00 на ногах без депрессий и усталости. Поэтому, мамочки с малышами – ПП в нашем деле – это наше все! Случались срывы и зажоры, но я к ним отношусь с пониманием. Жрать все подряд почти 40 лет и изменить все за 2 месяца не так-то просто. После срывов и зажоров легко возвращаюсь к ПП.</span>
                    </td>

                    <td width="27" valign="bottom">
                        <img src="http://i66.tinypic.com/15i6qme.jpg" alt="">
                    </td>
                </tr>

            </table>
        </td>
    </tr>
    <tr>
        <td width="1" height="30"></td>
    </tr>
</table>


<table>
    <tr>
        <td width="1" height="50"></td>
    </tr>
</table>

<table width="100%" align="center" border="0" style="border-collapse:collapse;max-width: 500px;min-width: 300px;">
    <tr>
        <td align="center" valign="top">
            <a href="http://feedback.beauty-matrix.ru?utm_source=email&utm_medium=game5&utm_campaign=motivation"
               target="_blank"
               style="background: #1fe2a6;border-radius: 50px;display: block;width: 330px;height: 37px;margin:0 auto;padding-top:22px;text-align: center;font-weight: bold;font-family: 'Roboto Slab', serif;text-transform: uppercase;font-size: 15px;color: #ffffff;text-decoration: none;"
               class="button">посмотреть истории</a>
        </td>
    </tr>
    <tr>
        <td width="1" height="40"></td>
    </tr>
</table>


<table width="100%" align="center" border="0" style="max-width:640px;" cellspacing="0" cellpadding="0">
    <tr>
        <td width="1" height="50"></td>
    </tr>
    <tr>
        <td align="center">
            <span style="color: #3e3e3e;font-size: 17px;">С наилучшими пожеланиями,</span> <br><b
                    style="color:#3e3e3e;font-size:17px;">команда Beauty Matrix</b>
        </td>
    </tr>
    <tr>
        <td width="1" height="50"></td>
    </tr>
</table>
<table width="100%" align="center" border="0" style="background:#232323;max-width:740px;" cellspacing="0"
       cellpadding="0">
    <tr>
        <td>

            <table width="100%" align="center" style="border-collapse:collapse;max-width:740px;" border="0"
                   cellspacing="0" cellpadding="0">
                <tr>
                    <td width="1" height="40"></td>
                </tr>
                <tr>
                    <td align="center">
                        <a href="#" target="_blank" style="text-decoration:none;">
                            <img src="https://image.ibb.co/goY1cv/logo_footer.png" alt=""></a>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <span style="color: #676767;">Все права защищены</span>
                    </td>
                </tr>
                <tr>
                    <td width="1" height="10"></td>
                </tr>
            </table>

        </td>
    </tr>
</table>
</body>
</html>