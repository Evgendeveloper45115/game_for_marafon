
    <div class="congrat">
        <div class="congrat-top fireworks fireworks--lg fireworks--lg-congrat">
            <h1 class="congrat-top__title">Поздравляем вас!</h1>
        </div>
        <div class="congrat-content">
            <p class="congrat-top__desc">Вы прошли все уровни в нашей игре, а значит вы всего в одном шаге от победы!</p>

            <p class="congrat__text text">Осталось финальное тестирование, после успешного прохождения которого вы получите приятный бонус! Желаем удачи!</p><a class="btn btn--congrat"
                                                                          href="/test/pravilnoe-pitanie">Начать</a>
        </div>
    </div>
