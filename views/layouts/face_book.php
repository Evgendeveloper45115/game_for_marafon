<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAssetSignin;

//AppAsset::register($this);
AppAssetSignin::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:title" content="Игра: «Построй стройную фигуру»"/>
    <meta property="og:image" content="https://game.beauty-matrix.ru/uploads/sss.jpg"/>
    <meta property="og:description"
          content="Выполняй задания и худей, узнавай новую и эффективную информацию о питании и построй фигуру своей мечты"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <!-- Facebook Pixel Code -->
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="content">
    <?= $content ?>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
