<?php

use yii\db\Migration;

class m170928_105911_add_level_icon_field extends Migration
{
    public function safeUp()
    {
        $this->addColumn('level', 'icon', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('level', 'icon');
    }

}
