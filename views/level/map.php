<?php

use yii\helpers\Url;

$user_level = Yii::$app->user->identity->profile->level;
?>

    <div class="levels levels--map">
        <?php foreach ($levels as $key => $level): ?>
            <?php
            if ($level->number_int > Yii::$app->user->identity->profile->level) {
                $class = 'level--locked';
                $link = '#task-complete-plus';
                $id = 'task-complete-plus';
            } else {
                $class = 'level--active';
                $link = Url::to(['level/index', 'id' => $level->id]);
                $id = '';
            }
            ?>
            <a class="level level--map <?= $class ?>" href="<?= $link ?>">
                <div class="level__round" style="background-image: url('/uploads/<?= $level->icon ?>')">
                    <?php
                    $style = null;
                    if (($level->number_int == Yii::$app->user->identity->profile->level) && (($level->number_int != count($levels) && Yii::$app->user->identity->profile->level != count($levels)))) {
                        echo '<span class="btn btn--level">Начать</span>';
                    } elseif ($level->number_int <= $user_level) {
                        if ($level->number_int == 6 && count($passedArt) == 5 && $key == 5) {
                            $style = 'background-color: transparent!important; box-shadow: none!important;';
                            echo '<span style="' . $style . '" class="btn btn--level">Пройдено</span>';
                        } elseif ($level->number_int == 6 && count($passedArt) != 5 && $key == 5) {
                            echo '<span class="btn btn--level">Начать</span>';
                        } else {
                            $style = 'background-color: transparent!important; box-shadow: none!important;';
                            echo '<span style="' . $style . '" class="btn btn--level">Пройдено</span>';
                        }
                    } ?>
                </div>
                <p class="level__name level__name--map"><?= $level->number_int ?>-й уровень</p>
                <p class="level__desc"><?= $level->name ?></p>
            </a>
        <?php endforeach; ?>

        <?php
        if (count($passedArt) == 5) {
            if ($finalTest == false && $key == 5) {
                $finalTest = true;
                ?>
                <a class="level level--map level--exam" href="/test/pravilnoe-pitanie">
                    <div class="level__round"></div>
                    <p class="level__name level__name--map">Экзамен</p>
                </a>
                <?php
            }
        }
        ?>
    </div>
<?= $this->render('/popups/no_level') ?>