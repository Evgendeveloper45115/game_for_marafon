<?php

namespace app\helpers;

use Yii;
use yii\helpers\VarDumper;
use yii\helpers\Url;

class MenuHelper
{

    public static function isSpecialOfferAvailable(){
        if(!Yii::$app->user->isGuest){
            $profile = Yii::$app->user->identity->profile;
            if($profile->passed_test && time() - $profile->test_start_time)
            {
                return true;
            }
        }
        return false;

    }

    public static function isTestAvailable(){
        if(!Yii::$app->user->isGuest) {
            $profile = Yii::$app->user->identity->profile;
            if (!$profile->passed_test && $profile->level == 7) {
                return true;
            }
        }
        return false;
    }

    public static function backToTask($article){
        if($article->level_id){
            return Url::to(['level/index', 'id' => $article->level_id]);
        }

        if($article->theme_id){
            return Url::to(['theme/index', 'id' => $article->theme_id]);
        }

    }

}