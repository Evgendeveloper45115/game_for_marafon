<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TaskAnswers */

$this->title = 'Create Task Answers';
$this->params['breadcrumbs'][] = ['label' => 'Task Answers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-answers-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
