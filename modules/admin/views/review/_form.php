<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Review */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="review-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php if ($model->photo_before) { ?>
    <?= Html::img(Yii::getAlias('@rel_path_to_uploads') . '/' . $model->photo_before, ['class' => 'photo_before']); ?>
    <?php } ?>

    <?= $form->field($model, 'file_before')->fileInput(['onchange' => 'readURL(this, "photo_before");']); ?>

    <?php if ($model->photo_after) { ?>
        <?=  Html::img(Yii::getAlias('@rel_path_to_uploads') . '/' . $model->photo_after, ['class' => 'photo_after']); ?>
    <?php } ?>
    <?= $form->field($model, 'file_after')->fileInput(['onchange' => 'readURL(this, "photo_after");']); ?>

    <?= $form->field($model, 'weight')->textInput() ?>

    <?= $form->field($model, 'review')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
