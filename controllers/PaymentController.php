<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

class PaymentController extends Controller
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {

        return $this->render('index', [

        ]);
    }

    public function actionSuccess()
    {

        return $this->render('success', [
        ]);
    }

    public function actionFail()
    {

        return $this->render('fail', [
        ]);
    }


}