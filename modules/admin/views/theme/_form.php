<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Article */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="level-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'level_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Level::find()->asArray()->orderBy('id')->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'name')->textInput() ?>
    <?= $form->field($model, 'type',
        ['template' => '{label}<div class="">{input}{hint} {error}</div>']
    )
        ->radioList(
            $model->types,
            [
                'item' => function ($index, $label, $name, $checked, $value) {
                    return '<label class="btn btn-white btn-info btn-round' . ($checked ? " active" : null) . ' ">' . Html::radio($name, $checked, ['value' => $value, 'autocomplete' => 'off']) . $label . '</label>';
                },
                'tag' => 'div', // this is by default
                'class' => 'btn-group',
                'data-toggle' => 'buttons',
            ]
        )->label(false) ?>

    <?= $form->field($model, 'description', [
        'template' => "<div>{input}\n<div class=\"error\">{error}</div></div>",
    ])->widget(\zxbodya\yii2\tinymce\TinyMce::className(), [
        'options' => ['rows' => 15, 'style' => 'margin-top:5vw'],
        'language' => 'ru',
//        'spellcheckerUrl'=>'http://speller.yandex.net/services/tinyspell',
        'fileManager' => [
            'class' => \zxbodya\yii2\elfinder\TinyMceElFinder::className(),
            'connectorRoute' => 'el-finder/connector',
        ],
    ]); ?>
    <?php if ($model->img_radius) { ?>
        <?= Html::img(Yii::getAlias('@rel_path_to_uploads') . '/' . $model->img_radius, ['class' => 'profile_foto']); ?>
        <div class="del_link">
            <?php
            if ($model->img_radius != 'blank.jpg') {
                echo Html::a('Удалить', ['article/delete-img'], [
                    'data-method' => 'POST',
                    'data-params' => [
                        'id' => $model->id,
                    ],
                ]);
            }
            ?>
        </div>
    <?php } ?>

    <?php
    if (!$model->img_radius) {
        echo Html::img('/uploads/blank.jpg', ['class' => 'profile_foto']);
    }
    ?>


    <?= $form->field($model, 'image_radius')->fileInput(['onchange' => 'readURL(this);']); ?>

    <?= $form->field($model, 'weight')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
