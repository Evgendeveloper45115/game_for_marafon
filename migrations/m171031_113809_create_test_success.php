<?php

use yii\db\Migration;

class m171031_113809_create_test_success extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%passed_test}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'test_id' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk-testSuccess-test_id-test-id', 'passed_test', 'test_id', 'test', 'id',
            'RESTRICT', 'CASCADE'
        );
        $this->addForeignKey(
            'fk-testSuccess-user_id-user-id', 'passed_test', 'user_id', 'user', 'id',
            'RESTRICT', 'CASCADE'
        );

    }

    public function safeDown()
    {
        echo "m171031_113809_create_test_success cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171031_113809_create_test_success cannot be reverted.\n";

        return false;
    }
    */
}
