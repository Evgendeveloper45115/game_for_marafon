<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use dosamigos\tinymce\TinyMce;


 $form = ActiveForm::begin(); ?>


<?=
$form->field($model, 'description_short')->widget(
    \zxbodya\yii2\tinymce\TinyMce::className(),
    [
        'fileManager' => [
            'class' => \zxbodya\yii2\elfinder\TinyMceElFinder::className(),
            'connectorRoute' => '/el-finder/connector',
        ],
    ]
)
?>
    <?php ActiveForm::end(); ?>
