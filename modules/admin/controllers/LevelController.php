<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Level;
use app\models\LevelSearch;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * LevelController implements the CRUD actions for Level model.
 */
class LevelController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * Lists all Level models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LevelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Level model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Level model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Level();
        $model->FillBlankImg();
        $model->FillBlankIcon();



        if ($model->load(Yii::$app->request->post())) {

            $model->file = UploadedFile::getInstance($model, 'file');
            if($model->file){
                $model->dropOldImg();
                $file_name = uniqid();
                $model->img = $file_name . '.' . $model->file->extension;
                if (!$model->file->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/' . $file_name . '.' . $model->file->extension)){
                    throw new \RuntimeException('Ошибка сохранения файла');
                }
                $model->file = null;
            }

            $model->icon_file = UploadedFile::getInstance($model, 'icon_file');
            if($model->icon_file){
                $model->dropOldIcon();
                $file_name = uniqid();
                $model->icon = $file_name . '.' . $model->icon_file->extension;
                if (!$model->icon_file->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/' . $file_name . '.' . $model->icon_file->extension)){
                    throw new \RuntimeException('Ошибка сохранения файла');
                }
                $model->icon_file = null;
            }

            $model->save();
            return $this->redirect('index');
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Level model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $model->file = UploadedFile::getInstance($model, 'file');
            if($model->file){
                $model->dropOldImg();
                $file_name = uniqid();
                $model->img = $file_name . '.' . $model->file->extension;
                if (!$model->file->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/' . $file_name . '.' . $model->file->extension)){
                    throw new \RuntimeException('Ошибка сохранения файла');
                }
                $model->file = null;
            }

            $model->icon_file = UploadedFile::getInstance($model, 'icon_file');
            if($model->icon_file){
                $model->dropOldIcon();
                $file_name = uniqid();
                $model->icon = $file_name . '.' . $model->icon_file->extension;
                if (!$model->icon_file->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/' . $file_name . '.' . $model->icon_file->extension)){
                    throw new \RuntimeException('Ошибка сохранения файла');
                }
                $model->icon_file = null;
            }

            $model->save();

            return $this->redirect('index');
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Level model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try{
            $this->findModel($id)->dropOldImg();
            $this->findModel($id)->delete();
        }
        catch (yii\db\IntegrityException $e){
            Yii::$app->session->setFlash('error', 'Уровень содержит контент');
            return $this->redirect(['index']);
        }

        return $this->redirect(['index']);
    }

    public function actionDeleteImg()
    {
        if(Yii::$app->request->post()){
            $id = Yii::$app->request->post('id');
            $this->findModel($id)->dropOldImg();
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionDeleteIcon()
    {
        if(Yii::$app->request->post()){
            $id = Yii::$app->request->post('id');
            $this->findModel($id)->dropOldIcon();
        }

        return $this->redirect(Yii::$app->request->referrer);
    }


    /**
     * Finds the Level model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Level the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Level::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
