<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Email</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,700&amp;subset=cyrillic" rel="stylesheet">

    <style type="text/css">
        *{
            font-family:Arial,Helvetica,sans-serif;
            font-size:15px;
        }
        div,p,span,strong,b,em,i,a,li,td{
            -webkit-text-size-adjust:none;
        }
        img{
            max-width:100%;
            text-align:center;
        }
        @media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            body{
                margin:0 auto;
                max-width:320px;
            }

        }	@media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            table{
                width:320px !important;
            }

        }	@media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            span.size28{
                font-size:28px !important;
            }

        }	@media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            .block{
                display:block;
                width:257px !important;
                margin:0 auto !important;
            }

        }	@media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            .margin0{
                margin:0 !important;
            }

        }	@media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            .margin_bl{
                display:block;
                margin:0 auto 28px;
            }

        }	@media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            .button{
                width:295px !important;
                margin-top:30px !important;
            }

        }	@media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            .padding{
                padding:6px 0 0 32px !important;
            }

        }	@media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            .block2{
                display:table;
                width:145px !important;
                vertical-align:bottom;
            }

        }	@media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            .block3{
                width:140px !important;
                display:table;
            }

        }	@media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            .block4{
                width:140px !important;
                display:table;
            }

        }	@media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            .height0{
                height:0 !important;
            }

        }	@media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            .center{
                font-size:14px !important;
            }

        }	@media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            .width155{
                width:147px !important;
            }

        }	@media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            .widht150{
                width:150px !important;
            }

        }	@media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            .height{
                height:0 !important;
            }

        }	@media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            .float{
                float:none !important;
                margin:0 auto !important;
            }

        }	@media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            .none{
                display:none !important;
            }

        }	@media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            .width100{
                width:100% !important;
            }

        }	@media only screen and (max-device-width: 748px),only screen and (max-width: 748px){
            .paddi{
                padding:6px 0 0 !important;
            }

        }</style></head>
<body style="margin: 0 auto;padding: 0;">



<table cellpadding="0" cellspacing="0" width="100%" border="0" style="min-width:300px;max-width:740px;border-collapse:collapse;" align="center">
    <tr>
        <td align="center">

            <table cellpadding="0" cellspacing="0" width="100%" border="0" style="min-width:300px;max-width:740px;border-collapse:collapse;background:url('https://beauty-matrix.ru/img2/anna-hello.jpg') no-repeat top;background-size:cover;" align="center" bgcolor="#55bd9d">
                <tr>
                    <td align="center">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0" style="border-collapse:collapse;max-width:700px;" align="center">
                            <tr>
                                <td height="250" align="center" valign="top">
                                    <table align="center" style="max-width:700px;margin-top:30px;border-collapse:collapse;" cellspacing="0" cellpadding="0" border="0" width="100%">
                                        <tr>
                                            <td align="center" width="96">
                                                <a href="#" target="_blank">
                                                    <img src="https://image.ibb.co/cUZUvF/logo.png" alt=""></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span style="margin-top: 20px; display: block; font-family: 'Roboto Slab', serif; font-size: 34px;color:#ffffff;text-align: center;line-height: 35px;" class="size28">Все начинается с первого шага</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="1" height="80"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>


            <table cellpadding="0" cellspacing="0" width="100%" border="0" style="border-collapse:collapse;max-width: 300px;" align="center">
                <tr><td width="1" height="50"></td></tr>
                <tr>
                    <td width="220" align="center">
                        <img src="http://i63.tinypic.com/339lxs5.jpg" alt="">
                    </td>
                </tr>

                <tr><td width="1" height="25"></td></tr>

                <tr>
                    <td align="center">
                        <span style="text-align: center;font-size: 17px;text-transform: uppercase;color: #676767;">Добрый день!</span><br>
                        <p style="line-height: 0px; font-weight: 300;font-size: 17px;color:#676767;text-align: center;">На связи Анна из Beauty-Matrix</p>

                    </td>
                </tr>

                <tr><td width="1" height="10"></td></tr>
            </table>




            <table cellpadding="0" cellspacing="0" width="100%" border="0" style="border-collapse:collapse;max-width: 750px;min-width: 300px;" align="center">
                <tr>
                    <td>
                        <p style="font-weight:bold;font-size: 22px;font-family: 'Roboto Slab', serif;">Меня зовут Анна, я фитнес тренер и нутрициолог (специалист по правильному питанию), создатель групповой программы похудения Beauty Matrix</p>
                    </td>
                </tr>
                <tr><td width="1" height="30"></td></tr>
            </table>

            <table width="100%" border="0" style="border-collapse:collapse;max-width:250px;margin: 0 37px 0 0;" align="right">
                <tr>
                    <td width="250" class="widht300 block" style="display: block;">
                        <img src="http://i67.tinypic.com/2w24zzo.jpg" alt="" width="250">
                    </td>
                </tr>
            </table>

            <table width="100%" align="left" border="0" style="border-collapse:collapse; max-width:400px;min-width: 300px;">
                <tr>
                    <td>
                        <table align="left" width="100%" border="0" style="border-collapse:collapse;max-width:400px;">
                            <tr><td width="1" height="0"></td></tr>
                            <tr>
                                <td align="left" width="100%" style="color: #676767;line-height: 25px;font-weight: 300;font-size: 17px;" class="widht300 margin0 center">

                                    <p style="color: #676767;line-height: 25px;font-weight: 300;font-size: 17px;">Вы прошли уже целых 3 уровня в игре "Построй стройную фигуру", а это значит, что тема здорового образа жизни, питания и тренировок Вам, скорее всего, близка! И это здорово! Не важно, хотите вы похудеть, наладить питание или набрать мышечную массу, главное, что вы уже сделали первый шаг и если продолжите свой путь в мир ЗОЖ, результат не заставит себя ждать! Чтобы вас мотивировать пройти игру до конца хочу немного вам рассказать о своей истории, как я пришла к правильному питанию и тренировкам.
                                    </p> </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr><td width="1" height="0"></td></tr>
            </table>
        </td>
    </tr>
</table>











<table cellpadding="0" cellspacing="0" width="100%" border="0" style="border-collapse:collapse;max-width:600px;" align="center">
    <tr>
        <td width="1" height="50"></td>
    </tr>
    <tr>
        <td align="center">
            <span style="display:block;color: #2a2a2a;font-size: 22px;font-family: 'Roboto Slab', serif;font-weight: bold;">С чего начинается первый шаг?</span><br/>
        </td>
    </tr>
    <tr>
        <td>
                  <span style="font-size: 17px;color: #676767;">Мой первый шаг начался с безысходности... Осенью 2012 я пребывала в прострации. Уже год не работала, не могла найти себя, мне было почти 32 года. Выглядела тоже неважно, перманентно была собой недовольна, не хотела покупать новую одежду, тк она на мне просто не сидела.<br/><br/>
                  Есть такое выражение: потенциал (личности) минус факт (что вы из себя представляете) = страдание. Вот у меня это проявлялось на все 100. Тогда я увлекалась бьюти-блогами, лаками для ногтей и мой папа как-то высказал мне свое разочарование моей личностью.. Мол, я ожидал от тебя одного, а ты представляешь собой другое. Это было очень больно и вообще как пощечина. Мой обожаемый папа, который всегда поддерживал меня (морально), вдруг разочарован во мне...<br/><br/>
                  У меня не было пошагового плана изменений. Мой вес благодаря поеданию сладкого и черного хлеба с маслом (по 4 куска с тарелкой супа) в тот момент дошел до 57.. вроде немного, но выглядела я ужасно. Изменения пошли после не очень корректного замечания мужа, я поняла, что дошла до ручки и надо бы похудеть. Наши близкие - это зачастую наше зеркало, и они транслируют нам то, что мы иногда сами боимся сказать себе. В тот момент высказывание мужа про меня и моих толстожопых подруг было в яблочко.

               </span>
        </td>
    </tr>
    <tr>
        <td style="padding:20px 0;">
            <a href="https://www.instagram.com/beatrix_fit/" style="position:relative;min-height:40px;display:block;">
                <span style="position:absolute;top:10px;left:65px;">Перейти в инстаграм Анны @beatrix_fit</span>
                <img src="https://beauty-matrix.ru/img2/anna-result.jpg" style="position:relative;display:block;width:100%;height:auto;">
            </a>
        </td>
    </tr>
    <tr>
        <td>
                  <span style="font-size: 17px;color: #676767;">На фото выше слева я 5 лет назад, справа моя текущая форма. Выглядела раньше я так себе (несмотря на то, что уже начала худеть), отношения с мужем были ни к черту, работы у меня не было. Я была обычной 31 летней унылой домохозяйкой...<br/><br/>

                  Та форма - это среднестатистически нормально выглядящая женщина, с хорошими от природы пропорциями... Мне не очень приятно, когда пишут, что тогда я выглядела прекрасно)) Хотя уровень несомненно ближе к большинству. Многие посчитали бы за счастье выглядеть так, но только не я. Моя планка намного выше, и это касается абсолютно всего. Внешности, семьи, дома, работы, карьеры. Однажды давным-давно, лет за 10 до того фото, я работала операционистом в банке, недолго. Окружающие мне несколько раз делали замечания, что у меня есть большие амбиции по жизни. Как всегда (следуя феномену "ведра с крабами"), окружающие пытаются нам подрезать крылья, из зависти, или с целью сберечь нас от жестокости и несправедливости мира.<br/><br/>

                  Что я хочу сказать в этом письме? Стремиться к бОльшему - абсолютно нормально! Хотеть добиться больших высот - норма. Плохо сидеть в болоте, если вам в нем неуютно. Не позволяйте никому наступать на горло вашей песне. Иметь амбиции, добиваться своего честным путем - не стыдно! Поставьте себе высокие цели, идите к ним. После того, как начнете реализовывать, увидите, что вся Вселенная помогает вам! Что появляется энергия на реализацию из ниоткуда. Пусть для большинства это будет "уже хорошо", "предел мечтаний", "космос". Для вас это может быть только ступень.

               </span>
        </td>
    </tr>
    <tr>
        <td width="1" height="40"></td>
    </tr>
    <tr>
        <td>
            <table width="100%" align="center" border="0" style="border-collapse:collapse;max-width:500px;min-width:300px;background:url('http://i66.tinypic.com/ff2yjr.jpg') no-repeat;">
                <tr>
                    <td align="center" valign="top">
                        <a href="https://game.beauty-matrix.ru?utm_source=email&utm_medium=game4&utm_campaign=anna-motivation" target="" style="background:#1fe2a6;border-radius:50px;display:block;max-width:330px;height:37px;margin:0 auto;padding:20px 0 0;text-align:center;font-weight:bold;font-family:'Roboto Slab', serif;text-transform:uppercase;font-size:15px;color:#ffffff;text-decoration:none;" class="button">Продолжить играть</a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td width="1" height="40"></td>
    </tr>
</table>
</td>
</tr>
</table>
<table width="100%" align="center" border="0" style="border-collapse:collapse;max-width:750px;min-width:300px;" bgcolor="#f9f9f9">
    <tr>
        <td width="1" height="20"></td>
    </tr>
    <tr>
        <td>
            <table cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse;max-width:640px;" width="100%" align="center">
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse;max-width:390px;display:inline-block;" width="100%" align="center">
                            <tr>
                                <td align="center">
                                    <p style="padding:0 0 0 5px;font-size:17px;color:#676767;line-height:25px;">
                                        Узнать больше о правильном питании, тренировках, мотивации и психологии, чтобы похудеть и сделать стройную фигуру:
                                    </p>
                                    <p>
                                        <a href="https://game.beauty-matrix.ru/?utm_source=email&utm_medium=game4&utm_campaign=anna-motivation" target="" style="background:#1fe2a6;border-radius:50px;display:block;max-width:330px;height:37px;padding:20px 0 0;text-align:center;font-weight:bold;font-family:'Roboto Slab', serif;text-transform:uppercase;font-size:15px;color:#ffffff;text-decoration:none;" class="button">Вернуться в игру</a>
                                    </p>
                                </td>
                            </tr>
                        </table>
                        <table cellspacing="0" cellpadding="0" border="0" style="margin:15px 0 0 80px;border-collapse:collapse;max-width:160px;display:inline-block;" width="100%">
                            <tr>
                                <td>
                                    <img src="https://beauty-matrix.ru/img2/5.png" alt="">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td width="1" height="20"></td>
    </tr>
</table>
<table width="100%" align="center" border="0" style="max-width:640px;" cellspacing="0" cellpadding="0">
    <tr>
        <td width="1" height="50"></td>
    </tr>
    <tr>
        <td align="center">
            <span style="color: #3e3e3e;font-size: 17px;">С наилучшими пожеланиями,</span> <br><b style="color:#3e3e3e;font-size:17px;">команда Beauty Matrix</b>
        </td>
    </tr>
    <tr>
        <td width="1" height="50"></td>
    </tr>
</table>
<table width="100%" align="center" border="0" style="background:#232323;max-width:740px;" cellspacing="0" cellpadding="0">
    <tr>
        <td>

            <table width="100%" align="center" style="border-collapse:collapse;max-width:740px;" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="1" height="40"></td>
                </tr>
                <tr>
                    <td align="center">
                        <a href="#" target="_blank" style="text-decoration:none;">
                            <img src="https://image.ibb.co/goY1cv/logo_footer.png" alt=""></a>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <span style="color: #676767;">Все права защищены</span>
                    </td>
                </tr>
                <tr>
                    <td width="1" height="10"></td>
                </tr>
            </table>

        </td>
    </tr>
</table>
</body>
</html>