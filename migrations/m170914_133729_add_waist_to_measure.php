<?php

use yii\db\Migration;

class m170914_133729_add_waist_to_measure extends Migration
{
    public function safeUp()
    {
        $this->addColumn('measure', 'waist', $this->integer()->notNull());
    }

    public function safeDown()
    {
        $this->dropColumn('measure', 'waist');
    }

}
