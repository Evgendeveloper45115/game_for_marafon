
<div class="final-offer">
    <div class="final-offer__top clearfix">
        <div class="final-offer__top-left col-4">
            <img src="../images/bg_girls.png">
        </div>
        <div class="final-offer__top-left-mobile col-4">
            <img src="../images/bg_girls-mobile.png">
        </div>
        <div class="final-offer__top-right col-7">
            <div class="clearfix final-offer__title"><span class="final-offer__title-week col-3">9 недель</span>
                <h2 class="final-offer__title-text col-9"  style="text-align: left;">Программа создание стройной фигуры дистационные групповые занятия</h2>
            </div>
            <p class="final-offer__top-text" style="text-align: left;">Вы прошли нашу игру и следующим этом, который с помощью, который вы можете соpдать фигуры совей мечты, это наша групповая дистанционная программа, на которой будет куча крутых заданий, индивидуальный рацион и поддержка профессиональных кураторов.</p>
            <div class="final_pay_block"><a class="btn btn--final-offer-pay js-pay">Оплата</a></div>
        </div>
    </div>
    <div class="final-offer__program">
        <p class="program__title">Программа включает в себя:</p>
        <div class="program__items">
            <div class="program__item">
                <div class="program__item-img">
                    <img src="../images/restaurant.png">
                </div>
                <p class="program__text">Питание</p>
            </div>
            <div class="program__item">
                <div class="program__item-img">
                    <img src="../images/dumb.png">
                </div>
                <p class="program__text">Тренировки</p>
            </div>
            <div class="program__item">
                <div class="program__item-img">
                    <img src="../images/target.png">
                </div>
                <p class="program__text">Мотивация</p>
            </div>
            <div class="program__item">
                <div class="program__item-img">
                    <img src="../images/people.png">
                </div>
                <p class="program__text">Единомышленники</p>
            </div>
            <div class="program__item">
                <div class="program__item-img">
                    <img src="../images/curator.png">
                </div>
                <p class="program__text">Кураторы</p>
            </div>
        </div><a class="btn btn--program" href="https://beauty-matrix.ru/">Перейти на сайт программы</a>
    </div>


    <div class="final-offer__comment">
        <p class="comment__title">Отзывы:</p>
        <div class="js-slider-comments comment__items">
            <div class="comment__item">
                <div class="comment__item-img">
                    <img src="../images/lady1_before.png">
                    <img src="../images/lady1_after.png"><span class="comment__item-kg">-11.5 кг</span>
                </div>
                <p class="comment__text">Елена Андреевна</p><a class="comment__link" href="">Читать отзыв</a>
            </div>
            <div class="comment__item">
                <div class="comment__item-img">
                    <img src="../images/lady2_before.png">
                    <img src="../images/lady2_after.png"><span class="comment__item-kg">-23.5 кг</span>
                </div>
                <p class="comment__text">Мария Кушкова</p><a class="comment__link" href="">Читать отзыв</a>
            </div>
            <div class="comment__item">
                <div class="comment__item-img">
                    <img src="../images/lady3_before.png">
                    <img src="../images/lady3_after.png"><span class="comment__item-kg">-11.5 кг</span>
                </div>
                <p class="comment__text">Елена Андреевна</p><a class="comment__link" href="">Читать отзыв</a>
            </div>
            <div class="comment__item">
                <div class="comment__item-img">
                    <img src="../images/lady3_before.png">
                    <img src="../images/lady3_after.png"><span class="comment__item-kg">-11.5 кг</span>
                </div>
                <p class="comment__text">Елена Андреевна</p><a class="comment__link" href="">Читать отзыв</a>
            </div>
            <div class="comment__item">
                <div class="comment__item-img">
                    <img src="../images/lady3_before.png">
                    <img src="../images/lady3_after.png"><span class="comment__item-kg">-11.5 кг</span>
                </div>
                <p class="comment__text">Елена Андреевна</p><a class="comment__link" href="">Читать отзыв</a>
            </div>
        </div><a class="btn btn--comments" href="#">Смотреть все отзывы</a>
    </div>
</div>
