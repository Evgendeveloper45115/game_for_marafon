<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "review".
 *
 * @property integer $id
 * @property string $name
 * @property string $photo_before
 * @property string $photo_after
 * @property integer $weight
 * @property string $review
 */
class Review extends \yii\db\ActiveRecord
{

    public $file_before;
    public $file_after;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'review';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['review'], 'string'],
            [['file_before', 'file_after'], 'file'],
            [['name', 'photo_before', 'photo_after', 'weight'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя Фамилия',
            'photo_before' => 'Photo Before',
            'photo_after' => 'Photo After',
            'weight' => 'Вес',
            'review' => 'Текст отзыва',
            'file_before' => 'Фото',
            'file_after' => 'Фото после',
        ];
    }

    public function dropPhotoBefore()
    {
        if ($this->photo_before) {
            $path_to_file = Yii::getAlias('@abs_path_to_uploads') . '/' . $this->photo_before;

            if (file_exists($path_to_file)) {
                unlink($path_to_file);
            }
        }
    }

    public function dropPhotoAfter()
    {
        if ($this->photo_after) {
            $path_to_file = Yii::getAlias('@abs_path_to_uploads') . '/' . $this->photo_after;

            if (file_exists($path_to_file)) {
                unlink($path_to_file);
            }
        }
    }

}
