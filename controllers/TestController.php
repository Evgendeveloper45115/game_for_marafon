<?php

namespace app\controllers;

use app\models\PassedTest;
use app\models\Profile;
use app\models\Review;
use app\models\Test;
use app\models\TestQuestionLevel;
use app\models\User;
use app\modules\quiz\models\QuizQuestion;
use SebastianBergmann\GlobalState\RuntimeException;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Yii;
use yii\db\Exception;
use yii\db\Expression;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\Controller;
use app\forms\PointForm;
use app\components\TriggerMailer;
use yii\filters\AccessControl;
use app\models\MainPage;

class TestController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {

        return $this->render('index', []);
    }

    public function actionGreetings()
    {

        return $this->render('greetings', [
        ]);
    }

    public function actionGreetingTest($id)
    {
        $test = Test::findOne($id);
        $passedTest = PassedTest::find()->where(['test_id' => $id, 'user_id' => Yii::$app->user->id])->one();
        return $this->render('greeting-test', ['model' => $test, 'passedTest' => $passedTest]);
    }

    public function actionProgram()
    {
        if (\Yii::$app->request->isAjax) {
            return $this->renderPartial('_program.php');
        }

    }

    public function actionPravilnoePitanie($level = false)
    {
        $this->layout = 'final_test';
        if ($level) {
            if (Yii::$app->request->isAjax) {
                $post = Yii::$app->getRequest()->post('answers');
                $successAnswers = 0;
                if (!empty($post)) {
                    foreach ($post as $id => $val) {
                        if ($val != '') {
                            $quiz = TestQuestionLevel::find()
                                ->where(['id' => $id])
                                ->andWhere(['test_one' => $val])
                                ->one();
                            if ($quiz) {
                                $successAnswers += 1;
                            }
                        }
                    }
                    $test = Test::findOne($level);
                    $testSuccess = PassedTest::find()->where(['user_id' => Yii::$app->user->getId(), 'test_id' => $level])->one();
                    $new = false;
                    if (!$testSuccess) {
                        $testSuccess = new PassedTest();
                        Yii::$app->user->identity->profile->account += $test->point;
                        $new = true;
                    }
                    $success = 'false';
                    if ($test->percent <= $successAnswers) {
                        $testSuccess->test_id = $level;
                        $testSuccess->user_id = Yii::$app->user->getId();
                        $testSuccess->percent = $test->percent;
                        $testSuccess->save(false);
                        $success = 'true';
                        if ($new) {
                            Yii::$app->user->identity->profile->save(false);
                        }
                    }
                    echo Json::encode(['success' => $success, 'successAnswers' => $successAnswers]);
                    return;

                }
            }
            $tests = Test::find()->where(['id' => $level])->one();
            return $this->render('level_test', ['tests' => $tests, 'successAnswers' => $successAnswers]);

        } else {
            if (Yii::$app->user->identity->profile->passed_test) {
                return $this->redirect('greetings');
            }
            $successAnswers = 0;
            if (Yii::$app->request->isAjax) {
                $post = Yii::$app->getRequest()->post('answers');
                if (!empty($post)) {
                    $i = 0;
                    Yii::$app->user->identity->testAnswer = 0;
                    foreach ($post as $id => $val) {
                        if ($val != '') {
                            $quiz = QuizQuestion::find()
                                ->where(['id' => $id])
                                ->andWhere(['answer' => $val])
                                ->one();
                            if ($quiz) {
                                Yii::$app->user->identity->testAnswer += 1;
                                Yii::$app->user->identity->save(false);
                            }

                        }
                    }
                }
                echo Json::encode(['success' => '/test/result']);

                return;
            }
            $tests = QuizQuestion::find()
                //  ->where(['category_id' => 1])
                ->orderBy(new Expression('rand()'))
                ->all();
            return $this->render('final', ['tests' => $tests]);
        }
    }

    public function actionPoints()
    {
        $session = Yii::$app->session;
        $model = new PointForm();
        $points = 0;  // запоминаем  в сессии сколько всего баллов пользователь еще может списать
        $total_points = 0; // запоминаем  в сессии сколько всего баллов пользователь уже списал

        if (\Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {

                if ($session->has('points')) {
                    $points = $session->get('points');
                } else {
                    $session->set('points', Yii::$app->user->identity->profile->account);
                }

                $points = $session->get('points');


                if ($session->has('total_points')) {
                    $total_points = $session->get('total_points');
                }
                  if (Yii::$app->user->identity->profile->account - $total_points > 0) {
                    $points = Yii::$app->user->identity->profile->account - $total_points;
                }


                if ($model->amount > $points) {
                    return 'error';
                } else {
                    $session->set('points', $points - $model->amount);
                    $session->set('total_points', $total_points + $model->amount);
                    return $model->amount;
                }
            }
        }
    }

    public function actionResult($param = null)
    {
        //var_dump(time());die;
        $main = MainPage::findOne(1);
        $reviews = Review::find()->all();

        /**
         * @var $user User
         */
        $user = Yii::$app->user->getIdentity();
        if (!$user) {
            throw new AccessDeniedException('Пользователь не найден');
        }
        $point_form = new PointForm();
        $point_form->amount = 0;

        $points_discount = 0;


        $error = false;
        if ($point_form->load(Yii::$app->request->post()) && $point_form->validate()) {
            if ($user->profile->account >= $point_form->amount) {
               // $points_discount = $point_form->amount;
            } else {
                $error = true;
            }
        }
        $isPassedTest = false;
        if ($user->testAnswer >= 5) {
            if (!$user->profile->passed_test) { // чтобы не срабатывало многократно при рефреше и списании баллов
                $user->profile->account += 100;
                $user->profile->test_start_time = time();
                $user->profile->passed_test = true;
            }
            $isPassedTest = true;
            TriggerMailer::passedTest(Yii::$app->user->getIdentity());
        } else {
            $user->profile->test_start_time = time();
            TriggerMailer::notPassedTest(Yii::$app->user->getIdentity());
        }

        if ($user->profile) {
            $user->profile->save(false);
        }

        //осталось времени спецпредложения
        $spec_time = strtotime('+48 hours', $user->profile->test_start_time) - time();
        $discount = 0;

        if (isset($user->profile->passed_test)) {
            $isPassedTest = $user->profile->passed_test;
        }

        if ($spec_time) {
            $discount = 3000;
        }

        $full_price = 11900;
        $points = $user->profile->account;  // сколько всего баллов пользователь еще может списать
        $total_points = 0; //  сколько всего баллов пользователь уже списал
        $session = Yii::$app->session;

        if ($session->has('total_points')) {
            $total_points = $session->get('total_points');
        }

        if ($session->has('points')) {
            $points = $session->get('points');
            if ($user->profile->account - $total_points > 0) {
                $points = $user->profile->account - $total_points;
            }
        }


        $full_price = $full_price - $total_points;

        return $this->render('result', [
            'isPassedTest' => $isPassedTest, // сдал ли тест
            'points4test' => $user->profile->account, // баллов за тест
            'good_answers' => $user->testAnswer,
            'amount_questions' => 10,
            'user' => $user,
            'full_price' => $full_price,
            'discount' => $discount, // размер скидки
            'point_form' => $point_form,
            'error' => $error,
            'points_discount' => $points_discount, //сколько списать с баланса
            'spec_time' => $spec_time, // остаток времени спецпредложения
            'param' => $param, // если параметр = program, то рендерим инфу о программе
            'main' => $main, // для информации о программе
            'reviews' => $reviews,
            'points' => $points,
        ]);
    }

}
