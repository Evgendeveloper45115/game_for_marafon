<?php

use yii\db\Migration;

class m170926_113543_social_friend extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%social_friend}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),

        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%social_friend}}');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170926_113543_social_friend cannot be reverted.\n";

        return false;
    }
    */
}
