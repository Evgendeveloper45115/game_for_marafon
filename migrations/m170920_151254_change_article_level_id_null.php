<?php

use yii\db\Migration;

class m170920_151254_change_article_level_id_null extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('article','level_id','integer NULL');
    }

    public function safeDown()
    {
        $this->alterColumn('article','level_id','integer NOT NULL');
    }

}
