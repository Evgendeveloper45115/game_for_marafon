<?php

use yii\db\Migration;

class m171031_113331_create_test extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%test}}', [
            'id' => $this->primaryKey(),
            'level_id' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk-test-level_id-level-id', 'test', 'level_id', 'level', 'id',
            'RESTRICT', 'CASCADE'
        );

    }

    public function safeDown()
    {
        echo "m171031_113331_create_test cannot be reverted.\n";

        return false;
    }
}
