var $, console;
function validateEmail(email) {
    'use strict';
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
function make_input_info(str, $el) {
    'use strict';
    var top_el, left_el;
    $el.parent().css('position', 'relative');
    top_el = $el.position().top + $el.outerHeight();
    left_el = $el.position().left;
    $el.parent().css('position', 'relative').append('<div class="input_error" style="top:' + top_el + 'px;left:' + left_el + 'px"><svg  version="1.1" preserveAspectRatio="xMidYMid meet" viewBox="0 0 100 100"><polygon points="0,100 50,0 100,100" /></svg>' + str + '</div>');
    setTimeout(function () {
        $('.input_error').animate({'opacity': 0}, 300, function () {
            $(this).remove();
        });
    }, 1300);
}
function creat_lb(data) {
    'use strict';
    var speed,
        height_content,
        fin_tpl,
        tpl,
        height_condition;
    if ($('#lb_box').length > 0) {
        speed = 0;
        $('#lb_box').remove();
    } else {
        speed = 400;
    }
    tpl = document.getElementById('lb').innerHTML;
    fin_tpl = tpl.split('{*content*}').join(data);
    $('body').append(fin_tpl).addClass('block_scrool');
    height_content = $('#lb_wrap_scroll').outerHeight(true);
    $('#lb_wrap').height(height_content);
    height_condition = (height_content < ($(window).height() * 0.8));
    if (!height_condition) {
        $('#lb_wrap_scroll').addClass('scroll');
    }
    $('#lb_box').animate({'opacity': 1}, speed);
}

$(function () {
    'use strict';

    function defaultIn() {
        var count = 0;
        $('.sing_input').each(function () {
            if ($(this).val() !== '') {
                count++;
                if ($(this).attr('name') === 'LoginForm[email]' && !validateEmail($('input[name="LoginForm[email]"]').val())) {
                    count--;
                    $('input[name=mail]').addClass('fail');
                    make_input_info('Недопустимый формат Email', $('input[name="LoginForm[email]"]'));
                }
            } else {
                $(this).addClass('fail');
            }
        });

        if (count === 2) {
            document.sign_in.submit();
        }
    }
    function recoveryQuery() {
        var $inputMail = $('input[name="LoginForm[email]"]'),
            count = 0,
            data,
            answer;

        if ($inputMail.val() !== '') {
            count++;
            if (!validateEmail($inputMail.val())) {
                count--;
                $('input[name=mail]').addClass('fail');
                make_input_info('Недопустимый формат Email', $inputMail);
            }
        } else {
            $inputMail.addClass('fail');
        }
        if (count > 0) {
            creat_lb(document.getElementById('tpl_loader').innerHTML);
            data = 'type=recovery&mail=' + $inputMail.val();
            $.ajax({
                type: 'post',
                data: data,
                url: '/site/recovery-password',
                success: function (msg) {
                    try {
                        answer = JSON.parse(msg);
                    } catch (e) {
                        creat_lb('<p class="content" style="color:white;">Извините, произошла ошибка, попробуйте позже</p>');
                        console.log('хреновый JSON + ' + e);
                        return false;
                    }
                    $('#lb_close').click();
                    if (answer.code === 'ok') {
                        $inputMail.remove();
                        $('#form_action button').removeClass('recovery').addClass('reset').text('Войти');
                        $('#error_str').text('На вашу почту высланы инструкции по входу в личный кабинет');
                    } else {
                        $('#error_str').text(answer.value);
                    }
                }
            });
        }
    }

    $('#form_action a').click(function (e) {
        e.preventDefault();
        $('input[name="LoginForm[password]"]').remove();
        $('.field-loginform-rememberme').remove();
        $(this).remove();
        $('.field-loginform-password').remove();
        $('button').addClass('recovery').text('Отправить');
        $('input[name=signin]').val('recovery_pass');
        var str = 'Для востановления пароля введите вашу почту, и нажмите оптравить.<br/>На указаный почтовый ящик будет выслана инструкция для восстановления пароля';
        if ($('#error_str').length > 0) {
            $('#error_str').text(str);
        } else {
            $('form').prepend('<p id="error_str">' + str + '</p>');
        }
    });

    $('#form_action button').click(function (e) {
        e.preventDefault();
        if ($(this).hasClass('recovery')) {
            recoveryQuery();
        } else {
            if ($(this).hasClass('reset')) {
                document.location.reload();
            } else {
                document.sign_in.submit();
            }
        }

    });
    $('input').focus(function () {
        $(this).removeClass('fail');
    });

    /*$(document).on('keyup change', '#loginform-password', function (e) {
        if ($(this).val() != '') {
            $(document).find('#form_action div').css('display', 'block');
            $(document).find('#form_action a').css('display', 'none');
        } else {
            $(document).find('#form_action div').css('display', 'none');
            $(document).find('#form_action a').css('display', 'block');
        }
    });*/

    $(document).on('click', '#lb_close', function () {
        $('#lb_box').animate({'opacity': 0}, 400, function () {
            $(this).remove();
        });
        $('body').removeClass('block_scrool');
    });

    $(document).on('click', '#lb_close_ico', function () {
        $('#lb_close').click();
    });
    if ($('#lb').length === 0) {
        $('head').append('<script type="text/tpl" id="lb"><div id=lb_box><div id=lb_close></div><div id=lb_wrap><svg id=lb_close_ico viewBox="0 0 20 20"><path d="M 0,0 L 20,20 M 0,20 L 20,0" stroke-width=4/></svg><div id=lb_wrap_scroll>{*content*}</div></div></div></script>');
    }
    if ($('#tpl_loader').length === 0) {
        $('head').append('<script type="text/tpl" id="tpl_loader" ><div class="floatingBarsG"><div class="blockG rotateG_01"></div><div class="blockG rotateG_02"></div><div class="blockG rotateG_03"></div><div class="blockG rotateG_04"></div><div class="blockG rotateG_05"></div><div class="blockG rotateG_06"></div><div class="blockG rotateG_07"></div><div class="blockG rotateG_08"></div></div></script>');
    }

    function fix_mobile_height() {
        if ($(window).width() < 760) {
            $('.page_wrapper').height($(window).height());
        }
    }

    fix_mobile_height();
    $(window).resize(function () {
        fix_mobile_height();
    });
    $(document).on('focusout', '#loginform-email', function () {
        $(this).val($(this).val().trim())
    });
});