<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;


?>

    <h2 class="top-content__title" style="text-align: center">Загрузить фотографию профиля</h2>

<?php $form = ActiveForm::begin(
    [
        'action' => ['/avatar/save-avatar/'],
    ]
); ?>

    <div class="ava">
        <div class="ava_int">
            <?=

            $form->field($model, 'file')->widget(FileInput::classname(), [
                'options' => ['accept' => 'image/*','multiple' => false],
                'pluginOptions' => [
                    'uploadUrl' => Url::to(['/avatar/upload-avatar']),
                    'maxFileCount' => 1,
                    'maxFileSize'=>28000,
                    'showUpload' => false
                ]
            ])->label('');

            // $form->field($model, 'file')->fileInput();

            ?>
        </div>
    </div>


    <div class="form-group" style="text-align: center">
        <?= Html::submitButton('Загрузить', ['class' => 'btn btn--size']) ?>
    </div>

<?php ActiveForm::end(); ?>


<?php
$js = <<<JS

$(document).ready(function(){
    if($('.file-drop-zone-title').length){
        $('.file-drop-zone-title').html('Добавить фото');
    }
});


 $('#w0').on('beforeSubmit', function(){
 //$($('.kv-file-upload')[0]).trigger('click');
 return false;
 });

$('#w0').on('beforeValidate', function(){    
    $($('.kv-file-upload')[0]).trigger('click');

    return $('#avatarform-file').on('fileuploaded', function(event, data, previewId, index) {
              var form = data.form, 
              files = data.files, extra = data.extra,          
              response = data.response, reader = data.reader;         
              //console.log('File uploaded triggered');   
              //console.log(response);  
              location.reload();     
    return false;
    });    
    return false;

});




$('.file-drop-zone').click(function() {
   $('#avatarform-file').trigger('click');
});


JS;
$this->registerJs($js);
