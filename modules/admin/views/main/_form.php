<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use  \zxbodya\yii2\tinymce\TinyMce;
use app\models\Level;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\MainPage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'header')->textInput(['maxlength' => true]) ?>
    <?= '<label class="control-label">Выбрать цвет</label>' ?>;
    <?= \kartik\color\ColorInput::widget([
        'model' => $model,
        'attribute' => 'block_color',
    ]) ?>
    <?= $form->field($model, 'content_block_color')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'description', [
        'template' => "<div>{input}\n<div class=\"error\">{error}</div></div>",
    ])->widget(TinyMce::className(), [
        'options' => ['rows' => 15],
        'language' => 'ru',
//        'spellcheckerUrl'=>'http://speller.yandex.net/services/tinyspell',
        'fileManager' => [
            'class' => \zxbodya\yii2\elfinder\TinyMceElFinder::className(),
            'connectorRoute' => 'el-finder/connector',
        ],
    ]); ?>
    <?php if ($model->image) { ?>
        <?= Html::img(Yii::getAlias('@rel_path_to_uploads') . '/' . $model->image, ['class' => 'profile_foto']); ?>
        <div class="del_link">
            <?php
            if ($model->image != 'blank.jpg') {
                echo Html::a('Удалить', ['article/delete-img'], [
                    'data-method' => 'POST',
                    'data-params' => [
                        'id' => $model->id,
                    ],
                ]);
            }
            ?>
        </div>
    <?php } ?>

    <?php
    if (!$model->image) {
        echo Html::img('/uploads/blank.jpg', ['class' => 'profile_foto']);
    }
    ?>


    <?= $form->field($model, 'image')->fileInput(['onchange' => 'readURL(this);']); ?>
    <?= $form->field($model, 'link_video')->textInput(['maxlength' => true]) ?>


    <h2>Программа</h2>
    <?= $form->field($model, 'program_name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'program_description')->textarea() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
