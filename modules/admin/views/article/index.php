<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use app\models\Level;
use app\models\Theme;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Статьи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить статью', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            [
                'attribute' => 'level_name',
                'label' => 'Уровень',
                'filter'=>Level::getValuesArray(),
                'value' => function($data) {
                    return $data->level->name;
                },
            ],
            [
                'attribute' => 'theme_name',
                'label' => 'Тема',
                'filter'=>Theme::getValuesArray(),
                'value' => function($data) {
                    return $data->theme->name;
                },
            ],
            'name',
            //'description',
            [
                'label' => 'Картинка',
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::img(Yii::getAlias('@rel_path_to_uploads') . '/' . $data->img, [
                        'alt' => 'yii2 - картинка в gridview',
                        'style' => 'width:150px;'
                    ]);
                },
            ],
            'timer',
            'points',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}&nbsp;{delete}'
            ],
        ],
    ]); ?>
</div>
