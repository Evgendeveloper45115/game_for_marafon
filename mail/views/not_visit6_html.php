<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Email</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,700&amp;subset=cyrillic" rel="stylesheet">

    <style type="text/css">
        * {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 15px;
        }

        div, p, span, strong, b, em, i, a, li, td {
            -webkit-text-size-adjust: none;
        }

        img {
            max-width: 100%;
            text-align: center;
        }

        @media only screen and (max-device-width: 748px), only screen and (max-width: 748px) {
            body {
                margin: 0 auto;
                max-width: 320px;
            }

        }

        @media only screen and (max-device-width: 748px), only screen and (max-width: 748px) {
            table {
                width: 320px !important;
            }

        }

        @media only screen and (max-device-width: 748px), only screen and (max-width: 748px) {
            span.size28 {
                font-size: 28px !important;
            }

        }

        @media only screen and (max-device-width: 748px), only screen and (max-width: 748px) {
            .block {
                display: block;
                width: 257px !important;
                margin: 0 auto !important;
            }

        }

        @media only screen and (max-device-width: 748px), only screen and (max-width: 748px) {
            .margin0 {
                margin: 0 !important;
            }

        }

        @media only screen and (max-device-width: 748px), only screen and (max-width: 748px) {
            .margin_bl {
                display: block;
                margin: 0 auto 28px;
            }

        }

        @media only screen and (max-device-width: 748px), only screen and (max-width: 748px) {
            .button {
                width: 295px !important;
                margin-top: 30px !important;
            }

        }

        @media only screen and (max-device-width: 748px), only screen and (max-width: 748px) {
            .padding {
                padding: 6px 0 0 32px !important;
            }

        }

        @media only screen and (max-device-width: 748px), only screen and (max-width: 748px) {
            .block2 {
                display: table;
                width: 145px !important;
                vertical-align: bottom;
            }

        }

        @media only screen and (max-device-width: 748px), only screen and (max-width: 748px) {
            .block3 {
                width: 140px !important;
                display: table;
            }

        }

        @media only screen and (max-device-width: 748px), only screen and (max-width: 748px) {
            .block4 {
                width: 140px !important;
                display: table;
            }

        }

        @media only screen and (max-device-width: 748px), only screen and (max-width: 748px) {
            .height0 {
                height: 0 !important;
            }

        }

        @media only screen and (max-device-width: 748px), only screen and (max-width: 748px) {
            .center {
                font-size: 14px !important;
            }

        }

        @media only screen and (max-device-width: 748px), only screen and (max-width: 748px) {
            .width155 {
                width: 147px !important;
            }

        }

        @media only screen and (max-device-width: 748px), only screen and (max-width: 748px) {
            .widht150 {
                width: 150px !important;
            }

        }

        @media only screen and (max-device-width: 748px), only screen and (max-width: 748px) {
            .height {
                height: 0 !important;
            }

        }

        @media only screen and (max-device-width: 748px), only screen and (max-width: 748px) {
            .float {
                float: none !important;
                margin: 0 auto !important;
            }

        }

        @media only screen and (max-device-width: 748px), only screen and (max-width: 748px) {
            .none {
                display: none !important;
            }

        }

        @media only screen and (max-device-width: 748px), only screen and (max-width: 748px) {
            .width100 {
                width: 100% !important;
            }

        }

        @media only screen and (max-device-width: 748px), only screen and (max-width: 748px) {
            .paddi {
                padding: 6px 0 0 !important;
            }

        }</style>
</head>
<body style="margin: 0 auto;padding: 0;">


<table cellpadding="0" cellspacing="0" width="100%" border="0"
       style="min-width:300px;max-width:740px;border-collapse:collapse;" align="center">
    <tr>
        <td align="center">

            <table cellpadding="0" cellspacing="0" width="100%" border="0"
                   style="min-width:300px;max-width:740px;border-collapse:collapse;background:url('https://beauty-matrix.ru/img2/bg7.jpg') no-repeat top;background-size:cover;"
                   align="center" bgcolor="#55bd9d">
                <tr>
                    <td align="center">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0"
                               style="border-collapse:collapse;max-width:700px;" align="center">
                            <tr>
                                <td height="250" align="center" valign="top">
                                    <table align="center"
                                           style="max-width:700px;margin-top:30px;border-collapse:collapse;"
                                           cellspacing="0" cellpadding="0" border="0" width="100%">
                                        <tr>
                                            <td align="center" width="96">
                                                <a href="#" target="_blank">
                                                    <img src="https://image.ibb.co/cUZUvF/logo.png" alt=""></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span style="margin-top: 20px; display: block; font-family: 'Roboto Slab', serif; font-size: 34px;color:#ffffff;text-align: center;line-height: 35px;"
                                                      class="size28">Продолжи игру: <br/>"Построй стройную фигуру!"</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="1" height="80"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>


            <table width="100%" align="center" border="0"
                   style="border-collapse:collapse; max-width: 660px;min-width: 300px;">
                <tr>
                    <td width="1" height="50"></td>
                </tr>
                <tr>
                    <td align="center">
                        <span style="display:block;color: #2a2a2a;font-size: 22px;font-family: 'Roboto Slab', serif;font-weight: bold;">О движении бодипозитив и любви к себе</span><br/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table align="left" width="100%" border="0"
                               style="border-collapse:collapse;max-width: 400px;margin:0 10px 0 0;" cellspacing="0"
                               cellpadding="0">
                            <tr>
                                <td width="1" height="0"></td>
                            </tr>

                            <tr>
                                <td align="left" width="100%"
                                    style="color: #676767;line-height: 25px;font-weight: 300;font-size: 17px;"
                                    class="widht300 margin0 center">
                                    <span style="color: #676767;font-size:17px;">На западе есть такая профессия, как   <span
                                                style="font-size: 17px; color: #b460d2;font-style: italic;font-weight: 600;"
                                                class="center">лайф-коуч, </span> помогающий людям чувствовать себя комфортно в том теле, которое у них есть (конкретно с этой специализацией). </span>
                                </td>
                            </tr>
                            <tr>
                                <td width="1" height="20"></td>
                            </tr>
                            <tr>
                                <td align="left" width="100%"
                                    style="color: #676767;line-height: 25px;font-weight: 300;font-size: 17px;"
                                    class="widht300 margin0 center">
                                    <span style="color: #676767;font-size:17px;">Пример - инстаграм некой Джесс <span
                                                style="font-size: 17px; color: #b460d2;font-style: italic;font-weight: 600;"
                                                class="center">@plankingforpizza</span> (даже ник говорящий), 26-летней спортивной, симпатичной и полненькой девушки. </span>
                                </td>
                            </tr>

                            <tr>
                                <td width="1" height="20"></td>
                            </tr>
                        </table>

                        <table width="100%" border="0"
                               style="border-collapse:collapse;max-width: 191px;display:inline-block;margin:0 0 0 50px;"
                               cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="191" class="widht300 block" style="display: block;">
                                    <img src="http://i68.tinypic.com/w82jo7.jpg" alt="" width="191">
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
                <tr>
                    <td width="1" height="20"></td>
                </tr>
            </table>

            <table width="100%" align="center" border="0"
                   style="border-collapse:collapse;max-width: 650px;min-width: 300px;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="border-left: 3px solid #1fe2a6;padding: 0 0 0 22px;">
                        <span style="line-height:25px; font-weight:300;color: #676767;font-size: 17px;" class="center">На западе вследствие  <span
                                    style="font-size: 17px; color: #b460d2;font-style: italic;font-weight: 600;"
                                    class="center">массового ожирения</span> вообще стали популярны такие течения, как  <span
                                    style="font-size: 17px; color: #b460d2;font-style: italic;font-weight: 600;"
                                    class="center">бодипозитив</span> и данные лайфкоучи - для страдающего от своей полноты человека – его путь к довольству собой (и сладкому самообману!). Человеку говорится - “Кушай пироженку! Ты прекрасна в любом теле.” <br><br> Складывается ощущение, что эти движения на самом деле спонсируются какими-то масонами (шууутка), производителями фастфуда и сахарной промышленности.</span>
                    </td>
                </tr>
            </table>

        </td>
    </tr>
</table>

</td>
</tr>
</table>

<table width="100%" align="center" border="0" style="border-collapse:collapse;max-width: 770px;" valign="top">
    <tr>
        <td>
            <table width="100%" style="border-collapse:collapse;max-width: 220px;margin:0 36px 25px 30px;" align="left">
                <tr>
                    <td width="1" height="30"></td>
                </tr>
                <tr>
                    <td align="left" width="220" class="widht300 block">
                        <img src="http://i66.tinypic.com/2up8yki.jpg" alt="" width="220" style="display:block;">
                    </td>
                </tr>

            </table>


            <table width="100%" style="border-collapse:collapse;max-width: 430px;min-width: 300px;margin:45px 0 0 0;">

                <tr>
                    <td width="100%" style="max-width:500px;display: inline-block;" class="widht300 margin0">
                        <span style="display: inline-block;color: #676767;line-height: 25px;font-weight: 300;font-size: 17px;padding:0 0 0 5px;"
                              class="center">Настоящий бодипозитив и комфорт в своем теле – это когда вы воспринимаете свои природные данные как факт,  <span
                                    style="font-size: 17px; color: #b460d2;font-style: italic;font-weight: 600;"
                                    class="center"> не спеша пытаться поменять</span>.. есть константы, есть переменные.</span>
                    </td>
                </tr>

                <tr>
                    <td width="1" height="10"></td>
                </tr>

            </table>
        </td>
    </tr>
</table>


<table width="100%" align="center" border="0" style="border-collapse:collapse;max-width: 650px;min-width: 300px;"
       cellpadding="0" cellspacing="0">
    <tr>
        <td style="border-left: 3px solid #1fe2a6;padding: 0 0 0 22px;">
              <span style="line-height:25px; font-weight:300;color: #676767;font-size: 17px;" class="center"> <span
                          style="font-size: 17px; color: #b460d2;font-style: italic;font-weight: 600;" class="center">Константы</span>
- рост, тип телосложения, тип фигуры, форма черепа, ног.  <span
                          style="font-size: 17px; color: #b460d2;font-style: italic;font-weight: 600;" class="center">Переменные</span> - цвет волос, объем мышц и количество жира в теле... Про форму носа, губ, размер и форму груди умолчим, кажется пластика стала явлением обыденным.. женщины сейчас ложатся под нож многократно.</span>
        </td>
    </tr>
</table>


</td>
</tr>
</table>

<table width="100%" align="center" border="0" style="border-collapse:collapse;max-width: 700px;" valign="top">
    <tr>
        <td>
            <table width="100%" style="border-collapse:collapse;max-width: 440px;min-width: 300px;margin:45px 0 0 0;"
                   border="0" cellspacing="0" cellpadding="0" align="right">

                <tr>
                    <td width="100%" style="max-width:500px;display: inline-block;" class="widht300 margin0">
                   <span style="display: inline-block;color: #676767;line-height: 25px;font-weight: 300;font-size: 17px;padding:0 0 0 5px;"
                         class="center"><span style="color: #676767;font-size:17px;">Принять свои константы как данность,  <span
                                   style="font-size: 17px; color: #b460d2;font-style: italic;font-weight: 600;"
                                   class="center"> полюбить себя</span>. А вот с переменными - <span
                                   style="font-size: 17px; color: #b460d2;font-style: italic;font-weight: 600;"
                                   class="center">можно и нужно работать.  Количество жира </span> в теле не только влияет на эстетику, но еще и на здоровье, ожирение внутренних органов, риск сердечно-сосудистых заболеваний, риск диабета 2 типа и т.д. Прочие переменные можно менять до бесконечности.</span>
                    </td>
                </tr>

                <tr>
                    <td width="1" height="10"></td>
                </tr>

            </table>

            <table width="100%" style="border-collapse:collapse;max-width: 184px;margin:0 0 25px 30px;" align="left"
                   border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="1" height="30"></td>
                </tr>
                <tr>
                    <td align="left" width="184" class="widht300 block">
                        <img src="http://i64.tinypic.com/24phoq9.jpg" alt="" width="184" style="display:block;">
                    </td>
                </tr>

            </table>
        </td>
    </tr>
    <tr>
        <td width="1" height="20"></td>
    </tr>
</table>


<table cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse;max-width:750px;" width="100%"
       align="center" bgcolor="#f6f6f6">
    <tr>
        <td>

            <table cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse;max-width:650px;"
                   width="100%" align="center">
                <tr>
                    <td width="1" height="30"></td>
                </tr>
                <tr>
                    <td align="left">
                        <span style="display:block;text-align: left; font-family:'Roboto Slab', serif;color:#2b2b2b;font-weight:bold;font-size:22px;padding:0 0 0 10px;">Ключ к гармонии</span>
                    </td>
                </tr>
            </table>


            <table cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse;max-width:640px;"
                   width="100%" align="center">
                <tr>
                    <td width="1" height="20"></td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" align="center" border="0"
                               style="border-collapse:collapse;max-width: 650px;min-width: 300px;" cellpadding="0"
                               cellspacing="0">
                            <tr>
                                <td style="border-left: 3px solid #1fe2a6;padding: 0 0 0 22px;">
                                    <span style="line-height:25px; font-weight:300;color: #676767;font-size: 17px;"
                                          class="center">Принять себя таким, какой ты есть, и <span
                                                style="font-size: 17px; color: #b460d2;font-style: italic;font-weight: 600;"
                                                class="center">при желании улучшать </span> там, где это возможно.</span>
                                </td>
                            </tr>
                        </table>


                        <table width="100%" align="center" border="0"
                               style="border-collapse:collapse; max-width: 660px;min-width: 300px;">
                            <tr>
                                <td width="1" height="30"></td>
                            </tr>
                            <tr>
                                <td>
                                    <table align="left" width="100%" border="0"
                                           style="border-collapse:collapse;max-width: 412px;margin:0 10px 0 0;"
                                           cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="1" height="0"></td>
                                        </tr>
                                        <tr>
                                            <td align="left" width="100%"
                                                style="color: #676767;line-height: 25px;font-weight: 300;font-size: 17px;"
                                                class="widht300 margin0 center">
                                                <span style="color: #676767;font-size:17px;">В основном это касается вашей личности, работать над собой постоянно и отмечать свои успехи, промежуточные результаты, <span
                                                            style="font-size: 17px; color: #b460d2;font-style: italic;font-weight: 600;"
                                                            class="center"> не скатываясь</span> ни в самоуничижение и самоедство, ни в самодовольство и потворство своим слабостям. Очень тонкая грань.</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="1" height="20"></td>
                                        </tr>
                                    </table>

                                    <table width="100%" border="0"
                                           style="border-collapse:collapse;max-width:168px;display:inline-block;margin:0 0 0 45px;"
                                           cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td width="168" class="widht300 block" style="display: block;">
                                                <img src="http://i66.tinypic.com/oaatyr.jpg" alt="" width="168">
                                            </td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>
                            <tr>
                                <td width="1" height="40"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            <table width="100%" align="center" border="0"
                   style="border-collapse:collapse;max-width:750px;min-width:300px;" bgcolor="#ffffff">
                <tr>
                    <td width="1" height="20"></td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0" border="0"
                               style="border-collapse:collapse;max-width:640px;" width="100%" align="center">
                            <tr>
                                <td>
                                    <table cellspacing="0" cellpadding="0" border="0"
                                           style="border-collapse:collapse;max-width:390px;display:inline-block;"
                                           width="100%" align="center">
                                        <tr>
                                            <td align="center">
                                                <p style="padding:0 0 0 5px;font-size:17px;color:#676767;line-height:25px;">
                                                    Узнать больше о правильном питании, тренировках, мотивации и
                                                    психологии, чтобы похудеть и построить стройную фигуру:
                                                </p>
                                                <p>
                                                    <a href="https://game.beauty-matrix.ru/?utm_source=email&utm_medium=game3&utm_campaign=vozvrat72"
                                                       target=""
                                                       style="background:#1fe2a6;border-radius:50px;display:block;max-width:330px;height:37px;padding:20px 0 0;text-align:center;font-weight:bold;font-family:'Roboto Slab', serif;text-transform:uppercase;font-size:15px;color:#ffffff;text-decoration:none;"
                                                       class="button">Вернуться в игру</a>
                                                </p>
                                            </td>
                                        </tr>
                                    </table>
                                    <table cellspacing="0" cellpadding="0" border="0"
                                           style="margin:15px 0 0 80px;border-collapse:collapse;max-width:160px;display:inline-block;"
                                           width="100%">
                                        <tr>
                                            <td>
                                                <img src="https://beauty-matrix.ru/img2/4.png" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="1" height="20"></td>
                </tr>
            </table>
            <table width="100%" align="center" border="0" style="max-width:640px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="1" height="50"></td>
                </tr>
                <tr>
                    <td align="center">
                        <span style="color: #3e3e3e;font-size: 17px;">С наилучшими пожеланиями,</span> <br><b
                                style="color:#3e3e3e;font-size:17px;">команда Beauty Matrix</b>
                    </td>
                </tr>
                <tr>
                    <td width="1" height="50"></td>
                </tr>
            </table>
            <table width="100%" align="center" border="0" style="background:#232323;max-width:740px;" cellspacing="0"
                   cellpadding="0">
                <tr>
                    <td>

                        <table width="100%" align="center" style="border-collapse:collapse;max-width:740px;" border="0"
                               cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="1" height="40"></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <a href="#" target="_blank" style="text-decoration:none;">
                                        <img src="https://image.ibb.co/goY1cv/logo_footer.png" alt=""></a>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <span style="color: #676767;">Все права защищены</span>
                                </td>
                            </tr>
                            <tr>
                                <td width="1" height="10"></td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </table>
</body>
</html>