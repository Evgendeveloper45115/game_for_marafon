<a style="" class="btn js-venobox vbox-item" href="#share" id="sharer" data-vbtype="inline">share</a>
<div class="js-popup" id="share">
    <div class="popup popup--share">
        <div class="popup-head">
            <h1 class="popup__title">Поделись с друзьями</h1><span class="popup-close close-friend"></span>
        </div>
        <div class="popup-content popup-content--share">
            <div class="popup-content__img popup-content__img--share">
                <img src="/images/star-popup.png">
            </div>
            <div class="popup-content__right popup-content__right--share">
                <p class="popup-content__text popup-content__text--share">Расскажите о нашей игре друзьями, чтобы они
                    тоже могли совершенствоваться.
                    <span class="get10">
                        <b>и получите дополнительных 10 баллов</b>
                    </span>
                    <span class="choose_net">
                        выберите вашу соц. сеть
                    </span>
                    <span class="popup__social">
                    <a class="popup__link popup__link--vk popup__link--soc"
                       href="http://vk.com/share.php?url=http://game.beauty-matrix.ru&title=Выполняй задания и худей, узнавай новую и эффективную информацию о питании и построй фигуру своей мечты.&description=Выполняй задания и худей, узнавай новую и эффективную информацию о питании и построй фигуру своей мечты.&image=https://game.beauty-matrix.ru/uploads/59d7d36bb2fea.jpg"
                       target="_blank"></a>
                    <a class="popup__link popup__link--fb popup__link--soc"
                       href="https://www.facebook.com/sharer/sharer.php?u=http://game.beauty-matrix.ru"
                       target="_blank"></a>
                </span>
                </p>
            </div>
        </div>
    </div>
</div>


<?php
$js = <<<JS


$(document).on("click",".popup__link--soc", function(){
    
    $('.close-friend').click();
    
    $.ajax({
    url: '/site/point/',
    type: 'Get',
    success: function(res){
    //console.log('попап');
    },
    error: function(){
    //console.log('error');
    }
    });
    
    $('#did_it').trigger('click');
});

/*
        $(function(){
        $('#sharer').trigger('click');
$('.vbox-overlay').on('click', function(){
//console.log('here');
//$('.popup-close').trigger('click');
$('#did_it').trigger('click');
$.ajax({
url: '/site/point/',
type: 'Get',
success: function(res){
//console.log('попап');
},
error: function(){
//console.log('error');
}
});
});

$(document).on('click','.popup__link--soc',function(){
$('.close-friend').click();
//$('button[type="submit"]').click();
});
})*/

JS;
$this->registerJs($js);
?>
