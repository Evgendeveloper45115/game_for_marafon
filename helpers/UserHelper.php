<?php

namespace app\helpers;

use Yii;
use yii\helpers\VarDumper;

class UserHelper{

    public static function getUserLevel(){
        if(Yii::$app->user->isGuest){
            return 1;
        }
        else {
            return Yii::$app->user->identity->profile->level;
            }
        }


}