<?php

use yii\db\Migration;

class m170911_072748_triggers_mail extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%triggers_mail}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'not_visit1' => $this->boolean(),
            'not_visit3' => $this->boolean(),
            'not_visit6' => $this->boolean(),
            'finished_5_level' => $this->boolean(),
            'passed_test' => $this->boolean(),
            'not_passed_test' => $this->boolean(),
            'not_passed_not_buy' => $this->boolean(),
            'not_passed_not_retest' => $this->boolean(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk-triggers_mail-user_id-user-id', 'triggers_mail', 'user_id', 'user', 'id',
            'RESTRICT', 'CASCADE'
        );

    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-triggers_mail-user_id-user-id', 'triggers_mail');
        $this->dropTable('triggers_mail');
    }

}
