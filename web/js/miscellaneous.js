function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.js_preview')
                .attr('src', e.target.result)
                .width(150);
            $('.js_preview').show();
        };

        reader.onload = function (e) {
            $('.img_profile')
                .attr('src', e.target.result);
            $('.img_profile').show();
        };

        reader.readAsDataURL(input.files[0]);
    }
};

function readURLClassName(input, classname) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        if (classname) {
            reader.onload = function (e) {
                $('.' + classname)
                    .attr('src', e.target.result)
                    .css('background-image', 'url(' + e.target.result + ')');
            };
        }
        reader.readAsDataURL(input.files[0]);
    }
};


$('.js-show_old_sizes').click(function () {
    $('.old-sizes').show();
    $('html, body').animate({scrollTop: $('.old-sizes').offset().top}, 300);
});


$(".points_mob").on("tap", function () {
    $('.tooltop_wrapper').show();
    window.setTimeout(function () {
        $('.tooltop_wrapper').css('display', 'none');
    }, 5000);
});


$(document).ready(function () {


    $('.youtube-link').click(function () {
        window.setTimeout(function () {
            var venoOptions = {
                // is called after plugin initialization.
                cb_init: function (plugin) {
                    $('.vbox-close').css('display', 'block');
                },
            }
            $('.venobox').venobox(venoOptions);
        }, 200);
    });

    $('.js-video').click(function () {
        window.setTimeout(function () {
            var venoOptions = {
                // is called after plugin initialization.
                cb_init: function (plugin) {
                    $('.vbox-close').css('display', 'block');
                },
            }
            $('.venobox').venobox(venoOptions);
        }, 200);
    });

});


