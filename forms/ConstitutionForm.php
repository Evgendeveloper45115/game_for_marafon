<?php
namespace app\forms;

use yii\base\Model;

class ConstitutionForm extends Model
{
    public $constitution;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['constitution'], 'required'],
            [['constitution'], 'integer'],
        ];
    }

}
