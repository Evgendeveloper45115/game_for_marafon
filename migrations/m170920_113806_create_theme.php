<?php

use yii\db\Migration;

class m170920_113806_create_theme extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%theme}}', [
            'id' => $this->primaryKey(),
            'level_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'description' => $this->text()->notNull(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk-theme-level_id-level-id', 'theme', 'level_id', 'level', 'id',
            'RESTRICT', 'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropTable('{{%theme}}');
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170920_113806_create_theme cannot be reverted.\n";

        return false;
    }
    */
}
