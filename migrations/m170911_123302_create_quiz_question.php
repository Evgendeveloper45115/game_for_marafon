<?php

use yii\db\Migration;

class m170911_123302_create_quiz_question extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%quiz_question}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(),
            'title' => $this->string(),
            'answer' => $this->string(),
            'answer2' => $this->string(),
            'answer3' => $this->string(),
            'answer4' => $this->string(),
            'answer5' => $this->string(),
            'answer6' => $this->string(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk-quiz_question-category_id-category-id', 'quiz_question', 'category_id', 'quiz_category', 'id',
            'RESTRICT', 'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropTable('quiz_category');
    }
}
