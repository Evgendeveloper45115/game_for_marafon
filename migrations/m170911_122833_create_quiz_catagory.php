<?php

use yii\db\Migration;

class m170911_122833_create_quiz_catagory extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%quiz_category}}', [
            'id' => $this->primaryKey(),
            'name' => $this->text(),
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('quiz_category');
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170911_122833_create_quiz_catagory cannot be reverted.\n";

        return false;
    }
    */
}
