<?php

namespace app\controllers;

use app\components\TriggerMailer;
use app\models\PassedArticle;
use app\models\SocialFriend;
use app\models\Test;
use app\models\Theme;
use app\models\User;
use Yii;
use yii\web\Controller;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\helpers\VarDumper;
use app\models\Level;
use app\models\Article;
use app\models\Video;
use yii\helpers\ArrayHelper;


class LevelController extends Controller
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }


    public function actionIndex($id)
    {
        $level = Level::findOne($id);
        if (Yii::$app->user->identity->profile->level < $level->number_int) {
            return $this->redirect('/level/index/' . Yii::$app->user->identity->profile->level);
        }
        $articlesTheory = null;
        $articlesTask = null;
        $videoTheory = null;
        $videoTask = null;
        $test = null;

        if (!$level) {
            throw new NotFoundHttpException('Уровень не найден');
        }

        $themesTheory = Theme::find()
            ->where(['level_id' => $level->id])
            ->andWhere(['type' => Theme::THEORY])
            ->all();

        $themesTask = Theme::find()
            ->where(['level_id' => $level->id])
            ->andWhere(['type' => Theme::TASK])
            ->all();

        $articlesTheory = Article::find()
            ->where(['level_id' => $level->id])
            ->andWhere(['type' => Article::THEORY])
            ->orderBy('weight')
            ->all();


        $articlesTask = Article::find()
            ->where(['level_id' => $level->id])
            ->andWhere(['type' => Article::TASK])
            ->orderBy('weight')
            ->all();

        if ($level->type == Level::BLOCK) {
            if (count($articlesTheory) + count($themesTheory) < 2) {
                $videoTheory = Video::find()
                    ->where(['level_id' => $level->id])
                    ->andWhere(['type' => Video::THEORY])
                    ->orderBy('weight')
                    ->all();
            }
            if (count($articlesTheory) + count($themesTheory) + count($videoTheory) < 2) {
                $test = Test::find()->where(['level_id' => $level->id])->all();
            }
            if (count($articlesTask) + count($themesTask) < 2) {
                $videoTask = Video::find()
                    ->where(['level_id' => $level->id])
                    ->andWhere(['type' => Video::TASK])
                    ->orderBy('weight')
                    ->all();
            }
            if (count($articlesTask) + count($themesTask) + count($videoTask) < 2) {
                $test = Test::find()->where(['level_id' => $level->id])->all();
            }
        } elseif (($videoCount = (count($articlesTheory) + count($themesTheory)) + (count($articlesTask) + count($themesTask))) < 5) {
            $videoTheory = Video::find()
                ->where(['level_id' => $level->id])
                ->andWhere(['type' => Video::THEORY])
                ->limit(5 - $videoCount)
                ->orderBy('weight')
                ->all();
            if (empty($videoTheory)) {
                $videoTask = Video::find()
                    ->where(['level_id' => $level->id])
                    ->andWhere(['type' => Video::TASK])
                    ->orderBy('weight')
                    ->limit(5 - $videoCount)
                    ->all();
            }
            if (((count($articlesTheory) + count($themesTheory)) + (count($articlesTask) + count($themesTask)) + (count($videoTheory) + count($videoTask))) < 5) {
                $test = Test::find()->where(['level_id' => $level->id])->all();
            }
        }

        $theory = $this->initTheory($themesTheory, $articlesTheory, $videoTheory);
        $tasks = $this->initTasks($themesTask, $articlesTask, $videoTask, $test);
        $line = ArrayHelper::merge($theory, $tasks);
        ArrayHelper::multisort($theory, ['weight'], [SORT_ASC]);
        ArrayHelper::multisort($tasks, ['weight'], [SORT_ASC]);
        ArrayHelper::multisort($line, ['weight'], [SORT_ASC]);
        $count = 0;

        $count_levels = count(Level::find()
            ->where(['is_active' => true])
            ->all());

        foreach ($line as $value) {
            $relationName = lcfirst(\yii\helpers\StringHelper::basename(get_class($value))) . '_id';
            $class = 'app\models\Passed' . \yii\helpers\StringHelper::basename(get_class($value));
            $passedClass = $class::find()
                ->where(['user_id' => Yii::$app->getUser()->getId()])
                ->andWhere(["$relationName" => $value->id])
                ->one();
            if ($passedClass) {
                $count++;
            }
        }
        $levelUP = false;
        $finalTest = false;
        $emailPopup = false;
        $text = '';
        if (Yii::$app->session->get('greetings') !== 1) {
            Yii::$app->session->set('greetings', 0);
        };
        $start = 0;
        if ($count == 0) {
            $start = 1;
        }
        if ($level->type == Level::BLOCK && $count == 4) {
            if ($id + 1 > Yii::$app->user->identity->profile->level) {
                if (Yii::$app->user->identity->profile->level != $count_levels) {
                    $levelUP = true;
                } elseif (Yii::$app->user->identity->testAnswer == 0 && Yii::$app->user->identity->profile->level == $count_levels) {
                    if (Yii::$app->session->get('greetings') == 0) {
                        $finalTest = '/level/greetings';
                    }
                    Yii::$app->session->set('greetings', 1);
                } elseif (Yii::$app->user->identity->profile->level == $count_levels && Yii::$app->user->identity->testAnswer > 5) {
                    if (Yii::$app->session->get('greetings') == 0) {
                        $finalTest = '/test/greetings';
                    }
                    Yii::$app->session->set('greetings', 1);
                } elseif (Yii::$app->user->identity->profile->level == $count_levels && Yii::$app->user->identity->testAnswer < 5 && Yii::$app->user->identity->testAnswer != 0) {
                    $finalTest = '/test/result';
                }
                Yii::$app->user->identity->profile->level += Yii::$app->user->identity->profile->level < $count_levels ? 1 : 0;
                if (Yii::$app->user->identity->profile->level == 4) {
                    Yii::$app->user->identity->profile->account += 100;
                    $text = 'Бонус: +100 баллов';
                    TriggerMailer::finished4Level(Yii::$app->user->identity);
                } elseif (Yii::$app->user->identity->profile->level == 6) {
                    TriggerMailer::finished6Level(Yii::$app->user->identity);
                    if (Yii::$app->user->identity->profile->account < 975) {
                        Yii::$app->user->identity->profile->account += 200;
                        $text = 'Бонус: +200 баллов';
                    }
                } elseif (Yii::$app->user->identity->profile->level == 5) {
                    TriggerMailer::finished5Level(Yii::$app->user->identity);
                }
                Yii::$app->user->identity->profile->save(false);
            }

        } elseif ($level->type == Level::LINE && $count == 5) {
            if ($id + 1 > Yii::$app->user->identity->profile->level) {
                if (Yii::$app->user->identity->profile->level != $count_levels) {
                    $levelUP = true;
                } elseif (Yii::$app->user->identity->testAnswer == 0 && Yii::$app->user->identity->profile->level == $count_levels) {
                    if (Yii::$app->session->get('greetings') == 0) {
                        $finalTest = '/level/greetings';
                    }
                    Yii::$app->session->set('greetings', 1);
                } elseif (Yii::$app->user->identity->profile->level == $count_levels && Yii::$app->user->identity->testAnswer > 5) {
                    TriggerMailer::passedTest(Yii::$app->user->identity);
                    if (Yii::$app->session->get('greetings') == 0) {
                        $finalTest = '/test/greetings';
                    }
                    Yii::$app->session->set('greetings', 1);
                } elseif (Yii::$app->user->identity->profile->level == $count_levels && Yii::$app->user->identity->testAnswer < 5 && Yii::$app->user->identity->testAnswer != 0) {
                    TriggerMailer::notPassedTest(Yii::$app->user->identity);
                    if (Yii::$app->session->get('greetings') == 0) {
                        $finalTest = '/test/result';
                    }
                    Yii::$app->session->set('greetings', 1);
                }

                Yii::$app->user->identity->profile->level += Yii::$app->user->identity->profile->level < $count_levels ? 1 : 0;;
                if (Yii::$app->user->identity->profile->level == 4) {
                    Yii::$app->user->identity->profile->account += 100;
                    $text = 'Бонус: +100 баллов';
                    TriggerMailer::finished4Level(Yii::$app->user->identity);

                } elseif (Yii::$app->user->identity->profile->level == 6) {
                    TriggerMailer::finished6Level(Yii::$app->user->identity);
                    if (Yii::$app->user->identity->profile->account < 975) {
                        Yii::$app->user->identity->profile->account += 200;
                        $text = 'Бонус: +200 баллов';
                    }
                } elseif (Yii::$app->user->identity->profile->level == 5) {
                    TriggerMailer::finished5Level(Yii::$app->user->identity);
                }
                Yii::$app->user->identity->profile->save(false);
            }
        } elseif ($level->type == Level::LINE && $count == 2 && $level->id == 1 && !Yii::$app->user->identity->email || $level->type == Level::LINE && $count == 2 && $level->id == 1 && stristr(Yii::$app->user->identity->email, 'soc') ) {
            $emailPopup = true;
        }
        if ($finalTest != false) {
            return $this->redirect($finalTest);
        }
        $social = false;
        if (Yii::$app->user->identity->profile->level == 4 && $id == 4) {
            $soc = SocialFriend::find()->where(['user_id' => Yii::$app->getUser()->getId()])->one();
            if ($soc == null) {
                $soc = new SocialFriend();
                $soc->user_id = Yii::$app->getUser()->getId();
                $soc->save();
                $social = true;
            }
        }
        return $this->render('index', [
            'theory' => $theory,
            'tasks' => $tasks,
            'line' => $line,
            'level' => $level,
            'levelUP' => $levelUP,
            'social' => $social,
            'count' => $count,
            'text' => $text,
            'emailPopup' => $emailPopup,
            'start' => $start,
        ]);
    }

    /**
     * @param $themesTheory
     * @param $articlesTheory
     * @param $videoTheory
     * @return array
     */
    public function initTheory($themesTheory, $articlesTheory, $videoTheory)
    {

        $theory = [];

        if (!empty($themesTheory)) {
            foreach ($themesTheory as $item) {
                $theory[] = $item;
            }
        }

        if (!empty($articlesTheory)) {
            foreach ($articlesTheory as $item) {
                $theory[] = $item;
            }
        }

        if (!empty($videoTheory)) {
            foreach ($videoTheory as $item) {
                $theory[] = $item;
            }
        }
        return $theory;
    }

    /**
     * @param $themesTask
     * @param $articlesTask
     * @param $videoTask
     * @param $test
     * @return array
     */
    public function initTasks($themesTask, $articlesTask, $videoTask, $test)
    {

        $tasks = [];

        if (!empty($themesTask)) {
            foreach ($themesTask as $item) {
                $tasks[] = $item;
            }
        }

        if (!empty($articlesTask)) {
            foreach ($articlesTask as $item) {
                $tasks[] = $item;
            }
        }

        if (!empty($videoTask)) {
            foreach ($videoTask as $item) {
                $tasks[] = $item;
            }
        }
        if (!empty($test)) {
            foreach ($test as $item) {
                $tasks[] = $item;
            }
        }
        return $tasks;
    }

    public
    function actionGreetings()
    {

        return $this->render('greetings', [
        ]);
    }

    public
    function actionMap()
    {
        $levels = Level::find()
            ->where(['is_active' => true])
            ->all();
        $passedArt = PassedArticle::find()->where(['article_id' => [60, 61, 62, 63, 64], 'user_id' => Yii::$app->user->id])->all();

        return $this->render('map', [
            'levels' => $levels,
            'passedArt' => $passedArt,
        ]);
    }

    public function actionAddEmail()
    {
        $user = User::findOne(Yii::$app->user->id);
        $user->email = Yii::$app->request->post('email');
        $user->save(false);
        echo 'ok';
        return;
    }

    public function actionAddWatsapp()
    {
        \Yii::$app->mail->compose('views/watsapp_html',
            ['phone' => Yii::$app->request->post('email'), 'name' => Yii::$app->request->post('name'), 'htmlLayout' => 'layouts/trigger_mail_html'])
            ->setTo('vdovenkova2@gmail.com')
            ->setSubject('Бесплатная консультация по питанию с геймификации')
            ->send();
        echo 'ok';
        return;
    }

    public function actionTestMail()
    {
          \Yii::$app->mail->compose('views/finished_6_level_html',
            ['user' => 1, 'htmlLayout' => 'layouts/trigger_mail_html'])
            ->setTo('evgen45115@mail.ru')
            ->setSubject(\Yii::t('app', 'Password reset for ') .
                \Yii::$app->params['dw'])
            ->send();
        \Yii::$app->mail->compose('views/finished_6_level_html',
            ['user' => 1, 'htmlLayout' => 'layouts/trigger_mail_html'])
            ->setTo('test_marafon6@mail.ru')
            ->setSubject(\Yii::t('app', 'Password reset for ') .
                \Yii::$app->params['dw'])
            ->send();
Yii::$app->mail->compose('views/finished_6_level_html',
            ['user' => 1, 'htmlLayout' => 'layouts/trigger_mail_html'])
            ->setTo('d.smirnov@specialview.ru')
            ->setSubject(\Yii::t('app', 'Password reset for ') .
                \Yii::$app->params['dw'])
            ->send();
    }
}
