<?php

use yii\db\Migration;

class m171102_134100_change_type_main_page extends Migration
{
    public function safeUp()
    {

        $this->dropColumn('main_page', 'description');
        $this->addColumn('main_page', 'description', $this->text());
    }

    public function safeDown()
    {
        echo "m171102_134100_change_type_main_page cannot be reverted.\n";

        return false;
    }

}
