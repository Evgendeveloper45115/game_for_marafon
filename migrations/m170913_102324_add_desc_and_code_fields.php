<?php

use yii\db\Migration;

class m170913_102324_add_desc_and_code_fields extends Migration
{
    public function safeUp()
    {
        $this->addColumn('video', 'code', $this->string());
        $this->addColumn('level', 'description', $this->text());
    }

    public function safeDown()
    {
        $this->dropColumn('video', 'code');
        $this->dropColumn('level', 'description');
    }

}
