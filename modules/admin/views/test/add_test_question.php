<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TestQuestionLevel */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Добавить вопросы к тесту';
$this->params['breadcrumbs'][] = ['label' => 'Вопросы к тесту', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="test-question-create">

    <h1><?= Html::encode($this->title) ?></h1>


    <div class="test-question-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'test_id')->dropDownList(['' => ''] + \yii\helpers\ArrayHelper::map(\app\models\Test::find()->asArray()->orderBy('id')->all(), 'id', 'name')) ?>
        <?= $form->field($model, 'title')->textInput() ?>
        <?= $form->field($model, 'test_one')->textInput() ?>
        <?= $form->field($model, 'test_two')->textInput() ?>
        <?= $form->field($model, 'test_three')->textInput() ?>
        <?= $form->field($model, 'test_four')->textInput() ?>
        <?= $form->field($model, 'test_five')->textInput() ?>
        <?php if ($model->img) { ?>
            <?= Html::img(Yii::getAlias('@rel_path_to_uploads') . '/test_level_Img/' . $model->img, ['class' => 'profile_foto']); ?>
        <?php } ?>

        <?php
        if (!$model->img) {
            echo Html::img('/uploads/blank.jpg', ['class' => 'profile_foto']);
        }
        ?>

        <?= $form->field($model, 'image')->fileInput(['onchange' => 'readURL(this);']); ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
