<?php

use demi\cropper\Cropper;
use yii\web\JsExpression;


echo Cropper::widget([
// If true - it's output button for toggle modal crop window
    'modal' => true,
// You can customize modal window. Copy /vendor/demi/cropper/views/modal.php
//'modalView' => '@backend/views/image/custom_modal',
// URL-address for the crop-handling request
// By default, sent the following post-params: x, y, width, height, rotate
    'cropUrl' => ['/crop'],
// Url-path to original image for cropping
    'image' => Yii::getAlias('@avatar_web') . '/' . $profile->avatar,
// The aspect ratio for the area of cropping
    'aspectRatio' => 1 / 1, // or 16/9(wide) or 1/1(square) or any other ratio. Null - free ratio
// Additional params for JS cropper plugin
    'pluginOptions' => [
// All possible options: https://github.com/fengyuanchen/cropper/blob/master/README.md#options
        'minCropBoxWidth' => 150, // minimal crop area width
        'minCropBoxHeight' => 150, // minimal crop area height
    ],
// HTML-options for widget container
    'options' => [],
// HTML-options for cropper image tag
    'imageOptions' => [],
// Additional ajax-options for send crop-request. See jQuery $.ajax() options
    'ajaxOptions' => [
        'success' => new JsExpression(<<<JS
    function(data) {
// data - your JSON response from [[cropUrl]]
$("#myImage").attr("src", data.croppedImageSrc);
}
JS
        ),
    ],
]);

$script = <<< JS
    $('#w0').modal('show');
JS;
//маркер конца строки, обязательно сразу, без пробелов и табуляции
$this->registerJs($script, yii\web\View::POS_READY);

?>
