<?php
/**
 * @var $levelUp \app\models\LevelUp
 * @var $this \yii\web\View
 */
?>
<a style="display:none;" class="btn js-venobox vbox-item" href="#share" id="sharer" data-vbtype="inline">share</a>
<div class="js-popup" id="share">
    <div class="popup popup--share">
        <div class="popup-head">
            <h1 class="popup__title">Ошибка</h1><span class="popup-close"></span>
        </div>
        <div class="popup-content popup-content--share">
            У вас недостаточно бонусных баллов для оплаты программы
        </div>
    </div>
</div>


<?php
$js = <<<JS
        $(function(){
                $('#sharer').trigger('click');
                
                    window.setTimeout(function(){
                    var venoOptions = {
                    // is called after plugin initialization.
                    cb_init: function(plugin){
                    $('.vbox-close').css('left', '-1000px');
              
                    },
                }
            $('.venobox').venobox(venoOptions);
        }, 200);
                
                 $(document).on('click','.popup__link--fb',function(){
                $('.popup-close').click();
$('button[type="submit"]').click();
                });
    })
JS;
$this->registerJs($js);
?>
