<?php

use yii\db\Migration;

class m170920_130149_update_article_theme extends Migration
{
    public function safeUp()
    {
        $this->addColumn('theme', 'type', $this->integer());
        $this->addColumn('article', 'type', $this->integer());
    }

    public function safeDown()
    {
        echo "m170920_130149_update_article_theme cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170920_130149_update_article_theme cannot be reverted.\n";

        return false;
    }
    */
}
