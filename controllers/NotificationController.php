<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\httpclient\Client;

class NotificationController extends Controller
{

    /*public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'check' => ['post'],
                    'aviso' => ['post'],
                ],
            ]
        ];
    }*/


    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;

        return parent::beforeAction($action);
    }


    public function actionCheck()
    {

        $file_path = '/var/www/gamebm/www/game_beauty/web/uploads/checklog.txt';
        $file = fopen($file_path,"w+");
        ob_start();
        var_dump(Yii::$app->request->post());
        $output = ob_get_clean();
        fwrite($file, $output);



        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        Yii::$app->response->headers->add('HTTP/1.0', '200');
        Yii::$app->response->headers->add('Content-Type', 'application/xml');


        $ret = $this->renderPartial('check', [
            'date' => Yii::$app->request->post('requestDatetime'),
            'code' => 0,
            'invoiceId' => Yii::$app->request->post('invoiceId'),
            'shopId' => 159464,
        ]);

        fwrite($file, $ret);
        fclose($file);

        return $ret;
    }

    public function actionAviso()
    {
        if ($this->checkMD5(Yii::$app->request)) {
            $code = 0;
        } else {
            $code = 1;
        }

        $file_path = '/var/www/gamebm/www/game_beauty/web/uploads/avisolog.txt';
        $file = fopen($file_path,"w+");
        ob_start();
        var_dump(Yii::$app->request->post());
        $output = ob_get_clean();
        fwrite($file, $output);

        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        Yii::$app->response->headers->add('HTTP/1.0', '200');
        Yii::$app->response->headers->add('Content-Type', 'application/xml');


        $dt = new \DateTime();
        $performedDatetime = $dt->format("Y-m-d") . "T" . $dt->format("H:i:s") . ".000" . $dt->format("P");

        $ret = $this->renderPartial('aviso', [
            'date' => $performedDatetime,
            'code' => $code,
            'invoiceId' => Yii::$app->request->post('invoiceId'),
            'shopId' => 159464,
        ]);


        fwrite($file, $ret);
        fclose($file);

        return $ret;
    }

    private function checkMD5($request)
    {
        $str = $request->post('action') . ";"
            . $request->post('orderSumAmount') . ";"
            . $request->post('orderSumCurrencyPaycash') . ";"
            . $request->post('orderSumBankPaycash') . ";"
            . $request->post('shopId') . ";"
            . $request->post('invoiceId') . ";"
            . $request->post('customerNumber') . ";"
            . 'UgY62g';
        $md5 = strtoupper(md5($str));

        $file_path = '/var/www/gamebm/www/game_beauty/web/uploads/md5log.txt';
        $file = fopen($file_path,"w+");
        fwrite($file, $md5);
        fclose($file);

        if ($md5 != strtoupper($request->post('md5'))) {
            return false;
        }
        return true;
    }

    public function actionTest(){
        $str = 'paymentAviso' . ";"
            . '1740.00' . ";"
            . '10643' . ";"
            . '1003' . ";"
            . '159464' . ";"
            . '2000001636463' . ";"
            . '1507045574' . ";"
            . 'UgY62g';

        $md5 = strtoupper(md5($str));

        var_dump($md5);die;
    }

}