<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title = 'Игра: «Построй стройную фигуру» | Beauty-Matrix';
?>

<div class="girls">
    <a class="girls__logo" href="/site/signup">
        <img src="../landing/images/logo2.png">
    </a>
    <h1 class="title girls__title">Игра: построй стройную фигуру!<span class="girls__free">БЕСПЛАТНО</span></h1>
    <p class="girls__text">Участвуй в игре и начни строить стройную фигуру прямо сейчас.</p><a class="btn btn--girls" href="/site/signup">Начать игру!</a>
</div>
<div class="content">
    <div class="lady">
        <div class="lady__img">
            <img src="../landing/images/lady.png">
        </div>
        <div class="lady-desc"><span class="lady__name">Анна Лысенко</span><a class="lady__login" href="https://www.instagram.com/beatrix_fit/" target="_blank">@beatrix_fit</a>
            <p class="lady__text">Получай проверенную и эффективную информацию от известного нутриолога</p>
        </div>
    </div>
    <div class="levels">
        <p class="levels__text">Проходи уровни, в которых полностью расскрывается каждое составляющих идеальной фигуры, ведь результата получается, когда питание, тренировки, психология и мотивация складываются воедино.</p>
        <div class="levels-items">
            <a class="level level--active" href="/site/signup">
                <div class="level__round">
                    <img src="../landing/images/level1.png"><span class="btn btn--level">Начать</span>
                </div>
                <p class="level__name">Борец Сахаром</p>
            </a><span class="level level--locked">
						<div class="level__round level__round--locked"><img src="../landing/images/level2.png"></div>
						<p class="level__name">Свобода от пищевой <br>зависимости</p></span><span class="level level--locked">
						<div class="level__round level__round--locked"><img src="../landing/images/level3.png"></div>
						<p class="level__name">Охотник за мифами</p></span><span class="level level--locked">
						<div class="level__round level__round--locked"><img src="../landing/images/level4.png"></div>
						<p class="level__name">Исследователь</p></span>
        </div><a class="btn btn--levels" href="/site/signup">Зарегистрироваться</a>
    </div>
    <div class="tasks">
        <h2 class="tasks__title">Выполняй задания и набирай баллы выигрывай крутые призы</h2>
        <div class="prizes clearfix">
            <div class="prizes-left col-7">
                <div class="prize prize--pants">
                    <img class="prize__img" src="../landing/images/pants.png">
                </div>
                <div class="prize prize--shirt">
                    <img class="prize__img" src="../landing/images/shirt.png">
                </div>
                <div class="prize prize--iphone">
                    <img class="prize__img" src="../landing/images/iphone.png">
                </div>
            </div>
            <div class="prizes-right col-5"><a href="/site/signup" class="btn btn--prize">Начать выигрывать <br>призы</a>
            </div>
        </div>
    </div>
    <div class="slim">
        <h2 class="slim__title">А самое главное <b>худей </b>и становись <b>стройнее!</b></h2>
        <div class="slim-girl">
            <img src="../landing/images/girl.png"><a class="btn btn--slim" href="/site/signup">Играть</a>
        </div>
    </div>
</div>
<div class="join">
    <h3 class="join__text">Присоединятесь к бесплатной игре</h3>
    <h2 class="join__game">«Построй стройную фигуру»</h2>

    <?php $form = ActiveForm::begin(['action' => '/site/signup',
     'options' => [
        'class' => 'join-form'
    ]
    ]); ?>

    <?= $form->field($model, 'name', ['template' => '{input}'])->textInput(['class' => 'textbox textbox--join', 'placeholder' => 'Имя'])->label(false); ?>
    <?= $form->field($model, 'email', ['template' => '{input}'])->textInput(['class' => 'textbox textbox--join', 'placeholder' => 'E-mail'])->label(false); ?>
    <?= $form->field($model, 'password', ['template' => '{input}'])->passwordInput(['class' => 'textbox textbox--join', 'placeholder' => 'Пароль'])->label(false); ?>

    <?= Html::submitButton('Присоединиться', ['class' => 'btn btn--join', 'name' => 'signup-button']) ?>

    <?php ActiveForm::end(); ?>

</div>