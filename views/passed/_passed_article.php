<?php
/**
 * @var $points string
 * @var $link string
 * @var $point_img bool
 */
?>
<div class="js-popup" id="task-complete-plus">
    <?php
    if ($point_img) {
        ?>
        <div class="popup popup--points">
            <div class="popup-head">
                <h1 class="popup__title">+<?= $points ?> баллов</h1><span class="popup-close"></span>
            </div>
            <div class="popup-content popup-content--bages clearfix">
                <div class="popup-content__img popup-content__img--points col-3">
                    <img src="../images/popup-points.png">
                </div>
                <div class="popup-content__right popup-content__right--bages col-9">
                    <p class="popup-content__text">За каждое выполненное задание вы будете получать баллы в зависимости
                        от их сложности. Зарабатывайте баллы, вы сможете обменять их на классные бонусы</p>
                </div>

                <div class="popup-task__item popup-task__item--task2 clearfix col-12 aligncenter">
                    <p class="popup-content__next-task" style="">Приступить к следующему заданию.</p>
                    <div style="text-align: center">
                        <a class="btn btn--popup" href="<?= $link ?>">Перейти</a>
                    </div>
                </div>
            </div>
        </div>
        <?php
    } else {
        ?>
        <div class="popup popup--share">
            <div class="popup popup--task">
                <div class="popup-head">
                    <h1 class="popup__title">Вы выполнили задание!</h1>

                    <h2 class="popup__subtitle">+<?= $points ?> баллов</h2><span class="popup-close"></span>
                </div>
                <div class="popup-content">
                    <p class="popup-content__next-task">Приступить к следующему заданию.</p><a class="btn btn--popup"
                                                                                               href="<?= $link ?>">Перейти</a>
                </div>
            </div>
        </div>

        <?php
    }
    ?>
</div>
