<?php

use yii\db\Migration;

class m170925_092208_add_weight_to_theme extends Migration
{
    public function safeUp()
    {
        $this->addColumn('theme', 'weight', $this->integer()->defaultValue(5));
    }

    public function safeDown()
    {
        $this->dropColumn('theme', 'weight');
    }

}
