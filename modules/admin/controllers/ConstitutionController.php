<?php

namespace app\modules\admin\controllers;

use app\models\Shape;
use Yii;
use app\models\Constitution;
use app\models\ConstitutuinSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ConstitutionController implements the CRUD actions for Constitution model.
 */
class ConstitutionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Constitution models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ConstitutuinSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Constitution model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Constitution model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Constitution();
        $model->FillBlankImg();

        if ($model->load(Yii::$app->request->post())) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if($model->file){
                $model->dropOldImg();
                $file_name = uniqid();
                $model->foto = $file_name . '.' . $model->file->extension;
                if (!$model->file->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/' . $file_name . '.' . $model->file->extension)){
                    throw new \RuntimeException('Ошибка сохранения файла');
                }
                $model->file = null;
            }
            $model->save();
            return $this->redirect('index');
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Constitution model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if($model->file){
                $model->dropOldImg();
                $file_name = uniqid();
                $model->foto = $file_name . '.' . $model->file->extension;
                if (!$model->file->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/' . $file_name . '.' . $model->file->extension)){
                    throw new \RuntimeException('Ошибка сохранения файла');
                }
                $model->file = null;
            }
            $model->save();
            return $this->redirect('index');
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Constitution model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->dropOldImg();
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDeleteImg()
    {
        if(Yii::$app->request->post()){
            $id = Yii::$app->request->post('id');
            $this->findModel($id)->dropOldImg();
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the Constitution model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Constitution the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Constitution::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
