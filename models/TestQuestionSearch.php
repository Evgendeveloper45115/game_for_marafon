<?php

namespace app\models;

use app\modules\quiz\models\QuizQuestion;
use app\modules\quiz\Quiz;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TestQuestion;
use yii\helpers\VarDumper;

/**
 * FinalTestSearch represents the model behind the search form about `app\models\TestQuestion`.
 */
class TestQuestionSearch extends TestQuestionLevel
{

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TestQuestionLevel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'test_id' => $this->test_id,
            'title' => $this->title,
            'test_one' => $this->test_one,
            'test_two' => $this->test_id,
            'test_three' => $this->test_three,
            'test_four' => $this->test_four,
            'test_five' => $this->test_five,
        ]);


        return $dataProvider;
    }
}
