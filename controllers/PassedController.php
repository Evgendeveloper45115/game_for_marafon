<?php

namespace app\controllers;

use app\models\PassedVideo;
use app\models\Profile;
use app\models\Video;
use Yii;
use yii\web\Controller;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\helpers\VarDumper;
use app\models\Article;
use app\models\PassedArticle;
use app\helpers\CommonHelper;


class PassedController extends Controller
{
    public $enableCsrfValidation = false;

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }


    public function actionArticle($article_id, $points = 0)
    {
        /**
         * @var $profile Profile
         */
        if (\Yii::$app->request->isAjax) {

            //если идет повторный запрос на прохождение той же статьи - шлем лесом (бывал баг выполнено 4/3)
            if(PassedArticle::find()
                ->where(['user_id' => Yii::$app->user->getId()])
                ->andWhere(['article_id' => $article_id])
                ->one())
            {
                return;
            }

            $article = Article::findOne(['id' => $article_id]);
            $points = $article->points;
            $passed_article = new PassedArticle();
            $passed_article->user_id = Yii::$app->user->getId();
            $passed_article->article_id = $article_id;
            if ($passed_article->save()) {
                $profile = Profile::find()->where(['user_id' => $passed_article->user_id])->one();
                $profile->account += $points;
                $profile->save(false);
            }
            $link = '/level/index/' . $article->level_id;
            if ($article->theme_id) {
                $link = '/theme/' . $article->theme_id;
            }
            $passed_article = PassedArticle::find()->where(['user_id' => Yii::$app->user->getId()])->all();
            $point_img = false;
            if (count($passed_article) == 1) {
                $point_img = true;
            }
            return $this->renderPartial('_passed_article', [
                'point_img' => $point_img,
                'points' => $points,
                'link' => $link,
            ]);
        }
    }

    public function actionVideo($video_id, $points = 0)
    {
        /**
         * @var $profile Profile
         * @var $video Video
         */
        if (\Yii::$app->request->isAjax) {
            $video = Video::findOne($video_id);
            VarDumper::dump($video_id, 11, 1);
            $points = $video->points;
            $passed_video = new PassedVideo();
            $passed_video->user_id = Yii::$app->user->getId();
            $passed_video->video_id = $video_id;
            if ($passed_video->save()) {
                $profile = Profile::find()->where(['user_id' => $passed_video->user_id])->one();
                $profile->account += $points;
                $profile->save(false);
            }
            return $this->renderPartial('_passed_video', [
                'points' => $points,
            ]);
        }
    }

    public function actionGetClosedItems()
    {
        $fd = fopen("/var/www/game.dev/web/mylog.txt", 'w+') or die("не удалось открыть файл");

        $links = $_POST['urls'];
        $links_array = explode('#', $links);

        $result = $links_array[0];
        $user_id = Yii::$app->user->identity->getId();

        if (\Yii::$app->request->isAjax) {

            foreach ($links_array as $link) {

                if (preg_match("/article\/(\d+)/i", $link, $matches) && CommonHelper::isPassedArticleByUser($matches[1], $user_id)) {
                    $result .= '#' . $link;
                }

                if (preg_match("/theme\/(\d+)/i", $link, $matches) && CommonHelper::isPassedThemeByUser($matches[1], $user_id)) {
                    $result .= '#' . $link;
                }

                if (preg_match("/video\/(\d+)/i", $link, $matches) && CommonHelper::isPassedVideoByUser($matches[1], $user_id)) {
                    $result .= '#' . $link;
                }
            }

            return $result;
        }
    }

    public function actionTest()
    {
        $result = [];
        $links = ["/article/10", "/article/8", "/article/11", "/article/9"];
        $user_id = Yii::$app->user->identity->getId();


        foreach ($links as $link) {

            if (preg_match("/article\/(\d+)/i", $link, $matches) && !CommonHelper::isPassedArticleByUser($matches[1], $user_id)) {
                $result[] = $link;
            }

            if (preg_match("/theme\/(\d+)/i", $link, $matches) && !CommonHelper::isPassedThemeByUser($matches[1], $user_id)) {
                $result[] = $link;
            }

            if (preg_match("/video\/(\d+)/i", $link, $matches) && !CommonHelper::isPassedVideoByUser($matches[1], $user_id)) {
                $result[] = $link;
            }
        }

        var_dump($result);

    }


}
