<?php

namespace app\modules\admin\controllers;

use app\models\MainPage;
use Yii;
use app\models\Level;
use app\models\LevelSearch;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * LevelController implements the CRUD actions for Level model.
 */
class MainController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionCreate()
    {
        $model = new MainPage();
        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'image');
            if ($image) {
                $model->dropOldImg();
                $file_name = uniqid();
                $model->image = $file_name . '.' . $image->extension;
                if (!$image->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/' . $file_name . '.' . $image->extension)) {
                    throw new \RuntimeException('Ошибка сохранения файла');
                }
            }
            if ($model->save()) {
                return $this->redirect(['/admin/main/update', 'id' => $model->id]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Level model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $images = $model->image;
        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'image');
            if ($image) {
                $model->dropOldImg();
                $file_name = uniqid();
                $model->image = $file_name . '.' . $image->extension;
                if (!$image->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/' . $file_name . '.' . $image->extension)) {
                    throw new \RuntimeException('Ошибка сохранения файла');
                }
            } else {
                $model->image = $images;
            }
            $model->save();
            return $this->redirect(['/admin/main/update', 'id' => $id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Level model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteImg()
    {
        if (Yii::$app->request->post()) {
            $id = Yii::$app->request->post('id');
            $this->findModel($id)->dropOldImg();
        }

        return $this->redirect(Yii::$app->request->referrer);
    }


    /**
     * Finds the Level model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Level the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MainPage::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
