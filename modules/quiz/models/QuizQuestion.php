<?php
/**
 * Yii quiz
 *
 * @author Marc Oliveras Galvez <oligalma@gmail.com>
 * @link http://www.oligalma.com
 * @copyright 2016 Oligalma
 * @license GPL License
 */

namespace app\modules\quiz\models;

use yii\db\ActiveRecord;

class QuizQuestion extends ActiveRecord
{
    public $questionCount;
    public $image;

    /**
     * @return string the associated database table name
     */
    public static function tableName()
    {
        return '{{%quiz_question}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array(['id', 'category_id', 'img', 'title', 'answer', 'answer2', 'answer3', 'answer4', 'answer5', 'answer6'], 'safe'),
        );
    }

    public function getCategory()
    {
        return $this->hasOne(QuizCategory::className(), ['id' => 'category_id']);
    }

    public function attributeLabels()
    {
        return [
            'id' => 'Ид',
            'category_id' => 'Категория вопроса',
            'title' => 'Вопрос',
            'answer' => 'Правильный ответ',
            'answer2' => 'ответ-1',
            'answer3' => 'ответ-2',
            'answer4' => 'ответ-3',
            'answer5' => 'ответ-4',
            'answer6' => 'ответ-5',
            'img' => 'картинка',
            'image' => 'картинка',
        ];
    }
}