<?php

use yii\db\Migration;

class m170914_090824_add_date_to_measure extends Migration
{
    public function safeUp()
    {
        $this->addColumn('measure', 'date', $this->integer()->notNull());
    }

    public function safeDown()
    {
        $this->dropColumn('measure', 'date');
    }

}
