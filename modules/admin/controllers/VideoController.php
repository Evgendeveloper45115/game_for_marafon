<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Video;
use app\models\VideoSearch;
use yii\db\Exception;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * VideoController implements the CRUD actions for Video model.
 */
class VideoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Video models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VideoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Video model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Video model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Video();
        $model->weight = 5;
        $model->FillBlankImg();

        if ($model->load(Yii::$app->request->post())) {

            $model->file = UploadedFile::getInstance($model, 'file');
            if($model->file){
                $model->dropOldImg();
                $file_name = uniqid();
                $model->img = $file_name . '.' . $model->file->extension;
                if (!$model->file->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/' . $file_name . '.' . $model->file->extension)){
                    throw new \RuntimeException('Ошибка сохранения файла');
                }
                $model->file = null;

            }
            $model->image_radius = UploadedFile::getInstance($model, 'image_radius');
            if ($model->image_radius) {
                $model->dropOldImg3();
                $file_name = uniqid();
                $model->img_radius = $file_name . '.' . $model->image_radius->extension;
                if (!$model->image_radius->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/' . $file_name . '.' . $model->image_radius->extension)) {
                    throw new \RuntimeException('Ошибка сохранения файла');
                }
                $model->image_radius = null;
            }

            $model->save();
            return $this->redirect('index');

        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Video model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->init();

        if ($model->load(Yii::$app->request->post())) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if($model->file){
                $model->dropOldImg();
                $file_name = uniqid();
                $model->img = $file_name . '.' . $model->file->extension;
                if (!$model->file->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/' . $file_name . '.' . $model->file->extension)){
                    throw new \RuntimeException('Ошибка сохранения файла');
                }
                $model->file = null;
            }
            $model->image_radius = UploadedFile::getInstance($model, 'image_radius');
            if ($model->image_radius) {
                $model->dropOldImg3();
                $file_name = uniqid();
                $model->img_radius = $file_name . '.' . $model->image_radius->extension;
                if (!$model->image_radius->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/' . $file_name . '.' . $model->image_radius->extension)) {
                    throw new \RuntimeException('Ошибка сохранения файла');
                }
                $model->image_radius = null;
            }

            $model->save();
            return $this->redirect('index');
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Video model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->dropOldImg();
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDeleteImg()
    {
        if(Yii::$app->request->post()){
            $id = Yii::$app->request->post('id');
            $this->findModel($id)->dropOldImg();
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the Video model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Video the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Video::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
