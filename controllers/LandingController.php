<?php

namespace app\controllers;

use app\forms\SignupForm;
use Yii;
use yii\web\Controller;

class LandingController extends Controller
{

    public function actionFirst()
    {
        $this->layout = 'landing';
        $model = new SignupForm();

        return $this->render('first', [
            'model' => $model,
        ]);
    }

    public function actionSecond()
    {
        $this->layout = 'landing';

        $model = new SignupForm();

        return $this->render('second', [
            'model' => $model,
        ]);
    }


}