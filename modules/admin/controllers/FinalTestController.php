<?php

namespace app\modules\admin\controllers;

use app\models\FinalTestSearch;
use app\modules\quiz\models\QuizCategory;
use app\modules\quiz\models\QuizQuestion;
use Yii;
use app\models\TestQuestion;
use app\models\TestQuestionSearch;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * TestQuestionController implements the CRUD actions for TestQuestion model.
 */
class FinalTestController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TestQuestion models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FinalTestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TestQuestion model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TestQuestion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new QuizQuestion();

        if ($model->load(Yii::$app->request->post())) {
            $model->image = UploadedFile::getInstance($model, 'image');
            if ($model->image) {
                if (!is_dir(Yii::getAlias('@abs_path_to_uploads') . '/testImg')) {
                    mkdir(Yii::getAlias('@abs_path_to_uploads') . '/testImg', 0777, true);
                }
                $file_name = uniqid();
                $model->img = $file_name . '.' . $model->image->extension;
                if (!$model->image->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/testImg/' . $file_name . '.' . $model->image->extension)) {
                    throw new \RuntimeException('Ошибка сохранения файла');
                }
                $model->image = null;
            }
            $model->save();

            return $this->redirect('/admin/final-test');
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    public function actionCreateCategory()
    {
        $model = new QuizCategory();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect('/admin/final-test');
        } else {
            return $this->render('create_category', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TestQuestion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->image = UploadedFile::getInstance($model, 'image');
            if ($model->image) {
                if (!is_dir(Yii::getAlias('@abs_path_to_uploads') . '/testImg')) {
                    mkdir(Yii::getAlias('@abs_path_to_uploads') . '/testImg', 0777, true);
                }
                $file_name = uniqid();
                $model->img = $file_name . '.' . $model->image->extension;
                if (!$model->image->saveAs(Yii::getAlias('@abs_path_to_uploads') . '/testImg/' . $file_name . '.' . $model->image->extension)) {
                    throw new \RuntimeException('Ошибка сохранения файла');
                }
                $model->image = null;
            }
            $model->save();

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TestQuestion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TestQuestion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TestQuestion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = QuizQuestion::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
